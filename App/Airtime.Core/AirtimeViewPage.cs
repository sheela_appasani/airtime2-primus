﻿using System.Web.Mvc;

using Airtime.Data.Account;
using Airtime.Data.Client;
using Airtime.Data.Remote;
using Airtime.Services.Configuration;
using Airtime.Services.Globalization;
using Airtime.Services.Security;
using Airtime.Services.Server;


namespace Airtime.Core
{
	
	public abstract class AirtimeViewPage : WebViewPage
	{
		public ISecurityService SecurityService { get; set; }
		public IConfigurationService ConfigurationService { get; set; }
		public IGlobalizationService GlobalizationService { get; set; }
		public IServerService ServerService { get; set; }

		public IAppRepository AppRepository { get; set; }
		public IChannelRepository ChannelRepository { get; set; }
		public IMembershipRepository MembershipRepository { get; set; }
	}


	public abstract class AirtimeViewPage<T> : WebViewPage<T>
	{
		public ISecurityService SecurityService { get; set; }
		public IConfigurationService ConfigurationService { get; set; }
		public IGlobalizationService GlobalizationService { get; set; }
		public IServerService ServerService { get; set; }

		public IAppRepository AppRepository { get; set; }
		public IChannelRepository ChannelRepository { get; set; }
		public IMembershipRepository MembershipRepository { get; set; }
	}

}
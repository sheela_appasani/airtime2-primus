using System;
using System.Diagnostics;
using System.Web.Hosting;


namespace Airtime.Core
{

	public sealed class AppDomainNotifier : IRegisteredObject
	{

		public event EventHandler<AppDomainEventArgs> OnStop;


		public AppDomainNotifier(): this(null) {}


		public AppDomainNotifier(EventHandler<AppDomainEventArgs> onStop)
		{
			try {
				if (onStop != null) {
					OnStop += onStop;
				}
				HostingEnvironment.RegisterObject(this);
			} catch (Exception ex) {
				Debug.WriteLine(ex.Message);
			}
		}


		public void Stop(bool immediate)
		{
			// ReSharper disable EmptyGeneralCatchClause
			try {
				var handler = OnStop;
				if (handler != null) {
					handler(this, new AppDomainEventArgs(immediate));
				}
			} catch {
			} finally {
				HostingEnvironment.UnregisterObject(this);
			}
			// ReSharper restore EmptyGeneralCatchClause
		}

	}


	public sealed class AppDomainEventArgs : EventArgs
	{

		public bool StopImmediately { get; set; }

		public AppDomainEventArgs(bool immediate)
		{
			StopImmediately = immediate;
		}

	}

}

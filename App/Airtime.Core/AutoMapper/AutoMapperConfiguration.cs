using System;
using System.Globalization;

using Airtime.Data.Account;
using Airtime.Data.Messaging;

using Xtra.Common;

using MapperClass = AutoMapper.Mapper;


namespace Airtime.Core.AutoMapper
{

	public class AutoMapperConfiguration
	{

		public static void Configure()
		{
			MapperClass.Initialize(x => {
				x.CreateMap<DateTime, string>().ConvertUsing(d => (d.Year > 9000) ? String.Empty : d.ToString("g"));
				x.CreateMap<string, DateTime>().ConvertUsing(d => DateTime.Parse(d, CultureInfo.CurrentCulture));
				x.CreateMap<EmailBlastStatusEnum, string>().ConvertUsing(s => s.ToResourceString());
				x.CreateMap<PersonGenderEnum, string>().ConvertUsing(g => g.ToResourceString());
				x.AddProfile<MappingProfile>();
			});
		}

	}

}
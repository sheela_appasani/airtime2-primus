using System;

using AutoMapper;


namespace Airtime.Core.AutoMapper
{

	public class Mapper<TDomain, TDataContract> : IMapper<TDomain, TDataContract>
	{

		public Mapper(IMappingEngine engine)
		{
			Engine = engine;
		}


		/// <summary>
		/// Maps fields from a domain entity onto a new view model.
		/// </summary>
		/// <param name="domain">Domain entity object</param>
		/// <returns>View model object</returns>
		public TDataContract Map(TDomain domain)
		{
			if (domain == null) {
				throw new ArgumentNullException("domain");
			}
			return Engine.Map<TDomain, TDataContract>(domain);
		}


		/// <summary>
		/// Maps fields from a domain entity onto an existing view model, extending it.
		/// </summary>
		/// <param name="domain">Domain entity object</param>
		/// <param name="dataContract">View model object</param>
		/// <returns>View model object</returns>
		public TDataContract Map(TDomain domain, TDataContract dataContract)
		{
			if (domain == null) {
				throw new ArgumentNullException("domain");
			}
			return Engine.Map<TDomain, TDataContract>(domain, dataContract);
		}


		/// <summary>
		/// Maps fields from a view model onto a new domain entity.
		/// </summary>
		/// <param name="dataContract">View model object</param>
		/// <returns>Domain entity object</returns>
		public TDomain Map(TDataContract dataContract)
		{
			if (dataContract == null) {
				throw new ArgumentNullException("dataContract");
			}
			return Engine.Map<TDataContract, TDomain>(dataContract);
		}


		/// <summary>
		/// Maps fields from a view model onto an existing domain entity, extending it.
		/// </summary>
		/// <param name="dataContract">View model object</param>
		/// <param name="domain">Domain entity object</param>
		/// <returns>Domain entity object</returns>
		public TDomain Map(TDataContract dataContract, TDomain domain)
		{
			if (dataContract == null) {
				throw new ArgumentNullException("dataContract");
			}
			return Engine.Map<TDataContract, TDomain>(dataContract, domain);
		}


		private IMappingEngine Engine { get; set; }

	}


	public class Mapper<TSource> : IMapper<TSource>
	{

		public Mapper(IMappingEngine engine)
		{
			Engine = engine;
		}


		/// <summary>
		/// Maps fields from an object onto a new object.
		/// </summary>
		/// <param name="source">Source object</param>
		/// <returns>Mapped object</returns>
		public TDest Map<TDest>(TSource source)
		{
			if (source == null) {
				throw new ArgumentNullException("source");
			}
			return Engine.Map<TSource, TDest>(source);
		}


		/// <summary>
		/// Maps fields from a domain entity onto an existing view model, extending it.
		/// </summary>
		/// <param name="source">Source object</param>
		/// <param name="destination">Destination object</param>
		/// <returns>Mapped object</returns>
		public TDest Map<TDest>(TSource source, TDest destination)
		{
			if (source == null) {
				throw new ArgumentNullException("source");
			}
			return Engine.Map<TSource, TDest>(source, destination);
		}


		private IMappingEngine Engine { get; set; }

	}


	public class Mapper : IMapper
	{

		public Mapper(IMappingEngine engine)
		{
			Engine = engine;
		}


		/// <summary>
		/// Maps fields from an object onto a new object.
		/// </summary>
		/// <param name="source">Source object</param>
		/// <returns>Mapped object</returns>
		public T2 Map<T1, T2>(T1 source)
		{
			if (source == null) {
				throw new ArgumentNullException("source");
			}
			return Engine.Map<T1, T2>(source);
		}


		/// <summary>
		/// Maps fields from a domain entity onto an existing view model, extending it.
		/// </summary>
		/// <param name="source">Source object</param>
		/// <param name="destination">Destination object</param>
		/// <returns>Mapped object</returns>
		public T2 Map<T1, T2>(T1 source, T2 destination)
		{
			if (source == null) {
				throw new ArgumentNullException("source");
			}
			return Engine.Map<T1, T2>(source, destination);
		}


		private IMappingEngine Engine { get; set; }

	}

}
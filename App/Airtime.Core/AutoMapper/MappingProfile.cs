using System;
using System.Globalization;

using Airtime.Data.Media;
using Airtime.Data.Account;
using Airtime.Data.Messaging;
using Airtime.Data.Surveying;
using Airtime.Data.Surveying.Questions;
using Airtime.Models.Emails;
using Airtime.Models.Media;
using Airtime.Models.Members;
using Airtime.Models.Questions;
using Airtime.Models.Surveys;

using Xtra.Common;

using AutoMapper;

using MapperClass = AutoMapper.Mapper;

namespace Airtime.Core.AutoMapper
{

	public class MappingProfile : Profile
	{

		public override string ProfileName { get { return "MappingProfile"; } }

		protected override void Configure()
		{
			//Domain to View Model mappings
			MapperClass.CreateMap<Survey, SurveyModel>()
				.ForMember(model => model.StatusName, option => option.MapFrom(survey => survey.Status.ToResourceString()))
				.ForMember(model => model.TimeZoneInfoDisplayName, option => option.MapFrom(survey => survey.Schedule.TimeZoneInfo.DisplayName))
				.ForMember(model => model.ScheduleStartDate, option => option.MapFrom(survey => survey.Schedule.LocalStartDate))
				.ForMember(model => model.ScheduleEndDate, option => option.MapFrom(survey => survey.Schedule.LocalEndDate));

			MapperClass.CreateMap<QuestionBase, QuestionIndexModel>();
			MapperClass.CreateMap<Song, SongModel>();
			MapperClass.CreateMap<Membership, UserModel>()
				.ForMember(
					model => model.PersonPersonalGenderName,
					option => option.MapFrom(
						user => user.Person.Personal.Gender.HasValue
							? user.Person.Personal.Gender.Value.ToResourceString()
							: null))
				.ForMember(
					model => model.PersonPersonalDateOfBirth,
					option => option.MapFrom(
						user => user.Person.Personal.DateOfBirth.HasValue
							? user.Person.Personal.DateOfBirth.Value.ToString("d")
							: String.Empty));

			MapperClass.CreateMap<SongModel, Song>()
			    .ForMember(model => model.Filename, option => option.Ignore())
				.ForMember(model => model.Length, option => option.Ignore())
				.ForMember(model => model.DateCreated, option => option.Ignore());

			MapperClass.CreateMap<EmailBlast, EmailBlastModel>()
				.ForMember(
					model => model.StatusName,
					option => option.MapFrom(blast => blast.Status.ToResourceString())
				)
				.ForMember(
					model => model.RecipientSourceName,
					option => option.MapFrom(blast => blast.RecipientSource.ToResourceString())
				)
				.ForMember(
					model => model.ScheduleStartDate,
					option => option.MapFrom(
						blast => blast.Schedule.StartDate.HasValue
							? TimeZoneInfo.ConvertTimeFromUtc(blast.Schedule.StartDate.Value, (blast.Survey != null) ? blast.Survey.Schedule.TimeZoneInfo : blast.Channel.TimeZoneInfo)
							: (DateTime?)null
					)
				)
				.ForMember(
					model => model.ScheduleEndDate,
					option => option.MapFrom(
						blast => blast.Schedule.EndDate.HasValue
							? TimeZoneInfo.ConvertTimeFromUtc(blast.Schedule.EndDate.Value, (blast.Survey != null) ? blast.Survey.Schedule.TimeZoneInfo : blast.Channel.TimeZoneInfo)
							: (DateTime?)null
					)
				);

			MapperClass.CreateMap<EmailBlast, EmailBlastEditModel>()
				.ForMember(
					model => model.StatusName,
					option => option.MapFrom(blast => blast.Status.ToResourceString())
				)
				.ForMember(
					model => model.RecipientSourceName,
					option => option.MapFrom(blast => blast.RecipientSource.ToResourceString())
				)
				.ForMember(
					model => model.ScheduleStartDate,
					option => option.MapFrom(
						blast => blast.Schedule.StartDate.HasValue
							? TimeZoneInfo.ConvertTimeFromUtc(blast.Schedule.StartDate.Value, (blast.Survey != null) ? blast.Survey.Schedule.TimeZoneInfo : blast.Channel.TimeZoneInfo)
							: (DateTime?) null
					)
				)
				.ForMember(
					model => model.ScheduleEndDate,
					option => option.MapFrom(
						blast => blast.Schedule.EndDate.HasValue
							? TimeZoneInfo.ConvertTimeFromUtc(blast.Schedule.EndDate.Value, (blast.Survey != null) ? blast.Survey.Schedule.TimeZoneInfo : blast.Channel.TimeZoneInfo)
							: (DateTime?)null
					)
				)
				.ForMember(
					model => model.CriteriaLowerCompletedSurveyDate,
					option => option.MapFrom(
						blast => blast.Criteria.LowerCompletedSurveyDate.HasValue
							? TimeZoneInfo.ConvertTimeFromUtc(blast.Criteria.LowerCompletedSurveyDate.Value, (blast.Survey != null) ? blast.Survey.Schedule.TimeZoneInfo : blast.Channel.TimeZoneInfo)
							: (DateTime?)null
					)
				)
				.ForMember(
					model => model.CriteriaUpperCompletedSurveyDate,
					option => option.MapFrom(
						blast => blast.Criteria.UpperCompletedSurveyDate.HasValue
							? TimeZoneInfo.ConvertTimeFromUtc(blast.Criteria.UpperCompletedSurveyDate.Value, (blast.Survey != null) ? blast.Survey.Schedule.TimeZoneInfo : blast.Channel.TimeZoneInfo)
							: (DateTime?)null
					)
				);
		}

	}

}
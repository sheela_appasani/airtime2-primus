using System;

using Airtime.Core.AutoMapper;

using Autofac;
using Autofac.Integration.Web;

using AutoMapper;
using AutoMapper.Mappers;

using MapperClass = AutoMapper.Mapper;
using Module = Autofac.Module;


namespace Airtime.Core
{

	public class AutoMapperModule : Module
	{

		protected override void Load(ContainerBuilder builder)
		{
			if (builder == null) {
				throw new ArgumentNullException("builder");
			}

			builder.Register(c => MapperClass.Engine).As(typeof(IMappingEngine));
			builder.Register(c => MapperRegistry.AllMappers());
			builder.RegisterType<ConfigurationStore>()
				.As<ConfigurationStore>()
				.As<IConfigurationProvider>()
				.As<IConfiguration>();

			AutoMapperConfiguration.Configure();

			builder.RegisterGeneric(typeof(Mapper<,>))
				.As(typeof(IMapper<,>))
				.InstancePerMatchingLifetimeScope(WebLifetime.Request);

			builder.RegisterGeneric(typeof(Mapper<>))
				.As(typeof(IMapper<>))
				.InstancePerMatchingLifetimeScope(WebLifetime.Request);

			builder.Register(c => new AutoMapper.Mapper(c.Resolve<IMappingEngine>()))
				.As(typeof(IMapper))
				.InstancePerMatchingLifetimeScope(WebLifetime.Request);
		}

	}

}
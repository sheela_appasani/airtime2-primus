﻿using System;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Airtime.Core
{

	public class DelegatingResult : ActionResult
	{

		public string ContentType { get; set; }
		public Encoding ContentEncoding { get; set; }
		public string FileDownloadName { get; set; }
		public Action<ControllerContext> Command { get; private set; }


		public DelegatingResult(Action<ControllerContext> command)
		{
			Command = command;
		}


		public override void ExecuteResult(ControllerContext context)
		{
			if (context == null) {
				throw new ArgumentNullException("context");
			}

			HttpResponseBase response = context.HttpContext.Response;

			if (!String.IsNullOrEmpty(ContentType)) {
				response.ContentType = ContentType;
			}

			if (ContentEncoding != null) {
				response.ContentEncoding = ContentEncoding;
			} 

			if (!String.IsNullOrEmpty(FileDownloadName)) {
				response.AddHeader("Content-Disposition", ContentDispositionUtil.GetHeaderValue(FileDownloadName));
			} 

			Command(context);
		}

	}

}

using System;
using System.Globalization;

namespace Airtime.Core
{
	public static class ExtendDateTime
	{
		// returns the number of milliseconds since Jan 1, 1970 (useful for converting C# dates to javascript dates)
		public static double UnixTicksDouble(this DateTime dt)
		{
			var epochDate = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
			var dtUtc = dt.ToUniversalTime();
			// atlernate floor(( dtUtc.Ticks - epochDate.Ticks ) * scale factor) to get milliseconds
			var ts = new TimeSpan(dtUtc.Ticks - epochDate.Ticks); // convenience to not have to scale ticks.
			return ts.TotalMilliseconds;
		}

		public static long UnixTicksLong(this DateTime dt)
		{
			return (long)dt.UnixTicksDouble();
		}

		/// <summary>
		/// Culture invariant string representation of milliseconds since 1970/1/1.
		/// </summary>
		public static string UnixTicks(this DateTime dt)
		{
			return dt.UnixTicksLong()
				.ToString(CultureInfo.InvariantCulture);
		}

		/// <summary>
		/// Culture invariant string representation of milliseconds since 1970/1/1.
		/// If date is not set it returns Ticks for DateTime.MaxValue,
		/// </summary>
		public static string UnixTicks(this DateTime? dt)
		{
			if (dt.HasValue) {
				return dt.Value.UnixTicksLong()
					.ToString(CultureInfo.InvariantCulture);
			}
			return DateTime.MaxValue.UnixTicks();
		}
	}
}
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Optimization;

using System.Globalization;

using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;


namespace Airtime.Core
{

	public static class ExtendHtml
	{

		public static string LayoutDirection(this HtmlHelper htmlHelper)
		{
			return CultureInfo.CurrentCulture.TextInfo.IsRightToLeft ? "rtl" : "ltr";
		}


		public static MvcHtmlString ButtonRow(this HtmlHelper helper, List<SelectListItem> list, object htmlAttributes = null)
		{
			var sb = new StringBuilder();
			foreach (var item in list) {
				sb.AppendFormat("<button type=\"button\" class=\"buttonrow-{0}\">{1}</button>", item.Value, item.Text);
			}
			return new MvcHtmlString(sb.ToString());
		}


		public static IHtmlString RenderScript(this HtmlHelper helper, params string[] paths)
		{
			return Scripts.Render(FilterBundleItems(RenderedScriptsKey, paths));
		}


		public static IHtmlString RenderStyle(this HtmlHelper helper, params string[] paths)
		{
			return Styles.Render(FilterBundleItems(RenderedStylesKey, paths));
		}


		private static string[] FilterBundleItems(string collectionKey, params string[] paths)
		{
			var context = HttpContext.Current;
			if (context != null) {
				var renderedItems = context.Items[collectionKey] as HashSet<string>;
				if (renderedItems == null) {
					renderedItems = new HashSet<string>();
					context.Items[collectionKey] = renderedItems;
				}

				var pathsToRender = new List<string>();
				foreach (string path in paths) {
					if (!renderedItems.Contains(path)) {
						pathsToRender.Add(path);
						renderedItems.Add(path);
					}
				}

				return pathsToRender.ToArray();
			}

			return paths;
		}


		private const string RenderedScriptsKey = "RenderedScripts";
		private const string RenderedStylesKey = "RenderedStyles";


		/*
		public static MvcHtmlString RadioButtonForEnum<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
		{
			var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
			var sb = new StringBuilder();

			foreach (var value in metaData.ModelType.GetEnumValues()) {
				var fi = metaData.ModelType.GetField(value.ToString());
				var attr = (DisplayAttribute)Attribute.GetCustomAttribute(fi, typeof(DisplayAttribute));
				var name = (attr != null) ? attr.GetName() : value.ToString();

				var id = string.Format(
					"{0}_{1}_{2}",
					htmlHelper.ViewData.TemplateInfo.HtmlFieldPrefix,
					metaData.PropertyName,
					value.ToString()
				);

				var radio = htmlHelper.RadioButtonFor(expression, name, new { id }).ToHtmlString();
				sb.AppendFormat("<label for=\"{0}\">{1}</label> {2}", id, HttpUtility.HtmlEncode(name), radio);
			}

			return MvcHtmlString.Create(sb.ToString());
		}
		*/
	}

}

using System;
using System.Web.Mvc;


namespace Airtime.Core
{

	public static class ExtendObject
	{

		public static T ConvertToOrDefault<T>(this ValueProviderResult valueProviderResult, T defaultValue = default(T))
		{
			if (valueProviderResult == null) {
				return defaultValue;
			}

			try {
				return (T)valueProviderResult.ConvertTo(typeof(T));
			} catch {
				return defaultValue;
			}
		}

	}

}
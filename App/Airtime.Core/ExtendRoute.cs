using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;


namespace Airtime.Core
{

	public static class ExtendRoute
	{

		public static Route MapHttpRouteWithName(this AreaRegistrationContext context, string name, string routeTemplate, object defaults = null, object constraints = null, HttpMessageHandler handler = null)
		{
			var route = context.Routes.MapHttpRoute(name, routeTemplate, defaults, constraints);
			InitializeRouteDataTokens(route);
			route.DataTokens["area"] = context.AreaName;
			route.DataTokens["routeName"] = name;
			return route;
		}


		public static Route MapRouteWithName(this AreaRegistrationContext context, string name, string url, object defaults = null, object constraints = null, string[] namespaces = null)
		{
			var route = context.MapRoute(name, url, defaults, constraints, namespaces);
			InitializeRouteDataTokens(route);
			route.DataTokens["routeName"] = name;
			return route;
		}


		public static Route MapRouteWithName(this RouteCollection routes, string name, string url, object defaults = null, object constraints = null, string[] namespaces = null)
		{
			var route = routes.MapRoute(name, url, defaults, constraints, namespaces);
			InitializeRouteDataTokens(route);
			route.DataTokens["routeName"] = name;
			return route;
		}


		public static Route MapRouteWithName(this RouteCollection routes, string name, string url, object defaults, string[] namespaces)
		{
			return MapRouteWithName(routes, name, url, defaults, constraints: null, namespaces: namespaces);
		}


		public static Route MapRouteWithName(this RouteCollection routes, string name, string url, string[] namespaces)
		{
			return MapRouteWithName(routes, name, url, defaults: null, constraints: null, namespaces: namespaces);
		}


		private static void InitializeRouteDataTokens(Route route)
		{
			if (route.DataTokens == null) {
				route.DataTokens = new RouteValueDictionary();
			}
		}

	}

}

using System;
using System.Web;

using System.Web.Routing;

using Airtime.Core.Framework;


namespace Airtime.Core
{

	public static class ExtendUri
	{

		public static bool IsRouteMatch(this Uri uri, string controllerName, string actionName)
		{
			var routeInfo = new RouteInfo(uri, HttpContext.Current.Request.ApplicationPath);
			return (routeInfo.RouteData.Values["controller"].ToString() == controllerName && routeInfo.RouteData.Values["action"].ToString() == actionName);
		}

		public static bool IsRouteMatch(this Uri uri, string controllerName, string actionName, string areaName)
		{
			var routeInfo = new RouteInfo(uri, HttpContext.Current.Request.ApplicationPath);
			return (
				routeInfo.RouteData.Values["controller"].ToString() == controllerName 
				&& routeInfo.RouteData.Values["action"].ToString() == actionName
				&& routeInfo.RouteData.Values["area"].ToString() == areaName
			);
		}

		public static string GetRouteDataValue(this Uri uri, string parameterName)
		{
			var routeInfo = new RouteInfo(uri, HttpContext.Current.Request.ApplicationPath);
			return routeInfo.RouteData.Values[parameterName] != null
				? routeInfo.RouteData.Values[parameterName].ToString()
				: null;
		}

		public static RouteData ToRouteData(this Uri uri)
		{
			return new RouteInfo(uri, HttpContext.Current.Request.ApplicationPath).RouteData;
		}

	}

}
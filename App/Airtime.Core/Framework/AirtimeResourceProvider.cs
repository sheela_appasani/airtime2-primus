﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using System.Web.Compilation;


namespace Airtime.Core.Framework
{

	public class CustomResourceProviderFactory : ResourceProviderFactory
	{

		public CustomResourceProviderFactory()
		{
			Type type = Type.GetType(FallbackFactoryTypeName);
			Debug.Assert(type != null, "type != null");

			_fallbackFactory = (ResourceProviderFactory)Activator.CreateInstance(type);
		}


		public override IResourceProvider CreateGlobalResourceProvider(string classname)
		{
			return new AirtimeResourceProvider(_fallbackFactory.CreateGlobalResourceProvider(classname));
		}


		public override IResourceProvider CreateLocalResourceProvider(string virtualPath)
		{
			return new AirtimeResourceProvider(_fallbackFactory.CreateLocalResourceProvider(virtualPath));
		}


		private readonly ResourceProviderFactory _fallbackFactory;

		private const string FallbackFactoryTypeName = "System.Web.Compilation.ResXResourceProviderFactory, System.Web";
		//private const string FallbackFactoryTypeName = "System.Web.Compilation.ResXResourceProviderFactory, System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";
	}


	public class AirtimeResourceProvider : IResourceProvider
	{

		public IResourceReader ResourceReader {
			get { return _fallbackProvider.ResourceReader; }
		}

	
		public AirtimeResourceProvider(IResourceProvider fallbackProvider)
		{
			_fallbackProvider = fallbackProvider;
		}


		public object GetObject(string resourceKey, CultureInfo culture)
		{
			return _fallbackProvider.GetObject(resourceKey, culture);
		}


		private readonly IResourceProvider _fallbackProvider;

	}


	public class AirtimeResourceReader : IResourceReader
	{

		public AirtimeResourceReader(IDictionary resources)
		{
			_resources = resources;
		}


		IDictionaryEnumerator IResourceReader.GetEnumerator()
		{
			return _resources.GetEnumerator();
		}


		void IResourceReader.Close()
		{
		}


		IEnumerator IEnumerable.GetEnumerator()
		{
			return _resources.GetEnumerator();
		}


		void IDisposable.Dispose()
		{
		}


		private readonly IDictionary _resources;

	}

}

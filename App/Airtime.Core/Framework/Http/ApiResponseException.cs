﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace Airtime.Core.Framework.Http
{

	public class ApiResponseException : HttpResponseException
	{

		public ApiResponseException(HttpStatusCode statusCode)
			: base(statusCode)
		{
		}


		public ApiResponseException(HttpResponseMessage response)
			: base(response)
		{
		}


		public ApiResponseException(HttpStatusCode statusCode, string message, params object[] messageParams)
			: this(statusCode)
		{
			Response.Content = new StringContent(messageParams.Length > 0 ? String.Format(message, messageParams) : message);
		}


		public ApiResponseException(HttpStatusCode statusCode, Exception exception)
			: this(statusCode)
		{
			Response.Content = new StringContent(exception.Message);
		}


		public ApiResponseException(string message, params object[] messageParams)
			: this(HttpStatusCode.InternalServerError)
		{
			Response.Content = new StringContent(messageParams.Length > 0 ? String.Format(message, messageParams) : message);
		}


		public ApiResponseException(Exception exception)
			: this(HttpStatusCode.InternalServerError)
		{
			Response.Content = new StringContent(exception.Message);
		}

	}

}

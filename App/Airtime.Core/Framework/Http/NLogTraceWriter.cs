﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Tracing;

using NLog;


namespace Airtime.Core.Framework.Http
{

	public class NLogTraceWriter : ITraceWriter
	{

		public void Trace(HttpRequestMessage request, string category, TraceLevel level, Action<TraceRecord> traceAction)
		{
			var rec = new TraceRecord(request, category, level);
			traceAction(rec);
			LogTrace(rec);
		}


		protected void LogTrace(TraceRecord rec)
		{
			// ReSharper disable EmptyGeneralCatchClause
			try {
				if (LogManager.IsLoggingEnabled()) {
					var loggerName = rec.Category + (String.IsNullOrEmpty(rec.Operator) ? "" : "." + rec.Operator);
					var log = LogManager.GetLogger(loggerName);

					if (log.IsTraceEnabled) {
						string messageFormat;
						bool gotOp = !String.IsNullOrEmpty(rec.Operation);
						bool gotMsg = !String.IsNullOrEmpty(rec.Message);

						if (gotOp && gotMsg) {
							messageFormat = "[{0}]: {1}";
						} else if (gotOp) {
							messageFormat = "[{0}]";
						} else if (gotMsg) {
							messageFormat = "{1}";
						} else {
							messageFormat = "";
						}
					
						log.Trace(String.Format(messageFormat, rec.Operation, rec.Message));
					}
				}
			} catch {
			}
			// ReSharper restore EmptyGeneralCatchClause
		}

	}

}

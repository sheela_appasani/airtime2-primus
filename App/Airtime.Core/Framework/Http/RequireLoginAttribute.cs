using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Controllers;

using Airtime.Core.Framework.Security;
using Airtime.Data.Account;
using Airtime.Services.Security;


namespace Airtime.Core.Framework.Http
{

	public class RequireLoginAttribute : AuthorizeAttribute
	{

		public ISecurityService SecurityService { get; set; }


		protected override bool IsAuthorized(HttpActionContext actionContext)
		{
			var authorized = base.IsAuthorized(actionContext);
			if (authorized) {
				if (SecurityService.CurrentPerson == null) {
					authorized = false;
				}
			}
			if (authorized) {
				//Check if the authenticated Membership has all of the current action's required Permissions
				var attributes = actionContext.ActionDescriptor.GetCustomAttributes<RequirePermissionAttribute>();
				if (!SecurityService.CurrentPerson.HasAdminMembership() && attributes.Any(attribute => !SecurityService.ValidatePermissions(attribute.Permissions))) {
					authorized = false;
				}
			}
			return authorized;
		}

	}

}

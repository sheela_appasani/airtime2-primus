﻿using System;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;

using Elmah;

using NLog;


namespace Airtime.Core.Framework.Http
{

	public class UnhandledExceptionFilter : ExceptionFilterAttribute
	{

		public override void OnException(HttpActionExecutedContext context)
		{
			var exception = context.Exception;

			if (exception is HttpResponseException) {
				LogExceptionShort(exception);
				throw exception;
			}

			LogExceptionLong(exception);
			throw new ApiResponseException(HttpStatusCode.InternalServerError, exception);
		}


		private void LogExceptionShort(Exception exception)
		{
			Log.Error(exception.Message, exception);
			ErrorLog.GetDefault(HttpContext.Current).Log(new Error(exception));
		}


		private void LogExceptionLong(Exception exception)
		{
			Log.Error(exception.Message + "\n" + exception.StackTrace, exception);
			ErrorLog.GetDefault(HttpContext.Current).Log(new Error(exception));
		}


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

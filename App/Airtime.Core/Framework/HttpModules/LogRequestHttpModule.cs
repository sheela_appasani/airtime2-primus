﻿using System;
using System.IO;
using System.Linq;
using System.Web;

using NLog;


namespace Airtime.Core.Framework.HttpModules
{

	public class LogRequestHttpModule : IHttpModule
	{

		public void Init(HttpApplication httpApplication)
		{
			httpApplication.BeginRequest += OnBeginRequest;
		}


		public void Dispose()
		{
		}

		
		private void OnBeginRequest(object sender, EventArgs e)
		{
			if (Log.IsTraceEnabled) {
				var context = HttpContext.Current;
				var request = context.Request;
				var inputStream = request.GetBufferedInputStream();
				using (var streamReader = new StreamReader(inputStream)) {
					var headersArray = request.Headers.AllKeys.Select(k => k + ": " + request.Headers[k]).ToArray();
					string headers = String.Join("\n", headersArray);

					var requestInput = streamReader.ReadToEnd();
					if (requestInput.Length > 0) {
						Log.Trace("REQUEST: {0} {1}\n{2}\n\n{3}", request.RequestType, request.RawUrl, headers, requestInput);
					} else {
						Log.Trace("REQUEST: {0} {1}\n{2}", request.RequestType, request.RawUrl, headers);
					}
				}
			}
		}


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

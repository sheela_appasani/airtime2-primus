﻿using System;
using System.Diagnostics;
using System.IO;
using System.Web;

using NLog;


namespace Airtime.Core.Framework.HttpModules
{

	public class LogTimingHttpModule : IHttpModule
	{

		public void Init(HttpApplication httpApplication)
		{
			httpApplication.BeginRequest += OnBeginRequest;
			httpApplication.EndRequest += OnEndRequest;
		}


		public void Dispose()
		{
		}

		
		private void OnBeginRequest(object sender, EventArgs e)
		{
			var context = HttpContext.Current;
			context.Items[StopwatchKey] = Stopwatch.StartNew();
		}


		private void OnEndRequest(object sender, EventArgs e)
		{
			if (Log.IsTraceEnabled) {
				var context = HttpContext.Current;
				var stopwatch = context.Items[StopwatchKey] as Stopwatch;
				if (stopwatch != null) {
					stopwatch.Stop();
					Log.Trace(() => String.Format("Rendered in {0} ms: {1}", stopwatch.ElapsedMilliseconds, context.Request.RawUrl));
				}
			}
		}


		private static readonly string StopwatchKey = typeof(LogTimingHttpModule).FullName + "#Stopwatch";
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

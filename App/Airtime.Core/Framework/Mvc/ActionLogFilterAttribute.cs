﻿using System;
using System.Diagnostics;
using System.Web.Mvc;

using Airtime.Data.Server;
using Airtime.Services.Configuration;
using Airtime.Services.Security;

using NLog;

using Xtra.Common;


namespace Airtime.Core.Framework.Mvc
{

	// ReSharper disable EmptyGeneralCatchClause
	public class ActionLogFilterAttribute : ActionFilterAttribute
    {

		public ActionLogFilterAttribute(ISecurityService securityService, IConfigurationService configurationService, IRequestLogRepository requestLogRepository)
		{
			SecurityService = securityService;
			ConfigurationService = configurationService;
			RequestLogRepository = requestLogRepository;
		}


        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
			filterContext.HttpContext.Items[StopwatchKey] = Stopwatch.StartNew();

            base.OnActionExecuting(filterContext);
        }


        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
			try {
				double? renderTime = null;
				string controllerName = filterContext.Controller.GetType().FullName;
				var context = filterContext.HttpContext;

				var stopwatch = context.Items[StopwatchKey] as Stopwatch;
				if (stopwatch != null) {
					stopwatch.Stop();
					renderTime = stopwatch.Elapsed.TotalMilliseconds;

					var log = LogManager.GetLogger(controllerName);
					if (log.IsInfoEnabled) {
						log.Info(() =>
							String.Format("Executed action in {0} ms: {1}", stopwatch.ElapsedMilliseconds, context.Request.RawUrl)
						);
					}
				}

				var currentUser = SecurityService.CurrentMembership;
				var requestLog = new RequestLog {
					Membership = currentUser,
					Username = currentUser != null ? currentUser.Username : null,
					ClassName = controllerName,
					Url = context.Request.RawUrl,
					IpAddress = context.Request.RemoteHostAddress(),
					RenderTime = renderTime,
					HttpUserAgent = context.Request.UserAgent,
					HttpReferrer = context.Request.UrlReferrer != null ? context.Request.UrlReferrer.ToString() : null,
					HostCode = ConfigurationService.HostCode
				};
				RequestLogRepository.Save(requestLog);

			} catch {
				//Silently ignore any errors with logging
			}

            base.OnActionExecuted(filterContext);
        }


		private IRequestLogRepository RequestLogRepository { get; set; }
		private IConfigurationService ConfigurationService { get; set; }
		private ISecurityService SecurityService { get; set; }


		private static readonly string StopwatchKey = typeof(ActionLogFilterAttribute).FullName + "#Stopwatch";

    }
	// ReSharper restore EmptyGeneralCatchClause

}

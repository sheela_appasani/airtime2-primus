using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using Airtime.Core.Framework.Security;
using Airtime.Core.Globalization;
using Airtime.Data.Account;
using Airtime.Services.Security;

using Xtra.Common;


namespace Airtime.Core.Framework.Mvc
{

	public class RequireLoginAttribute : AuthorizeAttribute
	{

		public ISecurityService SecurityService { get; set; }
		public IPersonRepository PersonRepository { get; set; }
		public IMembershipRepository MembershipRepository { get; set; }


		public new string Roles
		{
			get { return _roles ?? String.Empty; }
			set {
				_roles = value;
				_rolesSplit = SplitString(value);
				_rolesSplitEnum = _rolesSplit.Select(x => x.ToEnum<MembershipRoleEnum>()).ToArray();
			}
		}


		public new string Users
		{
			get { return _users ?? String.Empty; }
			set { _users = value; _usersSplit = SplitString(value); }
		}


		public override void OnAuthorization(AuthorizationContext filterContext)
		{
			if (filterContext == null) {
				throw new ArgumentNullException("filterContext");
			}

			//Check if user is authenticated and authorized for this action
			bool authorized = AuthorizeCore(filterContext.HttpContext);

			if (authorized) {
				//Check if the authenticated user has all of the current action's required Permissions
				var attributes = filterContext.ActionDescriptor.GetCustomAttributes(typeof(RequirePermissionAttribute), true).Cast<RequirePermissionAttribute>();
				authorized = attributes.All(attribute => SecurityService.ValidatePermissions(attribute.Permissions));
			}

			if (authorized) {
				//Now check channel-access before finally passing authorization
				int channelId = (filterContext.RouteData.Values["channelId"] ?? HttpContext.Current.Request.Params["channelId"]).ConvertToOrDefault(0);
				if (!SecurityService.ValidateChannelRole(channelId, _rolesSplitEnum)) {
					authorized = false;
				}
			}

			if (!authorized) {
				HandleUnauthorizedRequest(filterContext);
				return;
			}

			//User is authenticated and authorized for this action
			SetCachePolicy(filterContext);
		}


		protected override bool AuthorizeCore(HttpContextBase httpContext)
		{
			if (httpContext == null) {
				throw new ArgumentNullException("httpContext");
			}

			var principal = httpContext.User;
			var identity = (principal != null) ? principal.Identity as AirtimeIdentity : null;
			if (identity == null || !identity.IsAuthenticated) {
				return false;
			}

			if (identity.MembershipId.HasValue) {
				//Got authentication data for a Membership (shouldn't be any need to do database lookups - all required data should've been encoded in the auth-token)
				if (_usersSplit.Length > 0 && !_usersSplit.Contains(principal.Identity.Name, StringComparer.OrdinalIgnoreCase)) {
					return false;
				}
				if (_rolesSplit.Length > 0 && !_rolesSplit.Any(principal.IsInRole)) {
					return false;
				}
				return true;
			}

			if (identity.PersonId.HasValue) {
				//Got authentication data for a Person
				if (_usersSplit.Length > 0 && !_usersSplit.Contains(principal.Identity.Name, StringComparer.OrdinalIgnoreCase)) {
					return false;
				}
				//Authorizing of roles for a Person is a bit more complex than for a Membership, delay that until a later step where ChannelId is known
				return true;
			}

			return true;
		}


		protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
		{
			base.HandleUnauthorizedRequest(filterContext);

			if (filterContext.IsChildAction) {
				filterContext.Result = new HttpUnauthorizedResult();

			} else {
				var cookie = filterContext.HttpContext.Request.Cookies["culture"];
				string cultureName = cookie != null
					&& !String.IsNullOrEmpty(cookie.Value)
					&& CultureSupport.AllowedCultures.Contains(cookie.Value)
						? CultureInfo.CreateSpecificCulture(cookie.Value).Name
						: Thread.CurrentThread.CurrentCulture.Name;

				filterContext.Result = new RedirectToRouteResult(
					new RouteValueDictionary {
						{"culture", cultureName},
						{"controller", "Home"},
						{"action", "LogOn"},
						{"ReturnUrl", filterContext.HttpContext.Request.RawUrl}
					}
				);
			}
		}


		protected void SetCachePolicy(AuthorizationContext filterContext)
		{
			// ** IMPORTANT **
			// Since we're performing authorization at the action level, the authorization code runs
			// after the output caching module. In the worst case this could allow an authorized Membership
			// to cause the page to be cached, then an unauthorized Membership would later be served the
			// cached page. We work around this by telling proxies not to cache the sensitive page,
			// then we hook our custom authorization code into the caching mechanism so that we have
			// the final say on whether a page should be served from the cache.
			HttpCachePolicyBase cachePolicy = filterContext.HttpContext.Response.Cache;
			cachePolicy.SetProxyMaxAge(new TimeSpan(0));
			cachePolicy.AddValidationCallback(CacheValidateHandler, null);
		}


		private void CacheValidateHandler(HttpContext context, object data, ref HttpValidationStatus validationStatus)
		{
			validationStatus = OnCacheAuthorization(new HttpContextWrapper(context));
		}


		private static string[] SplitString(string original)
		{
			if (String.IsNullOrEmpty(original)) {
				return new string[0];
			}

			var split = from piece in original.Split(',')
						let trimmed = piece.Trim()
						where !String.IsNullOrEmpty(trimmed)
						select trimmed;
			return split.ToArray();
		}


		private string _users;
		private string[] _usersSplit = new string[0];
		private string _roles;
		private string[] _rolesSplit = new string[0];
		private MembershipRoleEnum[] _rolesSplitEnum = new MembershipRoleEnum[0];

	}

}

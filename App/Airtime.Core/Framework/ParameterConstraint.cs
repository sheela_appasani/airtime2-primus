﻿using System.Text.RegularExpressions;
using System.Web;
using System.Web.Routing;

using NLog;


namespace Airtime.Core.Framework
{
	
	public class ParameterConstraint : IRouteConstraint
	{

		public ParameterConstraint(string constraintRegex, bool parameterOptional = true)
		{
			_constraintRegex = new Regex(constraintRegex, RegexOptions.IgnoreCase | RegexOptions.Compiled);
			_parameterParameterOptional = parameterOptional;
		}


		public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
		{
			string param = httpContext.Request.QueryString[parameterName];
			if (param != null) {
				return _constraintRegex.IsMatch(param);
			}
			return _parameterParameterOptional;
		}


		private readonly Regex _constraintRegex;
		private readonly bool _parameterParameterOptional;

	}

}

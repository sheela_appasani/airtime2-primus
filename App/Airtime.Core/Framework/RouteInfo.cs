﻿using System;
using System.Web;
using System.Web.Routing;


namespace Airtime.Core.Framework
{

	public class RouteInfo
	{
		
		public RouteData RouteData { get; private set; }


		public RouteInfo(Uri uri, string applicationPath)
		{
			RouteData = RouteTable.Routes.GetRouteData(new InternalHttpContext(uri, applicationPath));
		}


		private class InternalHttpContext : HttpContextBase
		{
			public override HttpRequestBase Request { get { return _request; } }
			private readonly HttpRequestBase _request;

			public InternalHttpContext(Uri uri, string applicationPath)
			{
				_request = new InternalRequestContext(uri, applicationPath);
			}
		}


		private class InternalRequestContext : HttpRequestBase
		{
			public override string PathInfo { get { return _pathInfo; } }
			private readonly string _pathInfo;

			public override string AppRelativeCurrentExecutionFilePath { get { return _appRelativePath; } }
			private readonly string _appRelativePath;

			public InternalRequestContext(Uri uri, string applicationPath)
			{
				_pathInfo = uri.Query;
				_appRelativePath = !String.IsNullOrEmpty(applicationPath) && !uri.AbsolutePath.StartsWith(applicationPath, StringComparison.OrdinalIgnoreCase)
					? String.Concat("~", uri.AbsolutePath.Substring(applicationPath.Length))
				    : String.Concat("~", uri.AbsolutePath);
			}
		}

	}

}
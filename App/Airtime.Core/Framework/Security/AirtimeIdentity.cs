using System.Security.Principal;


namespace Airtime.Core.Framework.Security
{

	public class AirtimeIdentity : IIdentity
	{

		public string AuthenticationType { get; private set; }
		public bool IsAuthenticated { get; private set; }
		public string Name { get; private set; }
		public int? PersonId { get; private set; }
		public int? MembershipId { get; private set; }


		public AirtimeIdentity(string name, int? personId, int? membershipId)
		{
			AuthenticationType = "FormsAuthentication";
			IsAuthenticated = true;
			Name = name;
			PersonId = personId;
			MembershipId = membershipId;
		}

	}

}

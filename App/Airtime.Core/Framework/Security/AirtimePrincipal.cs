using System.Security.Principal;


namespace Airtime.Core.Framework.Security
{

	public class AirtimePrincipal : GenericPrincipal
	{

		public new AirtimeIdentity Identity { get; private set; }


		public AirtimePrincipal(AirtimeIdentity identity, string[] roles)
			: base(identity, roles)
		{
			Identity = identity;
		}

	}

}

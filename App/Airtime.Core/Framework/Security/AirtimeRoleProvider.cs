using System;
using System.Configuration.Provider;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Security;

using Airtime.Data.Account;

using Microsoft.Practices.ServiceLocation;

using Xtra.Common;


namespace Airtime.Core.Framework.Security
{

	//TODO: probably remove the AirtimeRoleProvider class
	[Obsolete]
	public sealed class AirtimeRoleProvider : RoleProvider
	{

		public override sealed string ApplicationName { get; set; }


		public AirtimeRoleProvider()
		{
			ApplicationName = HostingEnvironment.ApplicationVirtualPath;
		}


		public override bool IsUserInRole(string username, string roleName)
		{
			if (string.IsNullOrEmpty(username)) {
				throw new ProviderException("Username cannot be null or empty.");
			}
			if (string.IsNullOrEmpty(roleName)) {
				throw new ProviderException("RoleName cannot be null or empty.");
			}

			var user = MembershipRepository.FindByUsername(username);
			return user != null && user.Role == roleName.ToEnum<MembershipRoleEnum>();
		}


		public override string[] GetRolesForUser(string username)
		{
			if (string.IsNullOrEmpty(username)) {
				throw new ProviderException("Username cannot be null or empty.");
			}

			var user = MembershipRepository.FindByUsername(username);
			return (user != null)
				? new [] { user.Role.ToString() }
				: new string[0];
		}


		public override void CreateRole(string roleName)
		{
			throw new NotImplementedException();
		}


		public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
		{
			throw new NotImplementedException();
		}


		public override bool RoleExists(string roleName)
		{
			return roleName.IsEnum<MembershipRoleEnum>();
		}


		public override void AddUsersToRoles(string[] usernames, string[] roleNames)
		{
			if (usernames.Any(username => string.IsNullOrEmpty(username))) {
				throw new ProviderException("Username cannot be null or empty.");
			}
			if (roleNames.Length != 1) {
				throw new ProviderException("Exactly one role must be specified.");
			}
			if (roleNames.Any(roleName => string.IsNullOrEmpty(roleName))) {
				throw new ProviderException("RoleName cannot be null or empty.");
			}

			foreach (var username in usernames) {
				var user = MembershipRepository.FindByUsername(username);
				if (user != null) {
					user.Role = roleNames[0].ToEnum<MembershipRoleEnum>();
				}
				MembershipRepository.Save(user);
			}
		}


		public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
		{
			throw new NotImplementedException();
		}


		public override string[] GetUsersInRole(string roleName)
		{
			if (string.IsNullOrEmpty(roleName)) {
				throw new ProviderException("RoleName cannot be null or empty.");
			}

			MembershipRoleEnum role;
			try {
				role = roleName.ToEnum<MembershipRoleEnum>();
			} catch (Exception ex) {
				throw new ProviderException("RoleName is not valid for this application.", ex);
			}

			return MembershipRepository.Items
				.Where(u => u.Role == role)
				.Select(u => u.Username)
				.ToArray();
		}


		public override string[] GetAllRoles()
		{
			return Enum.GetNames(typeof(MembershipRoleEnum));
		}


		public override string[] FindUsersInRole(string roleName, string usernameToMatch)
		{
			if (string.IsNullOrEmpty(roleName)) {
				throw new ProviderException("RoleName cannot be null or empty.");
			}

			MembershipRoleEnum role;
			try {
				role = roleName.ToEnum<MembershipRoleEnum>();
			} catch (Exception ex) {
				throw new ProviderException("RoleName is not valid for this application.", ex);
			}
			
			return MembershipRepository.Items
				.Where(u => u.Role == role && u.Username.Contains(usernameToMatch))
				.Select(u => u.Username)
				.ToArray();
		}


		//Note: we have to use the Service Locator pattern here because there is no way to DI this Role Provider (ASP.NET instantiates it too early)
		private IMembershipRepository MembershipRepository
		{
			get { return ServiceLocator.Current.GetInstance<IMembershipRepository>(); }
		}

	}

}

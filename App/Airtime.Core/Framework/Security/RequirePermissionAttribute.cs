using System;

using Airtime.Data.Account;


namespace Airtime.Core.Framework.Security
{

	public class RequirePermissionAttribute : Attribute
	{

		public PermissionEnum[] Permissions { get; private set; }

		public RequirePermissionAttribute(params PermissionEnum[] permissions)
		{
			Permissions = permissions;
		}

	}

}

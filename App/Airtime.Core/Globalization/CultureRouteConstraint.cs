using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Routing;


namespace Airtime.Core.Globalization
{

	public class CultureRouteConstraint : IRouteConstraint
	{

		public CultureRouteConstraint(params string[] values)
		{
			_cultures = new HashSet<string>(values, StringComparer.OrdinalIgnoreCase);
		}


		public CultureRouteConstraint(IEnumerable<string> values)
		{
			_cultures = new HashSet<string>(values, StringComparer.OrdinalIgnoreCase);
		}


		public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
		{
			return _cultures.Contains(values[parameterName].ToString());
		}


		private readonly HashSet<string> _cultures;

	}

}
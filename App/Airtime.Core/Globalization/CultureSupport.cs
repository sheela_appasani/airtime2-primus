using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;


namespace Airtime.Core.Globalization
{

	public static class CultureSupport
	{

		public static readonly string DefaultCulture = "en-AU";

		public static readonly HashSet<string> AllowedCultures = new HashSet<string>(
			CultureInfo.GetCultures(CultureTypes.AllCultures).Select(c => c.Name),
			StringComparer.OrdinalIgnoreCase
		);

		public static readonly List<string> SupportedCultures = new List<string> {
			"en-AU", "en-MY", "en-NZ", "en-SG", "en-GB", "en-US",

			//"ar-JO", "ar-SA", "ar-AE",
			//"es", "es-MX", "es-ES",
			//"zh-CN", "zh-SG",
			//"zh-TW",

			/*
			"en-AU", "en-CA", "en-IN", "en-IE", "en-MY", "en-NZ", "en-SG", "en-ZA", "en-GB", "en-US",
			"cs-CZ",
			"da-DK",
			"nl", "nl-BE", "nl-NL",
			"fr-BE", "fr-CA", "fr-FR", "fr-LU", "fr-MC", "fr-CH",
			"de", "de-AT", "de-DE", "de-LI", "de-LU", "de-CH", "el-GR",
			"hi", "hi-IN",
			"hu", "hu-HU",
			"id", "id-ID",
			"it", "it-IT",
			"ja", "ja-JP",
			"ms", "ms-MY",
			"no",
			"ru-RU",
			"smn-FI",
			"sr-Cyrl",
			"sv",
			"th-TH",
			"tr-TR"
			*/
		};

	}

}
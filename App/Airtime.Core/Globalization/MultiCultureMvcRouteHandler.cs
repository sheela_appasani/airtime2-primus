using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;


namespace Airtime.Core.Globalization
{

	public class MultiCultureMvcRouteHandler : MvcRouteHandler
	{

		protected override IHttpHandler GetHttpHandler(RequestContext requestContext)
		{
			var culture = requestContext.RouteData.Values["culture"].ToString();
			if (!CultureSupport.AllowedCultures.Contains(culture)) {
				culture = CultureSupport.DefaultCulture;
			}

			var ci = CultureInfo.GetCultureInfo(culture);

			//Set culture for resources
			Thread.CurrentThread.CurrentUICulture = ci;

			//Set culture for formatting (also, .NET prior to v4 required a Specific culture for this property)
			Thread.CurrentThread.CurrentCulture = (ci.IsNeutralCulture && Environment.Version.Major < 4)
				? CultureInfo.CreateSpecificCulture(culture)
				: ci;

			return base.GetHttpHandler(requestContext);
		}

	}

}

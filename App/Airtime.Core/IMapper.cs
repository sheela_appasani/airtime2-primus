namespace Airtime.Core
{

	public interface IMapper<TDomain, TDataContract>
	{
		TDataContract Map(TDomain domain);
		TDataContract Map(TDomain domain, TDataContract dataContract);
		TDomain Map(TDataContract dataContract);
		TDomain Map(TDataContract dataContract, TDomain domain);
	}


	public interface IMapper<TSource>
	{
		TDest Map<TDest>(TSource source);

		TDest Map<TDest>(TSource source, TDest destination);
	}


	public interface IMapper
	{
		TDest Map<TSource, TDest>(TSource source);

		TDest Map<TSource, TDest>(TSource source, TDest destination);
	}

}
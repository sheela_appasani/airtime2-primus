﻿using System;

namespace Airtime.Core
{

	public class JavascriptException : Exception
	{

		public JavascriptException(string message) : base(message)
		{
		}

	}

}
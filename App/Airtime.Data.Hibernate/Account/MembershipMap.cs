using Airtime.Data.Account;

using FluentNHibernate.Mapping;

using NHibernate.Type;


namespace Airtime.Data.Hibernate.Account
{

	public class MembershipMap : ClassMap<Membership>
	{

		public MembershipMap()
		{
			Table("xr_user");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			Map(x => x.Guid).Column("token").Not.Nullable();
			Map(x => x.ExternalId).Column("externalToken").Length(100);

			Map(x => x.Role).Not.Nullable();
			Map(x => x.Username).Length(255).Not.Nullable();
			Map(x => x.Password).Length(255).Not.Nullable();
			Map(x => x.Email).Length(255);
			//Map(x => x.GivenName).Column("firstName").Length(255).Not.Nullable().Default("");
			//Map(x => x.FamilyName).Column("lastName").Length(255).Not.Nullable().Default("");

			Map(x => x.Enabled).Column("isEnabled").CustomType<YesNoType>();
			Map(x => x.EnabledFrom).Column("activeFrom").Nullable();
			Map(x => x.EnabledTo).Column("activeTo").Nullable();
			Map(x => x.MembershipStatus).Column("membershipStatus");

			Map(x => x.Notes);

			References(x => x.Person).Column("personId").Cascade.None();
			References(x => x.Company).Column("companyId").Cascade.None();

			HasMany(x => x.Permissions)
				.Table("xr_user_permission")
				.KeyColumn("userId")
				.Element("permission", e => e.Type<EnumStringType<PermissionEnum>>())
				.Fetch.Join()
				.Cascade.AllDeleteOrphan();

			HasManyToMany(x => x.Channels)
				.Table("xr_user_channel")
				.ParentKeyColumn("userId")
				.ChildKeyColumn("channelId")
				.Cascade.None();

			HasMany(x => x.RespondentStates)
				.KeyColumn("userId")
				.Inverse()
				.Cascade.AllDeleteOrphan()
				.LazyLoad();

			HasMany(x => x.ProfileAnswers)
				.KeyColumn("userId")
				.Where("surveyRespondentId is null and isProfileAnswer = 'Y'")
				.Cascade.AllDeleteOrphan()
				.Inverse()
				.LazyLoad();

			//Component(x => x.Personal, m => {
			//	m.Map(x => x.Gender).Nullable();
			//	m.Map(x => x.DateOfBirth).Nullable();
			//});

			//Component(x => x.Contact, m => {
			//	m.Map(x => x.DaytimePhone).Length(50);
			//	m.Map(x => x.MobilePhone).Length(50);
			//	m.Map(x => x.Address).Length(100);
			//	m.Map(x => x.Suburb).Length(50);
			//	m.Map(x => x.City).Length(50);
			//	m.Map(x => x.Postcode).Length(10);
			//	m.Map(x => x.State).Length(50);
			//	m.Map(x => x.Country).Length(50);
			//});

			Component(x => x.OptIns, m => {
				m.Map(x => x.Surveys).Column("optInSurveys").CustomType<YesNoType>();
				m.Map(x => x.Newsletters).Column("optInInformation").CustomType<YesNoType>();
			});

			Component(x => x.Flags, m => {
				m.Map(x => x.Screened).CustomType<YesNoType>();
				m.Map(x => x.SystemChecked).CustomType<YesNoType>();
				m.Map(x => x.SampleTested).CustomType<YesNoType>();
			});
		}

	}

}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using Airtime.Data.Account;
using Airtime.Data.Client;
using Airtime.Data.Surveying;

using NHibernate.Linq;


namespace Airtime.Data.Hibernate.Account
{

	public class MembershipRepository : HibernateRepository<Membership, int>, IMembershipRepository
	{

		public MembershipRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}


		public Membership FindByUsername(string username)
		{
			return Items.Where(u => u.Username == username).Cacheable().SingleOrDefault();
		}


		public Membership FindByGuid(Guid guid)
		{
			return Items.Where(u => u.Guid == guid).Cacheable().SingleOrDefault();
		}


		/// <summary>
		/// Find person's oldest active membership for this channel, giving priority to Respondent memberships
		/// </summary>
		/// <param name="person"></param>
		/// <param name="channel"></param>
		/// <returns></returns>
		public Membership FindByPersonChannel(Person person, Channel channel)
		{
			Membership membership = null;

			if (channel != null) {
				membership = FindByPersonChannelQuery(
					person, channel,
					q => q.Where(m => m.Role == MembershipRoleEnum.Respondent)
				);
			}

			if (membership == null && person.HasAdminMembership()) {
				membership = FindByPersonChannelQuery(
					person, null,
					q => q.Where(m => m.Role == MembershipRoleEnum.Administrator || m.Role == MembershipRoleEnum.Developer)
				);
			}

			if (membership == null) {
				return null;
			}

			return membership;
		}


		private Membership FindByPersonChannelQuery(Person person, Channel channel, Func<IQueryable<Membership>, IQueryable<Membership>> expression = null)
		{
			var query = Items;

			if (expression != null) {
				query = expression(query);
			}

			if (channel != null) {
				query = query.Where(u => u.Channels.Contains(channel));
			}

			return query
				.Where(u => u.Username != "purged_user")
				.Where(u => u.Person == person && u.Enabled)
				.OrderBy(u => u.Id)
				.Cacheable()
				.FirstOrDefault();
		}


		public IQueryable<Membership> QueryBySurvey(Survey survey)
		{
			return
				from membership in Items
				from respondentState in membership.RespondentStates
				where survey == respondentState.Survey
				select membership;
		}


		public void Purge(Membership membership)
		{
			membership.MembershipStatus = MembershipStatusEnum.Wiped;
			membership.Enabled = false;
			membership.Username = "purged_user";
			membership.Email = "purged@user.xtr";
			membership.Password = "";
			//membership.GivenName = "";
			//membership.FamilyName = "";
			//membership.Contact.Address = "";
			//membership.Contact.DaytimePhone = null;
			//membership.Contact.MobilePhone = null;
			membership.ExternalId = null;
			Save(membership);
		}

	}

}

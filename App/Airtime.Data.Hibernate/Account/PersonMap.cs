using Airtime.Data.Account;

using FluentNHibernate.Mapping;


namespace Airtime.Data.Hibernate.Account
{

	public class PersonMap : ClassMap<Person>
	{

		public PersonMap()
		{
			Table("xr_person");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			Map(x => x.Email).Length(255);
			Map(x => x.Password).Length(255);
			Map(x => x.GivenName).Length(60);
			Map(x => x.FamilyName).Length(60);

			HasMany(x => x.Memberships)
				.KeyColumn("personId")
				.Inverse()
				.Cascade.AllDeleteOrphan()
				.LazyLoad();

			HasMany(x => x.DeviceRegistrations)
				.KeyColumn("personId")
				.Inverse()
				.Cascade.AllDeleteOrphan()
				.LazyLoad();

			Component(x => x.Personal, m => {
				m.Map(x => x.Gender).Nullable();
				m.Map(x => x.DateOfBirth).Nullable();
			});

			Component(x => x.Contact, m => {
				m.Map(x => x.DaytimePhone).Length(50);
				m.Map(x => x.MobilePhone).Length(50);
				m.Map(x => x.Address).Length(100);
				m.Map(x => x.Suburb).Length(50);
				m.Map(x => x.City).Length(50);
				m.Map(x => x.Postcode).Length(10);
				m.Map(x => x.State).Length(50);
				m.Map(x => x.Country).Length(50);
			});
		}

	}

}

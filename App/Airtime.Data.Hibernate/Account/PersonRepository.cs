using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using Airtime.Data.Account;

using NHibernate.Linq;


namespace Airtime.Data.Hibernate.Account
{

	public class PersonRepository : HibernateRepository<Person, int>, IPersonRepository
	{

		public PersonRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}


		public Person FindByEmail(string email)
		{
			return Items.SingleOrDefault(u => u.Email == email);
		}

	}

}

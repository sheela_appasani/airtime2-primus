using System.Reflection;


namespace Airtime.Data.Hibernate
{
	public abstract class AssemblyHook
	{
		public static Assembly Assembly { get { return typeof(AssemblyHook).Assembly; } }

		//The following variables are required so that msbuild doesn't incorrectly remove their types' assemblies when it's attempting to optimise deployment
		#pragma warning disable 0169
		private NHibernate.Caches.SysCache.SysCacheProvider _deploymentHack2;
		#pragma warning restore 0169
	}

}

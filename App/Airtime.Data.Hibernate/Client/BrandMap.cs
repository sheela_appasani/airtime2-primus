using System;

using Airtime.Data.Client;

using FluentNHibernate.Mapping;


namespace Airtime.Data.Hibernate.Client
{

	public class BrandMap : ClassMap<Brand>
	{

		public BrandMap()
		{
			Table("xr_brand");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			Map(x => x.Name).Length(255).Not.Nullable();

			HasMany(x => x.Channels)
				.KeyColumn("brandId")
				.Cascade.None()
				.Inverse()
				.LazyLoad();

			HasMany(x => x.Apps)
				.KeyColumn("brandId")
				.Cascade.None()
				.Inverse()
				.LazyLoad();
		}

	}

}

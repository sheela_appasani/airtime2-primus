using System;

using Airtime.Data.Content;

using FluentNHibernate.Mapping;


namespace Airtime.Data.Hibernate.Client
{

	public class PageContentMap : ClassMap<CustomContent>
	{

		public PageContentMap()
		{
			Table("xr_content");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			References(x => x.Channel).Column("channelId").Cascade.None();
			References(x => x.Survey).Column("surveyId").Cascade.None();

			Map(x => x.ContentType).Not.Nullable();
			Map(x => x.Text);
		}

	}

}

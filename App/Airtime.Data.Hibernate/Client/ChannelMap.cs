using System;

using Airtime.Data.Client;

using FluentNHibernate.Mapping;

using NHibernate.Type;


namespace Airtime.Data.Hibernate.Client
{

	public class ChannelMap : ClassMap<Channel>
	{

		public ChannelMap()
		{
			Table("xr_channel");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			References(x => x.Company).Column("companyId").Cascade.None();
			References(x => x.Brand).Column("brandId").Cascade.None();

			Map(x => x.Name).Column("name").Length(255).Not.Nullable();
			Map(x => x.AdminName).Column("title").Length(255).Not.Nullable();
			Map(x => x.InternalName).Column("internalName").Length(255).Not.Nullable();

			Map(x => x.EmailFrom).Length(255);
			Map(x => x.EmailCancellationsTo).Column("emailNotificationsTo").Length(255);
			Map(x => x.EmailFeedbackTo).Length(255);

			Map(x => x.HomePageUrl).Column("externalUrl").Length(1024);
			Map(x => x.ContestRulesUrl).Length(1024);
			Map(x => x.PrivacyPolicyUrl).Length(1024);
			Map(x => x.ContactDetailsUrl).Length(1024);
			Map(x => x.LogoUrl).Length(1024);
			Map(x => x.TimeZone).Length(32);

			Map(x => x.FastTrackPods).CustomType<YesNoType>().Not.Nullable();
			Map(x => x.UseShortRegoForm).CustomType<YesNoType>().Not.Nullable();
			Map(x => x.EnableRegistrationDetailsApp).Column("useRegistrationDetailsApp").Not.Nullable().CustomType<YesNoType>();
			Map(x => x.EnableRegistrationDetailsWeb).Column("useRegistrationDetailsWeb").Not.Nullable().CustomType<YesNoType>();
			Map(x => x.EnableLandingPage).Not.Nullable().CustomType<YesNoType>();

			References(x => x.Language).Column("languageId").Cascade.None();
			References(x => x.Screener).Column("screenerId").Cascade.All();

			HasManyToMany(x => x.Apps)
				.Table("xr_app_channel")
				.ParentKeyColumn("channelId")
				.ChildKeyColumn("appId")
				.Cascade.None()
				.ExtraLazyLoad();

			HasManyToMany(x => x.Memberships)
				.Table("xr_user_channel")
				.ParentKeyColumn("channelId")
				.ChildKeyColumn("userId")
				.Cascade.None()
				.ExtraLazyLoad();

			HasMany(x => x.Surveys)
				.KeyColumn("channelId")
				.Cascade.AllDeleteOrphan()
				.Inverse()
				.LazyLoad();

			HasMany(x => x.ProfileQuestions)
				.KeyColumn("channelId")
				.Where("channelId is not null and surveyId is null")
				.AsList(index => index.Column("ordering"))
				.OrderBy("ordering")
				.Cascade.AllDeleteOrphan()
				.Inverse()
				.LazyLoad();

			HasMany(x => x.Contents)
				.KeyColumn("channelId")
				.Cascade.AllDeleteOrphan()
				.Inverse()
				.LazyLoad();

			HasMany(x => x.Widgets)
				.KeyColumn("ChannelId")
				.Cascade.AllDeleteOrphan()
				.Inverse()
				.LazyLoad();

			Map(x => x.TargetMinimumAge);
			Map(x => x.TargetMaximumAge);
			Map(x => x.TargetGender).Nullable();
		}

	}

}

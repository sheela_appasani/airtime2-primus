using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using Airtime.Data.Client;

using NHibernate;
using NHibernate.Linq;

namespace Airtime.Data.Hibernate.Client
{

	public class ChannelRepository : HibernateRepository<Channel, int>, IChannelRepository
	{

		public ChannelRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

	}

}

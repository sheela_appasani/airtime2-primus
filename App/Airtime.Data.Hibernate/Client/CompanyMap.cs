using System;

using Airtime.Data.Client;

using FluentNHibernate.Mapping;

namespace Airtime.Data.Hibernate.Client
{

	public class CompanyMap : ClassMap<Company>
	{

		public CompanyMap()
		{
			Table("xr_company");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			Map(x => x.Name).Column("Company").Length(255).Not.Nullable();
			HasMany(x => x.Channels)
				.KeyColumn("companyId")
				.Cascade.AllDeleteOrphan()
				.Inverse()
				.LazyLoad();
		}

	}

}
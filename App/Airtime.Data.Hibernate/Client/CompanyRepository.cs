using System;
using System.Linq;

using Airtime.Data.Client;

using NHibernate;
using NHibernate.Linq;

namespace Airtime.Data.Hibernate.Client
{

	public class CompanyRepository : HibernateRepository<Company, int>, ICompanyRepository
	{

		public CompanyRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}


		public Company this[string name]
		{
			get { return FindByName(name); }
		}


		public Company FindByName(string name)
		{
			return Items.Where(c => c.Name == name).Cacheable().SingleOrDefault();
		}

	}

}
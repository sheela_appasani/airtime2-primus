using Airtime.Data.Content.Fields;

using FluentNHibernate.Mapping;


namespace Airtime.Data.Hibernate.Content.Fields
{

	public class FieldBaseMap : ClassMap<FieldBase>
	{

		public FieldBaseMap()
		{
			Table("xr_field");
			DiscriminateSubClassesOnColumn("FieldTypeName").AlwaysSelectWithValue();
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			References(x => x.FieldType).Column("FieldTypeId");
			Map(x => x.Name).Length(255);
		}

	}

}

using Airtime.Data.Content.Fields;


namespace Airtime.Data.Hibernate.Content.Fields
{

	public class FieldRepository : HibernateRepository<FieldBase, int>, IFieldRepository
	{

		public FieldRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

	}

}

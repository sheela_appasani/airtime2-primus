using Airtime.Data.Content.Fields;
using Airtime.Data.Content.Fields.Types;

using FluentNHibernate.Mapping;


namespace Airtime.Data.Hibernate.Content.Fields
{

	public class SongFieldMap : SubclassMap<SongField>
	{

		public SongFieldMap()
		{
			DiscriminatorValue(SongFieldType.Discriminator);

			References(x => x.Scope).Column("ScopeId").Cascade.None();
		}

	}

}

using System.Linq;

using Airtime.Data.Client;
using Airtime.Data.Content.Fields;
using Airtime.Data.Content.Fields.Types;

using NHibernate.Linq;


namespace Airtime.Data.Hibernate.Content.Fields
{

	public class SongFieldRepository : HibernateRepository<SongField, int>, ISongFieldRepository
	{

		public SongFieldRepository(IUnitOfWork unitOfWork, IFieldTypeRepository fieldTypeRepository)
			: base(unitOfWork)
		{
			FieldTypeRepository = fieldTypeRepository;
		}


		public SongField FindByNameAndCompany(string name, Company company)
		{
			return Items.SingleOrDefault(x => x.Name == name && x.Scope == company);
		}


		public SongField CreateAndSaveField(string name, Company company)
		{
			var field = new SongField(FieldTypeRepository[SongFieldType.Discriminator], name, company);
			Save(field);
			return field;
		}


		private IFieldTypeRepository FieldTypeRepository { get; set; }

	}

}

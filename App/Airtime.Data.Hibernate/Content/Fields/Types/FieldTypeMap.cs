using Airtime.Data.Content.Fields.Types;

using FluentNHibernate.Mapping;


namespace Airtime.Data.Hibernate.Content.Fields.Types
{

	public class FieldTypeMap : ClassMap<FieldType>
	{

		public FieldTypeMap()
		{
			Table("xr_field_type");
			DiscriminateSubClassesOnColumn("DataContext").AlwaysSelectWithValue();
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			Map(x => x.DataContext);
			Map(x => x.DataScope);
			Map(x => x.DataType);
			Map(x => x.Name);
		}

	}

}

using System.Linq;

using Airtime.Data.Content.Fields.Types;

using NHibernate.Linq;


namespace Airtime.Data.Hibernate.Content.Fields.Types
{

	public class FieldTypeRepository : HibernateRepository<FieldType, int>, IFieldTypeRepository
	{

		public FieldTypeRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}


		public FieldType this[string name]
		{
			get { return FindByName(name); }
		}


		public FieldType FindByName(string name)
		{
			return Items.Cacheable().SingleOrDefault(x => x.Name == name);
		}

	}

}

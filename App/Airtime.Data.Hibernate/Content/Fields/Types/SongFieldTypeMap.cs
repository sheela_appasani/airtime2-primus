using Airtime.Data.Content.Fields.Types;

using FluentNHibernate.Mapping;


namespace Airtime.Data.Hibernate.Content.Fields.Types
{

	public class SongFieldTypeMap : SubclassMap<SongFieldType>
	{

		public SongFieldTypeMap()
		{
			DiscriminatorValue(FieldContextEnum.Song.ToString());
		}

	}

}

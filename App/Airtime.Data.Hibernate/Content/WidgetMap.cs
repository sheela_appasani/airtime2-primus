using System;

using Airtime.Data.Client;
using Airtime.Data.Content;

using FluentNHibernate.Mapping;

using NHibernate.Type;


namespace Airtime.Data.Hibernate.Content
{

	public class WidgetMap : ClassMap<Widget>
	{

		public WidgetMap()
		{
			Table("xr_widget");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			References(x => x.Channel).Column("ChannelId").Cascade.None();

			Map(x => x.WidgetType);
			Map(x => x.Enabled).CustomType<YesNoType>().Not.Nullable();

			Map(x => x.Heading).Length(255);
			Map(x => x.Content);
		}

	}

}
using System;
using System.Linq;

using Airtime.Data.Content;

using NHibernate;

namespace Airtime.Data.Hibernate.Content
{

	public class WidgetRepository : HibernateRepository<Widget, int>, IWidgetRepository
	{

		public WidgetRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

	}

}
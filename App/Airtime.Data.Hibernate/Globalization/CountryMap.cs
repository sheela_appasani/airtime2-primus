using System;

using Airtime.Data.Globalization;

using FluentNHibernate.Mapping;


namespace Airtime.Data.Hibernate.Globalization
{

	public class CountryMap : ClassMap<Country>
	{

		public CountryMap()
		{
			Table("xr_sms_country");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();

			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			Map(x => x.Name).Column("Country").Length(255).Not.Nullable();
			Map(x => x.PhoneCountryCode).Column("Code").Length(50).Not.Nullable();
			Map(x => x.PhoneNddPrefix).Column("NDD").Length(5).Nullable();
		}

	}

}
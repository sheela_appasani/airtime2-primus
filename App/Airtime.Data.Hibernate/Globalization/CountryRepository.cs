﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Airtime.Data.Globalization;

namespace Airtime.Data.Hibernate.Globalization
{

	/// <summary>
	/// This is a minimal repository with a hard coded list of country names for data source.
	/// </summary>
	public class CountryRepository : IRepository<Country, int>, ICountryRepository
	{

		public CountryRepository()
		{
			_countryList = _countryNameList
				.OrderBy(n => n) // for now ensure order in case we messed in data below.
				.Select(n => new Country { Name = n })
				.ToList();
			_queryable = _countryList.AsQueryable();
		}


		#region Implementation of IEnumerable

		public IEnumerator<Country> GetEnumerator()
		{
			return _countryList.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		#endregion


		#region Implementation of IQueryable

		public Expression Expression
		{
			get { return _queryable.Expression; }
		}

		public Type ElementType
		{
			get { return _queryable.ElementType; }
		}

		public IQueryProvider Provider
		{
			get { return _queryable.Provider; }
		}

		#endregion


		#region Implementation of IRepository<Country,int>

		public Country this[int id]
		{
			get { throw new NotImplementedException(); }
		}


		public IQueryable<Country> Items
		{
			get { return this; }
		}


		public Country FindById(int id)
		{
			throw new NotImplementedException();
		}


		public List<Country> FindAllById(ICollection<int> ids)
		{
			throw new NotImplementedException();
		}


		public int Save(Country instance)
		{
			throw new NotImplementedException();
		}


		public void Delete(Country instance)
		{
			throw new NotImplementedException();
		}


		public IQueryable<Country> SetCacheable(IQueryable<Country> query)
		{
			throw new NotImplementedException();
		}


		public IQueryable<Country> ApplyFetch<TRelated>(IQueryable<Country> query, Expression<Func<Country, TRelated>> relatedObjectSelector)
		{
			throw new NotImplementedException();
		}


		public IQueryable<Country> ApplyFetchMany<TRelated>(IQueryable<Country> query, Expression<Func<Country, IEnumerable<TRelated>>> relatedObjectSelector)
		{
			throw new NotImplementedException();
		}

		#endregion


		private readonly IList<string> _countryNameList = new List<string>() { "Australia", "Austria", "Belgium", "Brazil", "Bulgaria", "Canada", "Czech Republic", "Denmark", "Finland", "France", "Germany", "Greece", "Hungary", "India", "Indonesia", "Ireland", "Italy", "Japan", "Luxembourg", "Malaysia", "Mexico", "Netherlands", "New Zealand", "Norway", "Poland", "Portugal", "Romania", "Singapore", "South Africa", "South Korea", "Spain", "Sweden", "Switzerland", "Taiwan", "Thailand", "United Arab Emirates", "United Kingdom", "United States" };
		private readonly IList<Country> _countryList;
		private readonly IQueryable<Country> _queryable;

	}

}

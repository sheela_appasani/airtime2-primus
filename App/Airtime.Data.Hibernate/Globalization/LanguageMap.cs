using System;

using Airtime.Data.Globalization;

using FluentNHibernate.Mapping;


namespace Airtime.Data.Hibernate.Globalization
{

	public class LanguageMap : ClassMap<Language>
	{

		public LanguageMap()
		{
			Table("xr_language");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			Map(x => x.Name).Column("languageName").Length(50).Not.Nullable();
			Map(x => x.Guid).Column("code").Not.Nullable();
		}

	}

}
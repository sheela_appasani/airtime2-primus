using System;
using System.Linq;

using Airtime.Data.Globalization;

using NHibernate;
using NHibernate.Linq;

namespace Airtime.Data.Hibernate.Globalization
{

	public class LanguageRepository : HibernateRepository<Language, int>, ILanguageRepository
	{

		public LanguageRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}


		public Language FindByName(string name)
		{
			return Items.Where(c => c.Name == name).Cacheable().SingleOrDefault();
		}

	}

}
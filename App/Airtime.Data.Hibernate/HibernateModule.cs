using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;

using Autofac;

using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using HibernatingRhinos.Profiler.Appender.NHibernate;

using NHibernate;
using NHibernate.Caches.SysCache;
using NLog;

using Xtra.Common;

using Configuration = NHibernate.Cfg.Configuration;
using Environment = NHibernate.Cfg.Environment;


namespace Airtime.Data.Hibernate
{

	public class HibernateModule : Module
	{

		public HibernateModule(bool exportSchema = false)
		{
			ExportSchema = exportSchema;
		}


		protected override void Load(ContainerBuilder builder)
		{
			if (builder == null) {
				throw new ArgumentNullException("builder");
			}

			var enableNHibernateProfiler = ConfigurationManager.AppSettings["EnableNHibernateProfiler"].ConvertToOrDefault<bool>();
			if (enableNHibernateProfiler) {
				NHibernateProfiler.Initialize();
			}

			RegisterConfiguration(builder);
			BuildSessionFactory(builder);
			RegisterComponents(builder);
		}


		/// <summary>
		/// Load NHibernate configuration
		/// </summary>
		/// <param name="builder"></param>
		protected virtual void RegisterConfiguration(ContainerBuilder builder)
		{
			Action<MappingConfiguration> mappings = delegate(MappingConfiguration mappingConfig) {
				//Load fluent mappings and custom conventions
				var mappingContainer = mappingConfig
					.FluentMappings.AddFromAssemblyOf<Airtime.Data.Hibernate.AssemblyHook>()
					.Conventions.AddFromAssemblyOf<Airtime.Data.Hibernate.AssemblyHook>();

				//Add mappings from any .hbm.xml files in the assembly
				mappingConfig.HbmMappings.AddFromAssemblyOf<Airtime.Data.Hibernate.AssemblyHook>();

				if (ExportSchema) {
					//Export generated schema mappings into files, to enable easier debugging
					string schemaExportPath = Path.Combine(AppDomain.CurrentDomain.GetData("DataDirectory").ToString(), "Mappings");
					if (!Directory.Exists(schemaExportPath)) {
						Directory.CreateDirectory(schemaExportPath);
					}
					mappingContainer.ExportTo(schemaExportPath);
				}
			};

			//Configure NHibernate
			var config = Fluently
				.Configure()
					.Database(MsSqlConfiguration.MsSql2008
						.Dialect<XtraMsSql2008Dialect>()
						.ConnectionString(c => c.FromConnectionStringWithKey("MainDatabaseConnectionString"))
						.IsolationLevel(ConfigurationManager.AppSettings[Environment.Isolation].ConvertToOrDefault<IsolationLevel>(DefaultIsolationLevel, CultureInfo.InvariantCulture))
						.AdoNetBatchSize(ConfigurationManager.AppSettings[Environment.BatchSize].ConvertToOrDefault<int>(DefaultBatchSize, CultureInfo.InvariantCulture))
						.Raw(Environment.CommandTimeout, ConfigurationManager.AppSettings[Environment.CommandTimeout].ConvertToOrDefault<int>(DefaultCommandTimeout, CultureInfo.InvariantCulture).ToString(CultureInfo.InvariantCulture))
						.Raw(Environment.GenerateStatistics, ConfigurationManager.AppSettings[Environment.GenerateStatistics].ConvertToOrDefault<bool>(DefaultGenerateStatistics, CultureInfo.InvariantCulture).ToString())
						.Raw(Environment.PrepareSql, ConfigurationManager.AppSettings[Environment.PrepareSql].ConvertToOrDefault<bool>(DefaultPrepareSql, CultureInfo.InvariantCulture).ToString())
						.Raw(Environment.LinqToHqlGeneratorsRegistry, typeof(XtraLinqToHqlGeneratorsRegistry).AssemblyQualifiedName)
						.UseReflectionOptimizer()
					)
					.Cache(c => c
						.ProviderClass<SysCacheProvider>()
						.UseQueryCache()
						.UseSecondLevelCache()
						.UseMinimalPuts()
					)
				.Mappings(mappings)
				.ExposeConfiguration(c => c.SetProperty(Environment.ReleaseConnections, "on_close"))
				.BuildConfiguration();

			builder.RegisterInstance(config).As<Configuration>().SingleInstance();
		}


		protected virtual void BuildSessionFactory(ContainerBuilder builder)
		{
			builder.Register(c => c.Resolve<Configuration>().BuildSessionFactory())
				.As<ISessionFactory>()
				.SingleInstance();
		}


		protected virtual void RegisterComponents(ContainerBuilder builder)
		{
			builder.Register(c => c.Resolve<ISessionFactory>().OpenSession())
				.As<ISession>()
				.InstancePerLifetimeScope();

			builder.Register(c => new HibernateUnitOfWork(c.Resolve<ISessionFactory>()))
				.As<IUnitOfWork>()
				.InstancePerLifetimeScope();

			//Register repositories and query-classes
			builder.RegisterAssemblyTypes(Airtime.Data.Hibernate.AssemblyHook.Assembly)
				.Where(t => !t.IsAbstract && (t.IsAssignableTo<IDataWorker>() || t.IsAssignableTo<IRepositoryBase>()))
				.AsImplementedInterfaces()
				.InstancePerLifetimeScope();
		}


		protected bool ExportSchema;


		protected static readonly int DefaultBatchSize = 32;
		protected static readonly int DefaultCommandTimeout = 0;
		protected static readonly bool DefaultPrepareSql = true;
		protected static readonly bool DefaultGenerateStatistics = true;
		protected static readonly IsolationLevel DefaultIsolationLevel = IsolationLevel.ReadCommitted;


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

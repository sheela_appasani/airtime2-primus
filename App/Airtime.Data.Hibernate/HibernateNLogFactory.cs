using System;

using NHibernate;

using NLog;


namespace Airtime.Data.Hibernate
{
	
	public class HibernateNLogFactory : ILoggerFactory
	{

		public IInternalLogger LoggerFor(Type type)
		{
			return new HibernateNLogLogger(LogManager.GetLogger(type.FullName));
		}


		public IInternalLogger LoggerFor(string keyName)
		{
			return new HibernateNLogLogger(LogManager.GetLogger(keyName));
		}

	}

}

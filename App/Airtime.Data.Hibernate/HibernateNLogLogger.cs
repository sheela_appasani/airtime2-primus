using System;

using NLog;

using NHibernate;


namespace Airtime.Data.Hibernate
{

	public class HibernateNLogLogger : IInternalLogger
	{

		public HibernateNLogLogger(Logger logger)
		{
			_logger = logger;
		}


		public bool IsDebugEnabled { get { return _logger.IsDebugEnabled; } }

		public bool IsErrorEnabled { get { return _logger.IsErrorEnabled; } }

		public bool IsFatalEnabled { get { return _logger.IsFatalEnabled; } }

		public bool IsInfoEnabled { get { return _logger.IsInfoEnabled; } }

		public bool IsWarnEnabled { get { return _logger.IsWarnEnabled; } }


		public void Debug(object message, Exception exception)
		{
			_logger.DebugException(message.ToString(), exception);
		}


		public void Debug(object message)
		{
			_logger.Debug(message);
		}


		public void DebugFormat(string format, params object[] args)
		{
			_logger.Debug(format, args);
		}


		public void Error(object message, Exception exception)
		{
			_logger.ErrorException(message.ToString(), exception);
		}


		public void Error(object message)
		{
			_logger.Error(message);
		}


		public void ErrorFormat(string format, params object[] args)
		{
			_logger.Error(format, args);
		}


		public void Fatal(object message, Exception exception)
		{
			_logger.FatalException(message.ToString(), exception);
		}


		public void Fatal(object message)
		{
			_logger.Fatal(message);
		}


		public void Info(object message, Exception exception)
		{
			_logger.InfoException(message.ToString(), exception);
		}


		public void Info(object message)
		{
			_logger.Info(message);
		}


		public void InfoFormat(string format, params object[] args)
		{
			_logger.Info(format, args);
		}


		public void Warn(object message, Exception exception)
		{
			_logger.WarnException(message.ToString(), exception);
		}


		public void Warn(object message)
		{
			_logger.Warn(message);
		}


		public void WarnFormat(string format, params object[] args)
		{
			_logger.Warn(format, args);
		}


		private readonly Logger _logger;

	}

}

using NHibernate;


namespace Airtime.Data.Hibernate
{

	public class HibernateQueryObject : IQueryObject
	{

		public IUnitOfWork UnitOfWork { get { return _unitOfWork; } }
		public ISession Session { get { return _unitOfWork.Session; } }


		public HibernateQueryObject(IUnitOfWork unitOfWork)
		{
			_unitOfWork = (HibernateUnitOfWork)unitOfWork;
		}


		private readonly HibernateUnitOfWork _unitOfWork;

	}

}

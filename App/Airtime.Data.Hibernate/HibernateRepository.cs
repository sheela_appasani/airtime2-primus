using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using NHibernate;
using NHibernate.Linq;


namespace Airtime.Data.Hibernate
{

	public class HibernateRepository<TEntity, TId> : IHibernateRepository<TEntity, TId>
		where TEntity : class, IEntityWithTypedId<TId>
	{

		public IUnitOfWork UnitOfWork { get { return _unitOfWork; } }
		public ISession Session { get { return _unitOfWork.Session; } }

		public TEntity this[TId id] { get { return FindById(id); } }

		public virtual IQueryProvider Provider { get { return _queryable.Value.Provider; } }
		public virtual Expression Expression { get { return _queryable.Value.Expression; } }
		public virtual Type ElementType { get { return _queryable.Value.ElementType; } }

		public virtual IQueryable<TEntity> Items { get { return this; } }


		public HibernateRepository(IUnitOfWork unitOfWork)
		{
			_unitOfWork = (HibernateUnitOfWork)unitOfWork;
			_queryable = new Lazy<IQueryable<TEntity>>(() => Session.Query<TEntity>());
		}


		public virtual TId Save(TEntity instance)
		{
			return (TId)Session.Save(instance);
		}


		public virtual void Delete(TEntity instance)
		{
			Session.Delete(instance);
		}


		public virtual TEntity FindById(TId id)
		{
			return Session.Get<TEntity>(id);
		}


		public virtual List<TEntity> FindAllById(ICollection<TId> ids)
		{
			return this.Where(e => ids.Contains(e.Id)).ToList();
		}


		public virtual void Refresh(TEntity instance)
		{
			Session.Refresh(instance);
		}


		public virtual void Evict(TEntity instance)
		{
			Session.Evict(instance);
		}


		public virtual IEnumerator<TEntity> GetEnumerator()
		{
			return this.OrderBy(e => e.Id).GetEnumerator();
		}


		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}


		public IQueryable<TEntity> SetCacheable(IQueryable<TEntity> query)
		{
			return query.Cacheable();
		}


		public IQueryable<TEntity> ApplyFetch<TRelated>(IQueryable<TEntity> query, Expression<Func<TEntity, TRelated>> relatedObjectSelector)
		{
			return query.Fetch(relatedObjectSelector);
		}


		public IQueryable<TEntity> ApplyFetchMany<TRelated>(IQueryable<TEntity> query, Expression<Func<TEntity, IEnumerable<TRelated>>> relatedObjectSelector)
		{
			return query.FetchMany(relatedObjectSelector);
		}


		private readonly Lazy<IQueryable<TEntity>> _queryable;
		private readonly HibernateUnitOfWork _unitOfWork;

	}

}

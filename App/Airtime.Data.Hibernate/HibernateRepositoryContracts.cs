using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;

namespace Airtime.Data.Hibernate
{

	[ContractClassFor(typeof(IHibernateRepository<,>))]
	internal abstract class HibernateRepositoryContracts<TEntity, TId> : IHibernateRepository<TEntity, TId>
		where TEntity : class, IEntityWithTypedId<TId>
	{

		public void Refresh(TEntity instance)
		{
			Contract.Requires<ArgumentNullException>(instance != null, "instance");
		}

		public void Evict(TEntity instance)
		{
			Contract.Requires<ArgumentNullException>(instance != null, "instance");
		}

		public IEnumerator<TEntity> GetEnumerator()
		{
			throw new NotImplementedException();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public Expression Expression
		{
			get { throw new NotImplementedException(); }
		}

		public Type ElementType
		{
			get { throw new NotImplementedException(); }
		}

		public IQueryProvider Provider
		{
			get { throw new NotImplementedException(); }
		}

		public IUnitOfWork UnitOfWork
		{
			get { throw new NotImplementedException(); }
		}

		public TEntity this[TId id]
		{
			get { throw new NotImplementedException(); }
		}

		public IQueryable<TEntity> Items
		{
			get { throw new NotImplementedException(); }
		}

		public TEntity FindById(TId id)
		{
			throw new NotImplementedException();
		}

		public List<TEntity> FindAllById(ICollection<TId> ids)
		{
			throw new NotImplementedException();
		}

		public TId Save(TEntity instance)
		{
			throw new NotImplementedException();
		}

		public void Delete(TEntity instance)
		{
			throw new NotImplementedException();
		}

		public IQueryable<TEntity> SetCacheable(IQueryable<TEntity> query)
		{
			throw new NotImplementedException();
		}


		public IQueryable<TEntity> ApplyFetch<TRelated>(IQueryable<TEntity> query, Expression<Func<TEntity, TRelated>> relatedObjectSelector)
		{
			throw new NotImplementedException();
		}


		public IQueryable<TEntity> ApplyFetchMany<TRelated>(IQueryable<TEntity> query, Expression<Func<TEntity, IEnumerable<TRelated>>> relatedObjectSelector)
		{
			throw new NotImplementedException();
		}
	}

}

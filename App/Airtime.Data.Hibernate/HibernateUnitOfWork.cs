using System.Data.Common;
using System.Web;

using NHibernate;


namespace Airtime.Data.Hibernate
{

	public class HibernateUnitOfWork : IUnitOfWork
	{

		public ISession Session { get { return OpenSession(); } }


		public HibernateUnitOfWork(ISessionFactory sessionFactory)
		{
			_sessionFactory = sessionFactory;
		}


		public virtual void Dispose()
		{
			if (!_disposed) {
				_disposed = true;
				CloseSession();
			}
		}


		/// <summary>
		/// Begin a Unit of Work. This usually entails establishing a database session and starting a transaction.
		/// </summary>
		public virtual void Begin()
		{
			//Nothing needs to be done here in this implementation because the unit-of-work will automatically "begin" (open session & transaction as required) when it's first used
		}


		/// <summary>
		/// Cancel a Unit of Work. This is similar to ending a unit of work, but will always rollback the active transaction.
		/// </summary>
		public virtual void Cancel()
		{
			CloseSession(cancelTransaction: true);
		}


		/// <summary>
		/// End a Unit of Work. This usually commits the active transaction (or rolls back if it detects an error condition) and then closes the database session.
		/// </summary>
		public virtual void End()
		{
			CloseSession();
		}


		/// <summary>
		/// Enlists the specified database command in the active transaction.
		/// </summary>
		public virtual void Enlist(DbCommand command)
		{
			_transaction.Enlist(command);
		}


		/// <summary>
		/// Explicitly flushes the session. Usually there is no need to manually call this.
		/// </summary>
		public void Flush()
		{
			Session.Flush();
		}


		/// <summary>
		/// Explicitly commits the active transaction. Usually there is no need to manually call this.
		/// </summary>
		public virtual void Commit()
		{
			_transaction.Commit();
		}


		/// <summary>
		/// Explicitly rolls back the active transaction. Usually there is no need to manually call this.
		/// </summary>
		public virtual void Rollback()
		{
			_transaction.Rollback();
		}


		private ISession OpenSession()
		{
			if (_session == null) {
				_session = _sessionFactory.OpenSession();
				_transaction = Session.BeginTransaction();
			}
			return _session;
		}


		private void CloseSession(bool cancelTransaction = false)
		{
			if (_transaction != null && _transaction.IsActive) {
				if (cancelTransaction || (HttpContext.Current != null && HttpContext.Current.Error != null)) {
					Rollback();
				} else {
					Commit();
				}
			}

			if (_session != null) {
				if (_session.IsOpen) {
					_session.Close();
				}
				_session = null;
			}
		}


		private bool _disposed;

		private ISession _session;
		private ITransaction _transaction;

		private readonly ISessionFactory _sessionFactory;

	}

}

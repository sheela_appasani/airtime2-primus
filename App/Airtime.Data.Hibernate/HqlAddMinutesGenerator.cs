using System;
using System.Collections.ObjectModel;
using System.Linq.Expressions;
using System.Reflection;

using NHibernate.Hql.Ast;
using NHibernate.Linq;
using NHibernate.Linq.Functions;
using NHibernate.Linq.Visitors;


namespace Airtime.Data.Hibernate
{

	public class HqlAddMinutesGenerator : BaseHqlGeneratorForMethod
	{

		public HqlAddMinutesGenerator()
		{
			SupportedMethods = new[] { ReflectionHelper.GetMethodDefinition<DateTime?>(d => d.Value.AddMinutes(0)) };
		}


		public override HqlTreeNode BuildHql(MethodInfo method, Expression targetObject, ReadOnlyCollection<Expression> arguments, HqlTreeBuilder treeBuilder, IHqlExpressionVisitor visitor)
		{
			return treeBuilder.MethodCall("HqlAddMinutes",
			                              visitor.Visit(targetObject).AsExpression(),
			                              visitor.Visit(arguments[0]).AsExpression()
				);
		}

	}

}

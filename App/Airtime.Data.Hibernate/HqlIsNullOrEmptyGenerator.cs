using System;
using System.Collections.ObjectModel;
using System.Linq.Expressions;
using System.Reflection;

using NHibernate.Hql.Ast;
using NHibernate.Linq;
using NHibernate.Linq.Functions;
using NHibernate.Linq.Visitors;


namespace Airtime.Data.Hibernate
{

	public class HqlIsNullOrEmptyGenerator : BaseHqlGeneratorForMethod
	{

		public HqlIsNullOrEmptyGenerator()
		{
			// ReSharper disable ReturnValueOfPureMethodIsNotUsed
			SupportedMethods = new[] { ReflectionHelper.GetMethodDefinition(() => String.IsNullOrEmpty(null)) };
			// ReSharper restore ReturnValueOfPureMethodIsNotUsed
		}


		public override HqlTreeNode BuildHql(MethodInfo method, Expression targetObject, ReadOnlyCollection<Expression> arguments, HqlTreeBuilder treeBuilder, IHqlExpressionVisitor visitor)
		{
			return treeBuilder.BooleanMethodCall("HqlIsNullOrEmpty", new[] { visitor.Visit(arguments[0]).AsExpression() });
		}

	}

}

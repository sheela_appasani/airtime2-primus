using System;
using System.Diagnostics.Contracts;
using System.Linq;

namespace Airtime.Data.Hibernate
{

	[ContractClass(typeof(HibernateRepositoryContracts<,>))]
	public interface IHibernateRepository<TEntity, TId> : IDataRepository<TEntity, TId>
		where TEntity : class, IEntityWithTypedId<TId>
	{
		void Refresh(TEntity instance);
		void Evict(TEntity instance);
	}

}

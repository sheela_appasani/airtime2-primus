using System;

using Airtime.Data.Media;

using FluentNHibernate.Mapping;


namespace Airtime.Data.Hibernate.Media
{

	public class SongAttributeMap : ClassMap<SongAttribute>
	{

		public SongAttributeMap()
		{
			Table("xr_song_attribute");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			References(x => x.Song).Column("SongId").Cascade.None();
			References(x => x.Field).Column("FieldId").Cascade.None();
			Map(x => x.Value).Length(255);
		}

	}

}

using Airtime.Data.Media;


namespace Airtime.Data.Hibernate.Media
{

	public class SongAttributeRepository : HibernateRepository<SongAttribute, int>, ISongAttributeRepository
	{

		public SongAttributeRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

	}

}

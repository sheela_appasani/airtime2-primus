using System;

using Airtime.Data.Content.Fields;
using Airtime.Data.Media;

using FluentNHibernate.Mapping;

using NHibernate.Type;


namespace Airtime.Data.Hibernate.Media
{

	public class SongMap : ClassMap<Song>
	{

		public SongMap()
		{
			Table("xr_song");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			Map(x => x.Title).Length(255).Not.Nullable();
			Map(x => x.Artist).Length(255).Not.Nullable();
			Map(x => x.Year);
			Map(x => x.Genre).Length(255);
			Map(x => x.Tempo).Length(255);

			Map(x => x.Filename).Length(255).Not.Nullable();
			Map(x => x.Library).Length(50).Column("client").Not.Nullable().Default("GLOBAL");
			References(x => x.Company).Column("companyId").Cascade.None();

			Map(x => x.Length).Column("second").Not.Nullable();

			Map(x => x.Matched).CustomType<YesNoType>();
			Map(x => x.Confirmed).CustomType<YesNoType>();
			Map(x => x.InUse).CustomType<YesNoType>().Column("used");

			HasMany(x => x.Attributes)
				.Table("xr_song_attribute")
				.KeyColumn("SongId")
				.AsEntityMap("FieldId")
				.Element("Value")
				.Cascade.All();
		}

	}

}
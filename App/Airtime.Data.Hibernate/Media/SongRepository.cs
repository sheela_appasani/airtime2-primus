using System;
using System.IO;
using System.Linq;
using System.Web.Hosting;

using Airtime.Data.Media;

using NHibernate;

namespace Airtime.Data.Hibernate.Media
{

	public class SongRepository : HibernateRepository<Song, int>, ISongRepository
	{

		public SongRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}


		public override void Delete(Song song)
		{
			var filepath = HostingEnvironment.MapPath(song.Filename);
			base.Delete(song);
			if (!string.IsNullOrEmpty(filepath) && File.Exists(filepath)) {
				File.Delete(filepath);
			}
		}

	}

}
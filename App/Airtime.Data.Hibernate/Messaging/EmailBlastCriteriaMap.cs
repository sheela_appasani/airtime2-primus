using System;

using Airtime.Data.Messaging;

using FluentNHibernate.Mapping;

using NHibernate.Type;

namespace Airtime.Data.Hibernate.Messaging
{

	public class EmailBlastCriteriaMap : ClassMap<EmailBlastCriteria>
	{

		public EmailBlastCriteriaMap()
		{

			Table("xr_email_detail");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			HasOne(x => x.EmailBlast).Constrained();

			Map(x => x.TargetGroup).Column("ChannelEmailTarget");
			Map(x => x.TargetWinners).Column("RandomWinnersOption");

			Map(x => x.GenderMale).Column("TargetGenderMale").CustomType<YesNoType>();
			Map(x => x.GenderFemale).Column("TargetGenderFemale").CustomType<YesNoType>();

			Map(x => x.LowerAge).Column("TargetStartAge");
			Map(x => x.UpperAge).Column("TargetEndAge");

			Map(x => x.LowerBirthday).Column("TargetBirthdate");
			Map(x => x.UpperBirthday).Column("TargetBirthdateEnd");

			Map(x => x.Postcodes).Column("TargetPostcodes");

			Map(x => x.OptInSurveys).Column("TargetOptInSurveys").CustomType<YesNoType>();
			Map(x => x.OptInNewsletters).Column("TargetOptInNewsletters").CustomType<YesNoType>();

			Map(x => x.NumberOfMembers).Column("NumberOfWinners");
			Map(x => x.LowerCompletedSurveyDate).Column("CompletedStartDate");
			Map(x => x.UpperCompletedSurveyDate).Column("CompletedEndDate");

		}

	}

}
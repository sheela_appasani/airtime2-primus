using System;

using Airtime.Data.Messaging;

using FluentNHibernate.Mapping;

using NHibernate;
using NHibernate.Type;

namespace Airtime.Data.Hibernate.Messaging
{

	public class EmailBlastMap : ClassMap<EmailBlast>
	{

		public EmailBlastMap()
		{
			Table("xr_email");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			References(x => x.Channel).Column("channelId").Cascade.None();
			References(x => x.Survey).Column("surveyId").Cascade.None();

			Map(x => x.NoServerSideTemplate).Column("customTemplate").Not.Nullable().CustomType<YesNoType>();
			Map(x => x.Subject).Length(255);
			Map(x => x.Body);

			Map(x => x.Status).Not.Nullable();

			Map(x => x.EmailBlastType).Column("emailType").Not.Nullable();
		   

			Component(x => x.Schedule, m => {
				m.Map(x => x.StartDate).Column("sendAfter").Nullable();
				m.Map(x => x.EndDate).Column("sendFinish").Nullable();
			});

			Map(x => x.TargetPercentage);
			Map(x => x.RecipientSource);

			References(x => x.Criteria).Column("criteriaId").Unique().Cascade.All();

			HasMany(x => x.Emails)
				.KeyColumn("emailId")
				.Cascade.AllDeleteOrphan()
				.Inverse()
				.ExtraLazyLoad();	

			Component(x => x.Statistics, m => {
				m.Map(x => x.Scheduled).Column("statsScheduled").Not.Nullable();
				m.Map(x => x.Delivered).Column("statsDelivered").Not.Nullable();
				m.Map(x => x.Failed).Column("statsFailed").Not.Nullable();
				m.Map(x => x.Cancelled).Column("statsCancelled").Not.Nullable();
			});
		}

	}

}
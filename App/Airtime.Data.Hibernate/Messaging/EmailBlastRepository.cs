using System;
using System.Collections.Generic;
using System.Linq;

using Airtime.Data.Messaging;

using NHibernate;
using NHibernate.Linq;


namespace Airtime.Data.Hibernate.Messaging
{

	public class EmailBlastRepository : HibernateRepository<EmailBlast, int>, IEmailBlastRepository
	{

		public EmailBlastRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}


		public void Cancel(EmailBlast emailBlast)
		{
			if (emailBlast.Status == EmailBlastStatusEnum.Sent || emailBlast.Status == EmailBlastStatusEnum.Cancelled) {
				return;
			}
			
			Session.CreateQuery(@"
				update Email
				set Status = :cancelStatus
				where
					Status = :pendingStatus
					and EmailBlast = :emailBlast
				")
				.SetParameter("cancelStatus", EmailStatusEnum.Cancel)
				.SetParameter("pendingStatus", EmailStatusEnum.Pending)
				.SetParameter("emailBlast", emailBlast)
				.ExecuteUpdate();

			emailBlast.Status = EmailBlastStatusEnum.Cancelled;
			Save(emailBlast);
		}

	}

}
using System;

using Airtime.Data.Messaging;

using FluentNHibernate.Mapping;

namespace Airtime.Data.Hibernate.Messaging
{

	public class EmailMap : ClassMap<Email>
	{

		public EmailMap()
		{
			Table("xr_email_recipient");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			Map(x => x.Subject).Length(255);
			Map(x => x.Body);
			Map(x => x.Status).Column("deliveryStatus").Not.Nullable();

			References(x => x.Membership).Column("userId").Cascade.None();
			Map(x => x.ToAddress).Column("address").Not.Nullable();
			Map(x => x.FromAddress).Column("sender").Not.Nullable();

			References(x => x.EmailBlast).Column("emailId").Cascade.None();
		}

	}

}
using System;
using System.Linq;

using Airtime.Data.Messaging;

using NHibernate;

namespace Airtime.Data.Hibernate.Messaging
{

	public class EmailRepository : HibernateRepository<Email, int>, IEmailRepository
	{

		public EmailRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

	}

}
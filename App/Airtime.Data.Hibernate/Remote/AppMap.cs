using Airtime.Data.Remote;

using FluentNHibernate.Mapping;


namespace Airtime.Data.Hibernate.Remote
{

	public class AppMap : ClassMap<App>
	{

		public AppMap()
		{
			Table("xr_app");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			Map(x => x.Name).Length(255).Not.Nullable();
			Map(x => x.Code).Length(255).Not.Nullable();
			Map(x => x.AppStoreUrl).Length(1024);
			Map(x => x.PushMessage).Length(100);
			Map(x => x.Platform).Not.Nullable();

			HasManyToMany(x => x.Channels)
				.Table("xr_app_channel")
				.ParentKeyColumn("appId")
				.ChildKeyColumn("channelId")
				.Cascade.None();

			HasMany(x => x.DeviceRegistrations)
				.KeyColumn("appId")
				.Cascade.AllDeleteOrphan()
				.Inverse()
				.LazyLoad();
		}

	}

}

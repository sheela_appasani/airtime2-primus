using System;
using System.Linq;

using Airtime.Data.Remote;

using NHibernate;
using NHibernate.Linq;


namespace Airtime.Data.Hibernate.Remote
{

	public class AppRepository : HibernateRepository<App, int>, IAppRepository
	{

		public AppRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}


		public App FindByCode(string code)
		{
			return Items.Where(u => u.Code == code).Cacheable().SingleOrDefault();
		}

	}

}

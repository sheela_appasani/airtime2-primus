using Airtime.Data.Remote;

using FluentNHibernate;
using FluentNHibernate.Mapping;


namespace Airtime.Data.Hibernate.Remote
{

	public class DeviceRegistrationMap : ClassMap<DeviceRegistration>
	{

		public DeviceRegistrationMap()
		{
			Table("xr_device");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			References(x => x.App).Column("appId").Cascade.None();
			References(x => x.Person).Column("personId").Cascade.None();
			Map(x => x.NotificationToken).Column("deviceToken");

			Map(Reveal.Member<DeviceRegistration>("Email"));
		}

	}

}

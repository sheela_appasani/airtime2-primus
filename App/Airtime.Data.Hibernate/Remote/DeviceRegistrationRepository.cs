using System;
using System.Linq;

using Airtime.Data.Remote;

using NHibernate;
using NHibernate.Linq;


namespace Airtime.Data.Hibernate.Remote
{

	public class DeviceRegistrationRepository : HibernateRepository<DeviceRegistration, int>, IDeviceRegistrationRepository
	{

		public DeviceRegistrationRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

	}

}

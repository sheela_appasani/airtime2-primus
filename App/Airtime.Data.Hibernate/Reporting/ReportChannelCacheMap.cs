﻿using Airtime.Data.Reporting;
using FluentNHibernate.Mapping;

namespace Airtime.Data.Hibernate.Reporting
{
	class ReportChannelCacheMap : ClassMap<ReportChannelCache>
	{
		public ReportChannelCacheMap()
		{
			Table("xr_report_channel_cache");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			References(x => x.Channel).Column("channelId");
			Map(x => x.CacheUtcDate);

			HasMany(x => x.Stats)
				.KeyColumn("reportChannelCacheId")
				.Cascade.AllDeleteOrphan()
				.Inverse(); 

			Map(x => x.DemographicsTotal);
			Map(x => x.DemographicsTotalMale);
			Map(x => x.DemographicsTotalFemale);
			Map(x => x.DemographicsTotalUnknown);
			Map(x => x.DemographicsUnder20Male);
			Map(x => x.DemographicsUnder20Female);
			Map(x => x.DemographicsUnder20Unknown);
			Map(x => x.Demographics20_24Male);
			Map(x => x.Demographics20_24Female);
			Map(x => x.Demographics20_24Unknown);
			Map(x => x.Demographics25_29Male);
			Map(x => x.Demographics25_29Female);
			Map(x => x.Demographics25_29Unknown);
			Map(x => x.Demographics30_34Male);
			Map(x => x.Demographics30_34Female);
			Map(x => x.Demographics30_34Unknown);
			Map(x => x.Demographics35_39Male);
			Map(x => x.Demographics35_39Female);
			Map(x => x.Demographics35_39Unknown);
			Map(x => x.Demographics40_44Male);
			Map(x => x.Demographics40_44Female);
			Map(x => x.Demographics40_44Unknown);
			Map(x => x.DemographicsOver45Male);
			Map(x => x.DemographicsOver45Female);
			Map(x => x.DemographicsOver45Unknown);
		}
	}
}

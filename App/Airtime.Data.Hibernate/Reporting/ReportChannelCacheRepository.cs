﻿
using Airtime.Data.Reporting;

namespace Airtime.Data.Hibernate.Reporting
{
	public class ReportChannelCacheRepository : HibernateRepository<ReportChannelCache, int>, IReportChannelCacheRepository 
	{
		public ReportChannelCacheRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
		{
		}

	}
}

using Airtime.Data.Reporting;
using FluentNHibernate.Mapping;

namespace Airtime.Data.Hibernate.Reporting
{
	internal class ReportChannelCacheStatMap : ClassMap<ReportChannelCacheStat>
	{
		public ReportChannelCacheStatMap()
		{
			Table("xr_report_channel_cache_stat");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			References(x => x.ReportChannelCache).Column("reportChannelCacheId");
			Map(x => x.Report);
			Map(x => x.Count);
			Map(x => x.Total);
			Map(x => x.Label);
		}
	}
}
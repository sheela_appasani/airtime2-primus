﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Reflection;

using Airtime.Data.Client;
using Airtime.Data.Media;
using Airtime.Data.Account;
using Airtime.Data.Messaging;
using Airtime.Data.Reporting;

using NHibernate;
using NHibernate.Linq;
using NHibernate.Transform;

using NLog;


namespace Airtime.Data.Hibernate.Reporting
{

	public class ReportingQuery : HibernateQueryObject, IReportingQuery
	{

		public ReportingQuery(IUnitOfWork unitOfWork, IChannelRepository channelRepository)
			: base(unitOfWork)
		{
			ChannelRepository = channelRepository;
		}


		public IList GetChannelSummaryStationMost(Channel channel)
		{
			Contract.Requires(channel != null);

			return Session
				.GetNamedQuery("GetStationsMostSummary")
				.SetParameter("userId", null, NHibernateUtil.Int32)
				.SetEntity("channelId", channel)
				.List();
		}


		public IList GetChannelSummaryStationsCumed(Channel channel)
		{
			Contract.Requires(channel != null);

			return Session
				.GetNamedQuery("GetStationsListenSummary")
				.SetParameter("userId", null, NHibernateUtil.Int32)
				.SetEntity("channelId", channel)
				.List();
		}


		public IList GetChannelSummaryTimeSpentListening(Channel channel)
		{
			Contract.Requires(channel != null);

			return Session
				.GetNamedQuery("GetTimeSpentListeningSummary")
				.SetParameter("userId", null, NHibernateUtil.Int32)
				.SetEntity("channelId", channel)
				.List();
		}


		public IEnumerable<System.Tuple<PersonGenderEnum?, int>> GetDemographicCountQuery(Channel channel, DateRange birthdateRange)
		{
			Contract.Requires(channel != null);

			return ChannelRepository.Where(c => c == channel)
				.SelectMany(c => c.Memberships)
				.Where(u => u.Enabled && u.Role == MembershipRoleEnum.Respondent)
				.Where(
					u =>
					(!birthdateRange.From.HasValue || u.Person.Personal.DateOfBirth <= birthdateRange.From) &&
					(!birthdateRange.To.HasValue || u.Person.Personal.DateOfBirth > birthdateRange.To))
				.GroupBy(u => u.Person.Personal.Gender)
				.Select(g => new System.Tuple<PersonGenderEnum?, int>(g.Key, g.Count()))
				.ToFuture();
		}


		public IList<SongReport> GetMusicSummary(IList<int> surveyIds, IList<int> choiceIds = null, PersonGenderEnum? gender = null, int? lowerAge = null, int? upperAge = null)
		{
			Contract.Requires(surveyIds != null);
			Contract.Requires(surveyIds.Count > 0);

			if (lowerAge.HasValue && upperAge.HasValue && lowerAge.Value > upperAge.Value) {
				var tempAge = lowerAge;
				lowerAge = upperAge;
				upperAge = tempAge;
			}

			var query = Session
				.GetNamedQuery("GetMusicSummary")
				.SetParameter("surveyIds", String.Join(",", surveyIds))
				.SetParameter("choiceIds", choiceIds != null ? String.Join(",", choiceIds) : null)
				.SetParameter("gender", gender.HasValue ? gender.ToString() : null)
				.SetParameter("lowerAge", lowerAge)
				.SetParameter("upperAge", upperAge);

			return query.SetResultTransformer(Transformers.AliasToBean<SongReport>()).List<SongReport>();
		}


		public IList<Membership> GetMusicSummaryMembers(Song song, int lowerRating, int upperRating, IList<int> surveyIds, IList<int> choiceIds = null, PersonGenderEnum? gender = new PersonGenderEnum?(), int? lowerAge = new int?(), int? upperAge = new int?())
		{
			Contract.Requires(surveyIds != null);
			Contract.Requires(surveyIds.Count > 0);

			if (lowerAge.HasValue && upperAge.HasValue && lowerAge.Value > upperAge.Value) {
				var tempAge = lowerAge;
				lowerAge = upperAge;
				upperAge = tempAge;
			}

			return Session
				.GetNamedQuery("GetMusicSummaryMembers")
				.SetEntity("songId", song)
				.SetParameter("lowerRating", lowerRating)
				.SetParameter("upperRating", upperRating)
				.SetParameter("surveyIds", String.Join(",", surveyIds))
				.SetParameter("choiceIds", choiceIds != null ? String.Join(",", choiceIds) : null)
				.SetParameter("gender", gender.HasValue ? gender.ToString() : null)
				.SetParameter("lowerAge", lowerAge)
				.SetParameter("upperAge", upperAge)
				.List<Membership>();
		}


		public IList<OpenEndedReport> GetReportOpenEnded(IList<int> surveyIds, IList<int> choiceIds = null, PersonGenderEnum? gender = null, int? lowerAge = null, int? upperAge = null)
		{
			Contract.Requires(surveyIds != null);
			Contract.Requires(surveyIds.Count > 0);

			if (lowerAge.HasValue && upperAge.HasValue && lowerAge.Value > upperAge.Value) {
				var tempAge = lowerAge;
				lowerAge = upperAge;
				upperAge = tempAge;
			}

			var query = Session
				.GetNamedQuery("GetReportOpenEnded")
				.SetParameter("surveyIds", String.Join(",", surveyIds))
				.SetParameter("choiceIds", choiceIds != null ? String.Join(",", choiceIds) : null)
				.SetParameter("gender", gender.HasValue ? gender.ToString() : null)
				.SetParameter("lowerAge", lowerAge)
				.SetParameter("upperAge", upperAge);

			return query.SetResultTransformer(Transformers.AliasToBean<OpenEndedReport>()).List<OpenEndedReport>();
		}


		public IList<MultiChoiceReport> GetReportMultiChoice(IList<int> surveyIds, IList<int> choiceIds = null, PersonGenderEnum? gender = null, int? lowerAge = null, int? upperAge = null)
		{
			Contract.Requires(surveyIds != null);
			Contract.Requires(surveyIds.Count > 0);

			if (lowerAge.HasValue && upperAge.HasValue && lowerAge.Value > upperAge.Value) {
				var tempAge = lowerAge;
				lowerAge = upperAge;
				upperAge = tempAge;
			}

			var query = Session
				.GetNamedQuery("GetReportMultiChoice")
				.SetParameter("surveyIds", String.Join(",", surveyIds))
				.SetParameter("choiceIds", choiceIds != null ? String.Join(",", choiceIds) : null)
				.SetParameter("gender", gender.HasValue ? gender.ToString() : null)
				.SetParameter("lowerAge", lowerAge)
				.SetParameter("upperAge", upperAge);

			return query.SetResultTransformer(Transformers.AliasToBean<MultiChoiceReport>()).List<MultiChoiceReport>();
		}


		public int GetParticipationRateQuery(Channel channel, DateRange dateRange)
		{
			var query = Session.CreateSQLQuery(@"
					declare @lowerDate datetime = :lowerDate
					declare @upperDate datetime = :upperDate

					select
						sum(case when participation.complete > 0 then 1 else 0 end) as Count
					from (
						select u.Id, 
							sum(case when sr.completed = 'Y' then 1 else 0 end) as complete,
							sum(case when sr.completed = 'N' then 1 else 0 end) as incomplete
						from xr_user u
						inner join xr_user_channel uc on u.id = uc.userId and uc.channelId = :channelId
						inner join xr_survey_respondent sr on u.id = sr.userId 
						where
							u.role = 'Respondent'
							and u.optInSurveys = 'Y'
							and sr.completed in ('Y', 'N')
							and (@lowerDate is null or (@lowerDate is not null and sr.dateModified >= @lowerDate))
							and (@upperDate is null or (@upperDate is not null and sr.dateModified <= @upperDate))
						group by u.Id
					) as participation
				")
				.SetEntity("channelId", channel);

			query = dateRange.From.HasValue
				? query.SetDateTime("lowerDate", dateRange.From.Value)
				: query.SetParameter("lowerDate", null, NHibernateUtil.DateTime);

			query = dateRange.To.HasValue
				? query.SetDateTime("upperDate", dateRange.To.Value)
				: query.SetParameter("upperDate", null, NHibernateUtil.DateTime);

			return query.UniqueResult<int>();
		}


		public IList<LoyaltyReport> GetLoyaltyReport(List<Channel> channels, DateTime endFromDate, DateTime endToDate, int minimumInvited, int minimumCompleted, double minimumLoyalty)
		{
			Contract.Requires(channels != null);
			Contract.Requires(channels.Count > 0);

			var query = Session.CreateSQLQuery(@"
				declare @now datetime = GETDATE()

				select
					userStats.*,
					p.GivenName, p.FamilyName, p.DateOfBirth, dbo.CalculateAge(p.dateOfBirth, @now) as Age, p.Gender, p.DaytimePhone, p.Address, p.Suburb, p.City, p.Postcode
				from (
					select
						c.title as ChannelName,
						ROUND(CAST(SUM(userStats.completed) as float) / SUM(userStats.invited) * 100, 2) as Loyalty,
						SUM(userStats.invited) as InvitedCount,
						SUM(userStats.completed) as CompletedCount,
						SUM(userStats.iosCompleted) as IosCompletedCount,
						SUM(userStats.androidCompleted) as AndroidCompletedCount,
						MAX(userStats.dateCompleted) as LastSurveyCompleted,
						MIN(u.Id) as UserId, p.Email
					from (
						select
							sr.userId, sr.dateCreated,
							case when sr.completed = 'Y' or sr.invited = 'Y' then 1 else 0 end as invited,
							case when sr.completed = 'Y' then 1 else 0 end as completed,
							case when sr.usedApp = 'iOS' then 1 else 0 end as iosCompleted,
							case when sr.usedApp = 'Android' then 1 else 0 end as androidCompleted,
							case when sr.completed = 'Y' then sr.dateModified else null end as dateCompleted
						from xr_survey_respondent sr
						inner join xr_survey s on s.id = sr.surveyId and s.enddate between :endFromDate and :endToDate
						inner join xr_channel c on c.id = s.channelId and c.id in (:channelIds)
						where
							sr.invited = 'Y' or sr.completed = 'Y'
					) userStats
					inner join xr_user u on u.id = userStats.userId and u.isEnabled = 'Y'
					inner join xr_person p on p.id = u.personId
					inner join xr_user_channel uc on uc.userId = u.id
					inner join xr_channel c on c.id = uc.channelId and c.id in (:channelIds)
					group by c.title, p.email
					having
						ROUND(CAST(SUM(userStats.completed) as float) / SUM(userStats.invited) * 100, 2) >= :minimumLoyalty
						and SUM(userStats.invited) >= :minimumInvited
						and SUM(userStats.completed) >= :minimumCompleted
				) userStats
				inner join xr_user u on u.id = userStats.userId
				inner join xr_person p on p.id = u.personId
				order by
					userStats.ChannelName,
					userStats.Loyalty desc, userStats.CompletedCount desc, userStats.InvitedCount desc,
					userStats.Email
			")
				.SetParameterList("channelIds", channels)
				.SetDateTime("endFromDate", endFromDate)
				.SetDateTime("endToDate", endToDate)
				.SetDouble("minimumLoyalty", minimumLoyalty)
				.SetInt32("minimumInvited", minimumInvited)
				.SetInt32("minimumCompleted", minimumCompleted);

			return query
				.SetResultTransformer(Transformers.AliasToBean<LoyaltyReport>())
				.List<LoyaltyReport>();
		}


		public IList<EmailBlastOverview> GetEmailBlastOverviews()
		{
			var results = Session
				.GetNamedQuery("GetEmailBlastOverviews")
				.SetResultTransformer(Transformers.AliasToEntityMap)
				.List<Hashtable>();

			return results
				.Select(x => new EmailBlastOverview {
					EmailId = (int)x["EmailId"],
					SurveyId = (int?)x["SurveyId"],
					ChannelId = (int?)x["ChannelId"],
					Source = (string)x["Source"],
					Subject = (string)x["Subject"],
					Status = (EmailBlastStatusEnum)x["Status"],
					DateCreated = DateTime.SpecifyKind((DateTime)x["DateCreated"], DateTimeKind.Local),
					Schedule = {
						StartDate = x["StartDate"] != null ? DateTime.SpecifyKind(((DateTime?)x["StartDate"]).Value, DateTimeKind.Utc) : (DateTime?)null,
						EndDate = x["EndDate"] != null ? DateTime.SpecifyKind(((DateTime?)x["EndDate"]).Value, DateTimeKind.Utc) : (DateTime?)null
					},
					Statistics = {
						PopulatedTime = x["PopulatedTime"] != null ? DateTime.SpecifyKind(((DateTime?)x["PopulatedTime"]).Value, DateTimeKind.Local) : (DateTime?)null,
						FirstDeliveredTime = x["FirstDeliveredTime"] != null ? DateTime.SpecifyKind(((DateTime?)x["FirstDeliveredTime"]).Value, DateTimeKind.Local) : (DateTime?)null,
						LastDeliveredTime = x["LastDeliveredTime"] != null ? DateTime.SpecifyKind(((DateTime?)x["LastDeliveredTime"]).Value, DateTimeKind.Local) : (DateTime?)null,
						Scheduled = (int)x["Scheduled"],
						Delivered = (int)x["Delivered"],
						Failed = (int)x["Failed"],
						Cancelled = (int)x["Cancelled"]
					}
				})
				.ToList();
		}


		protected IChannelRepository ChannelRepository { get; set; }

	}

}
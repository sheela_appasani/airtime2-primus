using System;

using Airtime.Data.Server;

using FluentNHibernate.Mapping;


namespace Airtime.Data.Hibernate.Server
{

	public class RequestLogMap : ClassMap<RequestLog>
	{

		public RequestLogMap()
		{
			Table("xr_log");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			References(x => x.Membership).Column("userId").Cascade.None();

			Map(x => x.Username).Length(255);
			Map(x => x.IpAddress).Length(64);
			Map(x => x.Url).Length(1024);
			Map(x => x.ClassName).Length(255);
			Map(x => x.RenderTime);
			Map(x => x.HttpUserAgent).Length(1024);
			Map(x => x.HttpReferrer).Length(1024).Column("httpReferer");
			Map(x => x.HostCode);
		}

	}

}

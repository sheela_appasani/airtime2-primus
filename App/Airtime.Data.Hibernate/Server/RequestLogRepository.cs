using System;
using System.Linq;

using Airtime.Data.Server;


namespace Airtime.Data.Hibernate.Server
{

	public class RequestLogRepository : HibernateRepository<RequestLog, int>, IRequestLogRepository
	{

		public RequestLogRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

	}

}

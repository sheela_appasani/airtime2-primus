using Airtime.Data.Surveying.Actions;

using FluentNHibernate.Mapping;


namespace Airtime.Data.Hibernate.Surveying.Actions
{

	public class ActionBaseMap : ClassMap<ActionBase>
	{

		public ActionBaseMap()
		{
			Table("xr_question_action");
			DiscriminateSubClassesOnColumn("ActionType").AlwaysSelectWithValue();
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			References(x => x.Question).Column("QuestionId").Cascade.None();

		}

	}

}

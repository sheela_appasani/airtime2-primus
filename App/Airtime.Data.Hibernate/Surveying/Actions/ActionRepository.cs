using Airtime.Data.Surveying.Actions;


namespace Airtime.Data.Hibernate.Surveying.Actions
{

	public class ActionRepository : HibernateRepository<ActionBase, int>, IActionRepository
	{

		public ActionRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

	}

}

﻿using Airtime.Data.Surveying.Actions;

using FluentNHibernate.Mapping;


namespace Airtime.Data.Hibernate.Surveying.Actions
{

	public class GoToActionMap : SubclassMap<GoToAction>
	{

		public GoToActionMap()
		{
			DiscriminatorValue(ActionTypeEnum.GoTo.ToString());

			References(x => x.GoToQuestion).Column("GoToQuestionId");
		}

	}

}

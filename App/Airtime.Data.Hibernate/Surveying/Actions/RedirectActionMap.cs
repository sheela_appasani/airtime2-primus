﻿using Airtime.Data.Surveying.Actions;

using FluentNHibernate.Mapping;


namespace Airtime.Data.Hibernate.Surveying.Actions
{
	
	public class RedirectActionMap : SubclassMap<RedirectAction>
	{

		public RedirectActionMap()
		{
			DiscriminatorValue(ActionTypeEnum.Redirect.ToString());

			Map(x => x.RedirectUrl);
		}

	}

}

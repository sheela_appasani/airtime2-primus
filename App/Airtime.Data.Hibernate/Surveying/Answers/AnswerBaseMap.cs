using Airtime.Data.Surveying.Answers;
using FluentNHibernate.Mapping;
using NHibernate.Type;

namespace Airtime.Data.Hibernate.Surveying.Answers
{

	public class AnswerBaseMap : ClassMap<AnswerBase>
	{

		public AnswerBaseMap()
		{
			Table("xr_question_answer");
			DiscriminateSubClassesOnColumn("answertype");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			Map(x => x.GeneratedReport).Column("generatedReport").Not.Nullable().CustomType<YesNoType>();
			Map(x => x.IsProfileAnswer).Column("isProfileAnswer").Not.Nullable().CustomType<YesNoType>();
			Map(x => x.AnswerType).Column("answertype").ReadOnly();

			References(x => x.Membership).Column("userId").Cascade.None();
			References(x => x.RespondentState).Column("surveyRespondentId").Cascade.None();
			References(x => x.Question).Column("questionId").Cascade.None();
		}

	}

}
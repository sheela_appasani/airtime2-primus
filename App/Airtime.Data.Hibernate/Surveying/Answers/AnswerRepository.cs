using Airtime.Data.Surveying.Answers;
using NHibernate;
using System.Collections;
using Airtime.Data.Client;

namespace Airtime.Data.Hibernate.Surveying.Answers
{

	public class AnswerRepository : HibernateRepository<AnswerBase, int>, IAnswerRepository
	{

		public AnswerRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

	}

}
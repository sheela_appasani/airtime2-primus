﻿using Airtime.Data.Surveying.Answers;
using FluentNHibernate.Mapping;

namespace Airtime.Data.Hibernate.Surveying.Answers
{
	public class PlaylistAnswerMap : SubclassMap<PlaylistAnswer>
	{
		public PlaylistAnswerMap()
		{
			DiscriminatorValue(AnswerTypeEnum.AudioPod.ToString());
			DynamicUpdate();

			HasMany(x => x.SongScores)
				.KeyColumn("answerId")
				.Cascade.AllDeleteOrphan()
				.Inverse()
				.LazyLoad();
		}
	}
}
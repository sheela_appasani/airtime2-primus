﻿using Airtime.Data.Surveying.Answers;

using FluentNHibernate.Mapping;


namespace Airtime.Data.Hibernate.Surveying.Answers
{
	
	public class SingleAnswerMap : SubclassMap<SingleAnswer>
	{

		public SingleAnswerMap()
		{
			DiscriminatorValue(AnswerTypeEnum.Common.ToString());
			DynamicUpdate();

			Map(x => x.Text).Column("answer");
			References(x => x.Choice).Column("responseId");
		}

	}

}

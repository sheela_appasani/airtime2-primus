using Airtime.Data.Surveying.Answers;

using FluentNHibernate.Mapping;

namespace Airtime.Data.Hibernate.Surveying.Answers
{

	public class SongScoreMap : ClassMap<SongScore>
	{

		public SongScoreMap()
		{
			Table("xr_audiopod_answer_rates");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			References(x => x.Song).Column("songId").Cascade.None();
			References(x => x.PlaylistAnswer).Column("answerId").Cascade.None();

			Map(x => x.Burn).Column("burn").Not.Nullable();
			Map(x => x.Ratings).Column("rates").Not.Nullable();
			Map(x => x.FinalRating).Column("finalRating").Not.Nullable();
		}

	}

}

using Airtime.Data.Surveying.Answers;


namespace Airtime.Data.Hibernate.Surveying.Answers
{

	public class SongScoreRepository : HibernateRepository<SongScore, int>, ISongScoreRepository
	{

		public SongScoreRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

	}

}

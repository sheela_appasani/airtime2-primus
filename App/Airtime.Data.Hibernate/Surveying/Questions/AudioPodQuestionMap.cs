﻿using Airtime.Data.Surveying.Questions;

using FluentNHibernate.Mapping;

using NHibernate.Type;


namespace Airtime.Data.Hibernate.Surveying.Questions
{

	public class AudioPodQuestionMap : SubclassMap<AudioPodQuestion>
	{

		public AudioPodQuestionMap()
		{
			DiscriminatorValue(QuestionTypeEnum.AUDIOPOD.ToString());
			DynamicUpdate();

			HasManyToMany(x => x.Songs)
				.Table("xr_question_song")
				.ParentKeyColumn("questionId")
				.ChildKeyColumn("songId")
				.OrderBy("ordering")
				.AsList(index => index.Column("ordering"))
				.Cascade.None();

			Component(x => x.BurnButtons, m => {
				m.Map(x => x.NotTiredText).Column("notTiredText").Length(25);
				m.Map(x => x.LittleTiredText).Column("tiredText").Length(25);
				m.Map(x => x.VeryTiredText).Column("veryTiredText").Length(25);
				m.Map(x => x.UnfamiliarText).Column("unfamiliarText").Length(25);
				m.Map(x => x.ShowNotTired).Column("showNotTired").CustomType<YesNoType>();
				m.Map(x => x.ShowLittleTired).Column("showTired").CustomType<YesNoType>();
				m.Map(x => x.ShowVeryTired).Column("showVeryTired").CustomType<YesNoType>();
				m.Map(x => x.ShowUnfamiliar).Column("showUnfamiliar").CustomType<YesNoType>();
			});
		}

	}

}

﻿using Airtime.Data.Surveying.Questions;

using FluentNHibernate.Mapping;


namespace Airtime.Data.Hibernate.Surveying.Questions
{

	public class BreakQuestionMap : SubclassMap<BreakQuestion>
	{

		public BreakQuestionMap()
		{
			DiscriminatorValue(QuestionTypeEnum.BREAK.ToString());
			DynamicUpdate();
		}

	}

}

﻿using Airtime.Data.Surveying.Questions;

using FluentNHibernate.Mapping;


namespace Airtime.Data.Hibernate.Surveying.Questions
{

	public class MultiChoiceQuestionMap : SubclassMap<MultiChoiceQuestion>
	{

		public MultiChoiceQuestionMap()
		{
			DiscriminatorValue("MULTICHOICE");
			DynamicUpdate();

			Map(x => x.ReportChartType).Column("ReportType");

			HasMany(x => x.Choices)
				.KeyColumn("questionId")
				.AsList(index => index.Column("ordering"))
				.OrderBy("ordering")
				.Cascade.AllDeleteOrphan()
				.Inverse()
				.LazyLoad();
		}

	}

}

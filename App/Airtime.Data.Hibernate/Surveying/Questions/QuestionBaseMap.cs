using Airtime.Data.Surveying.Questions;

using FluentNHibernate.Mapping;

using NHibernate.Type;


namespace Airtime.Data.Hibernate.Surveying.Questions
{

	public class QuestionBaseMap : ClassMap<QuestionBase>
	{

		public QuestionBaseMap()
		{
			Table("xr_question");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			Map(x => x.Name).Length(255).Not.Nullable();
			Map(x => x.Text).Not.Nullable();
			Map(x => x.Position).Column("ordering");
			Map(x => x.Required).Column("IsRequired").Not.Nullable().CustomType<YesNoType>();
			Map(x => x.Progression).Column("answeringMode");
			Map(x => x.Category).Column("questionCategory");
			Map(x => x.QuestionType).Column("QuestionType");

			References(x => x.Survey).Column("surveyId").Cascade.None();
			References(x => x.Channel).Column("channelId").Cascade.None();
			References(x => x.TemplateQuestion).Column("templateQuestionId").Cascade.None();

			References(x => x.Song).Column("songId").Cascade.None();

			HasMany(x => x.PostActions)
				.KeyColumn("QuestionId")
				.AsList(index => index.Column("Position"))
				.OrderBy("Position")
				.Cascade.AllDeleteOrphan()
				.Inverse()
				.LazyLoad();

			DiscriminateSubClassesOnColumn("QuestionType").Formula(
				@"case 
					when QuestionType = '" + QuestionTypeEnum.RADIO + @"' then 'MULTICHOICE'
					when QuestionType = '" + QuestionTypeEnum.AUDIO + @"' then 'MULTICHOICE'
					when QuestionType = '" + QuestionTypeEnum.CHECKBOX + @"' then 'MULTICHOICE'
					when QuestionType = '" + QuestionTypeEnum.CUSTOMRADIO + @"' then 'MULTICHOICE'
					when QuestionType = '" + QuestionTypeEnum.CUSTOMCHECKBOX + @"' then 'MULTICHOICE'
					when QuestionType = '" + QuestionTypeEnum.CUSTOMTEXT + @"' then '" + QuestionTypeEnum.TEXT + @"'
					else QuestionType
				end");
		}

	}

}

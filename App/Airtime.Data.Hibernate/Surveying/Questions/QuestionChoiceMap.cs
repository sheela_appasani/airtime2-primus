using Airtime.Data.Surveying.Questions;

using FluentNHibernate.Mapping;

namespace Airtime.Data.Hibernate.Surveying.Questions
{

	public class QuestionChoiceMap : ClassMap<QuestionChoice>
	{

		public QuestionChoiceMap()
		{
			Table("xr_response");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			Map(x => x.Name).Length(255).Not.Nullable();
			Map(x => x.Code).Length(255).Not.Nullable();
			Map(x => x.Position).Column("ordering");

			References(x => x.Question).Column("questionId");
			References(x => x.BranchQuestion).Column("BranchQuestionId");
			References(x => x.TemplateChoice).Column("templateResponseId");
		}

	}

}
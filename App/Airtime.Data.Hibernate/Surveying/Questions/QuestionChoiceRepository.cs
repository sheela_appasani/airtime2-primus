using Airtime.Data.Surveying.Questions;
using NHibernate;


namespace Airtime.Data.Hibernate.Surveying.Questions
{

	public class QuestionChoiceRepository : HibernateRepository<QuestionChoice, int>, IQuestionChoiceRepository
	{

		public QuestionChoiceRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

	}

}

using Airtime.Data.Surveying.Questions;
using NHibernate;

namespace Airtime.Data.Hibernate.Surveying.Questions
{

	public class QuestionRepository : HibernateRepository<QuestionBase, int>, IQuestionRepository
	{

		public QuestionRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

	}

}
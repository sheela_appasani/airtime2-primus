﻿using Airtime.Data.Surveying.Questions;
using FluentNHibernate.Mapping;

namespace Airtime.Data.Hibernate.Surveying.Questions
{
	public class ScaleQuestionMap : SubclassMap<ScaleQuestion>
	{
		public ScaleQuestionMap()
		{
			DiscriminatorValue(QuestionTypeEnum.SCALE.ToString());
			DynamicUpdate();
		}
	}
}
﻿using Airtime.Data.Surveying.Questions;
using FluentNHibernate.Mapping;

namespace Airtime.Data.Hibernate.Surveying.Questions
{
	public class TextQuestionMap : SubclassMap<TextQuestion>
	{
		public TextQuestionMap()
		{
			DiscriminatorValue(QuestionTypeEnum.TEXT.ToString());
			DynamicUpdate();
		}
	}
}
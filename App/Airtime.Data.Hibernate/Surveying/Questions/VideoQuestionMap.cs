﻿using Airtime.Data.Surveying.Questions;
using FluentNHibernate.Mapping;

namespace Airtime.Data.Hibernate.Surveying.Questions
{
	public class VideoQuestionMap : SubclassMap<VideoQuestion>
	{
		public VideoQuestionMap()
		{
			DiscriminatorValue(QuestionTypeEnum.VIDEO.ToString());
			DynamicUpdate();

			HasMany(x => x.Choices)
				.KeyColumn("questionId")
				.OrderBy("ordering")
				.Cascade.AllDeleteOrphan()
				.Inverse()
				.LazyLoad();
		}
	}
}
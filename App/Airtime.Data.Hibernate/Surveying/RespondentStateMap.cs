using Airtime.Data.Surveying;

using FluentNHibernate.Mapping;

using NHibernate.Type;


namespace Airtime.Data.Hibernate.Surveying
{

	public class RespondentStateMap : ClassMap<RespondentState>
	{

		public RespondentStateMap()
		{
			Table("xr_survey_respondent");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			Map(x => x.Guid).Column("Token").Not.Nullable();

			References(x => x.Membership).Column("userId").Cascade.None();
			References(x => x.Survey).Column("surveyId").Cascade.None();

			Map(x => x.Enabled).Column("IsEnabled").CustomType<YesNoType>();
			Map(x => x.Invited).CustomType<YesNoType>();
			Map(x => x.Registered).CustomType<YesNoType>();
			Map(x => x.Completed).CustomType<YesNoType>();
			Map(x => x.PaymentDetailConfirmed).CustomType<YesNoType>();

			References(x => x.CurrentQuestion).Column("currentQuestionId").Cascade.None();

			HasMany(x => x.SurveyAnswers)
				.KeyColumn("surveyRespondentId")
				.Where("isProfileAnswer = 'N'")
				.Cascade.AllDeleteOrphan()
				.Inverse()
				.LazyLoad();

			HasMany(x => x.ProfileAnswers)
				.KeyColumn("surveyRespondentId")
				.Where("isProfileAnswer = 'Y'")
				.Cascade.AllDeleteOrphan()
				.Inverse()
				.LazyLoad();

			Map(x => x.UsedPlatform).Column("usedApp");
		}

	}

}

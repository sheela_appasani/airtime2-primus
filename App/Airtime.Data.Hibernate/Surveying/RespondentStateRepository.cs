using System;
using System.Linq;
using System.Linq.Expressions;

using Airtime.Data.Account;
using Airtime.Data.Surveying;
using NHibernate;
using NHibernate.Linq;


namespace Airtime.Data.Hibernate.Surveying
{

	public class RespondentStateRepository : HibernateRepository<RespondentState, int>, IRespondentStateRepository
	{

		public RespondentStateRepository(IUnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}


		public RespondentState FindByUserAndSurvey(Membership membership, Survey survey)
		{
			return Items.Where(state => state.Membership == membership && state.Survey == survey).OrderBy(state => state.Id).Cacheable().FirstOrDefault();
		}

	}

}

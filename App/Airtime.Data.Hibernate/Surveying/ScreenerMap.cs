using Airtime.Data.Surveying;
using FluentNHibernate.Mapping;
using NHibernate.Type;

namespace Airtime.Data.Hibernate.Surveying
{

	public class ScreenerMap : ClassMap<Screener>
	{

		public ScreenerMap()
		{
			Table("xr_screener");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			Map(x => x.IncludeGivenName).Column("includeFirstName").Not.Nullable().CustomType<YesNoType>();
			Map(x => x.IncludeFamilyName).Column("includeLastName").Not.Nullable().CustomType<YesNoType>();
			Map(x => x.IncludeEmail).Not.Nullable().CustomType<YesNoType>();
			Map(x => x.IncludeDateOfBirth).Not.Nullable().CustomType<YesNoType>();
			Map(x => x.IncludeGender).Not.Nullable().CustomType<YesNoType>();
			Map(x => x.IncludeDaytimePhone).Not.Nullable().CustomType<YesNoType>();
			Map(x => x.IncludeMobilePhone).Not.Nullable().CustomType<YesNoType>();
			Map(x => x.IncludeStreetAddress).Not.Nullable().CustomType<YesNoType>();
			Map(x => x.IncludeSuburb).Not.Nullable().CustomType<YesNoType>();
			Map(x => x.IncludeCity).Not.Nullable().CustomType<YesNoType>();
			Map(x => x.IncludeState).Not.Nullable().CustomType<YesNoType>();
			Map(x => x.IncludePostalCode).Not.Nullable().CustomType<YesNoType>();
			Map(x => x.IncludeCountry).Not.Nullable().CustomType<YesNoType>();
		}

	}

}
using Airtime.Data.Surveying;
using FluentNHibernate.Mapping;
using NHibernate.Type;

namespace Airtime.Data.Hibernate.Surveying
{

	public class SurveyAlertMap : ClassMap<SurveyAlert>
	{

		public SurveyAlertMap()
		{
			Table("xr_survey_alert");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			References(x => x.Survey).Column("surveyId").Cascade.None();
			Map(x => x.Sent).CustomType<YesNoType>();

			Map(x => x.Gender).Nullable();
			Map(x => x.LowerAge).Nullable();
			Map(x => x.UpperAge).Nullable();
			Map(x => x.SampleSize);
		}

	}

}
using Airtime.Data.Surveying;
using FluentNHibernate.Mapping;
using NHibernate.Type;

namespace Airtime.Data.Hibernate.Surveying
{

	public class SurveyMap : ClassMap<Survey>
	{

		public SurveyMap()
		{
			Table("xr_survey");
			Where("surveyType = 'Normal'");
			DynamicUpdate();

			Id(x => x.Id).GeneratedBy.Identity();
			Map(x => x.DateCreated).ReadOnly();
			Version(x => x.DateModified);

			Map(x => x.Name).Length(255).Not.Nullable();

			Map(x => x.Archived).Column("IsArchived").Not.Nullable().CustomType<YesNoType>();
			Map(x => x.Deleted).Column("IsDeleted").Not.Nullable();

			References(x => x.Language).Column("languageId").Cascade.None();
			References(x => x.Company).Column("companyId").Cascade.None();
			References(x => x.Channel).Column("channelId").Cascade.None();

			HasMany(x => x.Alerts)
				.KeyColumn("surveyId")
				.Cascade.AllDeleteOrphan()
				.Inverse()
				.LazyLoad();

			HasMany(x => x.RespondentStates)
				.KeyColumn("surveyId")
				.Cascade.AllDeleteOrphan()
				.Inverse()
				.LazyLoad();

			HasMany(x => x.Questions)
				.KeyColumn("surveyId")
				.Where("channelId is null and surveyId is not null")
				.AsList(index => index.Column("ordering"))
				.OrderBy("ordering")
				.Cascade.AllDeleteOrphan()
				.Inverse()
				.LazyLoad();

			HasMany(x => x.ProfileQuestions)
				.KeyColumn("surveyId")
				.Where("channelId is not null and surveyId is not null")
				.AsList(index => index.Column("ordering"))
				.OrderBy("ordering")
				.Cascade.AllDeleteOrphan()
				.Inverse()
				.LazyLoad();

			HasMany(x => x.EmailBlasts)
				.KeyColumn("surveyId")
				.Cascade.AllDeleteOrphan()
				.Inverse()
				.LazyLoad();

			HasMany(x => x.Contents)
				.KeyColumn("surveyId")
				.Cascade.AllDeleteOrphan()
				.Inverse()
				.LazyLoad();

			Component(x => x.Schedule, m => {
				m.Map(x => x.TimeZone).Length(32).Not.Nullable();
				m.Map(x => x.StartDate).Not.Nullable();
				m.Map(x => x.EndDate).Not.Nullable();
				m.Map(x => x.ClientViewDate).Not.Nullable();
				m.Map(x => x.RegistrationCutOffDate).Not.Nullable();
			});

			Component(x => x.Statistics, m => {
				m.Map(x => x.Invited).Not.Nullable();
				m.Map(x => x.Registered).Not.Nullable();
				m.Map(x => x.Completed).Not.Nullable();
				m.Map(x => x.IosCompleted).Not.Nullable();
				m.Map(x => x.AndroidCompleted).Not.Nullable();
			});

			Component(x => x.BurnButtons, m => {
				m.Map(x => x.NotTiredText).Column("podNotTiredText").Length(25).Not.Nullable();
				m.Map(x => x.LittleTiredText).Column("podTiredText").Length(25).Not.Nullable();
				m.Map(x => x.VeryTiredText).Column("podVeryTiredText").Length(25).Not.Nullable();
				m.Map(x => x.UnfamiliarText).Column("podUnfamiliarText").Length(25).Not.Nullable();
				m.Map(x => x.ShowNotTired).Column("podShowNotTired").CustomType<YesNoType>();
				m.Map(x => x.ShowLittleTired).Column("podShowTired").CustomType<YesNoType>();
				m.Map(x => x.ShowVeryTired).Column("podShowVeryTired").CustomType<YesNoType>();
				m.Map(x => x.ShowUnfamiliar).Column("podShowUnfamiliar").CustomType<YesNoType>();
			});

			Component(x => x.Options, m => {
				m.Map(x => x.RandomiseSongLists).Column("randomisePodHooks").Not.Nullable().CustomType<YesNoType>();
				m.Map(x => x.ShowSongSummaryAtEnd).Column("UseSongSummary").Not.Nullable().CustomType<YesNoType>();
				m.Map(x => x.EnableChat).Column("useChat").Not.Nullable().CustomType<YesNoType>();
				m.Map(x => x.EnableBurnButtons).Column("useBurnButton").Not.Nullable().CustomType<YesNoType>();
				m.Map(x => x.EnableSystemCheck).Column("useSystemCheck").Not.Nullable().CustomType<YesNoType>();
				m.Map(x => x.EnableSampleQuestions).Column("useSampleQuestions").Not.Nullable().CustomType<YesNoType>();
				m.Map(x => x.EnableProgressBar).Column("UseProgressBar").Not.Nullable().CustomType<YesNoType>();
				m.Map(x => x.EnableExternalRecruits).Column("useExternalRecruits").Not.Nullable().CustomType<YesNoType>();
				m.Map(x => x.MaxIdleSongScores).Column("podMaxIdleHooks").Not.Nullable();
			});

			Component(x => x.NotificationStates, m => {
				m.Map(x => x.NotifiedAndroid).Column("notifiedAndroid").Not.Nullable().CustomType<YesNoType>();
				m.Map(x => x.NotifiedIos).Column("notificationsSent").Not.Nullable().CustomType<YesNoType>();
			});

		}

	}

}
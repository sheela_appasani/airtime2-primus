using System;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;

using Airtime.Data.Messaging;
using Airtime.Data.Surveying;
using NHibernate;
using NHibernate.Linq;

namespace Airtime.Data.Hibernate.Surveying
{

	public class SurveyRepository : HibernateRepository<Survey, int>, ISurveyRepository
	{

		public override Expression Expression
		{
			get { return Session.Query<Survey>().Where(s => s.Deleted == 0).Expression; }
		}


		public SurveyRepository(IUnitOfWork unitOfWork, IEmailBlastRepository emailBlastRepository)
			: base(unitOfWork)
		{
			EmailBlastRepository = emailBlastRepository;
		}


		public override void Delete(Survey instance)
		{
			foreach (var emailBlast in instance.EmailBlasts) {
				EmailBlastRepository.Cancel(emailBlast);
			}
			instance.Deleted = instance.Id;
			Save(instance);
		}


		public void Archive(Survey survey)
		{
			survey.Archived = true;

			foreach (var emailBlast in survey.EmailBlasts) {
				EmailBlastRepository.Cancel(emailBlast);
			}

			Save(survey);
		}


		private IEmailBlastRepository EmailBlastRepository { get; set; }

	}

}
﻿using System;

using NHibernate.Linq.Functions;


namespace Airtime.Data.Hibernate
{

	public class XtraLinqToHqlGeneratorsRegistry : DefaultLinqToHqlGeneratorsRegistry
	{

		public XtraLinqToHqlGeneratorsRegistry()
		{
			this.Merge(new HqlIsNullOrEmptyGenerator());
			this.Merge(new HqlAddYearsGenerator());
			this.Merge(new HqlAddMonthsGenerator());
			this.Merge(new HqlAddDaysGenerator());
			this.Merge(new HqlAddHoursGenerator());
			this.Merge(new HqlAddMinutesGenerator());
			this.Merge(new HqlAddSecondsGenerator());
			this.Merge(new HqlAddMillisecondsGenerator());
		}

	}

}

﻿using System;

using NHibernate;
using NHibernate.Dialect;
using NHibernate.Dialect.Function;


namespace Airtime.Data.Hibernate
{

	public class XtraMsSql2008Dialect : MsSql2008Dialect
	{

		public XtraMsSql2008Dialect()
		{
			RegisterFunction("HqlIsNullOrEmpty", new SQLFunctionTemplate(NHibernateUtil.Boolean, "(?1 is null or LEN(?1) = 0)"));
			RegisterFunction("HqlAddYears", new SQLFunctionTemplate(NHibernateUtil.DateTime, "DATEADD(YEAR, ?2, ?1)"));
			RegisterFunction("HqlAddMonths", new SQLFunctionTemplate(NHibernateUtil.DateTime, "DATEADD(MONTH, ?2, ?1)"));
			RegisterFunction("HqlAddDays", new SQLFunctionTemplate(NHibernateUtil.DateTime, "DATEADD(DAY, ?2, ?1)"));
			RegisterFunction("HqlAddHours", new SQLFunctionTemplate(NHibernateUtil.DateTime, "DATEADD(HOUR, ?2, ?1)"));
			RegisterFunction("HqlAddMinutes", new SQLFunctionTemplate(NHibernateUtil.DateTime, "DATEADD(MINUTE, ?2, ?1)"));
			RegisterFunction("HqlAddSeconds", new SQLFunctionTemplate(NHibernateUtil.DateTime, "DATEADD(SECOND, ?2, ?1)"));
			RegisterFunction("HqlAddMilliseconds", new SQLFunctionTemplate(NHibernateUtil.DateTime, "DATEADD(MILLISECOND, ?2, ?1)"));
		}

	}

}

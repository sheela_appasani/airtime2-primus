﻿namespace Airtime.Data.Account
{

	public static class ExtendMembership
	{
	
		/// <summary>
		/// Returns true if the current Membership has administrative privileges and should have access to everything (is in the Administrator role or Developer role).
		/// </summary>
		/// <returns>Returns true if the current Membership is in the Administrator role or the Developer role.</returns>
		public static bool IsAdmin(this Membership self)
		{
			return self != null && (self.Role == MembershipRoleEnum.Administrator || self.Role == MembershipRoleEnum.Developer);
		}


		/// <summary>
		/// Returns true if the current Membership has developer privileges and should have completely unrestricted access to everything (is in the Administrator role or Developer role).
		/// </summary>
		/// <returns>Returns true if the current Membership is in the Developer role.</returns>
		public static bool IsDeveloper(this Membership self)
		{
			return self != null && self.Role == MembershipRoleEnum.Developer;
		}

	}

}

﻿using System.Linq;


namespace Airtime.Data.Account
{

	public static class ExtendPerson
	{

		/// <summary>
		/// Returns true if the person has any memberships with administrative privileges
		/// </summary>
		/// <returns>Returns true if the person has any memberships with the Administrator role or the Developer role.</returns>
		public static bool HasAdminMembership(this Person self)
		{
			return self != null && self.Memberships.Any(x => x.IsAdmin());
		}


		/// <summary>
		/// Returns true if the person has any memberships with developer privileges
		/// </summary>
		/// <returns>Returns true if the person has any memberships with the Developer role.</returns>
		public static bool HasDeveloperMembership(this Person self)
		{
			return self != null && self.Memberships.Any(x => x.IsDeveloper());
		}

	}

}

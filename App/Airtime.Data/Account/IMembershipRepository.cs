using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using Airtime.Data.Client;
using Airtime.Data.Surveying;


namespace Airtime.Data.Account
{

	public interface IMembershipRepository : IRepository<Membership, int>
	{
		Membership FindByUsername(string username);
		Membership FindByGuid(Guid guid);
		Membership FindByPersonChannel(Person person, Channel channel);

		IQueryable<Membership> QueryBySurvey(Survey survey);

		void Purge(Membership membership);
	}

}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace Airtime.Data.Account
{

	public interface IPersonRepository : IRepository<Person, int>
	{
		Person FindByEmail(string email);
	}

}

using System;
using System.Collections.Generic;

using Airtime.Data.Client;
using Airtime.Data.Surveying;
using Airtime.Data.Surveying.Answers;


namespace Airtime.Data.Account
{

	public class Membership : Entity
	{
		public virtual Guid Guid { get; set; }
		public virtual string ExternalId { get; set; }

		public virtual Person Person { get; set; }

		public virtual string Username { get; set; }
		public virtual string Password { get; set; }
		public virtual string Email { get; set; }
		
		//public virtual string GivenName { get; set; }
		//public virtual string FamilyName { get; set; }

		public virtual bool Enabled { get; set; }
		public virtual DateTime? EnabledFrom { get; set; }
		public virtual DateTime? EnabledTo { get; set; }
		public virtual MembershipStatusEnum MembershipStatus { get; set; }


		public virtual MembershipRoleEnum Role { get; set; }
		public virtual IList<PermissionEnum> Permissions { get; set; }
		public virtual Company Company { get; set; }

		public virtual IList<Channel> Channels { get; set; }
		public virtual IList<RespondentState> RespondentStates { get; set; }
		public virtual IList<AnswerBase> ProfileAnswers { get; set; }

		public virtual string Notes { get; set; }


		//private PersonPersonal _personal;
		//public virtual PersonPersonal Personal
		//{
		//	get { return ReferenceEquals(_personal, null) ? _personal = new PersonPersonal() : _personal; }
		//	set { _personal = value; }
		//}

		//private PersonContact _contact;
		//public virtual PersonContact Contact
		//{
		//	get { return ReferenceEquals(_contact, null) ? _contact = new PersonContact() : _contact; }
		//	set { _contact = value; }
		//}

		private MembershipOptIns _optIns;
		public virtual MembershipOptIns OptIns
		{
			get { return ReferenceEquals(_optIns, null) ? _optIns = new MembershipOptIns() : _optIns; }
			set { _optIns = value; }
		}

		private MembershipFlags _flags;
		public virtual MembershipFlags Flags
		{
			get { return ReferenceEquals(_flags, null) ? _flags = new MembershipFlags() : _flags; }
			set { _flags = value; }
		}


		public Membership()
		{
			Permissions = new List<PermissionEnum>();
			Channels = new List<Channel>();
			RespondentStates = new List<RespondentState>();
			ProfileAnswers = new List<AnswerBase>();

			MembershipStatus = MembershipStatusEnum.Active;
		}

	}

}

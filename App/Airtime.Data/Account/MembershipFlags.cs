using System;

namespace Airtime.Data.Account
{

	[Serializable]
	public class MembershipFlags
	{
		public virtual bool Screened { get; set; }
		public virtual bool SystemChecked { get; set; }
		public virtual bool SampleTested { get; set; }
	}

}
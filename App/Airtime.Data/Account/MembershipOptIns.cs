using System;

namespace Airtime.Data.Account
{

	[Serializable]
	public class MembershipOptIns
	{
		public virtual bool Surveys { get; set; }
		public virtual bool Newsletters { get; set; }
	}

}
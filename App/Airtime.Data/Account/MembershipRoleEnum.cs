namespace Airtime.Data.Account
{

	public enum MembershipRoleEnum
	{
		Developer,
		Administrator,
		Client_Turnkey,
		Client_Admin,
		Client_Manager,
		Client_Reports,
		Client_Marketing,
		Client,
		Recruiter,
		Moderator,
		Respondent
	}

}

﻿namespace Airtime.Data.Account
{

	public enum MembershipStatusEnum
	{
		Active,       //Account is active and usable
		Unconfirmed,  //Account has been created but the person has not confirmed the email

		Suspended,    //Account has been manually disabled by an administrator
		Expired,      //Account has been automatically disabled after reaching its expiry date

		Unsubscribed, //Account has been manually unsubscribed by the person
		Wiped         //Account has been manually wiped by an administrator
	}

}

﻿namespace Airtime.Data.Account
{
	public enum PermissionEnum
	{
		ViewSummaryReports,
		ViewMusicReports,
		ViewPerceptualReports,

		ViewSurveys,
		EditSurveys,
		DeleteSurveys,

		ViewEmails,
		EditEmails,
		DeleteEmails,

		ViewMembers,
		EditMembers,
		DeleteMembers,
		ExportMembers,

		EditLandingPages
	}
}

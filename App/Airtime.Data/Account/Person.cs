﻿using System.Collections.Generic;
using System.Linq;

using Airtime.Data.Remote;


namespace Airtime.Data.Account
{

	public class Person : Entity
	{

		public virtual string Email { get; set; }
		public virtual string Password { get; set; }
		public virtual string GivenName { get; set; }
		public virtual string FamilyName { get; set; }

	
		public virtual IList<Membership> Memberships { get; set; }
		public virtual IList<DeviceRegistration> DeviceRegistrations { get; set; } 


		private PersonPersonal _personal;
		public virtual PersonPersonal Personal
		{
			get { return ReferenceEquals(_personal, null) ? _personal = new PersonPersonal() : _personal; }
			set { _personal = value; }
		}


		private PersonContact _contact;
		public virtual PersonContact Contact
		{
			get { return ReferenceEquals(_contact, null) ? _contact = new PersonContact() : _contact; }
			set { _contact = value; }
		}


		public Person()
		{
			Memberships = new List<Membership>();
			DeviceRegistrations = new List<DeviceRegistration>();
		}

	}

}

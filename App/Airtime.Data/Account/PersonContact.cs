using System;

namespace Airtime.Data.Account
{

	[Serializable]
	public class PersonContact
	{
		public virtual string DaytimePhone { get; set; }
		public virtual string MobilePhone { get; set; }
		//TODO: rename Address to StreetAddress
		public virtual string Address { get; set; }
		public virtual string Suburb { get; set; }
		public virtual string City { get; set; }
		//TODO: rename Postcode to PostalCode
		public virtual string Postcode { get; set; }
		public virtual string State { get; set; }
		public virtual string Country { get; set; }
	}

}
using System.ComponentModel.DataAnnotations;

namespace Airtime.Data.Account
{
	public enum PersonGenderEnum
	{
		[Display(ResourceType = typeof(Resources.Account), Name = "Male")]
		M,

		[Display(ResourceType = typeof(Resources.Account), Name = "Female")]
		F
	}
}
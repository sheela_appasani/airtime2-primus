using System;

namespace Airtime.Data.Account
{

	[Serializable]
	public class PersonPersonal
	{

		public virtual DateTime? DateOfBirth { get; set; }
		public virtual PersonGenderEnum? Gender { get; set; }

		public virtual int? Age {
			get {
				if (!DateOfBirth.HasValue) {
					return null;
				}

				var now = DateTime.Now;
				int years = now.Year - DateOfBirth.Value.Year;
				if (now.Month < DateOfBirth.Value.Month || (now.Month == DateOfBirth.Value.Month && now.Day < DateOfBirth.Value.Day)) {
					years--;
				}
				return years;
			}
		}

	}

}

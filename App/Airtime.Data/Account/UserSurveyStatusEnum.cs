﻿
using System.ComponentModel.DataAnnotations;

namespace Airtime.Data.Account
{
	// ReSharper disable InconsistentNaming
	public enum UserSurveyStatusEnum
	{
		[Display(ResourceType = typeof(Resources.Shared), Name = "All")]
		All,
		[Display(ResourceType = typeof(Resources.Members), Name = "Invited")]
		Invited,
		[Display(ResourceType = typeof(Resources.Members), Name = "Completed")]
		Completed,
		[Display(ResourceType = typeof(Resources.Members), Name = "Not_Invited")]
		Not_Invited,
		[Display(ResourceType = typeof(Resources.Members), Name = "Uncompleted_Invitees")]
		Uncompleted_Invitees
	}
	// ReSharper restore InconsistentNaming
}

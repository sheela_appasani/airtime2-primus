﻿using System.Collections.Generic;

using Airtime.Data.Remote;


namespace Airtime.Data.Client
{

	public class Brand : Entity
	{
		public virtual string Name { get; set; }
		public virtual IList<App> Apps { get; set; }
		public virtual IList<Channel> Channels { get; set; }

		public Brand()
		{
			Apps = new List<App>();
			Channels = new List<Channel>();
		}

	}

}

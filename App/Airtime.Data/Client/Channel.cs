using System;
using System.Collections.Generic;

using Airtime.Data.Content;
using Airtime.Data.Globalization;
using Airtime.Data.Account;
using Airtime.Data.Remote;
using Airtime.Data.Surveying;
using Airtime.Data.Surveying.Questions;


namespace Airtime.Data.Client
{

	public class Channel : Entity
	{
		public virtual Brand Brand { get; set; }
		public virtual Company Company { get; set; }
		public virtual string Name { get; set; }
		public virtual string AdminName { get; set; }
		public virtual string InternalName { get; set; }

		public virtual Language Language { get; set; }
		public virtual Screener Screener { get; set; }

		public virtual int TargetMinimumAge { get; set; }
		public virtual int TargetMaximumAge { get; set; }
		public virtual PersonGenderEnum? TargetGender { get; set; }

		public virtual bool FastTrackPods { get; set; }
		public virtual bool UseShortRegoForm { get; set; }
		public virtual bool EnableRegistrationDetailsApp { get; set; }
		public virtual bool EnableRegistrationDetailsWeb { get; set; }
		public virtual bool EnableLandingPage { get; set; }

		public virtual IList<App> Apps { get; set; }
		public virtual IList<Survey> Surveys { get; set; }
		public virtual IList<Membership> Memberships { get; set; }
		public virtual IList<QuestionBase> ProfileQuestions { get; set; }
		public virtual IList<CustomContent> Contents { get; set; }
		public virtual IList<Widget> Widgets { get; set; }

		public virtual string EmailFrom { get; set; }
		public virtual string EmailCancellationsTo { get; set; }
		public virtual string EmailFeedbackTo { get; set; }

		public virtual string HomePageUrl { get; set; }
		public virtual string ContestRulesUrl { get; set; }
		public virtual string PrivacyPolicyUrl { get; set; }
		public virtual string ContactDetailsUrl { get; set; }
		public virtual string LogoUrl { get; set; }

		public virtual string TimeZone { get; set; }
		public virtual TimeZoneInfo TimeZoneInfo
		{
			get { return TimeZoneInfo.FindSystemTimeZoneById(TimeZone); }
			set { TimeZone = value.Id; }
		}


		public Channel()
		{
			Apps = new List<App>();
			Surveys = new List<Survey>();
			Memberships = new List<Membership>();
			ProfileQuestions = new List<QuestionBase>();
			Widgets = new List<Widget>();
		}

	}

}

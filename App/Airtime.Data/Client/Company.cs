using System.Collections.Generic;


namespace Airtime.Data.Client
{

	public class Company : Entity
	{
		public virtual string Name { get; set; }
		public virtual IList<Channel> Channels { get; set; }
	}

}
﻿namespace Airtime.Data.Client
{

	public enum ContentTypeEnum
	{
		Email_ReferFriend,
		Email_ReferFriendSubject,
		Email_Registered,
		Email_RegisteredSubject,
		Email_SongList,
		Email_SongListSubject,
		Email_SurveyCompletedNotice,
		Email_SurveyCompletedNoticeSubject,
		Email_SurveyCompletedNoticeToAddress,
		Email_SurveyCompletedThanks,
		Email_SurveyCompletedThanksSubject,
		Form_RegisterFooter,
		Global_FooterScript,
		Page_ActivationThankYou,
		Page_RegistrationThankYou,
		Page_SurveyWelcome
	}

}

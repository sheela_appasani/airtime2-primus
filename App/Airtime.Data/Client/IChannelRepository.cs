using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace Airtime.Data.Client
{

	public interface IChannelRepository : IRepository<Channel, int>
	{
	}

}

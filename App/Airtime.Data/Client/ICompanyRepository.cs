namespace Airtime.Data.Client
{
	public interface ICompanyRepository : IRepository<Company, int>
	{
		Company this[string name] { get; }
		Company FindByName(string name);
	}
}
﻿using Airtime.Data.Client;
using Airtime.Data.Surveying;


namespace Airtime.Data.Content
{

	public class CustomContent : Entity
	{
		public virtual Channel Channel { get; set; }
		public virtual Survey Survey { get; set; }

		public virtual ContentTypeEnum ContentType { get; set; }
		public virtual string Text { get; set; }
	}

}

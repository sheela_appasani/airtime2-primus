﻿using Airtime.Data.Content.Fields.Types;


namespace Airtime.Data.Content.Fields
{

	public abstract class FieldBase : Entity
	{
		public virtual string Name { get; set; }
		public virtual FieldType FieldType { get; set; }
		public virtual string FieldTypeName { get; set; }
		public virtual Entity Scope { get; set; }
	}

}

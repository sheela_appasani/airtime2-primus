namespace Airtime.Data.Content.Fields
{

	public interface IFieldRepository : IRepository<FieldBase, int>
	{
	}

}

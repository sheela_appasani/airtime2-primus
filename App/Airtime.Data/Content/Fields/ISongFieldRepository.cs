using Airtime.Data.Client;


namespace Airtime.Data.Content.Fields
{

	public interface ISongFieldRepository : IRepository<SongField, int>
	{
		SongField FindByNameAndCompany(string name, Company company);
		SongField CreateAndSaveField(string name, Company company);
	}

}

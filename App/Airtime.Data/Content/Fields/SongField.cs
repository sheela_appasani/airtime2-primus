﻿using System.Diagnostics.Contracts;

using Airtime.Data.Client;
using Airtime.Data.Content.Fields.Types;
using Airtime.Data.Media;


namespace Airtime.Data.Content.Fields
{

	public class SongField : FieldBase
	{

		public new virtual Company Scope
		{
			get { return (Company)base.Scope; }
			set { base.Scope = value; }
		}


		public SongField()
		{
			FieldTypeName = SongFieldType.Discriminator;
		}


		public SongField(FieldType fieldType, string name, Company company)
			: this()
		{
			//TODO: validate FieldType???
			Contract.Requires(fieldType != null);
			Contract.Requires(name != null);

			Name = name;
			Scope = company;
			FieldType = fieldType;
		}

	}

}

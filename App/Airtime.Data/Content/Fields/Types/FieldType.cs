﻿namespace Airtime.Data.Content.Fields.Types
{

	public abstract class FieldType : Entity
	{
		public virtual FieldContextEnum DataContext { get; set; }
		public virtual FieldScopeEnum DataScope { get; set; }
		public virtual FieldDataTypeEnum DataType { get; set; }
		public virtual string Name { get; protected set; }
		public virtual string Discriminator { get { return GetType().Name; } }
	}

}

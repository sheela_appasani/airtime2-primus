namespace Airtime.Data.Content.Fields.Types
{

	public interface IFieldTypeRepository : IRepository<FieldType, int>
	{
		FieldType this[string name] { get; }
		FieldType FindByName(string name);
	}

}

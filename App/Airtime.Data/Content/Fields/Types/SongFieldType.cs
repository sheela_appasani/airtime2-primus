﻿namespace Airtime.Data.Content.Fields.Types
{

	public class SongFieldType : FieldType
	{

		public new static string Discriminator { get { return typeof(SongFieldType).Name; } }

		public SongFieldType()
		{
			DataType = FieldDataTypeEnum.String;
			DataContext = FieldContextEnum.Song;
			DataScope = FieldScopeEnum.Company;
			Name = Discriminator;
		}

	}

}

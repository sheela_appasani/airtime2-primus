using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace Airtime.Data.Content
{

	public interface IWidgetRepository : IRepository<Widget, int>
	{
	}

}
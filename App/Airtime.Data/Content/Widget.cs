﻿using System.Diagnostics.Contracts;

using Airtime.Data.Client;


namespace Airtime.Data.Content
{

	public class Widget : Entity
	{

		public virtual Channel Channel { get; set; }
		public virtual WidgetTypeEnum WidgetType { get; set; }
		public virtual bool Enabled { get; set; }
		public virtual string Heading { get; set; }
		public virtual string Content { get; set; }


		public Widget()
		{
			Enabled = true;
		}


		public Widget(Widget copyFrom)
		{
			Contract.Requires(copyFrom != null);

			Channel = copyFrom.Channel;
			WidgetType = copyFrom.WidgetType;
			Enabled = copyFrom.Enabled;
			Heading = copyFrom.Heading;
			Content = copyFrom.Content;
		}

	}

}

﻿namespace Airtime.Data.Content
{
	public enum WidgetTypeEnum
	{
		ActiveSurveys,
		Custom,
		ExternalSurveys,
		MessageTheBoss,
		News,
		OthersTopSongs,
		ReferFriends,
		SocialMedia,
		SongsYouVotedOn,
		YourTopSongs
	}
}

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Xml.Serialization;


namespace Airtime.Data
{

	/// <summary>
	///     For a discussion of this object, see 
	///     http://devlicio.us/blogs/billy_mccafferty/archive/2007/04/25/using-equals-gethashcode-effectively.aspx
	/// </summary>
	[Serializable]
	[DebuggerDisplay("{GetDebugView()}")]
	public abstract class EntityWithTypedId<TId> : BaseObject, IEntityWithTypedId<TId>
	{

		/// <summary>
		///     Id may be of type string, int, custom type, etc.
		///     Setter is protected to allow unit tests to set this property via reflection and to allow 
		///     domain objects more flexibility in setting this for those objects with assigned Ids.
		///     It's virtual to allow NHibernate-backed objects to be lazily loaded.
		/// 
		///     This is ignored for XML serialization because it does not have a public setter (which is very much by design).
		///     See the FAQ within the documentation if you'd like to have the Id XML serialized.
		/// </summary>
		[XmlIgnore]
		public virtual TId Id { get; protected set; }


		public virtual DateTime DateCreated { get; set; }


		public virtual DateTime DateModified { get; set; }


		public override bool Equals(object obj)
		{
			var compareTo = obj as EntityWithTypedId<TId>;

			if (ReferenceEquals(this, compareTo)) {
				return true;
			}

			if (compareTo == null || !this.GetType().Equals(compareTo.GetTypeUnproxied())) {
				return false;
			}

			if (this.HasSameNonDefaultIdAs(compareTo)) {
				return true;
			}

			// Since the Ids aren't the same, both of them must be transient to 
			// compare domain signatures; because if one is transient and the 
			// other is a persisted entity, then they cannot be the same object.
			return this.IsTransient() && compareTo.IsTransient() && this.HasSameObjectSignatureAs(compareTo);
		}


		public override int GetHashCode()
		{
			if (this.cachedHashcode.HasValue) {
				return this.cachedHashcode.Value;
			}

			if (this.IsTransient()) {
				this.cachedHashcode = base.GetHashCode();
			} else {
				unchecked {
					// It's possible for two objects to return the same hash code based on 
					// identically valued properties, even if they're of two different types, 
					// so we include the object's type in the hash calculation
					var hashCode = this.GetType().GetHashCode();
					this.cachedHashcode = (hashCode * HashMultiplier) ^ this.Id.GetHashCode();
				}
			}

			return this.cachedHashcode.Value;
		}


		/// <summary>
		///     Transient objects are not associated with an item already in storage.  For instance,
		///     a Customer is transient if its Id is 0.  It's virtual to allow NHibernate-backed 
		///     objects to be lazily loaded.
		/// </summary>
		public virtual bool IsTransient()
		{
			return this.Id == null || this.Id.Equals(default(TId));
		}


		/// <summary>
		///     The property getter for SignatureProperties should ONLY compare the properties which make up 
		///     the "domain signature" of the object.
		/// 
		///     If you choose NOT to override this method (which will be the most common scenario), 
		///     then you should decorate the appropriate property(s) with [DomainSignature] and they 
		///     will be compared automatically.  This is the preferred method of managing the domain
		///     signature of entity objects.
		/// </summary>
		/// <remarks>
		///     This ensures that the entity has at least one property decorated with the 
		///     [DomainSignature] attribute.
		/// </remarks>
		protected override IEnumerable<PropertyInfo> GetTypeSpecificSignatureProperties()
		{
			return
				this.GetType().GetProperties().Where(
					p => Attribute.IsDefined(p, typeof(DomainSignatureAttribute), true));
		}


		/// <summary>
		///     Returns true if self and the provided entity have the same Id values 
		///     and the Ids are not of the default Id value
		/// </summary>
		private bool HasSameNonDefaultIdAs(EntityWithTypedId<TId> compareTo)
		{
			return !this.IsTransient() && !compareTo.IsTransient() && this.Id.Equals(compareTo.Id);
		}


		private string GetDebugView()
		{
			//If this is not a proxy, use simple format
			if (GetType().GetInterface("INHibernateProxy") == null) {
				return string.Format("{0} #{1}", GetType().FullName, Id);
			}

			//Retrieve the initializer
			var initializer = GetType().GetProperty("HibernateLazyInitializer").GetValue(this, null);

			//Determine if the proxy is initialized
			var uninitialized = (bool)initializer.GetType().GetProperty("IsUninitialized").GetValue(initializer, null);
			Type targetType = null;
			if (!uninitialized) {
				//Retrieve the actual target type
				targetType = initializer.GetType().GetProperty("Target", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(initializer, null).GetType();
			}

			//Get original proxied class type
			var type = GetType();
			while (type.Assembly is AssemblyBuilder) {
				type = type.BaseType;
			}

			return string.Format("{0} #{1} ({2} proxy)", type.FullName, Id, uninitialized ? "uninitialized" : targetType.FullName);
		}


		/// <summary>
		///     To help ensure hashcode uniqueness, a carefully selected random number multiplier 
		///     is used within the calculation.  Goodrich and Tamassia's Data Structures and
		///     Algorithms in Java asserts that 31, 33, 37, 39 and 41 will produce the fewest number
		///     of collissions.  See http://computinglife.wordpress.com/2008/11/20/why-do-hash-functions-use-prime-numbers/
		///     for more information.
		/// </summary>
		private const int HashMultiplier = 31;

		private int? cachedHashcode;

	}

}
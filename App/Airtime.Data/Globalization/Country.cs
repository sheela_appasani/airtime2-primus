namespace Airtime.Data.Globalization
{

	public class Country : Entity
	{
		[DomainSignature]
		public virtual string Name { get; set; }
		public virtual string PhoneCountryCode { get; set; }
		public virtual string PhoneNddPrefix { get; set; }
	}

}
﻿
namespace Airtime.Data.Globalization
{
	public interface ICountryRepository : IRepository<Country, int>
	{
	}
}

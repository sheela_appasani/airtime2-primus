namespace Airtime.Data.Globalization
{
	public interface ILanguageRepository : IRepository<Language, int>
	{
		Language FindByName(string name);
	}
}
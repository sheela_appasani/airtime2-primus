using System;


namespace Airtime.Data.Globalization
{
	public class Language : Entity
	{
		public virtual string Name { get; set; }
		public virtual Guid Guid { get; set; }
	}
}
namespace Airtime.Data
{

	public interface IDataRepository<TEntity, TId> : IRepository<TEntity, TId>, IDataWorker
		where TEntity : IEntityWithTypedId<TId>
	{
	}

}

namespace Airtime.Data
{

	public interface IDataWorker
	{
		IUnitOfWork UnitOfWork { get; }
	}

}
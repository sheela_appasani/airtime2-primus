﻿using System;
using System.Collections.Generic;
using System.Reflection;


namespace Airtime.Data
{
	/// <summary>
	///     This serves as a base interface for <see cref="EntityWithTypedId{TId}" /> and 
	///     <see cref = "Entity" />. Also provides a simple means to develop your own base entity.
	/// </summary>
	public interface IEntityWithTypedId<TId>
	{
		TId Id { get; }
		DateTime DateCreated { get; set; }
		DateTime DateModified { get; set; }

		IEnumerable<PropertyInfo> GetSignatureProperties();

		bool IsTransient();
	}
}
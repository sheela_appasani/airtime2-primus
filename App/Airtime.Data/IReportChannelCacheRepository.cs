using Airtime.Data.Reporting;

namespace Airtime.Data
{
	public interface IReportChannelCacheRepository : IRepository<ReportChannelCache, int>
	{
	}
}
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;


namespace Airtime.Data
{

	[ContractClass(typeof(RepositoryContracts<,>))]
	public interface IRepository<TEntity, TId> : IRepositoryBase, IQueryable<TEntity>
		where TEntity : IEntityWithTypedId<TId>
	{
		TEntity this[TId id] { get; }

		IQueryable<TEntity> Items { get; }

		TEntity FindById(TId id);
		List<TEntity> FindAllById(ICollection<TId> ids);

		/// <summary>
		/// Inserts or updates the entity and returns the ID
		/// </summary>
		/// <param name="instance"></param>
		/// <returns>The entity's primary key ID</returns>
		TId Save(TEntity instance);

		void Delete(TEntity instance);

		IQueryable<TEntity> SetCacheable(IQueryable<TEntity> query);
		IQueryable<TEntity> ApplyFetch<TRelated>(IQueryable<TEntity> query, Expression<Func<TEntity, TRelated>> relatedObjectSelector);
		IQueryable<TEntity> ApplyFetchMany<TRelated>(IQueryable<TEntity> query, Expression<Func<TEntity, IEnumerable<TRelated>>> relatedObjectSelector);
	}

}

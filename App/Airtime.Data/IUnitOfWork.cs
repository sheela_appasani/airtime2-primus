using System;
using System.Data.Common;


namespace Airtime.Data
{

	public interface IUnitOfWork : IDisposable
	{
		/// <summary>
		/// Begin a Unit of Work. This usually entails establishing a database session and starting a transaction.
		/// </summary>
		void Begin();

		/// <summary>
		/// Cancel a Unit of Work. This is similar to ending a unit of work, but will always rollback the active transaction.
		/// </summary>
		void Cancel();

		/// <summary>
		/// End a Unit of Work. This usually commits the active transaction (or rolls back if it detects an error condition) and then closes the database session.
		/// </summary>
		void End();


		/// <summary>
		/// Enlists the specified database command in the active transaction.
		/// </summary>
		void Enlist(DbCommand command);


		/// <summary>
		/// Explicitly flushes the session. Usually there is no need to manually call this.
		/// </summary>
		void Flush();

		/// <summary>
		/// Explicitly commits the active transaction. Usually there is no need to manually call this.
		/// </summary>
		void Commit();

		/// <summary>
		/// Explicitly rolls back the active transaction. Usually there is no need to manually call this.
		/// </summary>
		void Rollback();

	}

}

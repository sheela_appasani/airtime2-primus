namespace Airtime.Data.Media
{

	public interface ISongAttributeRepository : IRepository<SongAttribute, int>
	{
	}

}

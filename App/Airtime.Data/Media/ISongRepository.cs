namespace Airtime.Data.Media
{
	public interface ISongRepository : IRepository<Song, int>
	{
	}
}
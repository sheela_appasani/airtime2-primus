using System.Collections.Generic;

using Airtime.Data.Client;
using Airtime.Data.Content.Fields;


namespace Airtime.Data.Media
{

	public class Song : Entity
	{

		public virtual string Title { get; set; }
		public virtual string Artist { get; set; }
		public virtual int? Year { get; set; }
		public virtual string Genre { get; set; }
		public virtual string Tempo { get; set; }

		public virtual string Filename { get; set; }
		public virtual string Library { get; set; }
		public virtual Company Company { get; set; }

		public virtual int Length { get; set; }

		public virtual bool Confirmed { get; set; }
		public virtual bool Matched { get; set; }
		public virtual bool InUse { get; set; }

		public virtual IDictionary<SongField, string> Attributes { get; set; }


		public Song()
		{
			Attributes = new Dictionary<SongField, string>();
		}

	}

}

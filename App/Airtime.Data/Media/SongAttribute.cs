﻿using Airtime.Data.Content.Fields;


namespace Airtime.Data.Media
{

	public class SongAttribute : Entity
	{
		public virtual Song Song { get; set; }
		public virtual SongField Field { get; set; }
		public virtual string Value { get; set; }
	}

}

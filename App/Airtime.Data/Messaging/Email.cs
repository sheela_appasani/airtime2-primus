using System;

using Airtime.Data.Account;


namespace Airtime.Data.Messaging
{

	public class Email : Entity
	{

		public virtual string Subject { get; set; }
		public virtual string Body { get; set; }

		public virtual EmailStatusEnum Status { get; set; }

		public virtual Account.Membership Membership { get; set; }
		public virtual string ToAddress { get; set; }
		public virtual string FromAddress { get; set; }

		public virtual EmailBlast EmailBlast { get; set; }

	}

}
using System;
using System.Collections.Generic;

using Airtime.Data.Client;
using Airtime.Data.Surveying;


namespace Airtime.Data.Messaging
{

	public class EmailBlast : Entity
	{

		public virtual Channel Channel { get; set; }
		public virtual Survey Survey { get; set; }

		public virtual bool NoServerSideTemplate { get; set; }
		public virtual string Subject { get; set; }
		public virtual string Body { get; set; }

		public virtual EmailBlastStatusEnum Status { get; set; }
		private EmailBlastSchedule _schedule;
		public virtual EmailBlastSchedule Schedule
		{
			get { return ReferenceEquals(_schedule, null) ? _schedule = new EmailBlastSchedule() : _schedule; }
			set { _schedule = value; }
		}

		public virtual double TargetPercentage { get; set; }
		public virtual RecipientSourceEnum RecipientSource { get; set; }

		public virtual IList<Email> Emails { get; set; }

		public virtual EmailBlastStatistics Statistics { get; set; }

		public virtual EmailBlastTypeEnum EmailBlastType { get; set; }

		public virtual EmailBlastCriteria Criteria { get; set; }

		public EmailBlast()
		{
			Schedule = new EmailBlastSchedule();
			Statistics = new EmailBlastStatistics();
		}

	}

}
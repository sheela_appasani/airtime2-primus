using System;
using System.Collections.Generic;

using Airtime.Data.Client;
using Airtime.Data.Account;
using Airtime.Data.Surveying;


namespace Airtime.Data.Messaging
{

	public class EmailBlastCriteria : Entity
	{

		public virtual EmailBlast EmailBlast { get; set; }

		public virtual EmailBlastTargetGroupEnum TargetGroup { get; set; }
		public virtual EmailBlastTargetWinnersEnum? TargetWinners { get; set; }

		public virtual bool? GenderMale { get; set; }
		public virtual bool? GenderFemale { get; set; }

		public virtual int? LowerAge { get; set; }
		public virtual int? UpperAge { get; set; }

		public virtual DateTime? LowerBirthday { get; set; }
		public virtual DateTime? UpperBirthday { get; set; }

		public virtual string Postcodes { get; set; }

		public virtual bool? OptInSurveys { get; set; }
		public virtual bool? OptInNewsletters { get; set; }

		public virtual int? NumberOfMembers { get; set; }
		public virtual DateTime? LowerCompletedSurveyDate { get; set; }
		public virtual DateTime? UpperCompletedSurveyDate { get; set; }


		public virtual PersonGenderEnum? Gender
		{
			get {
				if (GenderMale.HasValue && GenderMale.Value && GenderFemale.HasValue && GenderFemale.Value) {
					return null;
				}
				if (GenderMale.HasValue && GenderMale.Value) {
					return PersonGenderEnum.M;
				}
				if (GenderFemale.HasValue && GenderFemale.Value) {
					return PersonGenderEnum.F;
				}
				return null;
			}
			set {
				switch (value) {
					case PersonGenderEnum.M:
						GenderMale = true;
						GenderFemale = false;
						break;
					case PersonGenderEnum.F:
						GenderMale = false;
						GenderFemale = true;
						break;
					default:
						GenderMale = null;
						GenderFemale = null;
						break;
				}
			}
		}


		public virtual EmailBlastTargetEnum Target
		{
			get {
				switch (TargetGroup) {
					case EmailBlastTargetGroupEnum.Entire_Database:
						return EmailBlastTargetEnum.All_Members;
					case EmailBlastTargetGroupEnum.Active_Members:
						return EmailBlastTargetEnum.Active_Members;
					case EmailBlastTargetGroupEnum.Inactive_Members:
						return EmailBlastTargetEnum.Inactive_Members;
					case EmailBlastTargetGroupEnum.Recent_Survey_Participants:
						return EmailBlastTargetEnum.Recent_Survey_Participants;
					case EmailBlastTargetGroupEnum.Recent_Survey_Non_Participants:
						return EmailBlastTargetEnum.Recent_Survey_Non_Participants;
					case EmailBlastTargetGroupEnum.Random_Winner:
						switch (TargetWinners) {
							case EmailBlastTargetWinnersEnum.Entire_Channel:
								return EmailBlastTargetEnum.Random_Members;
							case EmailBlastTargetWinnersEnum.Completed_Between:
								return EmailBlastTargetEnum.Random_Members_Who_Completed;
							case EmailBlastTargetWinnersEnum.First_Completed:
								return EmailBlastTargetEnum.First_Members_To_Complete;
							case EmailBlastTargetWinnersEnum.Recent_Survey_Participants:
								return EmailBlastTargetEnum.Recent_Survey_Participants;
							default:
								return EmailBlastTargetEnum.Random_Members;
						}
					default:
						return EmailBlastTargetEnum.All_Members;
				}
			}
			set {
				switch (value) {
					case EmailBlastTargetEnum.All_Members:
						TargetGroup = EmailBlastTargetGroupEnum.Entire_Database;
						TargetWinners = null;
						break;
					case EmailBlastTargetEnum.Active_Members:
						TargetGroup = EmailBlastTargetGroupEnum.Active_Members;
						TargetWinners = null;
						break;
					case EmailBlastTargetEnum.Inactive_Members:
						TargetGroup = EmailBlastTargetGroupEnum.Inactive_Members;
						TargetWinners = null;
						break;
					case EmailBlastTargetEnum.Recent_Survey_Participants:
						TargetGroup = EmailBlastTargetGroupEnum.Recent_Survey_Participants;
						TargetWinners = null;
						break;
					case EmailBlastTargetEnum.Recent_Survey_Non_Participants:
						TargetGroup = EmailBlastTargetGroupEnum.Recent_Survey_Non_Participants;
						TargetWinners = null;
						break;
					case EmailBlastTargetEnum.Random_Members:
						TargetGroup = EmailBlastTargetGroupEnum.Random_Winner;
						TargetWinners = EmailBlastTargetWinnersEnum.Entire_Channel;
						break;
					case EmailBlastTargetEnum.Random_Members_Who_Completed:
						TargetGroup = EmailBlastTargetGroupEnum.Random_Winner;
						TargetWinners = EmailBlastTargetWinnersEnum.Completed_Between;
						break;
					case EmailBlastTargetEnum.First_Members_To_Complete:
						TargetGroup = EmailBlastTargetGroupEnum.Random_Winner;
						TargetWinners = EmailBlastTargetWinnersEnum.First_Completed;
						break;
				}
			}
		}

	}

}
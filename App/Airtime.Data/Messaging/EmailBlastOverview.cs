using System;

using Airtime.Data.Client;
using Airtime.Data.Surveying;


namespace Airtime.Data.Messaging
{

	public class EmailBlastOverview
	{
		public virtual int EmailId { get; set; }
		public virtual int? SurveyId { get; set; }
		public virtual int? ChannelId { get; set; }
		public virtual string Source { get; set; }

		public virtual Survey Survey { get; set; }
		public virtual Channel Channel { get; set; }

		public virtual EmailBlastStatusEnum Status { get; set; }

		public virtual EmailBlastSchedule Schedule { get; set; }
		public virtual EmailBlastStatistics Statistics { get; set; }

		public virtual string Subject { get; set; }
		public virtual DateTime DateCreated { get; set; }

		public EmailBlastOverview()
		{
			Schedule = new EmailBlastSchedule();
			Statistics = new EmailBlastStatistics();
		}
	}

}

using System;


namespace Airtime.Data.Messaging
{
	public class EmailBlastSchedule
	{
		public virtual DateTime? StartDate { get; set; }
		public virtual DateTime? EndDate { get; set; }
	}
}
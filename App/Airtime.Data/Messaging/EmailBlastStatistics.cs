using System;


namespace Airtime.Data.Messaging
{

	public class EmailBlastStatistics
	{
		public virtual int Scheduled { get; set; }
		public virtual int Delivered { get; set; }
		public virtual int Failed { get; set; }
		public virtual int Cancelled { get; set; }

		public virtual int Pending { get { return Scheduled - Delivered - Failed - Cancelled; } }

		public virtual double? PercentCompleted { get { return Scheduled > 0 ? ((Delivered + Failed + Cancelled) / (double?)Scheduled) : null; } }

		public virtual DateTime? PopulatedTime { get; set; }
		public virtual DateTime? FirstDeliveredTime { get; set; }
		public virtual DateTime? LastDeliveredTime { get; set; }
	}

}

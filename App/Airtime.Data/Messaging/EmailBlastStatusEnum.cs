using System.ComponentModel.DataAnnotations;


namespace Airtime.Data.Messaging
{

	public enum EmailBlastStatusEnum
	{
		[Display(ResourceType = typeof(Resources.Emails), Name = "Pending")]
		Pending,

		[Display(ResourceType = typeof(Resources.Emails), Name = "Sending")]
		Sending,

		[Display(ResourceType = typeof(Resources.Emails), Name = "Sent")]
		Sent,

		[Display(ResourceType = typeof(Resources.Emails), Name = "Cancelled")]
		Cancelled
	}

}

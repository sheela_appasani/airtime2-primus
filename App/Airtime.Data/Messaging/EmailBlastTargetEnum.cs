﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Airtime.Data.Messaging
{
	public enum EmailBlastTargetEnum
	{
		All_Members,
		Active_Members,
		Inactive_Members,
		Recent_Survey_Participants,
		Recent_Survey_Non_Participants,
		Random_Members,
		Random_Members_Who_Completed,
		First_Members_To_Complete
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Airtime.Data.Messaging
{
	public enum EmailBlastTargetGroupEnum
	{
		Entire_Database,
		Active_Members,
		Inactive_Members,
		Recent_Survey_Participants,
		Recent_Survey_Non_Participants,
		Random_Winner
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Airtime.Data.Messaging
{
	public enum EmailBlastTargetWinnersEnum
	{
		Entire_Channel,
		Recent_Survey_Participants,
		Completed_Between,
		First_Completed
	}
}

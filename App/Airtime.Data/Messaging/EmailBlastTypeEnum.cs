﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Airtime.Data.Messaging
{
    public enum EmailBlastTypeEnum
    {
        ChannelEmail,
        SurveyEmail
    }
}

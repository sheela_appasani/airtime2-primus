namespace Airtime.Data.Messaging
{
	public enum EmailStatusEnum
	{
		Pending,
		Cancel,
		Sent,
		Fail
	}
}
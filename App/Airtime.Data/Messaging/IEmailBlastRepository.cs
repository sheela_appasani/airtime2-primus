using System;

namespace Airtime.Data.Messaging
{

	public interface IEmailBlastRepository : IRepository<EmailBlast, int>
	{
		void Cancel(EmailBlast emailBlast);
	}

}
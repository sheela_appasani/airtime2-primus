using System;

namespace Airtime.Data.Messaging
{

	public interface IEmailRepository : IRepository<Email, int>
	{
	}

}
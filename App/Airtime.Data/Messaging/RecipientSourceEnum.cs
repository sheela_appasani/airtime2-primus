using System.ComponentModel.DataAnnotations;


namespace Airtime.Data.Messaging
{
	public enum RecipientSourceEnum
	{
		[Display(ResourceType = typeof(Resources.Emails), Name = "SpecifiedAddresses")]
		Specified_Addresses,

		[Display(ResourceType = typeof(Resources.Emails), Name = "AllParticipants")]
		All_Participants,

		[Display(ResourceType = typeof(Resources.Emails), Name = "AllRegisteredParticipants")]
		All_Registered_Participants,

		[Display(ResourceType = typeof(Resources.Emails), Name = "AllUnregisteredParticipants")]
		All_Unregistered_Participants,

		[Display(ResourceType = typeof(Resources.Emails), Name = "AllStartedParticipants")]
		All_Started_Participants,

		[Display(ResourceType = typeof(Resources.Emails), Name = "AllNotStartedParticipants")]
		All_Not_Started_Participants,

		[Display(ResourceType = typeof(Resources.Emails), Name = "AllCompletedParticipants")]
		All_Completed_Participants,

		[Display(ResourceType = typeof(Resources.Emails), Name = "AllUncompletedParticipants")]
		All_Uncompleted_Participants,

		[Display(ResourceType = typeof(Resources.Emails), Name = "AllPaymentConfirmedParticipants")]
		All_Payment_Confirmed_Participants,

		[Display(ResourceType = typeof(Resources.Emails), Name = "AllPaymentUnconfirmedParticipants")]
		All_Payment_Unconfirmed_Participants,

		[Display(ResourceType = typeof(Resources.Emails), Name = "AllUncompletedInvitees")]
		All_Uncompleted_Invitees
	}
}
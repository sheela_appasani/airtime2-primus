using System;
using System.Collections.Generic;

using Airtime.Data.Client;


namespace Airtime.Data.Remote
{

	public class App : Entity
	{

		public virtual string Name { get; set; }
		public virtual string Code { get; set; }
		public virtual string AppStoreUrl { get; set; }
		public virtual string PushMessage { get; set; }
		public virtual PlatformEnum Platform { get; set; }

		public virtual Brand Brand { get; set; }
		public virtual IList<Channel> Channels { get; set; }
		public virtual IList<DeviceRegistration> DeviceRegistrations { get; set; }

		public App()
		{
			Channels = new List<Channel>();
			DeviceRegistrations = new List<DeviceRegistration>();
		}

	}

}

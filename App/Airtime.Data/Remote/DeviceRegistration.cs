using System;
using System.Collections.Generic;

using Airtime.Data.Account;


namespace Airtime.Data.Remote
{

	public class DeviceRegistration : Entity
	{
		public virtual App App { get; set; }
		public virtual Person Person { get; set; }
		public virtual string NotificationToken { get; set; }

		public virtual string Email {
			get { return Person.Email; }
			set { }
		}
	}

}

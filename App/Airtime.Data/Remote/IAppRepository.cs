namespace Airtime.Data.Remote
{

	public interface IAppRepository : IRepository<App, int>
	{
		App FindByCode(string code);
	}

}

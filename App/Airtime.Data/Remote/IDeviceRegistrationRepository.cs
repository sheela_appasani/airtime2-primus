namespace Airtime.Data.Remote
{

	public interface IDeviceRegistrationRepository : IRepository<DeviceRegistration, int>
	{
	}

}

﻿using System;


namespace Airtime.Data.Reporting
{

	public class AgeGenderTotal
	{
		public virtual string AgeRange { get; set; }

		public virtual int SubTotalMale { get; set; }
		public virtual int SubTotalFemale { get; set; }
		public virtual int SubTotalUnknown { get; set; }
		public virtual int SubTotal { get { return SubTotalMale + SubTotalFemale; } }

		public virtual int Total { get; set; }

		public virtual double PercentMale { get { return Total > 0 ? (double)SubTotalMale / (double)Total : 0; } }
		public virtual double PercentFemale { get { return Total > 0 ? (double)SubTotalFemale / (double)Total : 0; } }
		public virtual double PercentUnknown { get { return Total > 0 ? (double)SubTotalUnknown / (double)Total : 0; } }
		public virtual double Percent { get { return Total > 0 ? (double)SubTotal / (double)Total : 0; } }
	}

}
namespace Airtime.Data.Reporting
{

	public enum CacheReportEnum
	{
		Participation,
		NewMembers,
		NewMembersInTarget,
		StationMost,
		StationsListen,
		TimeSpentListening,
	}

}

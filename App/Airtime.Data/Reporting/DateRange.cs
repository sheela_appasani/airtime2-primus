﻿using System;

using Xtra.Common;


namespace Airtime.Data.Reporting
{

	public class DateRange : Range<DateTime?>
	{

		public DateRange(DateTime? from, DateTime? to, string label = null) : base(from, to, label) { }

	}

}
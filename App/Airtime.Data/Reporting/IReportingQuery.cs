using System;
using System.Collections;
using System.Collections.Generic;

using Airtime.Data.Client;
using Airtime.Data.Media;
using Airtime.Data.Account;
using Airtime.Data.Messaging;


namespace Airtime.Data.Reporting
{

	public interface IReportingQuery : IQueryObject
	{
		IList GetChannelSummaryStationMost(Channel channel);
		IList GetChannelSummaryStationsCumed(Channel channel);
		IList GetChannelSummaryTimeSpentListening(Channel channel);
		IEnumerable<Tuple<PersonGenderEnum?, int>> GetDemographicCountQuery(Channel channel, DateRange birthdateRange);

		IList<SongReport> GetMusicSummary(IList<int> surveyIds, IList<int> choiceIds = null, PersonGenderEnum? gender = null, int? lowerAge = null, int? upperAge = null);
		IList<Membership> GetMusicSummaryMembers(Song song, int lowerRating, int upperRating, IList<int> surveyIds, IList<int> choiceIds = null, PersonGenderEnum? gender = null, int? lowerAge = null, int? upperAge = null);

		IList<OpenEndedReport> GetReportOpenEnded(IList<int> surveyIds, IList<int> choiceIds = null, PersonGenderEnum? gender = null, int? lowerAge = null, int? upperAge = null);
		IList<MultiChoiceReport> GetReportMultiChoice(IList<int> surveyIds, IList<int> choiceIds = null, PersonGenderEnum? gender = null, int? lowerAge = null, int? upperAge = null);

		int GetParticipationRateQuery(Channel channel, DateRange dateRange);

		IList<LoyaltyReport> GetLoyaltyReport(List<Channel> channels, DateTime endFromDate, DateTime endToDate, int minimumInvited, int minimumCompleted, double minimumLoyalty);

		IList<EmailBlastOverview> GetEmailBlastOverviews();
	}

}

﻿using System;

using Airtime.Data.Account;


namespace Airtime.Data.Reporting
{

	public class LoyaltyReport
	{

		public string ChannelName { get; set; }

		public double Loyalty { get; set; }
		public int InvitedCount { get; set; }
		public int CompletedCount { get; set; }
		public int IosCompletedCount { get; set; }
		public int AndroidCompletedCount { get; set; }

		public DateTime? LastSurveyCompleted { get; set; }
		public int UserId { get; set; }
		public string Email { get; set; }
		public string GivenName { get; set; }
		public string FamilyName { get; set; }
		public DateTime? DateOfBirth { get; set; }
		public int? Age { get; set; }
		public string Gender { get; set; }
		public string DaytimePhone { get; set; }
		public string Address { get; set; }
		public string Suburb { get; set; }
		public string City { get; set; }
		public string Postcode { get; set; }

	}

}

using System;


namespace Airtime.Data.Reporting
{

	public class MultiChoiceReport
	{

		public virtual int QuestionId { get; set; }
		public virtual int ResponseId { get; set; }
		public virtual string ResponseName { get; set; }
		public virtual int SelectedTotal { get; set; }
		public virtual int UserTotal { get; set; }

		public virtual double SelectedPercent { get { return (double)SelectedTotal / UserTotal; } }

	}

}

using System;


namespace Airtime.Data.Reporting
{

	public class OpenEndedReport
	{

		public virtual int QuestionId { get; set; }
		public virtual string Answer { get; set; }
		public virtual int UserId { get; set; }
		public virtual string GivenName { get; set; }
		public virtual string FamilyName { get; set; }
		public virtual string Email { get; set; }
		public virtual string Gender { get; set; }
		public virtual int? Age { get; set; }

	}

}

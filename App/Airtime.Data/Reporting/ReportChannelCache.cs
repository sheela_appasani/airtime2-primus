﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Airtime.Data.Client;


namespace Airtime.Data.Reporting
{

	public class ReportChannelCache : Entity
	{

		// ReSharper disable InconsistentNaming
		public virtual Channel Channel { get; set; }
		public virtual DateTime CacheUtcDate { get; set; }

		public virtual int DemographicsTotal { get; set; }
		public virtual int DemographicsTotalMale { get; set; }
		public virtual int DemographicsTotalFemale { get; set; }
		public virtual int DemographicsTotalUnknown { get; set; }
		public virtual int DemographicsUnder20Male { get; set; }
		public virtual int DemographicsUnder20Female { get; set; }
		public virtual int DemographicsUnder20Unknown { get; set; }
		public virtual int Demographics20_24Male { get; set; }
		public virtual int Demographics20_24Female { get; set; }
		public virtual int Demographics20_24Unknown { get; set; }
		public virtual int Demographics25_29Male { get; set; }
		public virtual int Demographics25_29Female { get; set; }
		public virtual int Demographics25_29Unknown { get; set; }
		public virtual int Demographics30_34Male { get; set; }
		public virtual int Demographics30_34Female { get; set; }
		public virtual int Demographics30_34Unknown { get; set; }
		public virtual int Demographics35_39Male { get; set; }
		public virtual int Demographics35_39Female { get; set; }
		public virtual int Demographics35_39Unknown { get; set; }
		public virtual int Demographics40_44Male { get; set; }
		public virtual int Demographics40_44Female { get; set; }
		public virtual int Demographics40_44Unknown { get; set; }
		public virtual int DemographicsOver45Male { get; set; }
		public virtual int DemographicsOver45Female { get; set; }
		public virtual int DemographicsOver45Unknown { get; set; }

		public virtual IList<ReportChannelCacheStat> Stats { get; set; }


		public ReportChannelCache()
		{
			Stats = new List<ReportChannelCacheStat>();
		}


		public virtual IEnumerable<StatRate> GetStatRates(CacheReportEnum report)
		{
			return Stats
				.Where(s => s.Report == report)
				.Select(s => new StatRate { Label = s.Label, Count = s.Count, Total = s.Total });
		}


		public virtual bool HasStat(CacheReportEnum report)
		{
			return Stats.Any(s => s.Report == report);
		}

	}

}

namespace Airtime.Data.Reporting
{
	public class ReportChannelCacheStat : Entity
	{
		public virtual CacheReportEnum Report { get; set; }
		public virtual ReportChannelCache ReportChannelCache { get; set; }
		public virtual int Count { get; set; }
		public virtual int Total { get; set; }
		public virtual string Label { get; set; }
	}
}
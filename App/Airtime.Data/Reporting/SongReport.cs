using System;
using System.Collections;


namespace Airtime.Data.Reporting
{

	public class SongReport
	{

		public virtual int SongId { get; set; }
		public virtual string Artist { get; set; }
		public virtual string Title { get; set; }
		public virtual string Filename { get; set; }

		public virtual double RawAverage { get; set; }

		public virtual int LowBurnTotal { get; set; }       // Little Tired
		public virtual int HighBurnTotal { get; set; }      // Very Tired
		public virtual int LoveTotal { get; set; }          // 81-100
		public virtual int LikeTotal { get; set; }          // 61-80
		public virtual int OkayTotal { get; set; }          // 41-60
		public virtual int DislikeTotal { get; set; }       // 21-40
		public virtual int HateTotal { get; set; }          // 1-20
		public virtual int UserTotal { get; set; }

		public virtual int FamiliarTotal { get { return LoveTotal + LikeTotal + OkayTotal + DislikeTotal + HateTotal; } }
		public virtual int UnfamiliarTotal { get { return UserTotal - FamiliarTotal; } }


		public virtual double LowBurnPercent { get { return (double)LowBurnTotal / UserTotal; } }
		public virtual double HighBurnPercent { get { return (double)HighBurnTotal / UserTotal; } }
		public virtual double BurnPercent { get { return (double)(LowBurnTotal + HighBurnTotal) / UserTotal; } }

		public virtual double LovePercent { get { return (double)LoveTotal / UserTotal; } }
		public virtual double LikePercent { get { return (double)LikeTotal / UserTotal; } }
		public virtual double OkayPercent { get { return (double)OkayTotal / UserTotal; } }
		public virtual double DislikePercent { get { return (double)DislikeTotal / UserTotal; } }
		public virtual double HatePercent { get { return (double)HateTotal / UserTotal; } }

		public virtual double FamiliarPercent { get { return (double)FamiliarTotal / UserTotal; } }
		public virtual double UnfamiliarPercent { get { return (double)UnfamiliarTotal / UserTotal; } }

		public virtual double PositivePercent { get { return (double)(LoveTotal + LikeTotal) / UserTotal; } }
		public virtual double NegativePercent { get { return (double)(DislikeTotal + HateTotal) / UserTotal; } }

		public virtual double Score {
			get {
				return
					100 * (double)LoveTotal / UserTotal * 1.75
					+ 100 * (double)LikeTotal / UserTotal
					+ 100 * (double)OkayTotal / UserTotal * 0.5;
			}
		}

		public virtual double PotentialScore {
			get {
				return
					100 * (double)LoveTotal / FamiliarTotal * 1.75
					+ 100 * (double)LikeTotal / FamiliarTotal
					+ 100 * (double)OkayTotal / FamiliarTotal * 0.5;
			}
		}

		public virtual double Potential_TRN {
			get { return (double)(3 * LoveTotal + LikeTotal) * UnfamiliarTotal / (UserTotal*UserTotal) * 100; }
			//get { return (3 * 100 * LovePercent + 100 * LikePercent) * UnfamiliarPercent; }
		}

	}

}

﻿
namespace Airtime.Data.Reporting
{
	public class StatRate
	{
		public virtual string Label { get; set; }

		public virtual int Count { get; set; }
		public virtual int Total { get; set; }

		public virtual double PercentStat { get { return Total > 0 ? (double)Count / Total : 0; } }
	}
}
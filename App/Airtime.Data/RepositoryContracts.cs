﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;

namespace Airtime.Data
{
	
	[ContractClassFor(typeof(IRepository<,>))]
	internal abstract class RepositoryContracts<TEntity, TId> : IRepository<TEntity, TId>
		where TEntity : IEntityWithTypedId<TId>
	{

		public IUnitOfWork UnitOfWork
		{
			get { throw new NotImplementedException(); }
		}


		public TEntity this[TId id]
		{
			get { throw new NotImplementedException(); }
		}


		public IQueryable<TEntity> Items
		{
			get { throw new NotImplementedException(); }
		}


		public TEntity FindById(TId id)
		{
			throw new NotImplementedException();
		}


		public List<TEntity> FindAllById(ICollection<TId> ids)
		{
			throw new NotImplementedException();
		}


		public TId Save(TEntity instance)
		{
			Contract.Requires<ArgumentNullException>(instance != null, "instance");
			return default(TId);
		}


		public void Delete(TEntity instance)
		{
			Contract.Requires<ArgumentNullException>(instance != null, "instance");
		}


		public IQueryable<TEntity> SetCacheable(IQueryable<TEntity> query)
		{
			throw new NotImplementedException();
		}


		public IQueryable<TEntity> ApplyFetch<TRelated>(IQueryable<TEntity> query, Expression<Func<TEntity, TRelated>> relatedObjectSelector)
		{
			throw new NotImplementedException();
		}


		public IQueryable<TEntity> ApplyFetchMany<TRelated>(IQueryable<TEntity> query, Expression<Func<TEntity, IEnumerable<TRelated>>> relatedObjectSelector)
		{
			throw new NotImplementedException();
		}


		public IEnumerator<TEntity> GetEnumerator()
		{
			throw new NotImplementedException();
		}


		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}


		public Expression Expression
		{
			get { throw new NotImplementedException(); }
		}


		public Type ElementType
		{
			get { throw new NotImplementedException(); }
		}


		public IQueryProvider Provider
		{
			get { throw new NotImplementedException(); }
		}

	}

}
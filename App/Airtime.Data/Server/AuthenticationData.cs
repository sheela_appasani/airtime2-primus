﻿namespace Airtime.Data.Server
{

	public class AuthenticationData
	{
		public int? PersonId { get; set; }
		public int? MembershipId { get; set; }
		public string Role { get; set; }
	}

}

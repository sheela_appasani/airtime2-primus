namespace Airtime.Data.Server
{

	public interface IRequestLogRepository : IRepository<RequestLog, int>
	{
	}

}

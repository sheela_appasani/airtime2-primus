using Airtime.Data.Account;


namespace Airtime.Data.Server
{

	public class RequestLog : Entity
	{
		public virtual Account.Membership Membership { get; set; }
		public virtual string Username { get; set; }
		public virtual string IpAddress { get; set; }
		public virtual string Url { get; set; }
		public virtual string ClassName { get; set; }
		public virtual double? RenderTime { get; set; }
		public virtual string HttpUserAgent { get; set; }
		public virtual string HttpReferrer { get; set; }
		public virtual string HostCode { get; set; }
	}

}
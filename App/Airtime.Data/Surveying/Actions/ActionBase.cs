﻿using Airtime.Data.Surveying.Questions;


namespace Airtime.Data.Surveying.Actions
{

	public abstract class ActionBase : Entity
	{

		public virtual QuestionBase Question { get; set; }

	}

}

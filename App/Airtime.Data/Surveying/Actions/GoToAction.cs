﻿using Airtime.Data.Surveying.Questions;


namespace Airtime.Data.Surveying.Actions
{

	public class GoToAction : ActionBase
	{

		public virtual QuestionBase GoToQuestion { get; set; }

	}

}

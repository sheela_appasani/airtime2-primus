namespace Airtime.Data.Surveying.Actions
{

	public interface IActionRepository : IRepository<ActionBase, int>
	{
	}

}

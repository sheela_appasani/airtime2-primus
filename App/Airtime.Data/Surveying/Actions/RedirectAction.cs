﻿using Airtime.Data.Surveying.Questions;


namespace Airtime.Data.Surveying.Actions
{

	public class RedirectAction : ActionBase
	{

		public virtual string RedirectUrl { get; set; }

	}

}

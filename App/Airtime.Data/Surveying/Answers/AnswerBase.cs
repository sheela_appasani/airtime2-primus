using Airtime.Data.Account;
using Airtime.Data.Surveying.Questions;


namespace Airtime.Data.Surveying.Answers
{

	public class AnswerBase : Entity
	{
		public virtual Membership Membership { get; set; }
		public virtual RespondentState RespondentState { get; set; }
		public virtual QuestionBase Question { get; set; }
		public virtual AnswerTypeEnum AnswerType { get; set; }
		public virtual bool IsProfileAnswer { get; set; }
		public virtual bool GeneratedReport { get; set; }
	}

}

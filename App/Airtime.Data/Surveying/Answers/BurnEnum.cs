using System.ComponentModel.DataAnnotations;


namespace Airtime.Data.Surveying.Answers
{

	public enum BurnEnum
	{
		[Display(ResourceType = typeof(Resources.Surveys), Name = "NotTired")]
		NotTired = 0,

		[Display(ResourceType = typeof(Resources.Surveys), Name = "LittleTired")]
		Tired = 100,

		[Display(ResourceType = typeof(Resources.Surveys), Name = "VeryTired")]
		VeryTired = 200,

		[Display(ResourceType = typeof(Resources.Surveys), Name = "Unfamiliar")]
		Unfamiliar = -1
	}

}

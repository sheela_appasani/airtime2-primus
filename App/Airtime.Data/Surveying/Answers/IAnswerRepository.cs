namespace Airtime.Data.Surveying.Answers
{

	public interface IAnswerRepository : IRepository<AnswerBase, int>
	{
	}

}
namespace Airtime.Data.Surveying.Answers
{

	public interface ISongScoreRepository : IRepository<SongScore, int>
	{
	}

}

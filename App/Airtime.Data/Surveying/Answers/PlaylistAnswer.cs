using System.Collections.Generic;


namespace Airtime.Data.Surveying.Answers
{

	public class PlaylistAnswer : AnswerBase
	{

		public virtual IList<SongScore> SongScores { get; set; }


		public PlaylistAnswer()
		{
			AnswerType = AnswerTypeEnum.AudioPod;
			SongScores = new List<SongScore>();
		}

	}

}

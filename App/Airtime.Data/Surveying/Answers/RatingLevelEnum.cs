namespace Airtime.Data.Surveying.Answers
{
	public enum RatingLevelEnum
	{
		Unfamiliar,
		Hate,
		Dislike,
		Okay,
		Like,
		Love
	}
}
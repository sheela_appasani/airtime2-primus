using Airtime.Data.Surveying.Questions;


namespace Airtime.Data.Surveying.Answers
{

	public class SingleAnswer : AnswerBase
	{

		public virtual QuestionChoice Choice { get; set; }
		public virtual string Text { get; set; }


		public SingleAnswer()
		{
			AnswerType = AnswerTypeEnum.Common;
		}

	}

}

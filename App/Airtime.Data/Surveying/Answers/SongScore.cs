using Airtime.Data.Media;


namespace Airtime.Data.Surveying.Answers
{

	public class SongScore : Entity
	{

		public virtual PlaylistAnswer PlaylistAnswer { get; set; }
		public virtual Song Song { get; set; }
		public virtual BurnEnum Burn { get; set; }
		public virtual string Ratings { get; set; }
		public virtual int FinalRating { get; set; }


		public SongScore()
		{
		}


		public SongScore(SongScore copyFrom)
		{
			PlaylistAnswer = copyFrom.PlaylistAnswer;
			Song = copyFrom.Song;
			Burn = copyFrom.Burn;
			Ratings = copyFrom.Ratings;
			FinalRating = copyFrom.FinalRating;
		}

	}

}

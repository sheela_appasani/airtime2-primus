using Airtime.Data.Account;


namespace Airtime.Data.Surveying
{

	public interface IRespondentStateRepository : IRepository<RespondentState, int>
	{
		RespondentState FindByUserAndSurvey(Account.Membership membership, Survey survey);
	}

}

using System.Diagnostics.Contracts;

namespace Airtime.Data.Surveying
{

	[ContractClass(typeof(SurveyRepositoryContracts))]
	public interface ISurveyRepository : IRepository<Survey, int>
	{
		void Archive(Survey survey);
	}

}
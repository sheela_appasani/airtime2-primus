using System.Collections.Generic;
using System.Diagnostics.Contracts;

using Airtime.Data.Media;

namespace Airtime.Data.Surveying.Questions
{

	public class AudioPodQuestion : QuestionBase
	{
		public virtual IList<Song> Songs { get; set; }
		public virtual SurveyBurnButtons BurnButtons { get; set; }

		public AudioPodQuestion()
		{
			QuestionType = QuestionTypeEnum.AUDIOPOD;
			Progression = QuestionProgressionEnum.AutoGoNext;
			BurnButtons = new SurveyBurnButtons();
			Songs = new List<Song>();
		}

		public AudioPodQuestion(AudioPodQuestion copyFrom) : base(copyFrom)
		{
			Contract.Requires(copyFrom != null, "copyFrom");

			BurnButtons = new SurveyBurnButtons(copyFrom.BurnButtons);
			Songs = new List<Song>(copyFrom.Songs);
		}

	}

}
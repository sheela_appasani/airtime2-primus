using System.Diagnostics.Contracts;

using Airtime.Data.Media;

namespace Airtime.Data.Surveying.Questions
{

	public class BreakQuestion : QuestionBase
	{

		public BreakQuestion()
		{
			QuestionType = QuestionTypeEnum.BREAK;
		}

		public BreakQuestion(BreakQuestion copyFrom) : base(copyFrom)
		{
			Contract.Requires(copyFrom != null);
		}

	}

}
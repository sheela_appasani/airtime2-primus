﻿namespace Airtime.Data.Surveying.Questions
{

	public static class ExtendQuestion
	{
	
		/// <summary>
		/// Returns true if the Question is a Profile question as opposed to a Survey question
		/// </summary>
		/// <returns>Returns true if the Question is a Profile question.</returns>
		public static bool IsProfileQuestion(this QuestionBase self)
		{
			return self.Category == QuestionCategoryEnum.Registration
				|| self.Category == QuestionCategoryEnum.Registration_Age
				|| self.Category == QuestionCategoryEnum.Registration_Gender
				|| self.Category == QuestionCategoryEnum.Registration_StationMost
				|| self.Category == QuestionCategoryEnum.Registration_StationsListen
				|| self.Category == QuestionCategoryEnum.Registration_TimeSpentListening;
		}

	}

}

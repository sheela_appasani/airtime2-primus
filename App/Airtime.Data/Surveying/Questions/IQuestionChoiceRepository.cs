namespace Airtime.Data.Surveying.Questions
{

	public interface IQuestionChoiceRepository : IRepository<QuestionChoice, int>
	{
	}

}
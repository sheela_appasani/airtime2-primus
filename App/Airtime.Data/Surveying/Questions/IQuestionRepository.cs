namespace Airtime.Data.Surveying.Questions
{

	public interface IQuestionRepository : IRepository<QuestionBase, int>
	{
	}

}
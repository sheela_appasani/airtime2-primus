using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

using Airtime.Data.Media;

namespace Airtime.Data.Surveying.Questions
{

	public class MultiChoiceQuestion : QuestionBase
	{

		public virtual ReportChartTypeEnum ReportChartType { get; set; }
		public virtual IList<QuestionChoice> Choices { get; set; }

	
		protected MultiChoiceQuestion()
		{
			Choices = new List<QuestionChoice>();
		}


		public MultiChoiceQuestion(QuestionTypeEnum questionType)
		{
			if (questionType != QuestionTypeEnum.RADIO && questionType != QuestionTypeEnum.CHECKBOX && questionType != QuestionTypeEnum.AUDIO 
				&& questionType != QuestionTypeEnum.VIDEO && questionType != QuestionTypeEnum.CUSTOMCHECKBOX && questionType != QuestionTypeEnum.CUSTOMRADIO) {
				throw new ArgumentException("Not a supported multiple-choice question type", "questionType");
			}

			QuestionType = questionType;
			Choices = new List<QuestionChoice>();
		}


		public MultiChoiceQuestion(MultiChoiceQuestion copyFrom, bool copyBranches)
			: base(copyFrom)
		{
			Contract.Requires(copyFrom != null, "copyFrom");

			ReportChartType = copyFrom.ReportChartType;

			Choices = new List<QuestionChoice>(copyFrom.Choices.Count);
			foreach (var originalChoice in copyFrom.Choices) {
				var choice = new QuestionChoice(originalChoice, copyBranches);
				choice.Question = this;
				if (copyFrom.Survey == null) {
					//If the source question does not belong to a survey, then it's a "template question" (channel-level) - reference its choices so they're easily locatable
					choice.TemplateChoice = originalChoice;
				}
				Choices.Add(choice);
			}
		}

	}

}
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

using Airtime.Data.Client;
using Airtime.Data.Media;
using Airtime.Data.Surveying.Actions;


namespace Airtime.Data.Surveying.Questions
{

	public abstract class QuestionBase : Entity
	{
		public virtual string Name { get; set; }
		public virtual string Text { get; set; }
		public virtual int Position { get; set; }
		public virtual bool Required { get; set; }

		public virtual QuestionProgressionEnum Progression { get; set; }
		public virtual QuestionCategoryEnum Category { get; set; }
		public virtual QuestionTypeEnum QuestionType { get; protected set; }

		public virtual Channel Channel { get; set; }
		public virtual Survey Survey { get; set; }
		public virtual QuestionBase TemplateQuestion { get; set; }

		public virtual Song Song { get; set; }

		public virtual IList<ActionBase> PostActions { get; set; }


		protected QuestionBase()
		{
			Text = String.Empty;
			Required = true;
			PostActions = new List<ActionBase>();
		}


		protected QuestionBase(QuestionBase copyFrom)
		{
			Contract.Requires(copyFrom != null);

			Name = copyFrom.Name;
			Text = copyFrom.Text;
			Position = copyFrom.Position;
			Required = copyFrom.Required;
			Progression = copyFrom.Progression;
			Category = copyFrom.Category;
			QuestionType = copyFrom.QuestionType;
			Survey = copyFrom.Survey;
			Channel = copyFrom.Channel;
			Song = copyFrom.Song;

			if (copyFrom.Survey == null) {
				//If the source question does not belong to a survey, then it's a "template question" (channel-level) - reference it so it's easily locatable
				TemplateQuestion = copyFrom;
			}
		}

	}

}
namespace Airtime.Data.Surveying.Questions
{
	public enum QuestionCategoryEnum
	{
		Normal,
		Normal_RegistrationIntroduction,
		Normal_RegistrationCompleted,
		Normal_SurveyIntroduction,
		Normal_SurveyCompleted,
		Registration,
		Registration_Age,
		Registration_Gender,
		Registration_TimeSpentListening,
		Registration_StationsListen,
		Registration_StationMost
	}
}
using System.Diagnostics.Contracts;

namespace Airtime.Data.Surveying.Questions
{

	public class QuestionChoice : Entity
	{

		public virtual string Name { get; set; }
		public virtual string Code { get; set; }
		public virtual int Position { get; set; }

		public virtual QuestionBase Question { get; set; }
		public virtual QuestionBase BranchQuestion { get; set; }
		public virtual QuestionChoice TemplateChoice { get; set; }


		public QuestionChoice()
		{
			
		}

		public QuestionChoice(QuestionChoice copyFrom, bool copyBranches)
		{
			Contract.Requires(copyFrom != null);
			Name = copyFrom.Name;
			Code = copyFrom.Code;
			Position = copyFrom.Position;
			Question = copyFrom.Question;
			TemplateChoice = copyFrom.TemplateChoice;
			if (copyBranches) {
				BranchQuestion = copyFrom.BranchQuestion;
			}
		}

	}

}
namespace Airtime.Data.Surveying.Questions
{

	public enum QuestionProgressionEnum
	{
		Normal,
		AutoGoNext,
		NextAfterAnswering
	}

}
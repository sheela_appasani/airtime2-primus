using System.ComponentModel.DataAnnotations;

namespace Airtime.Data.Surveying.Questions
{

	public enum QuestionTypeEnum
	{
		[Display(ResourceType = typeof(Resources.Questions), Name = "OpenEnded")]
		TEXT,

		[Display(ResourceType = typeof(Resources.Questions), Name = "TextInstruction")]
		BREAK,

		[Display(ResourceType = typeof(Resources.Questions), Name = "MultipleAnswer")]
		CHECKBOX,

		[Display(ResourceType = typeof(Resources.Questions), Name = "SingleAnswer")]
		RADIO,

		[Display(ResourceType = typeof(Resources.Questions), Name = "AudioSingleAnswer")]
		AUDIO,

		[Display(ResourceType = typeof(Resources.Questions), Name = "Songs")]
		AUDIOPOD,

		[Display(ResourceType = typeof(Resources.Questions), Name = "FivePointScale")]
		SCALE,

		[Display(ResourceType = typeof(Resources.Questions), Name = "EmotionalVideoResponse")]
		VIDEO,

		[Display(ResourceType = typeof(Resources.Questions), Name = "SingleAnswer")]
		CUSTOMRADIO,

		[Display(ResourceType = typeof(Resources.Questions), Name = "MultipleAnswer")]
		CUSTOMCHECKBOX,

		[Display(ResourceType = typeof(Resources.Questions), Name = "OpenEnded")]
		CUSTOMTEXT
	}

}

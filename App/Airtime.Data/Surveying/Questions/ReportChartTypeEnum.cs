﻿namespace Airtime.Data.Surveying.Questions
{
	public enum ReportChartTypeEnum
	{
		AbsoluteValue,
		Percentage,
		Intensity
	}
}
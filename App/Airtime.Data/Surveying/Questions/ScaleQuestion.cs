using System.Diagnostics.Contracts;

using Airtime.Data.Media;

namespace Airtime.Data.Surveying.Questions
{

	public class ScaleQuestion : QuestionBase
	{

		public ScaleQuestion()
		{
			QuestionType = QuestionTypeEnum.SCALE;
		}


		public ScaleQuestion(ScaleQuestion copyFrom) : base(copyFrom)
		{
			Contract.Requires(copyFrom != null);
		}

	}

}
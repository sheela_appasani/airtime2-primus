using System.Diagnostics.Contracts;

namespace Airtime.Data.Surveying.Questions
{

	public class TextQuestion : QuestionBase
	{

		public TextQuestion()
		{
			QuestionType = QuestionTypeEnum.TEXT;
		}


		public TextQuestion(TextQuestion copyFrom) : base(copyFrom)
		{
			Contract.Requires(copyFrom != null);
		}

	}

}
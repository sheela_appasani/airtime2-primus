using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace Airtime.Data.Surveying.Questions
{

	public class VideoQuestion : MultiChoiceQuestion
	{

		public VideoQuestion()
		{
			QuestionType = QuestionTypeEnum.VIDEO;
			Choices = new List<QuestionChoice>();
		}


		public VideoQuestion(VideoQuestion copyFrom, bool copyBranches) : base(copyFrom, copyBranches)
		{
		}

	}

}
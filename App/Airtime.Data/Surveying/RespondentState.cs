using System;
using System.Collections.Generic;

using Airtime.Data.Account;
using Airtime.Data.Remote;
using Airtime.Data.Surveying.Answers;
using Airtime.Data.Surveying.Questions;


namespace Airtime.Data.Surveying
{

	public class RespondentState : Entity
	{
		public virtual Guid Guid { get; set; }

		public virtual Membership Membership { get; set; }
		public virtual Survey Survey { get; set; }

		public virtual bool Enabled { get; set; }
		public virtual bool Invited { get; set; }
		public virtual bool Registered { get; set; }
		public virtual bool Completed { get; set; }
		public virtual bool PaymentDetailConfirmed { get; set; }

		public virtual QuestionBase CurrentQuestion { get; set; }

		public virtual IList<AnswerBase> ProfileAnswers { get; set; }
		public virtual IList<AnswerBase> SurveyAnswers { get; set; }

		public virtual PlatformEnum? UsedPlatform { get; set; }


		public RespondentState()
		{
			Guid = Guid.NewGuid();
			ProfileAnswers = new List<AnswerBase>();
			SurveyAnswers = new List<AnswerBase>();
		}

	}

}

using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace Airtime.Data.Surveying
{

	public class Screener : Entity
	{
		public virtual bool IncludeGivenName { get; set; }
		public virtual bool IncludeFamilyName { get; set; }
		public virtual bool IncludeEmail { get; set; }
		public virtual bool IncludeDateOfBirth { get; set; }
		public virtual bool IncludeGender { get; set; }
		public virtual bool IncludeDaytimePhone { get; set; }
		public virtual bool IncludeMobilePhone { get; set; }
		public virtual bool IncludeStreetAddress { get; set; }
		public virtual bool IncludeSuburb { get; set; }
		public virtual bool IncludeCity { get; set; }
		public virtual bool IncludeState { get; set; }
		public virtual bool IncludePostalCode { get; set; }
		public virtual bool IncludeCountry { get; set; }


		public Screener()
		{
			//Defaults
			IncludeGivenName = true;
			IncludeFamilyName = true;
			IncludeEmail = true;
			IncludeDateOfBirth = true;
			IncludeGender = true;
		}


		public Screener(Screener copyFrom)
		{
			Contract.Requires(copyFrom != null, "copyFrom");

			IncludeGivenName = copyFrom.IncludeGivenName;
			IncludeFamilyName = copyFrom.IncludeFamilyName;
			IncludeEmail = copyFrom.IncludeEmail;
			IncludeDateOfBirth = copyFrom.IncludeDateOfBirth;
			IncludeGender = copyFrom.IncludeGender;
			IncludeDaytimePhone = copyFrom.IncludeDaytimePhone;
			IncludeMobilePhone = copyFrom.IncludeMobilePhone;
			IncludeStreetAddress = copyFrom.IncludeStreetAddress;
			IncludeSuburb = copyFrom.IncludeSuburb;
			IncludeCity = copyFrom.IncludeCity;
			IncludeState = copyFrom.IncludeState;
			IncludePostalCode = copyFrom.IncludePostalCode;
			IncludeCountry = copyFrom.IncludeCountry;
		}

	}

}
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

using Airtime.Data.Client;
using Airtime.Data.Content;
using Airtime.Data.Globalization;
using Airtime.Data.Messaging;
using Airtime.Data.Surveying.Questions;

namespace Airtime.Data.Surveying
{

	public class Survey : Entity
	{
		public virtual string Name { get; set; }
		public virtual bool Archived { get; set; }
		public virtual int Deleted { get; set; }

		public virtual Language Language { get; set; }
		public virtual Company Company { get; set; }
		public virtual Channel Channel { get; set; }

		public virtual SurveySchedule Schedule { get; set; }
		public virtual SurveyStatistics Statistics { get; set; }
		public virtual SurveyBurnButtons BurnButtons { get; set; }
		public virtual SurveyOptions Options { get; set; }
		public virtual SurveyNotificationStates NotificationStates { get; set; }

		public virtual IList<SurveyAlert> Alerts { get; set; }
		public virtual IList<QuestionBase> Questions { get; set; }
		public virtual IList<QuestionBase> ProfileQuestions { get; set; }
		public virtual IList<RespondentState> RespondentStates { get; set; }
		public virtual IList<EmailBlast> EmailBlasts { get; set; }
		public virtual IList<CustomContent> Contents { get; set; }


		public virtual SurveyStatusEnum Status
		{
			get {
				if (Deleted > 0) {
					return SurveyStatusEnum.Deleted;
				}
				if (Archived) {
					return SurveyStatusEnum.Archived;
				}
				if (Schedule != null) {
					var now = DateTime.UtcNow;
					if (Schedule.EndDate.HasValue && Schedule.EndDate <= now) {
						return SurveyStatusEnum.Ended;
					}
					if (Schedule.StartDate.HasValue && Schedule.StartDate <= now) {
						return SurveyStatusEnum.Active;
					}
				}
				return SurveyStatusEnum.Setup;
			}
		}


		public Survey()
		{
			Schedule = new SurveySchedule();
			Statistics = new SurveyStatistics();
			BurnButtons = new SurveyBurnButtons();
			Options = new SurveyOptions();
			NotificationStates = new SurveyNotificationStates();

			Alerts = new List<SurveyAlert>();
			Questions = new List<QuestionBase>();
			ProfileQuestions = new List<QuestionBase>();
			RespondentStates = new List<RespondentState>();
			EmailBlasts = new List<EmailBlast>();
		}


		public Survey(Survey copyFrom)
		{
			Contract.Requires(copyFrom != null, "copyFrom");

			Name = copyFrom.Name;
			Language = copyFrom.Language;
			Company = copyFrom.Company;
			Channel = copyFrom.Channel;
			Schedule = new SurveySchedule(copyFrom.Schedule);
			BurnButtons = new SurveyBurnButtons(copyFrom.BurnButtons);
			Options = new SurveyOptions(copyFrom.Options);

			//Statistics & NotificationStates are set as the survey is used
			Statistics = new SurveyStatistics();
			NotificationStates = new SurveyNotificationStates();

			//Copy survey alerts and set the survey that the copies belong to to this one
			Alerts = new List<SurveyAlert>(copyFrom.Alerts.Select(alert => new SurveyAlert(alert) { Survey = this }));

			//The following need to be copied/populated separately outside of this copy-constructor - see Airtime.Services.ISurveyingService
			Questions = new List<QuestionBase>();
			ProfileQuestions = new List<QuestionBase>();
			RespondentStates = new List<RespondentState>();

			EmailBlasts = new List<EmailBlast>();
			Contents = new List<CustomContent>();
		}

	}

}
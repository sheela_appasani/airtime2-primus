using System.Diagnostics.Contracts;

using Airtime.Data.Account;


namespace Airtime.Data.Surveying
{

	public class SurveyAlert : Entity
	{

		public virtual Survey Survey { get; set; }
		public virtual bool Sent { get; set; }

		public virtual PersonGenderEnum? Gender { get; set; }
		public virtual int? LowerAge { get; set; }
		public virtual int? UpperAge { get; set; }
		public virtual int? SampleSize { get; set; }


		public SurveyAlert()
		{
		}


		public SurveyAlert(SurveyAlert copyFrom)
		{
			Contract.Requires(copyFrom != null, "copyFrom");

			Gender = copyFrom.Gender;
			LowerAge = copyFrom.LowerAge;
			UpperAge = copyFrom.UpperAge;
			SampleSize = copyFrom.SampleSize;
		}

	}

}
using System;
using System.Diagnostics.Contracts;


namespace Airtime.Data.Surveying
{

	public class SurveyBurnButtons
	{

		public virtual string NotTiredText { get; set; }
		public virtual string LittleTiredText { get; set; }
		public virtual string VeryTiredText { get; set; }
		public virtual string UnfamiliarText { get; set; }

		public virtual bool ShowNotTired { get; set; }
		public virtual bool ShowLittleTired { get; set; }
		public virtual bool ShowVeryTired { get; set; }
		public virtual bool ShowUnfamiliar { get; set; }


		public SurveyBurnButtons()
		{
		}


		public SurveyBurnButtons(SurveyBurnButtons copyFrom)
		{
			Contract.Requires(copyFrom != null, "copyFrom");

			NotTiredText = copyFrom.NotTiredText;
			LittleTiredText = copyFrom.LittleTiredText;
			VeryTiredText = copyFrom.VeryTiredText;
			UnfamiliarText = copyFrom.UnfamiliarText;
			ShowNotTired = copyFrom.ShowNotTired;
			ShowLittleTired = copyFrom.ShowLittleTired;
			ShowVeryTired = copyFrom.ShowVeryTired;
			ShowUnfamiliar = copyFrom.ShowUnfamiliar;
		}

	}

}
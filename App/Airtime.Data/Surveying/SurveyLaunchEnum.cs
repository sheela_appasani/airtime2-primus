using System.ComponentModel.DataAnnotations;

namespace Airtime.Data.Surveying
{
	public enum SurveyLaunchEnum 
	{
		[Display(ResourceType = typeof(Resources.Surveys), Name = "LaunchNow")]
		Now = 0,

		[Display(ResourceType = typeof(Resources.Surveys), Name = "LaunchIn1Day")]
		In1Day = 1,

		[Display(ResourceType = typeof(Resources.Surveys), Name = "LaunchIn2Days")]
		In2Days = 2,

		[Display(ResourceType = typeof(Resources.Surveys), Name = "LaunchIn3Days")]
		In3Days = 3,

		//[Display(ResourceType = typeof(Resources.Surveys), Name = "LaunchCustom")]
		//Custom = 4,
	}
}
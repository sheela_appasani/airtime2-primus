using System.Diagnostics.Contracts;


namespace Airtime.Data.Surveying
{

	public class SurveyNotificationStates
	{

		public virtual bool NotifiedIos { get; set; }
		public virtual bool NotifiedAndroid { get; set; }


		public SurveyNotificationStates()
		{
		}


		public SurveyNotificationStates(SurveyNotificationStates copyFrom)
		{
			Contract.Requires(copyFrom != null, "copyFrom");

			NotifiedIos = copyFrom.NotifiedIos;
			NotifiedAndroid = copyFrom.NotifiedAndroid;
		}

	}

}
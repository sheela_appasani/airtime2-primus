using System.Diagnostics.Contracts;


namespace Airtime.Data.Surveying
{

	public class SurveyOptions
	{
		public virtual bool RandomiseSongLists { get; set; }
		public virtual bool ShowSongSummaryAtEnd { get; set; }
		public virtual bool EnableChat { get; set; }
		public virtual bool EnableBurnButtons { get; set; }
		public virtual bool EnableSystemCheck { get; set; }
		public virtual bool EnableSampleQuestions { get; set; }
		public virtual bool EnableProgressBar { get; set; }
		public virtual bool EnableExternalRecruits { get; set; }
		public virtual int MaxIdleSongScores { get; set; }


		public SurveyOptions()
		{
			EnableBurnButtons = true;
			MaxIdleSongScores = 3;
		}


		public SurveyOptions(SurveyOptions copyFrom)
		{
			Contract.Requires(copyFrom != null, "copyFrom");

			RandomiseSongLists = copyFrom.RandomiseSongLists;
			ShowSongSummaryAtEnd = copyFrom.ShowSongSummaryAtEnd;
			EnableChat = copyFrom.EnableChat;
			EnableBurnButtons = copyFrom.EnableBurnButtons;
			EnableSystemCheck = copyFrom.EnableSystemCheck;
			EnableSampleQuestions = copyFrom.EnableSampleQuestions;
			EnableProgressBar = copyFrom.EnableProgressBar;
			EnableExternalRecruits = copyFrom.EnableExternalRecruits;
			MaxIdleSongScores = copyFrom.MaxIdleSongScores;
		}

	}

}
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;

namespace Airtime.Data.Surveying
{

	[ContractClassFor(typeof(ISurveyRepository))]
	internal abstract class SurveyRepositoryContracts : ISurveyRepository
	{
		public IEnumerator<Survey> GetEnumerator()
		{
			throw new NotImplementedException();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public Expression Expression
		{
			get { throw new NotImplementedException(); }
		}

		public Type ElementType
		{
			get { throw new NotImplementedException(); }
		}

		public IQueryProvider Provider
		{
			get { throw new NotImplementedException(); }
		}

		public IUnitOfWork UnitOfWork
		{
			get { throw new NotImplementedException(); }
		}

		public Survey this[int id]
		{
			get { throw new NotImplementedException(); }
		}

		public IQueryable<Survey> Items
		{
			get { throw new NotImplementedException(); }
		}

		public Survey FindById(int id)
		{
			throw new NotImplementedException();
		}

		public List<Survey> FindAllById(ICollection<int> ids)
		{
			throw new NotImplementedException();
		}

		public int Save(Survey instance)
		{
			throw new NotImplementedException();
		}

		public void Delete(Survey instance)
		{
			throw new NotImplementedException();
		}


		public IQueryable<Survey> SetCacheable(IQueryable<Survey> query)
		{
			throw new NotImplementedException();
		}


		public IQueryable<Survey> ApplyFetch<TRelated>(IQueryable<Survey> query, Expression<Func<Survey, TRelated>> relatedObjectSelector)
		{
			throw new NotImplementedException();
		}


		public IQueryable<Survey> ApplyFetchMany<TRelated>(IQueryable<Survey> query, Expression<Func<Survey, IEnumerable<TRelated>>> relatedObjectSelector)
		{
			throw new NotImplementedException();
		}


		public void Archive(Survey survey)
		{
			Contract.Requires<ArgumentNullException>(survey != null, "survey");
		}
	}
}
using System;
using System.Diagnostics.Contracts;


namespace Airtime.Data.Surveying
{

	public class SurveySchedule
	{

		public virtual DateTime? StartDate { get; set; }
		public virtual DateTime? EndDate { get; set; }
		public virtual DateTime? ClientViewDate { get; set; }
		public virtual DateTime? RegistrationCutOffDate { get; set; }

		public virtual string TimeZone { get; set; }


		public virtual TimeZoneInfo TimeZoneInfo
		{
			get { return String.IsNullOrEmpty(TimeZone) ? null : TimeZoneInfo.FindSystemTimeZoneById(TimeZone); }
			set { TimeZone = value == null ? null : value.Id; }
		}

		public virtual DateTime? LocalStartDate
		{
			get { return StartDate.HasValue && TimeZoneInfo != null ? TimeZoneInfo.ConvertTimeFromUtc(StartDate.Value, TimeZoneInfo) : (DateTime?)null; }
			set {
				StartDate = value.HasValue && TimeZoneInfo != null
				            	? TimeZoneInfo.ConvertTimeToUtc(value.Value, TimeZoneInfo)
				            	: (TimeZoneInfo != null ? DateTime.MaxValue : (DateTime?) null);
			}
		}

		public virtual DateTime? LocalEndDate
		{
			get { return EndDate.HasValue && TimeZoneInfo != null ? TimeZoneInfo.ConvertTimeFromUtc(EndDate.Value, TimeZoneInfo) : (DateTime?)null; }
			set {
				EndDate = value.HasValue && TimeZoneInfo != null
				          	? TimeZoneInfo.ConvertTimeToUtc(value.Value, TimeZoneInfo)
				          	: (TimeZoneInfo != null ? DateTime.MaxValue : (DateTime?) null);
			}
		}

		public virtual DateTime? LocalClientViewDate
		{
			get { return ClientViewDate.HasValue && TimeZoneInfo != null ? TimeZoneInfo.ConvertTimeFromUtc(ClientViewDate.Value, TimeZoneInfo) : (DateTime?)null; }
			set {
				ClientViewDate = value.HasValue && TimeZoneInfo != null
				                 	? TimeZoneInfo.ConvertTimeToUtc(value.Value, TimeZoneInfo)
				                 	: (TimeZoneInfo != null ? DateTime.MaxValue : (DateTime?) null);
			}
		}

		public virtual DateTime? LocalRegistrationCutOffDate
		{
			get { return RegistrationCutOffDate.HasValue && TimeZoneInfo != null ? TimeZoneInfo.ConvertTimeFromUtc(RegistrationCutOffDate.Value, TimeZoneInfo) : (DateTime?)null; }
			set {
				RegistrationCutOffDate = value.HasValue && TimeZoneInfo != null
				                         	? TimeZoneInfo.ConvertTimeToUtc(value.Value, TimeZoneInfo)
				                         	: (TimeZoneInfo != null ? DateTime.MaxValue : (DateTime?) null);
			}
		}


		public SurveySchedule()
		{
		}

		public SurveySchedule(SurveySchedule copyFrom)
		{
			Contract.Requires(copyFrom != null, "copyFrom");

			StartDate = copyFrom.StartDate;
			EndDate = copyFrom.EndDate;
			ClientViewDate = copyFrom.ClientViewDate;
			RegistrationCutOffDate = copyFrom.RegistrationCutOffDate;
			TimeZone = copyFrom.TimeZone;
		}

	}

}
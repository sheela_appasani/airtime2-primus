using System.Diagnostics.Contracts;


namespace Airtime.Data.Surveying
{

	public class SurveyStatistics
	{
		public virtual int Invited { get; set; }
		public virtual int Registered { get; set; }
		public virtual int Completed { get; set; }
		public virtual int IosCompleted { get; set; }
		public virtual int AndroidCompleted { get; set; }

		public virtual int WebCompleted { get { return Completed - IosCompleted - AndroidCompleted; } }


		public SurveyStatistics()
		{
		}


		public SurveyStatistics(SurveyStatistics copyFrom)
		{
			Contract.Requires(copyFrom != null, "copyFrom");

			Invited = copyFrom.Invited;
			Registered = copyFrom.Registered;
			Completed = copyFrom.Completed;
			IosCompleted = copyFrom.IosCompleted;
			AndroidCompleted = copyFrom.AndroidCompleted;
		}

	}

}
﻿using System.ComponentModel.DataAnnotations;


namespace Airtime.Data.Surveying
{

	public enum SurveyStatusEnum
	{
		[Display(ResourceType = typeof(Resources.Surveys), Name = "Setup")]
		Setup,

		[Display(ResourceType = typeof(Resources.Surveys), Name = "Preview")]
		Preview,

		[Display(ResourceType = typeof(Resources.Surveys), Name = "Active")]
		Active,

		[Display(ResourceType = typeof(Resources.Surveys), Name = "Ended")]
		Ended,

		[Display(ResourceType = typeof(Resources.Surveys), Name = "Archived")]
		Archived,

		[Display(ResourceType = typeof(Resources.Surveys), Name = "Deleted")]
		Deleted
	}

}

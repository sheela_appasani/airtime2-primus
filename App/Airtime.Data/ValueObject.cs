﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Reflection;

namespace Airtime.Data
{

	/// <summary>
	///     Provides a standard base class for facilitating comparison of value objects using all the object's properties.
	/// 
	///     For a discussion of the implementation of Equals/GetHashCode, see 
	///     http://devlicio.us/blogs/billy_mccafferty/archive/2007/04/25/using-equals-gethashcode-effectively.aspx
	///     and http://groups.google.com/group/sharp-architecture/browse_thread/thread/f76d1678e68e3ece?hl=en for 
	///     an in depth and conclusive resolution.
	/// </summary>
	[Serializable]
	public abstract class ValueObject : BaseObject
	{

		public static bool operator ==(ValueObject valueObject1, ValueObject valueObject2)
		{
			if ((object)valueObject1 == null) {
				return (object)valueObject2 == null;
			}

			return valueObject1.Equals(valueObject2);
		}


		public static bool operator !=(ValueObject valueObject1, ValueObject valueObject2)
		{
			return !(valueObject1 == valueObject2);
		}


		/// <summary>
		///     The getter for SignatureProperties for value objects should include the properties 
		///     which make up the entirety of the object's properties; that's part of the definition 
		///     of a value object.
		/// </summary>
		protected override IEnumerable<PropertyInfo> GetTypeSpecificSignatureProperties()
		{
			return this.GetType().GetProperties();
		}


		public bool Equals(ValueObject other)
		{
			return base.Equals(other);
		}


		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) {
				return false;
			}
			if (ReferenceEquals(this, obj)) {
				return true;
			}
			return Equals(obj as ValueObject);
		}


		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

	}

}
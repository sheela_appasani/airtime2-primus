using System.Reflection;


namespace Airtime.Hubs
{

	public abstract class AssemblyHook
	{
		public static Assembly Assembly { get { return typeof(AssemblyHook).Assembly; } }
	}

}

﻿using System;
using System.Linq;

using Airtime.Core;
using Airtime.Core.Framework.Security;
using Airtime.Core.Framework.SignalR;
using Airtime.Data;
using Airtime.Data.Client;
using Airtime.Data.Account;

using NLog;


namespace Airtime.Hubs
{

	[RequireLogin(Roles="Developer")]
	public class CylonHub : DataHub
	{

		public CylonHub(IUnitOfWork unitOfWork, IMembershipRepository membershipRepository, IChannelRepository channelRepository)
		{
			UnitOfWork = unitOfWork;
			MembershipRepository = membershipRepository;
			ChannelRepository = channelRepository;
		}


		public virtual void Echo(string message)
		{
			var principal = Context.User as AirtimePrincipal;
			if (principal != null && principal.Identity.MembershipId.HasValue) {
				var user = MembershipRepository[principal.Identity.MembershipId.Value];
				Clients.All.AddMessage("From " + user.Username + ": " + message);
			} else {
				Clients.All.AddMessage(message);
			}
		}


		public IMembershipRepository MembershipRepository { get; set; }
		public IChannelRepository ChannelRepository { get; set; }

		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

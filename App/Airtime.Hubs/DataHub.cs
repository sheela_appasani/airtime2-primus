﻿using Airtime.Data;

using AutofacExtensions.Integration.SignalR;


namespace Airtime.Hubs
{

	public abstract class DataHub : LifetimeHub, IDataWorker
	{
		public IUnitOfWork UnitOfWork { get; protected set; }
	}

}

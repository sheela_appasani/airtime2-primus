﻿using Microsoft.AspNet.SignalR;


namespace Airtime.Hubs
{

	public class EchoHub : Hub
	{

		public virtual void Echo(string message)
		{
			Clients.All.AddMessage(message);
		}

	}

}

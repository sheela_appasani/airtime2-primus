﻿using System;

using Airtime.Data;

using Microsoft.AspNet.SignalR.Hubs;

using NLog;


namespace Airtime.Hubs.Framework
{

	internal class DataWorkerPipelineModule : HubPipelineModule
	{

		protected override bool OnBeforeIncoming(IHubIncomingInvokerContext context)
		{
			var unitOfWork = GetUnitOfWork(context.Hub);
			if (unitOfWork != null) {
				unitOfWork.Begin();
			}
			return base.OnBeforeIncoming(context);
		}


		protected override object OnAfterIncoming(object result, IHubIncomingInvokerContext context)
		{
			var unitOfWork = GetUnitOfWork(context.Hub);
			if (unitOfWork != null) {
				unitOfWork.End();
			}
			return base.OnAfterIncoming(result, context);
		}


		protected override void OnIncomingError(Exception ex, IHubIncomingInvokerContext context)
		{
			Log.Error(ex.Message, ex);

			var unitOfWork = GetUnitOfWork(context.Hub);
			if (unitOfWork != null) {
				unitOfWork.Cancel();
			}
			base.OnIncomingError(ex, context);
		}


		private static IUnitOfWork GetUnitOfWork(IHub hub)
		{
			var dataHub = hub as IDataWorker;
			return dataHub != null
				? dataHub.UnitOfWork
				: null;
		}


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

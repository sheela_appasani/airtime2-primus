using System;

using Airtime.Hubs.Framework;

using Autofac;
using Autofac.Integration.SignalR;

using AutofacExtensions.Integration.SignalR;

using Microsoft.AspNet.SignalR.Hubs;


namespace Airtime.Hubs
{

	public class HubsModule : Module
	{

		protected override void Load(ContainerBuilder builder)
		{
			if (builder == null) {
				throw new ArgumentNullException("builder");
			}

			builder.RegisterLifetimeHubManager();
			builder.RegisterType<DataWorkerPipelineModule>().As<IHubPipelineModule>().SingleInstance();
			builder.RegisterHubs(Airtime.Hubs.AssemblyHook.Assembly);
		}

	}

}

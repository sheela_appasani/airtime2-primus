﻿namespace Airtime.Models.Api.v1
{

	public class AcraExceptionApiModel
	{
		
		public string REPORT_ID { get; set; }
		public string APP_VERSION_CODE { get; set; }
		public string APP_VERSION_NAME { get; set; }
		public string PACKAGE_NAME { get; set; }
		public string PHONE_MODEL { get; set; }
		public string ANDROID_VERSION { get; set; }
		public string STACK_TRACE { get; set; }
		public string FILE_PATH { get; set; }
		public string BRAND { get; set; }
		public string PRODUCT { get; set; }
		public string BUILD { get; set; }
		public string TOTAL_MEM_SIZE { get; set; }
		public string AVAILABLE_MEM_SIZE { get; set; }
		public string CUSTOM_DATA { get; set; }
		public string INITIAL_CONFIGURATION { get; set; }
		public string CRASH_CONFIGURATION { get; set; }
		public string DISPLAY { get; set; }
		public string USER_COMMENT { get; set; }
		public string USER_APP_START_DATE { get; set; }
		public string USER_CRASH_DATE { get; set; }
		public string DUMPSYS_MEMINFO { get; set; }
		public string DROPBOX { get; set; }
		public string LOGCAT { get; set; }
		public string EVENTSLOG { get; set; }
		public string RADIOLOG { get; set; }
		public string IS_SILENT { get; set; }
		public string DEVICE_ID { get; set; }
		public string INSTALLATION_ID { get; set; }
		public string USER_EMAIL { get; set; }
		public string DEVICE_FEATURES { get; set; }
		public string ENVIRONMENT { get; set; }
		public string SHARED_PREFERENCES { get; set; }
		public string SETTINGS_SYSTEM { get; set; }
		public string SETTINGS_SECURE { get; set; }
		public string APPLICATION_LOG { get; set; }
		public string MEDIA_CODEC_LIST { get; set; }
		public string THREAD_DETAILS { get; set; }
 
	}

}

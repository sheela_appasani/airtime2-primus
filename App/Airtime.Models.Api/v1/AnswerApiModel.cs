using System;


namespace Airtime.Models.Api.v1
{

	public class AnswerApiModel
	{

		public int Id { get; set; }
		public DateTime DateCreated { get; set; }
		public DateTime DateModified { get; set; }

		public int QuestionId { get; set; }
		public int? ChoiceId { get; set; }
		public string Text { get; set; }


		public AnswerApiModel()
		{
		}


		public AnswerApiModel(AnswerApiModel copyFrom)
		{
			Id = copyFrom.Id;
			DateCreated = copyFrom.DateCreated;
			DateModified = copyFrom.DateModified;

			QuestionId = copyFrom.QuestionId;
			ChoiceId = copyFrom.ChoiceId;
			Text = copyFrom.Text;
		}

	}

}

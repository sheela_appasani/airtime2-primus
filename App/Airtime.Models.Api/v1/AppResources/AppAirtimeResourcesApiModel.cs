﻿using System;
using System.Collections.Generic;


namespace Airtime.Models.Api.v1.AppResources
{

	public class AppAirtimeResourcesApiModel : AppResourcesApiModel
	{

		public AppAirtimeResourcesApiModel()
		{
		}


		public AppAirtimeResourcesApiModel(string recoverPasswordUrl)
		{
			Generic = new GenericResourcesApiModel {
				Next = "Next",
				Loading = "Loading...",
				Home = "Home",
				Login = "Login",
				Logout = "Logout"
			};
			LoginPage = new LoginPageResourcesApiModel {
				Welcome = "Welcome to Airtime. Enter the email address and password that you use for taking surveys.",
				Login = "Login",
				Cancel = "Cancel",
				RememberMe = "Remember Me",
				InvalidEmailAddress = "Oops! Invalid Email address",
				PasswordCannotBeBlank = "Oops! Password cannot be blank!",
				NotAMember = "Not a Member?",
				YouNeedToRegister = "You need to register with a station near you.",
				RegistrationLinkText = "Visit AirtimeSurveys.com to find out more.",
				RegistrationLinkUrl = "http://airtimesurveys.com/androidappstations.aspx",
                ForgotPasswordLinkText = "Don't Know Your Password? Click here",
				ForgotPasswordLinkUrl = recoverPasswordUrl,
				EmailAddressHint = "Email Address",
				PasswordHint = "Password",
				Enter = "Enter",
				Buttons = new List<ButtonResourcesApiModel> {
					new ButtonResourcesApiModel {
						Name = "PrivacyPolicyButton",
						Text = "Privacy",
						Url = "http://airtimesurveys.com/privacy.aspx"
					},
					new ButtonResourcesApiModel {
						Name = "RulesButton",
						Text = "Rules",
						Url = "http://airtimesurveys.com/contest-rules.aspx"
					},
					new ButtonResourcesApiModel {
						Name = "FaqButton",
						Text = "FAQ",
						Url = "http://airtimesurveys.com/faq.aspx"
					}
				}
			};
			ChannelsPage = new ChannelsPageResourcesApiModel {
				Title = "Surveys",
				RefreshHint = "Refresh",
				SurveyAvailable = "Survey available",
				NoSurvey = "No survey",
				RetrievingData = "Retrieving data from server..."
			};
			SurveyPage = new SurveyPageResourcesApiModel {
				Title = "Survey",
				SliderNotMovedTitle = "Slider Not Moved",
				SliderNotMovedMessage = "Please move the slider to continue"
			};
			SurveyCompletedPage = new SurveyCompletedPageResourcesApiModel {
				Title = "Survey Completed",
				ThankYou = "Thank You",
				ListenToSongs = "Listen to Songs",
				ReferAFriend = "Refer a Friend"
			};
			ListenToSongsPage = new ListenToSongsPageResourcesApiModel {
				Title = "Listen to Songs"
			};
			Exceptions = new ExceptionResourcesApiModel {
				Error = "Error",
				WebService_Error = "Web Service Error",
				Download_Error = "Download Error",
				GCM_Error = "GCM Error",
				Cache_Error = "Oops! Unable to initialise the application. Please try again",
				Sync_Error = "Sync Error",
				Authentication_Error = "Cannot reach the server",
				Zip_Error = "Zip Error",
				Audio_Error = "Audio Error",
				Mplayer_Error = "Mplayer Error",
				Initialisation_Error = "Oops! Unable to initialise the application. Please ensure you have an active connection...",
				FileDownload_Error = "Unable to retrieve file from server",
				Data_Error = "There was a problem getting data from the server...",
				Network_Error = "Network unavailable",
				Configuration_Error = "Unable to configure application",
				EmailPassword_Error = "The email address/password are incorrect!",
				NetworkAccount_Error = "Network unavailable and local account not found",
				Account_Error = "No account found for PersonId",
				ServerAuthentication_Error = "Unable to connect. Check your User and Password",
				Application_Error = "Application Error",
				Application_Quit = "Quit"
			};
		}

	}

}

﻿using System;
using System.Collections.Generic;


namespace Airtime.Models.Api.v1.AppResources
{

	public class AppPilkadaResourcesApiModel : AppResourcesApiModel
	{

		public AppPilkadaResourcesApiModel()
		{
		}


		public AppPilkadaResourcesApiModel(string recoverPasswordUrl)
		{
			Generic = new GenericResourcesApiModel {
				Next = "Berikutnya",
				Loading = "Pemuatan...",
				Home = "Beranda",
				Login = "Masuk",
				Logout = "Keluar"
			};
			LoginPage = new LoginPageResourcesApiModel {
				Welcome = "Selamat datang di Pilkada. Masukkan alamat email dan password yang Anda gunakan untuk mengambil survei.",
				Login = "Masuk",
				Cancel = "Batal",
				RememberMe = "Ingat saya",
				InvalidEmailAddress = "Ups! Alamat Email tidak valid",
				PasswordCannotBeBlank = "Ups! Password tidak boleh kosong!",
				NotAMember = "Bukan Anggota?",
				YouNeedToRegister = "Anda harus mendaftar terlebih dahulu, silahkan kunjungi website kami.",
				RegistrationLinkText = "Kunjungi 101jakfm.com untuk mengetahui lebih lanjut.",
				RegistrationLinkUrl = "http://101jakfm.com",
				ForgotPasswordLinkText = "Lupa Password? Klik di sini",
				ForgotPasswordLinkUrl = recoverPasswordUrl,
				EmailAddressHint = "Alamat Email",
				PasswordHint = "Kata sandi",
				Enter = "Masuk",
				Buttons = new List<ButtonResourcesApiModel> {
					new ButtonResourcesApiModel {
						Name = "PrivacyPolicyButton",
						Text = "Pribadi",
						Url = "http://airtimesurveys.com/privacy.aspx"
					},
					new ButtonResourcesApiModel {
						Name = "RulesButton",
						Text = "Aturan",
						Url = "http://airtimesurveys.com/contest-rules.aspx"
					},
					new ButtonResourcesApiModel {
						Name = "FaqButton",
						Text = "F.A.Q",
						Url = "http://airtimesurveys.com/faq.aspx"
					}
				}
			};
			ChannelsPage = new ChannelsPageResourcesApiModel {
				Title = "Survei",
				RefreshHint = "Memuat ulang",
				SurveyAvailable = "Survei tersedia",
				NoSurvey = "Tidak ada Survei",
				RetrievingData = "Mengambil data dari server..."
			};
			SurveyPage = new SurveyPageResourcesApiModel {
				Title = "Survei",
				SliderNotMovedTitle = "Slider tidak bergerak",
				SliderNotMovedMessage = "Silakan pindahkan slider untuk melanjutkan"
			};
			SurveyCompletedPage = new SurveyCompletedPageResourcesApiModel {
				Title = "Survei Selesai",
				ThankYou = "Terima kasih",
				ListenToSongs = "Mendengarkan Lagu",
				ReferAFriend = "Merujuk teman"
			};
			ListenToSongsPage = new ListenToSongsPageResourcesApiModel {
				Title = "Dengarkan Lagu"
			};
			Exceptions = new ExceptionResourcesApiModel {
				Error = "Kesalahan",
				WebService_Error = "Kesalahan Layanan Jaringan",
				Download_Error = "Kesalahan Unduh",
				GCM_Error = "Kesalahan GCM",
				Cache_Error = "Ups! Tidak dapat menginisialisasi aplikasi. Silakan coba lagi",
				Sync_Error = "Kesalahan Sikronisasi",
				Authentication_Error = "Tidak dapat mencapai server",
				Zip_Error = "Kesalahan Zip",
				Audio_Error = "Kesalahan Audio",
				Mplayer_Error = "Kesalahan Mplayer",
				Initialisation_Error = "Ups! Tidak dapat menginisialisasi aplikasi. Pastikan Anda memiliki sambungan aktif...",
				FileDownload_Error = "Tidak dapat mengambil file dari Server",
				Data_Error = "Ada masalah mendapatkan data dari Server...",
				Network_Error = "Jaringan tidak tersedia",
				Configuration_Error = "Tidak dapat mengkonfigurasikan aplikasi",
				EmailPassword_Error = "Alamat Email/Kata sandi salah!",
				NetworkAccount_Error = "Jaringan tidak tersedia dan server tidak ditemukan",
				Account_Error = "Akun \"PersonId\" tidak dapat ditemukan",
				ServerAuthentication_Error = "Tidak dapat melakukan otentifikasi dengan server",
				Application_Error = "Kesalahan Aplikasi",
				Application_Quit = "Berhenti"
			};
		}

	}

}

﻿using System.Xml.Serialization;


namespace Airtime.Models.Api.v1.AppResources
{

	public class AppResourcesApiModel
	{
		public GenericResourcesApiModel Generic { get; set; }
		public LoginPageResourcesApiModel LoginPage { get; set; }
		public ChannelsPageResourcesApiModel ChannelsPage { get; set; }
		public SurveyPageResourcesApiModel SurveyPage { get; set; }
		public SurveyCompletedPageResourcesApiModel SurveyCompletedPage { get; set; }
		public ListenToSongsPageResourcesApiModel ListenToSongsPage { get; set; }
		public ExceptionResourcesApiModel Exceptions { get; set; }
	}

}

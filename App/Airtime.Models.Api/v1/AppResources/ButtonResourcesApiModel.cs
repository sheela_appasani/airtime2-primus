﻿namespace Airtime.Models.Api.v1.AppResources
{

	public class ButtonResourcesApiModel
	{
		public string Name { get; set; }
		public string Text { get; set; }
		public string Url { get; set; }
	}

}

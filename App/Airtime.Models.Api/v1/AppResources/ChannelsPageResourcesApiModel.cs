﻿namespace Airtime.Models.Api.v1.AppResources
{

	public class ChannelsPageResourcesApiModel
	{
		public string Title { get; set; }
		public string RefreshHint { get; set; }
		public string SurveyAvailable { get; set; }
		public string NoSurvey { get; set; }
		public string RetrievingData { get; set; }
	}

}

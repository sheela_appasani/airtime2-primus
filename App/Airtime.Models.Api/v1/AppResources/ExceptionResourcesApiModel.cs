﻿namespace Airtime.Models.Api.v1.AppResources
{

	public class ExceptionResourcesApiModel
	{
		public string Error { get; set; }
		public string WebService_Error { get; set; }
		public string Download_Error { get; set; }
		public string GCM_Error { get; set; }
		public string Cache_Error { get; set; }
		public string Sync_Error { get; set; }
		public string Authentication_Error { get; set; }
		public string Zip_Error { get; set; }
		public string Audio_Error { get; set; }
		public string Mplayer_Error { get; set; }
		public string Initialisation_Error { get; set; }
		public string FileDownload_Error { get; set; }
		public string Data_Error { get; set; }
		public string Network_Error { get; set; }
		public string Configuration_Error { get; set; }
		public string EmailPassword_Error { get; set; }
		public string NetworkAccount_Error { get; set; }
		public string Account_Error { get; set; }
		public string ServerAuthentication_Error { get; set; }
		public string Application_Error { get; set; }
		public string Application_Quit { get; set; }
	}

}

﻿namespace Airtime.Models.Api.v1.AppResources
{

	public class GenericResourcesApiModel
	{
		public string Next { get; set; }
		public string Loading { get; set; }
		public string Home { get; set; }
		public string Login { get; set; }
		public string Logout { get; set; }
	}

}

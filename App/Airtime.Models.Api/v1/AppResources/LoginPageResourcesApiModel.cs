﻿using System.Collections.Generic;


namespace Airtime.Models.Api.v1.AppResources
{

	public class LoginPageResourcesApiModel
	{
		public string Welcome { get; set; }

		public string Login { get; set; }
		public string Cancel { get; set; }
		public string RememberMe { get; set; }
		public string InvalidEmailAddress { get; set; }
		public string PasswordCannotBeBlank { get; set; }

		public string NotAMember { get; set; }
		public string YouNeedToRegister { get; set; }
		public string RegistrationLinkText { get; set; }
		public string RegistrationLinkUrl { get; set; }
		public string ForgotPasswordLinkText { get; set; }
		public string ForgotPasswordLinkUrl { get; set; }

		public string EmailAddressHint { get; set; }
		public string PasswordHint { get; set; }
		public string Enter { get; set; }

		public List<ButtonResourcesApiModel> Buttons { get; set; }
	}

}

﻿namespace Airtime.Models.Api.v1.AppResources
{

	public class SurveyCompletedPageResourcesApiModel
	{
		public string Title { get; set; }
		public string ThankYou { get; set; }
		public string ListenToSongs { get; set; }
		public string ReferAFriend { get; set; }
	}

}

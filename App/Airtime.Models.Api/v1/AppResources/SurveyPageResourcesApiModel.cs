﻿namespace Airtime.Models.Api.v1.AppResources
{

	public class SurveyPageResourcesApiModel
	{
		public string Title { get; set; }
		public string SliderNotMovedTitle { get; set; }
		public string SliderNotMovedMessage { get; set; }
	}

}

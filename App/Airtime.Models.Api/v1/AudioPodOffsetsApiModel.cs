namespace Airtime.Models.Api.v1
{

	public class AudioPodOffsetsApiModel
	{

		public int SliderTopOffsetY { get; set; }
		public int SliderBottomOffsetY { get; set; }
		public int SliderCenterOffsetX { get; set; }

		public int ScoreCenterOffsetX { get; set; }

		public int HandleCenterOffsetX { get; set; }
		public int HandleCenterOffsetY { get; set; }


		//public AudioPodOffsetsApiModel(AudioPodOffsetsApiModel copyFrom)
		//{
		//	SliderTopOffsetY = copyFrom.SliderTopOffsetY;
		//	SliderBottomOffsetY = copyFrom.SliderBottomOffsetY;
		//	SliderCenterOffsetX = copyFrom.SliderCenterOffsetX;

		//	ScoreCenterOffsetX = copyFrom.ScoreCenterOffsetX;

		//	HandleCenterOffsetX = copyFrom.HandleCenterOffsetX;
		//	HandleCenterOffsetY = copyFrom.HandleCenterOffsetY;
		//}
	
	}

}

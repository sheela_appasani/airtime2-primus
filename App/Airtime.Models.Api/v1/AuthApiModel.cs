namespace Airtime.Models.Api.v1
{

	public class AuthApiModel
	{
		public int PersonId { get; set; }
		public string AuthToken { get; set; }
	}

}

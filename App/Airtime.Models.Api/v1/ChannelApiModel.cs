﻿using System;

using Airtime.Data.Client;


namespace Airtime.Models.Api.v1
{
	
	public class ChannelApiModel
	{

		public int Id { get; set; }
		public DateTime DateCreated { get; set; }
		public DateTime DateModified { get; set; }

		public string Name { get; set; }

		public string HomePageUrl { get; set; }
		public string ContestRulesUrl { get; set; }
		public string PrivacyPolicyUrl { get; set; }
		public string ContactDetailsUrl { get; set; }
		public string LogoUrl { get; set; }

		public string TimeZone { get; set; }

		public bool FastTrackPods { get; set; }

		public ChannelContentsApiModel Contents { get; set; }


		public ChannelApiModel(Channel copyFrom)
		{
			Id = copyFrom.Id;
			DateCreated = copyFrom.DateCreated;
			DateModified = copyFrom.DateModified;

			Name = copyFrom.Name;
			HomePageUrl = copyFrom.HomePageUrl;
			ContestRulesUrl = copyFrom.ContestRulesUrl;
			PrivacyPolicyUrl = copyFrom.PrivacyPolicyUrl;
			ContactDetailsUrl = copyFrom.ContactDetailsUrl;
			LogoUrl = copyFrom.LogoUrl;

			TimeZone = copyFrom.TimeZone;

			FastTrackPods = copyFrom.FastTrackPods;

			Contents = new ChannelContentsApiModel(copyFrom.Contents);
		}

	}

}

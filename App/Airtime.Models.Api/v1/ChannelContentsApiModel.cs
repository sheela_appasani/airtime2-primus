﻿using System.Collections.Generic;
using System.Linq;

using Airtime.Data.Client;
using Airtime.Data.Content;

using Xtra.Common;


namespace Airtime.Models.Api.v1
{

	public class ChannelContentsApiModel
	{

		public string ReferFriendsEmailSubject { get; set; }
		public string ReferFriendsEmailBody { get; set; }


		public ChannelContentsApiModel()
		{
		}


		public ChannelContentsApiModel(IEnumerable<CustomContent> copyFrom)
		{
			ReferFriendsEmailSubject = copyFrom.FirstOrDefault(x => x.ContentType == ContentTypeEnum.Email_ReferFriendSubject).IfNotNull(x => x.Text);
			ReferFriendsEmailBody = copyFrom.FirstOrDefault(x => x.ContentType == ContentTypeEnum.Email_ReferFriend).IfNotNull(x => x.Text);
		}

	}

}

﻿using System;
using System.Collections.Generic;
using System.Linq;

using Airtime.Data.Surveying.Questions;


namespace Airtime.Models.Api.v1
{

	public class QuestionApiModel
	{

		public int Id { get; set; }
		public DateTime DateCreated { get; set; }
		public DateTime DateModified { get; set; }

		public string Name { get; set; }
		public string Text { get; set; }
		public int Position { get; set; }
		public bool Required { get; set; }

		public int SurveyId { get; set; }

		public QuestionCategoryEnum Category { get; set; }
		public QuestionTypeEnum QuestionType { get; set; }
		public QuestionProgressionEnum Progression { get; set; }

		public SurveyBurnButtonsApiModel BurnButtons { get; set; }

		public SongApiModel Audio { get; set; }
		public List<SongApiModel> AudioPlaylist { get; set; }
		public List<QuestionChoiceApiModel> Choices { get; set; }


		public QuestionApiModel(QuestionBase copyFrom)
		{
			Id = copyFrom.Id;
			DateCreated = copyFrom.DateCreated;
			DateModified = copyFrom.DateModified;

			Name = copyFrom.Name;
			Text = copyFrom.Text;
			Position = copyFrom.Position;
			Required = copyFrom.Required;

			SurveyId = copyFrom.Survey.Id;

			Category = copyFrom.Category;
			Progression = copyFrom.Progression;

			QuestionType = NormalizeQuestionType(copyFrom.QuestionType);

			if (copyFrom.Song != null) {
				Audio = new SongApiModel(copyFrom.Song);
				if (QuestionType == QuestionTypeEnum.RADIO) {
					QuestionType = QuestionTypeEnum.AUDIO;
				}
			}

			var audioPodQuestion = copyFrom as AudioPodQuestion;
			if (audioPodQuestion != null) {
				AudioPlaylist = new List<SongApiModel>(audioPodQuestion.Songs.Select(x => new SongApiModel(x)));
				if (audioPodQuestion.BurnButtons != null) {
					BurnButtons = new SurveyBurnButtonsApiModel(audioPodQuestion.BurnButtons);
				}
			}

			var multiChoiceQuestion = copyFrom as MultiChoiceQuestion;
			if (multiChoiceQuestion != null) {
				Choices = new List<QuestionChoiceApiModel>(multiChoiceQuestion.Choices.Select(x => new QuestionChoiceApiModel(x)));
			}
		}


		private QuestionTypeEnum NormalizeQuestionType(QuestionTypeEnum questionType)
		{
			switch (questionType) {
				case QuestionTypeEnum.CUSTOMCHECKBOX:
					return QuestionTypeEnum.CHECKBOX;

				case QuestionTypeEnum.CUSTOMRADIO:
					return QuestionTypeEnum.RADIO;

				case QuestionTypeEnum.CUSTOMTEXT:
					return QuestionTypeEnum.TEXT;

				default:
					return questionType;
			}
		}

	}

}

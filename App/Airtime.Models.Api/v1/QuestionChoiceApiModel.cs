using System;

using Airtime.Data.Surveying.Questions;


namespace Airtime.Models.Api.v1
{

	public class QuestionChoiceApiModel
	{
		public int Id { get; set; }
		public DateTime DateCreated { get; set; }
		public DateTime DateModified { get; set; }

		public string Name { get; set; }
		public string Code { get; set; }
		public int Position { get; set; }

		public int QuestionId { get; set; }
		public int? BranchQuestionId { get; set; }
		public int? TemplateChoiceId { get; set; }


		public QuestionChoiceApiModel(QuestionChoice copyFrom)
		{
			Id = copyFrom.Id;
			DateCreated = copyFrom.DateCreated;
			DateModified = copyFrom.DateModified;

			Name = copyFrom.Name;
			Code = copyFrom.Code;
			Position = copyFrom.Position;

			QuestionId = copyFrom.Question.Id;
			BranchQuestionId = copyFrom.BranchQuestion == null ? (int?)null : copyFrom.BranchQuestion.Id;
			TemplateChoiceId = copyFrom.TemplateChoice == null ? (int?)null : copyFrom.TemplateChoice.Id;
		}

	}

}

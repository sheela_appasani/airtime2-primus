using System;

using Airtime.Data.Media;


namespace Airtime.Models.Api.v1
{

	public class SongApiModel
	{

		public int Id { get; set; }
		public DateTime DateCreated { get; set; }
		public DateTime DateModified { get; set; }

		public string Title { get; set; }
		public string Artist { get; set; }

		public string Filename { get; set; }
		public int Seconds { get; set; }


		public SongApiModel(Song copyFrom)
		{
			Id = copyFrom.Id;
			DateCreated = copyFrom.DateCreated;
			DateModified = copyFrom.DateModified;

			Title = copyFrom.Title;
			Artist = copyFrom.Artist;

			Filename = copyFrom.Filename;
			Seconds = copyFrom.Length;
		}


		public SongApiModel()
		{
		}

	}

}

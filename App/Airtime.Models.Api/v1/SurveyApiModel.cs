﻿using System;

using Airtime.Data.Surveying;


namespace Airtime.Models.Api.v1
{
	
	public class SurveyApiModel
	{

		public int Id { get; set; }
		public DateTime DateCreated { get; set; }
		public DateTime DateModified { get; set; }

		public int ChannelId { get; set; }

		public SurveyScheduleApiModel Schedule { get; set; }
		public SurveyBurnButtonsApiModel BurnButtons { get; set; }
		public SurveyOptionsApiModel Options { get; set; }

		public string SurveyCompletedText { get; set; }


		public SurveyApiModel(Survey copyFrom)
		{
			Id = copyFrom.Id;
			DateCreated = copyFrom.DateCreated;
			DateModified = copyFrom.DateModified;

			ChannelId = copyFrom.Channel.Id;

			Schedule = new SurveyScheduleApiModel(copyFrom.Schedule);
			BurnButtons = new SurveyBurnButtonsApiModel(copyFrom.BurnButtons);
			Options = new SurveyOptionsApiModel(copyFrom.Options);
		}

	}

}

using Airtime.Data.Surveying;


namespace Airtime.Models.Api.v1
{

	public class SurveyBurnButtonsApiModel
	{

		public string NotTiredText { get; set; }
		public string LittleTiredText { get; set; }
		public string VeryTiredText { get; set; }
		public string UnfamiliarText { get; set; }

		public bool ShowNotTired { get; set; }
		public bool ShowLittleTired { get; set; }
		public bool ShowVeryTired { get; set; }
		public bool ShowUnfamiliar { get; set; }


		public SurveyBurnButtonsApiModel(SurveyBurnButtons copyFrom)
		{
			NotTiredText = copyFrom.NotTiredText;
			LittleTiredText = copyFrom.LittleTiredText;
			VeryTiredText = copyFrom.VeryTiredText;
			UnfamiliarText = copyFrom.UnfamiliarText;
			ShowNotTired = copyFrom.ShowNotTired;
			ShowLittleTired = copyFrom.ShowLittleTired;
			ShowVeryTired = copyFrom.ShowVeryTired;
			ShowUnfamiliar = copyFrom.ShowUnfamiliar;
		}

	}

}

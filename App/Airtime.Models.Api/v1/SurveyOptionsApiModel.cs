using Airtime.Data.Surveying;


namespace Airtime.Models.Api.v1
{

	public class SurveyOptionsApiModel
	{

		public bool RandomiseSongLists { get; set; }
		public bool ShowSongSummaryAtEnd { get; set; }
		public bool EnableBurnButtons { get; set; }
		public bool EnableSystemCheck { get; set; }
		public bool EnableProgressBar { get; set; }
		public int MaxIdleSongRatings { get; set; }


		public SurveyOptionsApiModel(SurveyOptions copyFrom)
		{
			RandomiseSongLists = copyFrom.RandomiseSongLists;
			ShowSongSummaryAtEnd = copyFrom.ShowSongSummaryAtEnd;
			EnableBurnButtons = copyFrom.EnableBurnButtons;
			EnableSystemCheck = copyFrom.EnableSystemCheck;
			EnableProgressBar = copyFrom.EnableProgressBar;
			MaxIdleSongRatings = copyFrom.MaxIdleSongScores;
		}

	}

}

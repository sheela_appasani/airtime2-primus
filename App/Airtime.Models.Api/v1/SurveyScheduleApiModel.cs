using System;

using Airtime.Data.Surveying;


namespace Airtime.Models.Api.v1
{

	public class SurveyScheduleApiModel
	{

		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }
		public DateTime? ClientViewDate { get; set; }
		public DateTime? RegistrationCutOffDate { get; set; }

		public string TimeZone { get; set; }


		public SurveyScheduleApiModel(SurveySchedule copyFrom)
		{
			StartDate = copyFrom.StartDate;
			EndDate = copyFrom.EndDate;
			ClientViewDate = copyFrom.ClientViewDate;
			RegistrationCutOffDate = copyFrom.RegistrationCutOffDate;
			TimeZone = copyFrom.TimeZone;
		}

	}

}

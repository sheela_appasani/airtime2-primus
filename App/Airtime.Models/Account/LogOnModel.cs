﻿using System;
using System.ComponentModel.DataAnnotations;


namespace Airtime.Models.Account
{

	public class LogOnModel
	{
		[Required]
		[Display(ResourceType = typeof(Resources.Account), Name = "EmailAddress")]
		public string Email { get; set; }

		[Required]
		[Display(ResourceType = typeof(Resources.Account), Name = "Username")]
		public string Username { get; set; }

		[Required]
		[DataType(DataType.Password)]
		[Display(ResourceType = typeof(Resources.Account), Name = "Password")]
		public string Password { get; set; }

		[DataType(DataType.Password)]
		public Guid? Guid { get; set; }

		[Display(ResourceType = typeof(Resources.Account), Name = "RememberMe")]
		public bool RememberMe { get; set; }
	}

}

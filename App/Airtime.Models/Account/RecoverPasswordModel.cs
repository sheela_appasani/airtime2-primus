﻿using DataAnnotationsExtensions;


namespace Airtime.Models.Account
{
	public class RecoverPasswordModel
	{
		[Email]
		public string Email { get; set; }
	}
}

namespace Airtime.Models
{

	public class DataColumnModel
	{
		public string Property { get; set; }
		public string QueryOn { get; set; }
		public string Title { get; set; }
		public string CssClass { get; set; }
		public string Template { get; set; }
		public bool Selectable { get; set; }
		public bool Sortable { get; set; }
		public DataTableSortDirection DefaultSort { get; set; }
	}

}
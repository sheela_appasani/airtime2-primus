using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

using Airtime.Data.Account;
using Airtime.Data.Messaging;
using Airtime.Models.Members;


namespace Airtime.Models.Emails
{

	public class EmailBlastEditModel
	{
		public int Id { get; set; }

		public bool MakeCopy { get; set; }
		public bool Disabled { get; set; }


		[AllowHtml]
		[Required]
		[UIHint("RichText")]
		[Display(ResourceType = typeof(Resources.Emails), Name = "EmailBody")]
		public string Body { get; set; }

		[Required]
		[StringLength(255)]
		[Display(ResourceType = typeof(Resources.Emails), Name = "EmailSubject")]
		public string Subject { get; set; }

		[Required]
		[UIHint("DateTimePicker")]
		[Display(ResourceType = typeof(Resources.Emails), Name = "StartDate")]
		public string ScheduleStartDate { get; set; }

		[UIHint("DateTimePicker")]
		[Display(ResourceType = typeof(Resources.Emails), Name = "EndDate")]
		public string ScheduleEndDate { get; set; }

		[Display(ResourceType = typeof(Resources.Emails), Name = "DeliveryStatus")]
		public EmailBlastStatusEnum Status { get; set; }

		[Display(ResourceType = typeof(Resources.Emails), Name = "DeliveryStatus")]
		public string StatusName { get; set; }

		[Display(ResourceType = typeof(Resources.Emails), Name = "SendTo")]
		public RecipientSourceEnum RecipientSource { get; set; }

		[Display(ResourceType = typeof(Resources.Emails), Name = "SendTo")]
		public string RecipientSourceName { get; set; }

		[Display(ResourceType = typeof(Resources.Emails), Name = "SendTo")]
		public List<RecipientSourceEnum> AvailableRecipientSources { get; set; }

		[Range((double)0, (double)100)]
		[Display(ResourceType = typeof(Resources.Emails), Name = "PercentToEmail")]
		public double TargetPercentage { get; set; }

		[Display(ResourceType = typeof(Resources.Emails), Name = "DontUseEmailTemplate")]
		public bool NoServerSideTemplate { get; set; }

		public bool GotTemplate { get; set; }


		public virtual EmailBlastTargetEnum CriteriaTarget { get; set; }

		[Display(ResourceType = typeof(Resources.Emails), Name = "Gender")]
		public virtual PersonGenderEnum? CriteriaGender { get; set; }

		[Display(ResourceType = typeof(Resources.Emails), Name = "AgeRange")]
		public virtual int? CriteriaLowerAge { get; set; }
		[Display(ResourceType = typeof(Resources.Emails), Name = "AgeRange")]
		public virtual int? CriteriaUpperAge { get; set; }

		[Display(ResourceType = typeof(Resources.Emails), Name = "BirthdayStartDate")]
		public virtual DateTime? CriteriaLowerBirthday { get; set; }
		[Display(ResourceType = typeof(Resources.Emails), Name = "BirthdayEndDate")]
		public virtual DateTime? CriteriaUpperBirthday { get; set; }

		[Display(ResourceType = typeof(Resources.Emails), Name = "PostalCodes")]
		public virtual string CriteriaPostcodes { get; set; }

		[Display(ResourceType = typeof(Resources.Emails), Name = "SubscribedToSurveys")]
		public virtual bool? CriteriaOptInSurveys { get; set; }

		[Display(ResourceType = typeof(Resources.Emails), Name = "SubscribedToNewsletters")]
		public virtual bool? CriteriaOptInNewsletters { get; set; }

		[Display(ResourceType = typeof(Resources.Emails), Name = "NumberOfMembers")]
		public virtual int? CriteriaNumberOfMembers { get; set; }

		[UIHint("DateTimePicker")]
		[Display(ResourceType = typeof(Resources.Emails), Name = "CompletedSurveyBetweenDates")]
		public virtual string CriteriaLowerCompletedSurveyDate { get; set; }

		[UIHint("DateTimePicker")]
		[Display(ResourceType = typeof(Resources.Emails), Name = "CompletedSurveyBetweenDates")]
		public virtual string CriteriaUpperCompletedSurveyDate { get; set; }


		public virtual List<UserModel> Users { get; set; }
	}

}

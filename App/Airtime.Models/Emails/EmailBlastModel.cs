using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

using Airtime.Data.Messaging;


namespace Airtime.Models.Emails
{

	public class EmailBlastModel
	{
		public int Id { get; set; }

		public bool MakeCopy { get; set; }
		public bool Disabled { get; set; }

		[AllowHtml]
		[Required]
		[UIHint("RichText")]
		[Display(ResourceType = typeof(Resources.Emails), Name = "EmailBody")]
		public string Body { get; set; }

		[Required]
		[StringLength(255)]
		[Display(ResourceType = typeof(Resources.Emails), Name = "EmailSubject")]
		public string Subject { get; set; }

		[UIHint("DateTimePicker")]
		[Display(ResourceType = typeof(Resources.Emails), Name = "StartDate")]
		public string ScheduleStartDate { get; set; }

		[UIHint("DateTimePicker")]
		[Display(ResourceType = typeof(Resources.Emails), Name = "EndDate")]
		public string ScheduleEndDate { get; set; }

		[Display(ResourceType = typeof(Resources.Emails), Name = "DeliveryStatus")]
		public EmailBlastStatusEnum Status { get; set; }

		[Display(ResourceType = typeof(Resources.Emails), Name = "DeliveryStatus")]
		public string StatusName { get; set; }

		[Display(ResourceType = typeof(Resources.Emails), Name = "SendTo")]
		public RecipientSourceEnum RecipientSource { get; set; }

		[Display(ResourceType = typeof(Resources.Emails), Name = "SendTo")]
		public string RecipientSourceName { get; set; }

		[Display(ResourceType = typeof(Resources.Emails), Name = "SendTo")]
		public List<RecipientSourceEnum> AvailableRecipientSources { get; set; }

		[Display(ResourceType = typeof(Resources.Emails), Name = "DontUseEmailTemplate")]
		public bool NoServerSideTemplate { get; set; }

		public bool GotTemplate { get; set; }

		[Range((double)0, (double)100)]
		[Display(ResourceType = typeof(Resources.Emails), Name = "PercentToEmail")]
		public double TargetPercentage { get; set; }

		public int StatisticsScheduled { get; set; }
		public int StatisticsDelivered { get; set; }
		public int StatisticsFailed { get; set; }
		public int StatisticsCancelled { get; set; }

		public string SurveyName { get; set; }
		public string SurveyScheduleStartTicks { get; set; }
		public string SurveyScheduleEndTicks { get; set; }
		public string PageCreateTicks { get; set; }
	}

}

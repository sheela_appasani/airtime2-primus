namespace Airtime.Models.Error
{

	public class NotFoundModel
	{
		public string RequestedUrl { get; set; }
		public string ReferrerUrl { get; set; }
	}

}
using System.ComponentModel.DataAnnotations;


namespace Airtime.Models.Media
{

	public class SongModel
	{
		public int Id { get; set;}

		[Required]
		[Display(ResourceType = typeof(Resources.Media), Name = "Artist")]
		public string Artist { get; set; }

		[Required]
		[Display(ResourceType = typeof(Resources.Media), Name = "Title")]
		public string Title { get; set; }

		[Display(ResourceType = typeof(Resources.Media), Name = "Year")]
		public int? Year { get; set; }

		[Display(ResourceType = typeof(Resources.Media), Name = "Genre")]
		public string Genre { get; set; }

		[Display(ResourceType = typeof(Resources.Media), Name = "Tempo")]
		public string Tempo { get; set; }

		public string Filename { get; set; }
		public string Library { get; set; }

		public int Length { get; set; }

		public int CompanyId { get; set; }
		public string CompanyName { get; set; }

		public string DateCreated { get; set; }

	}

}
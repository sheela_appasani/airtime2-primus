using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

using Airtime.Data.Account;


namespace Airtime.Models.Members
{

	public class MemberFilterModel
	{

		public IEnumerable<SelectListItem> SurveyDropDownList;
		public IEnumerable<SelectListItem> CountryDropDownList;
		public IEnumerable<SelectListItem> BirthMonthDropDownList;
		public IEnumerable<SelectListItem> BirthDayOfMonthDropDownList;

		[UIHint("BooleanNullableRadioButtons")]
		[Display(ResourceType = typeof(Resources.Members), Name = "Enabled")]
		public bool? Enabled { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "Gender")]
		public PersonGenderEnum? Gender { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "Username")]
		public string Username { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "Email")]
		public string Email { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "GivenName")]
		public string GivenName { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "FamilyName")]
		public string FamilyName { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "DaytimePhone")]
		public string DaytimePhone { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "MobilePhone")]
		public string MobilePhone { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "StreetAddress")]
		public string Address { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "Suburb")]
		public string Suburb { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "City")]
		public string City { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "State")]
		public string State { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "Country")]
		public string Country { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "Postcode")]
		public string Postcode { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "UserSurveyStatus")]
		public UserSurveyStatusEnum? UserSurveyStatus { get; set; }

		[Display(ResourceType = typeof(Resources.Shared), Name = "Survey")]
		public int? SurveyID { get; set; }

		[UIHint("DatePicker")]
		[Display(ResourceType = typeof(Resources.Members), Name = "SignedUpAfter")]
		public DateTime? SignedUpAfter { get; set; }

		[UIHint("DatePicker")]
		[Display(ResourceType = typeof(Resources.Members), Name = "SignedUpBefore")]
		public DateTime? SignedUpBefore { get; set; }

		[UIHint("DatePicker")]
		[Display(ResourceType = typeof(Resources.Members), Name = "BirthDateAfter")]
		public DateTime? BirthDateAfter { get; set; }

		[UIHint("DatePicker")]
		[Display(ResourceType = typeof(Resources.Members), Name = "BirthDateBefore")]
		public DateTime? BirthDateBefore { get; set; }

		[UIHint("BooleanNullableRadioButtons")]
		[Display(ResourceType = typeof(Resources.Members), Name = "OptInsSurveys")]
		public bool? OptInsSurveys { get; set; }

		[UIHint("BooleanNullableRadioButtons")]
		[Display(ResourceType = typeof(Resources.Members), Name = "OptInsNewsletters")]
		public bool? OptInsNewsletters { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "AgeFrom")]
		public int? AgeFrom { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "AgeTo")]
		public int? AgeTo { get; set; }

		[UIHint("DatePicker")]
		[Display(ResourceType = typeof(Resources.Members), Name = "BirthdayAfterMonthDayOfMonth")]
		public DateTime? BirthDateAfterMonthDay { get; set; }
		public DateTime? BirthDateAfterMonthDayAlt { get; set; } // use this value to get date from view

		[UIHint("DatePicker")]
		[Display(ResourceType = typeof(Resources.Members), Name = "BirthdayBeforeMonthDayOfMonth")]
		public DateTime? BirthDateBeforeMonthDay { get; set; }
		public DateTime? BirthDateBeforeMonthDayAlt { get; set; } // use this value to get date from view

	}

}

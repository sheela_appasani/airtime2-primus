using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Airtime.Data.Account;

using DataAnnotationsExtensions;


namespace Airtime.Models.Members
{

	public class UserModel
	{

		public int Id { get; set; }

		public string DateCreated { get; set; }
		public string DateModified { get; set; }

		public Guid Guid { get; set; }
		public string ExternalId { get; set; }

		public MembershipRoleEnum Role { get; set; }

		public string CompanyName { get; set; }
		public bool Enabled { get; set; }

		[Required]
		[Email]
		[Display(ResourceType = typeof(Resources.Members), Name = "Email")]
		public string PersonEmail { get; set; }

		[Required]
		[Display(ResourceType = typeof(Resources.Members), Name = "Username")]
		public string Username { get; set; }

		[Required]
		[Display(ResourceType = typeof(Resources.Members), Name = "Password")]
		public string Password { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "GivenName")]
		[StringLength(60)]
		public string PersonGivenName { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "FamilyName")]
		[StringLength(60)]
		public string PersonFamilyName { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "Gender")]
		public PersonGenderEnum? PersonPersonalGender { get; set; }
		public string PersonPersonalGenderName { get; set; }

		[UIHint("DatePicker")]
		[Display(ResourceType = typeof(Resources.Members), Name = "DateOfBirth")]
		public string PersonPersonalDateOfBirth { get; set; }

		public string PersonPersonalAge { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "DaytimePhone")]
		[StringLength(50)]
		public string PersonContactDaytimePhone { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "MobilePhone")]
		[StringLength(50)]
		public string PersonContactMobilePhone { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "StreetAddress")]
		[StringLength(100)]
		public string PersonContactAddress { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "Suburb")]
		[StringLength(50)]
		public string PersonContactSuburb { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "City")]
		[StringLength(50)]
		public string PersonContactCity { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "Postcode")]
		[StringLength(10)]
		public string PersonContactPostcode { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "State")]
		[StringLength(50)]
		public string PersonContactState { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "Country")]
		[StringLength(50)]
		public string PersonContactCountry { get; set; }

		[Display(ResourceType = typeof(Resources.Members), Name = "Notes")]
		public string Notes { get; set; }

		public bool OptInsSurveys { get; set; }
		public bool OptInsNewsletters { get; set; }

		public bool StatusScreened { get; set; }
		public bool StatusSystemChecked { get; set; }
		public bool StatusSampleTested { get; set; }
	}

}

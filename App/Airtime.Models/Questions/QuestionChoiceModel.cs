using System.ComponentModel.DataAnnotations;


namespace Airtime.Models.Questions
{

	public class QuestionChoiceModel
	{
		public int Id { get; set; }

		[Display(ResourceType = typeof(Resources.Questions), Name = "Position")]
		public int Position { get; set; }

		[Required]
		[StringLength(255)]
		[Display(ResourceType = typeof(Resources.Questions), Name = "Name")]
		public string Name { get; set; }

		public int? BranchQuestionId { get; set; }
		public string BranchQuestionName { get; set; }
	}

}

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

using Airtime.Data.Surveying.Questions;
using Airtime.Models.Media;


namespace Airtime.Models.Questions
{

	public class QuestionEditModel
	{

		public int Id { get; set; }

		[StringLength(255)]
		[Required]
		[Display(ResourceType = typeof(Resources.Questions), Name = "Name")]
		public string Name { get; set; }

		[AllowHtml]
		[Required]
		[UIHint("RichText")]
		[Display(ResourceType = typeof(Resources.Questions), Name = "Content")]
		public string Content { get; set; }

		[UIHint("SongsPicker")]
		public List<SongModel> Songs { get; set; }

		public List<QuestionChoiceModel> Choices { get; set; }
		public List<QuestionListModel> BranchToQuestions { get; set; }

		public QuestionTypeEnum QuestionType { get; set; }
		public QuestionCategoryEnum Category { get; set; }


		public QuestionEditModel()
		{
			Songs = new List<SongModel>();
			Choices = new List<QuestionChoiceModel>();
			BranchToQuestions = new List<QuestionListModel>();
		}

	}

}

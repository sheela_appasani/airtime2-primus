using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Airtime.Data.Surveying;
using Airtime.Data.Surveying.Questions;


namespace Airtime.Models.Questions
{

	public class QuestionIndexModel
	{
		[Required]
		[Display(ResourceType = typeof(Resources.Questions), Name = "SelectQuestionType")]
		public QuestionTypeEnum QuestionType { get; set; }

		public List<QuestionTypeEnum> AvailableQuestionTypes { get; set; }

		public Survey Survey { get; set; }

	}

}
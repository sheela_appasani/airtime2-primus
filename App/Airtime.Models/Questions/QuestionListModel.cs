using System.ComponentModel.DataAnnotations;


namespace Airtime.Models.Questions
{

	public class QuestionListModel
	{
		public int Id { get; set; }

		[Display(ResourceType = typeof(Resources.Questions), Name = "Name")]
		public string Name { get; set; }

		[Display(ResourceType = typeof(Resources.Questions), Name = "Position")]
		public int Position { get; set; }

		[Display(ResourceType = typeof(Resources.Questions), Name = "QuestionType")]
		public string QuestionType { get; set; }

		public string Category { get; set; }
	}

}
using System;
using System.Collections.Generic;

using Airtime.Data.Media;
using Airtime.Data.Reporting;


namespace Airtime.Models.Reports
{

	public class MusicReportModel
	{

		public ReportFilterModel Filter { get; set; }

		public ReportTypeEnum ReportType { get; set; }

		public List<SongReport> SongReports { get; set; }

		public Dictionary<int, string> SongAttributes { get; set; } 

		public int SampleSize { get; set; }

		public MusicReportModel()
		{
            SongAttributes = new Dictionary<int, string>();
			SongReports = new List<SongReport>();
			Filter = new ReportFilterModel();
		}

	}

}

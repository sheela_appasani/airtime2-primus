using System;
using System.Collections.Generic;

using Airtime.Data.Reporting;
using Airtime.Data.Surveying.Questions;


namespace Airtime.Models.Reports
{

	public class PerceptualReportModel
	{

		public ReportFilterModel Filter { get; set; }

		public ReportTypeEnum ReportType { get; set; }

		public Dictionary<QuestionBase, List<OpenEndedReport>> OpenEndedReports { get; set; }
		public Dictionary<QuestionBase, List<MultiChoiceReport>> MultiChoiceReports { get; set; }

		public List<QuestionBase> Questions { get; set; } 


		public PerceptualReportModel()
		{
			MultiChoiceReports = new Dictionary<QuestionBase, List<MultiChoiceReport>>();
			OpenEndedReports = new Dictionary<QuestionBase, List<OpenEndedReport>>();
			Questions = new List<QuestionBase>();
			Filter = new ReportFilterModel();
		}

	}

}

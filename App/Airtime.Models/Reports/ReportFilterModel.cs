using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Airtime.Data.Account;


namespace Airtime.Models.Reports
{

	public class ReportFilterModel
	{
		
		public List<int> SelectedSurveyIds { get; set; }
		public List<ReportFilterSurveyModel> Surveys { get; set; }


		public List<int> SelectedQuestionChoiceIds { get; set; }
		public List<ReportFilterQuestionModel> Questions { get; set; }


		[Display(ResourceType = typeof(Resources.Account), Name = "Gender")]
		public PersonGenderEnum? Gender { get; set; }


		[Display(ResourceType = typeof(Resources.Account), Name = "Age")]
		public int? LowerAge { get; set; }


		[Display(ResourceType = typeof(Resources.Account), Name = "Age")]
		public int? UpperAge { get; set; }


		public ReportFilterModel()
		{
			Surveys = new List<ReportFilterSurveyModel>();
			Questions = new List<ReportFilterQuestionModel>();
			SelectedSurveyIds = new List<int>();
			SelectedQuestionChoiceIds = new List<int>();
		}

	}

}

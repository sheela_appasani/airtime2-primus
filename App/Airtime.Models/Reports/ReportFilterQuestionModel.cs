using System;
using System.Collections.Generic;
using System.Web.Mvc;

using Airtime.Data.Surveying.Questions;


namespace Airtime.Models.Reports
{

	public class ReportFilterQuestionModel
	{
		public string Name { get; set; }
		public Dictionary<QuestionChoice, bool> Choices { get; set; } 

		public ReportFilterQuestionModel()
		{
			Choices = new Dictionary<QuestionChoice, bool>();
		}
	}

}

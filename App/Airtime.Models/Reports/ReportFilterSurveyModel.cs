using System;
using System.Collections.Generic;


namespace Airtime.Models.Reports
{

	public class ReportFilterSurveyModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string ScheduleStartDate { get; set; }
		public string ScheduleEndDate { get; set; }
	}

}

using System;
using System.Collections.Generic;

using Airtime.Data.Reporting;


namespace Airtime.Models.Reports
{

	public class SongDetailReportModel
	{

		public ReportFilterModel Filter { get; set; }

		public List<SongReport> SongReports { get; set; }


		public SongDetailReportModel()
		{
			SongReports = new List<SongReport>();
			Filter = new ReportFilterModel();
		}

	}

}

using System;
using System.Collections.Generic;
using System.Linq;

using Airtime.Data.Reporting;

using Xtra.Common;


namespace Airtime.Models.Reports
{

	public class SummaryReportModel
	{

		public TimeIt TimeIt { get; set; }
		public string CacheLocalDate;
		public bool ShowRefresh;

		public int TotalMembers { get; set; }
		public List<AgeGenderTotal> AgeGenderTotals { get; set; }
		public bool HaveAgeGenderTotals = false;

		public ILookup<string, KeyValuePair<CacheReportEnum, StatRate>> NewMemberRates;
		public bool HaveNewMemberRates = false;

		public string LatestSurveyName;
		public string LatestSurveyLocalStartDate;
		public string LatestSurveyLocalEndDate;
		public string LatestSurveyInvited;
		public string LatestSurveyCompleted;
		public string LatestSurveyResponseRate;
		public string LatestSurveyStatus;

		public IEnumerable<StatRate> ParticipationRates;
		public bool HaveParticipationRates = false;

		public IEnumerable<StatRate> StationMostRates;
		public bool HaveStationMostRates = false;

		public IEnumerable<StatRate> StationsListenRates;
		public bool HaveStationsListenRates = false;

		public IEnumerable<StatRate> TimeSpentListeningRates;
		public bool HaveTimeSpentListeningRates = false;

		public string ChannelTargetLabel;
		public string ChannelTargetMessage;


		public SummaryReportModel()
		{
			AgeGenderTotals = new List<AgeGenderTotal>();

			NewMemberRates = EmptyLookup<string, KeyValuePair<CacheReportEnum, StatRate>>.Instance; 
			StationMostRates = new List<StatRate>();
			StationsListenRates = new List<StatRate>();
			TimeSpentListeningRates = new List<StatRate>();
		}


		public void ApplyCacheValues(ReportChannelCache cache, IEnumerable<DateRange> ageGenderRanges)
		{
			AgeGenderTotals = ageGenderRanges
				.Select(dateRange => new AgeGenderTotal { AgeRange = dateRange.Label })
				.ToList();

			if (cache != null) {
				SetAgeGenderTotals(cache);
				HaveAgeGenderTotals = true;

				HaveParticipationRates = cache.HasStat(CacheReportEnum.Participation);
				ParticipationRates = cache.GetStatRates(CacheReportEnum.Participation);

				HaveNewMemberRates = cache.HasStat(CacheReportEnum.NewMembers) || cache.HasStat(CacheReportEnum.NewMembersInTarget);
				var rates1 = cache.GetStatRates(CacheReportEnum.NewMembers)
					.Select(x => new KeyValuePair<CacheReportEnum, StatRate>(CacheReportEnum.NewMembers, x));
				var rates2 = cache.GetStatRates(CacheReportEnum.NewMembersInTarget)
					.Select(x => new KeyValuePair<CacheReportEnum, StatRate>(CacheReportEnum.NewMembersInTarget, x));
				NewMemberRates = rates1.Union(rates2).ToLookup(x => x.Value.Label);

				HaveStationMostRates = cache.HasStat(CacheReportEnum.StationMost);
				StationMostRates = cache.GetStatRates(CacheReportEnum.StationMost);
				HaveStationsListenRates = cache.HasStat(CacheReportEnum.StationsListen);
				StationsListenRates = cache.GetStatRates(CacheReportEnum.StationsListen);
				HaveTimeSpentListeningRates = cache.HasStat(CacheReportEnum.TimeSpentListening);
				TimeSpentListeningRates = cache.GetStatRates(CacheReportEnum.TimeSpentListening);
			}
		}


		private void SetAgeGenderTotals(ReportChannelCache cache)
		{
			AgeGenderTotals[0].Total = cache.DemographicsTotal;
			AgeGenderTotals[1].Total = cache.DemographicsTotal;
			AgeGenderTotals[2].Total = cache.DemographicsTotal;
			AgeGenderTotals[3].Total = cache.DemographicsTotal;
			AgeGenderTotals[4].Total = cache.DemographicsTotal;
			AgeGenderTotals[5].Total = cache.DemographicsTotal;
			AgeGenderTotals[6].Total = cache.DemographicsTotal;
			AgeGenderTotals[7].Total = cache.DemographicsTotal;
			AgeGenderTotals[0].SubTotalMale    = cache.DemographicsTotalMale;
			AgeGenderTotals[0].SubTotalFemale  = cache.DemographicsTotalFemale;
			AgeGenderTotals[0].SubTotalUnknown = cache.DemographicsTotalUnknown;
			AgeGenderTotals[1].SubTotalMale    = cache.DemographicsUnder20Male;
			AgeGenderTotals[1].SubTotalFemale  = cache.DemographicsUnder20Female;
			AgeGenderTotals[1].SubTotalUnknown = cache.DemographicsUnder20Unknown;
			AgeGenderTotals[2].SubTotalMale    = cache.Demographics20_24Male;
			AgeGenderTotals[2].SubTotalFemale  = cache.Demographics20_24Female;
			AgeGenderTotals[2].SubTotalUnknown = cache.Demographics20_24Unknown;
			AgeGenderTotals[3].SubTotalMale    = cache.Demographics25_29Male;
			AgeGenderTotals[3].SubTotalFemale  = cache.Demographics25_29Female;
			AgeGenderTotals[3].SubTotalUnknown = cache.Demographics25_29Unknown;
			AgeGenderTotals[4].SubTotalMale    = cache.Demographics30_34Male;
			AgeGenderTotals[4].SubTotalFemale  = cache.Demographics30_34Female;
			AgeGenderTotals[4].SubTotalUnknown = cache.Demographics30_34Unknown;
			AgeGenderTotals[5].SubTotalMale    = cache.Demographics35_39Male;
			AgeGenderTotals[5].SubTotalFemale  = cache.Demographics35_39Female;
			AgeGenderTotals[5].SubTotalUnknown = cache.Demographics35_39Unknown;
			AgeGenderTotals[6].SubTotalMale    = cache.Demographics40_44Male;
			AgeGenderTotals[6].SubTotalFemale  = cache.Demographics40_44Female;
			AgeGenderTotals[6].SubTotalUnknown = cache.Demographics40_44Unknown;
			AgeGenderTotals[7].SubTotalMale    = cache.DemographicsOver45Male;
			AgeGenderTotals[7].SubTotalFemale  = cache.DemographicsOver45Female;
			AgeGenderTotals[7].SubTotalUnknown = cache.DemographicsOver45Unknown;
		}

	}

}

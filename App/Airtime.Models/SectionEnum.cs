namespace Airtime.Models
{
	public enum SectionEnum
	{
		Home,
		Reports,
		ReportsMusic,
		ReportsPerceptuals,
		Surveys,
		SurveysQuestions,
		SurveysInvitations,
		SurveysLaunch,
		MarketingEmails,
		Members,
		Media,
		DeveloperTools,
		AdminTools
	}
}
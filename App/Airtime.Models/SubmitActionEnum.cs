namespace Airtime.Models
{

	public enum SubmitActionEnum
	{
		Add,
		Back,
		Cancel,
		Copy,
		Edit,
		Delete,
		Logon,
		Preview,
		Save,
		Submit,
		Upload,
		Download,
		Export,
		ExportAsCsv,
		ExportAsExcel,
		AddReminder,
		Clean
	}

}

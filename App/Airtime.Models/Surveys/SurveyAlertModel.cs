using System.ComponentModel.DataAnnotations;

using Airtime.Data.Account;


namespace Airtime.Models.Surveys
{

	public class SurveyAlertModel
	{

		public int Id { get; set;}

		[Display(ResourceType = typeof(Resources.Surveys), Name = "Gender")]
		public PersonGenderEnum? Gender { get; set; }

		[Display(ResourceType = typeof(Resources.Surveys), Name = "LowerAge")]
		public int? LowerAge { get; set; }

		[Display(ResourceType = typeof(Resources.Surveys), Name = "UpperAge")]
		public int? UpperAge { get; set; }

		[Required]
		[Display(ResourceType = typeof(Resources.Surveys), Name = "SampleSize")]
		public int? SampleSize { get; set; }

	}

}
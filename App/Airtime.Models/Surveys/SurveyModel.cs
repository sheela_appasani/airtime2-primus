using System.ComponentModel.DataAnnotations;

using Airtime.Data.Surveying;


namespace Airtime.Models.Surveys
{

	public class SurveyModel
	{
		public int Id { get; set;}

		public bool MakeCopy { get; set; }
		public bool Disabled { get; set; }

		public SurveyAlertModel AlertModel { get; set; }

		public string Status { get; set; }

		public string StatusName { get; set; }


		[Required]
		[StringLength(255)]
		[Display(ResourceType = typeof(Resources.Surveys), Name = "Name")]
		public string Name { get; set; }

		public bool Archived { get; set; }


		[UIHint("DateTimePicker")]
		[Display(ResourceType = typeof(Resources.Surveys), Name = "StartDate")]
		public string ScheduleStartDate { get; set; }

		[UIHint("DateTimePicker")]
		[Display(ResourceType = typeof(Resources.Surveys), Name = "EndDate")]
		public string ScheduleEndDate { get; set; }


		[Display(ResourceType = typeof(Resources.Surveys), Name = "RandomiseSongLists")]
		public bool OptionsRandomiseSongLists { get; set; }

		[Display(ResourceType = typeof(Resources.Surveys), Name = "ShowSongSummaryAtEnd")]
		public bool OptionsShowSongSummaryAtEnd { get; set; }


		[Required]
		[StringLength(25)]
		[Display(ResourceType = typeof(Resources.Surveys), Name = "NotTired")]
		public string BurnButtonsNotTiredText { get; set; }

		[Required]
		[StringLength(25)]
		[Display(ResourceType = typeof(Resources.Surveys), Name = "LittleTired")]
		public string BurnButtonsLittleTiredText { get; set; }

		[Required]
		[StringLength(25)]
		[Display(ResourceType = typeof(Resources.Surveys), Name = "VeryTired")]
		public string BurnButtonsVeryTiredText { get; set; }

		[Required]
		[StringLength(25)]
		[Display(ResourceType = typeof(Resources.Surveys), Name = "Unfamiliar")]
		public string BurnButtonsUnfamiliarText { get; set; }


		public int StatisticsInvited { get; set; }
		public int StatisticsRegistered { get; set; }
		public int StatisticsCompleted { get; set; }
		public int StatisticsIosCompleted { get; set; }
		public int StatisticsAndroidCompleted { get; set; }
		public int StatisticsWebCompleted { get; set; }


		[Display(ResourceType = typeof(Resources.Surveys), Name = "Language")]
		public string LanguageName { get; set; }

		[Display(ResourceType = typeof(Resources.Surveys), Name = "TimeZone")]
		public string TimeZoneInfoDisplayName { get; set; }

		[Display(ResourceType = typeof(Resources.Surveys), Name = "EasyLaunchOption")]
		public SurveyLaunchEnum? EasyLaunchOption { get; set; }

		public SurveyModel()
		{
			AlertModel = new SurveyAlertModel();
		}

	}
}
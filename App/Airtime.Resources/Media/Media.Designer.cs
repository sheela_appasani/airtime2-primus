﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Media {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Media() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.Media.Media", typeof(Media).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Artist.
        /// </summary>
        public static string Artist {
            get {
                return ResourceManager.GetString("Artist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company.
        /// </summary>
        public static string Company {
            get {
                return ResourceManager.GetString("Company", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date Uploaded.
        /// </summary>
        public static string DateUploaded {
            get {
                return ResourceManager.GetString("DateUploaded", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Drag a song here from the Songs Library to select it..
        /// </summary>
        public static string DragASongHereToSelectIt {
            get {
                return ResourceManager.GetString("DragASongHereToSelectIt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Drag songs here from the Songs Library to add them to the top of the playlist, or drag these songs to re-order them..
        /// </summary>
        public static string DragSongsHereToAddToPlaylist {
            get {
                return ResourceManager.GetString("DragSongsHereToAddToPlaylist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit Song.
        /// </summary>
        public static string EditSong {
            get {
                return ResourceManager.GetString("EditSong", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File.
        /// </summary>
        public static string File {
            get {
                return ResourceManager.GetString("File", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Genre.
        /// </summary>
        public static string Genre {
            get {
                return ResourceManager.GetString("Genre", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to GLOBAL.
        /// </summary>
        public static string GLOBAL {
            get {
                return ResourceManager.GetString("GLOBAL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Length.
        /// </summary>
        public static string Length {
            get {
                return ResourceManager.GetString("Length", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Library.
        /// </summary>
        public static string Library {
            get {
                return ResourceManager.GetString("Library", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The maximum number of songs has already been added to the playlist.
        ///
        ///Please remove some first if you want to add more..
        /// </summary>
        public static string MaximumSongsAlreadyAdded {
            get {
                return ResourceManager.GetString("MaximumSongsAlreadyAdded", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search by Artist, Title or Library.
        /// </summary>
        public static string Placeholder_SearchBy {
            get {
                return ResourceManager.GetString("Placeholder_SearchBy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Play.
        /// </summary>
        public static string Play {
            get {
                return ResourceManager.GetString("Play", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Selected Song.
        /// </summary>
        public static string SelectedSong {
            get {
                return ResourceManager.GetString("SelectedSong", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to That song is already in the playlist..
        /// </summary>
        public static string SongAlreadyInPlaylist {
            get {
                return ResourceManager.GetString("SongAlreadyInPlaylist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Songs Library.
        /// </summary>
        public static string SongsLibrary {
            get {
                return ResourceManager.GetString("SongsLibrary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Songs Playlist.
        /// </summary>
        public static string SongsPlaylist {
            get {
                return ResourceManager.GetString("SongsPlaylist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tempo.
        /// </summary>
        public static string Tempo {
            get {
                return ResourceManager.GetString("Tempo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Title.
        /// </summary>
        public static string Title {
            get {
                return ResourceManager.GetString("Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Upload New Song.
        /// </summary>
        public static string UploadNewSong {
            get {
                return ResourceManager.GetString("UploadNewSong", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Year.
        /// </summary>
        public static string Year {
            get {
                return ResourceManager.GetString("Year", resourceCulture);
            }
        }
    }
}

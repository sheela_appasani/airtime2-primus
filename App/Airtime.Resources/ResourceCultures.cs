﻿using System;
using System.Collections.Generic;
using System.Globalization;


namespace Resources
{

	public static class ResourceCultures
	{

		public static readonly HashSet<string> Installed = new HashSet<string>(
			new[] {
				"ar", // Arabic
				"en", // English
				"es", // Spanish
				"zh-Hans", // Simplified Chinese

				"yo" // Yoruba (not really supported here, using as a "mock" testing language)
			},
			StringComparer.OrdinalIgnoreCase
		);


		public static string ConvertCultureForCkEditor(CultureInfo culture)
		{
			string name = culture.Name;
			if (CkEditorCultureNameMap.ContainsKey(name)) {
				return CkEditorCultureNameMap[name];
			}
			return name;
		}


		private static readonly Dictionary<string, string> CkEditorCultureNameMap = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase) {
			{ "zh", "zh-cn" },
			{ "zh-Hans", "zh-cn" },
			{ "zh-CHS", "zh-cn" },
			{ "zh-CN", "zh-cn" },
			{ "zh-SG", "zh-cn" },
			{ "zh-Hant", "zh" },
			{ "zh-CHT", "zh" },
			{ "zh-HK", "zh" },
			{ "zh-MO", "zh" },
			{ "zh-TW", "zh" },
		};

	}

}

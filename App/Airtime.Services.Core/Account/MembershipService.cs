﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

using Airtime.Data.Client;
using Airtime.Data.Account;
using Airtime.Data.Surveying;
using Airtime.Data.Surveying.Answers;
using Airtime.Data.Surveying.Questions;
using Airtime.Models.Members;
using Airtime.Services.Account;

using NLog;

using OfficeOpenXml;
using OfficeOpenXml.Style;

using Xtra.Common;


namespace Airtime.Services.Core.Account
{

	public class MembershipService : IMembershipService
	{

		public MembershipService(IMembershipRepository membershipRepository, IChannelRepository channelRepository, ISurveyRepository surveyRepository, IQuestionChoiceRepository questionChoiceRepository)
		{
			MembershipRepository = membershipRepository;
			ChannelRepository = channelRepository;
			SurveyRepository = surveyRepository;
			QuestionChoiceRepository = questionChoiceRepository;
		}


		public void ExportAsCsv(Stream stream, Channel channel, MemberFilterModel memberFilterModel)
		{
			Contract.Requires(stream != null, "stream");
			Contract.Requires(channel != null, "channel");

			var profileQuestions = channel.ProfileQuestions
				.Where(question => question.Category != QuestionCategoryEnum.Registration_Age && question.Category != QuestionCategoryEnum.Registration_Gender)
				.ToList();

			//Retrieve all the multi-choice responses for the profile questions so that they get cached
			QuestionChoiceRepository.Where(c => profileQuestions.Contains(c.Question)).Apply();

			using (var writer = new StreamWriter(stream, Encoding.UTF8)) {
				//Write the CSV file header
				var header = new List<string>(CommonHeadersForExport);
				header.AddRange(profileQuestions.Select(q => "\"" + q.Name + "\""));
				writer.WriteLine(String.Join(",", header));
				writer.Flush();

				var usersOrdered = GetFilteredUsers(memberFilterModel, channel);

				foreach (var user in usersOrdered) {
					// ReSharper disable RedundantAnonymousTypePropertyName
					writer.Write(CsvRowTemplate.NamedFormat(new {
						Id = user.Id,
						Enabled = user.Enabled ? Resources.Shared.Yes : Resources.Shared.No,
						Username = user.Username,
						Password = user.Password,
						Email = user.Person.Email,
						GivenName = user.Person.GivenName,
						FamilyName = user.Person.FamilyName,
						Phone = user.Person.Contact.DaytimePhone,
						Mobile = user.Person.Contact.MobilePhone,
						Street = user.Person.Contact.Address,
						Suburb = user.Person.Contact.Suburb,
						Postcode = user.Person.Contact.Postcode,
						Gender = user.Person.Personal.Gender.HasValue ? user.Person.Personal.Gender.Value.ToResourceString() : null,
						Age = user.Person.Personal.Age,
						DateOfBirth = user.Person.Personal.DateOfBirth.HasValue ? user.Person.Personal.DateOfBirth.Value.ToString("d") : null,
						BirthMonth = user.Person.Personal.DateOfBirth.HasValue ? user.Person.Personal.DateOfBirth.Value.ToString("MM") : null,
						BirthDayOfMonth = user.Person.Personal.DateOfBirth.HasValue ? user.Person.Personal.DateOfBirth.Value.ToString("dd") : null,
						OptInSurveys = user.OptIns.Surveys ? Resources.Shared.Yes : Resources.Shared.No,
						OptInNewsletters = user.OptIns.Newsletters ? Resources.Shared.Yes : Resources.Shared.No,
						Notes = user.Notes,
						DateModified = user.DateModified.ToString("g")
					}));
					// ReSharper restore RedundantAnonymousTypePropertyName

					foreach (var question in profileQuestions) {
						var answer = user.ProfileAnswers.OfType<SingleAnswer>().FirstOrDefault(a => a.Question == question);
						if (answer != null) {
							string answerText = answer.Choice != null
								? (!String.IsNullOrEmpty(answer.Choice.Name) ? answer.Choice.Name.StripHtml() : "") 
								: (!String.IsNullOrEmpty(answer.Text) ? answer.Text.StripHtml() : "");
							writer.Write(",\"");
							writer.Write(answerText);
							writer.Write("\"");
						} else {
							writer.Write(",");
						}
					}

					writer.WriteLine();
					writer.Flush();
				}
			}
		}


		public void ExportAsExcel(Stream stream, Channel channel, MemberFilterModel memberFilterModel)
		{
			Contract.Requires(stream != null, "stream");
			Contract.Requires(channel != null, "channel");
			Contract.Requires(memberFilterModel != null, "memberFilterModel");

			var profileQuestions = channel.ProfileQuestions
				.Where(question => question.Category != QuestionCategoryEnum.Registration_Age && question.Category != QuestionCategoryEnum.Registration_Gender)
				.ToList();

			//Retrieve all the multi-choice responses for the profile questions so that they get cached
			QuestionChoiceRepository.Where(c => profileQuestions.Contains(c.Question)).Apply();

			var header = new List<string>(CommonHeadersForExport);
			header.AddRange(profileQuestions.Select(q => q.Name));

			using (var excel = new ExcelPackage()) {
				var usersOrdered = GetFilteredUsers(memberFilterModel, channel);

				var worksheet = excel.Workbook.Worksheets.Add(channel.AdminName);
				worksheet.View.FreezePanes(2, 1);
				worksheet.PrinterSettings.RepeatRows = new ExcelAddress("1:1");

				var range = worksheet.Cells[1, 1, 1, header.Count];
				range.Style.Border.Top.Style = ExcelBorderStyle.Dotted;
				range.Style.Border.Right.Style = ExcelBorderStyle.Dotted;
				range.Style.Border.Bottom.Style = ExcelBorderStyle.Dotted;
				range.Style.Border.Left.Style = ExcelBorderStyle.Dotted;
				range.Style.Fill.PatternType = ExcelFillStyle.Solid;
				range.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#DFEFFC"));
				range.Style.Font.Color.SetColor(ColorTranslator.FromHtml("#2E6E9E"));
				range.Style.Font.Bold = true;

				var cursor = new ExcelWorksheetCursor(worksheet);
				cursor.AddCells(header, c => c.AutoFitColumns());
	
				foreach (var user in usersOrdered) {
					cursor.NextRow();
					cursor.AddCell(user.Id);
					cursor.AddCell(user.Enabled ? Resources.Shared.Yes : Resources.Shared.No);
					cursor.AddCell(user.Username);
					cursor.AddCell(user.Password);
					cursor.AddCell(user.Person.Email);
					cursor.AddCell(user.Person.GivenName);
					cursor.AddCell(user.Person.FamilyName);
					cursor.AddCell(user.Person.Contact.DaytimePhone, c => c.Style.Numberformat.Format = "@");
					cursor.AddCell(user.Person.Contact.MobilePhone, c => c.Style.Numberformat.Format = "@");
					cursor.AddCell(user.Person.Contact.Address);
					cursor.AddCell(user.Person.Contact.Suburb);
					cursor.AddCell(user.Person.Contact.Postcode, c => c.Style.Numberformat.Format = "@");
					cursor.AddCell(user.Person.Personal.Gender.HasValue ? user.Person.Personal.Gender.Value.ToResourceString() : null);
					cursor.AddCell(user.Person.Personal.Age);
					cursor.AddCell(user.Person.Personal.DateOfBirth, c => c.Style.Numberformat.Format = "mm-dd-yy");
					cursor.AddCell(user.Person.Personal.DateOfBirth.HasValue ? user.Person.Personal.DateOfBirth.Value.Month : (int?)null);
					cursor.AddCell(user.Person.Personal.DateOfBirth.HasValue ? user.Person.Personal.DateOfBirth.Value.Day : (int?)null);
					cursor.AddCell(user.OptIns.Surveys ? Resources.Shared.Yes : Resources.Shared.No);
					cursor.AddCell(user.OptIns.Newsletters ? Resources.Shared.Yes : Resources.Shared.No);
					cursor.AddCell(user.Notes);
					cursor.AddCell(user.DateModified, c => c.Style.Numberformat.Format = "m/d/yy h:mm");

					foreach (var question in profileQuestions) {
						var answer = user.ProfileAnswers.OfType<SingleAnswer>().FirstOrDefault(a => a.Question.Equals(question));
						if (answer != null) {
							cursor.AddCell(answer.Choice != null ? answer.Choice.Name.StripHtml() : answer.Text.StripHtml(), c => c.Style.Numberformat.Format = "@");
						} else {
							cursor.AddCell();
						}
					}
				}

				excel.SaveAs(stream);
			}
		}


		private IEnumerable<Membership> GetFilteredUsers(MemberFilterModel memberFilterModel, Channel channel)
		{
			var query = MembershipRepository
				.Where(u => u.Username != "purged_user")
				.Where(u => String.IsNullOrEmpty(u.ExternalId))
				.Where(u => u.Role == MembershipRoleEnum.Respondent)
				.Where(u => u.Channels.Contains(channel));

			query = ApplyMemberFilter(query, memberFilterModel, channel).OrderBy(u => u.Id);

			query = MembershipRepository.ApplyFetch(query, u => u.Person);
			query = MembershipRepository.ApplyFetchMany(query, u => u.ProfileAnswers);

			return query.AsEnumerable();
		}


		public IQueryable<Membership> ApplyMemberFilter(IQueryable<Membership> users, MemberFilterModel model, Channel channel)
		{
			Contract.Requires(users != null, "users");
			Contract.Requires(model != null, "model");
			Contract.Requires(channel != null, "channel");

			if (model.Enabled.HasValue) {
				users = users.Where(u => u.Enabled == model.Enabled.Value);
			}
			if (model.Gender.HasValue) {
				users = users.Where(u => (u.Person.Personal.Gender.HasValue
					&& u.Person.Personal.Gender.Value == model.Gender.Value));
			}

			if (!String.IsNullOrEmpty(model.Username)) {
				users = users.Where(u => u.Username.Contains(model.Username));
			}
			if (!String.IsNullOrEmpty(model.Email)) {
				users = users.Where(u => u.Email.Contains(model.Email));
			}
			if (!String.IsNullOrEmpty(model.GivenName)) {
				users = users.Where(u => u.Person.GivenName.Contains(model.GivenName));
			}
			if (!String.IsNullOrEmpty(model.FamilyName)) {
				users = users.Where(u => u.Person.FamilyName.Contains(model.FamilyName));
			}

			if (!String.IsNullOrEmpty(model.DaytimePhone)) {
				users = users.Where(u => u.Person.Contact.DaytimePhone.Contains(model.DaytimePhone));
			}
			if (!String.IsNullOrEmpty(model.MobilePhone)) {
				users = users.Where(u => u.Person.Contact.MobilePhone.Contains(model.MobilePhone));
			}
			if (!String.IsNullOrEmpty(model.Address)) {
				users = users.Where(u => u.Person.Contact.Address.Contains(model.Address));
			}
			if (!String.IsNullOrEmpty(model.Suburb)) {
				users = users.Where(u => u.Person.Contact.Suburb.Contains(model.Suburb));
			}
			if (!String.IsNullOrEmpty(model.City)) {
				users = users.Where(u => u.Person.Contact.City.Contains(model.City));
			}
			if (!String.IsNullOrEmpty(model.State)) {
				users = users.Where(u => u.Person.Contact.State.Contains(model.State));
			}
			if (!String.IsNullOrEmpty(model.Country)) {
				users = users.Where(u => u.Person.Contact.Country.Equals(model.Country));
			}
			if (!String.IsNullOrEmpty(model.Postcode)) {
				users = users.Where(u => u.Person.Contact.Postcode.Equals(model.Postcode));
			}

			if (model.SurveyID.HasValue) {
				var survey = SurveyRepository[model.SurveyID.Value];
				if (model.UserSurveyStatus.HasValue
					&& model.UserSurveyStatus.Value != UserSurveyStatusEnum.All) {
					users = ApplyMemberFilter(users, model.UserSurveyStatus.Value, survey, channel);
				} else {
					// ReSharper disable PossibleUnintendedReferenceComparison
					// ReSharper disable PossibleInvalidOperationException
					users = users.Where(u => u.RespondentStates.Any(rs => rs.Survey == survey));
					// ReSharper restore PossibleInvalidOperationException
					// ReSharper restore PossibleUnintendedReferenceComparison
				}
			}

			if (model.SignedUpAfter.HasValue) {
				users = users.Where(u => u.DateCreated >= model.SignedUpAfter.Value);
			}
			if (model.SignedUpBefore.HasValue) {
				users = users.Where(u => u.DateCreated < model.SignedUpBefore.Value);
			}

			if (model.BirthDateAfter.HasValue) {
				users = users.Where(u => u.Person.Personal.DateOfBirth >= model.BirthDateAfter.Value);
			}
			if (model.BirthDateBefore.HasValue) {
				users = users.Where(u => u.Person.Personal.DateOfBirth < model.BirthDateBefore.Value);
			}

			if (model.AgeFrom.HasValue) {
				users = users.Where(u =>
					u.Person.Personal.DateOfBirth <= DateTime.Now.Date.AddYears(-model.AgeFrom.Value));
			}

			if (model.AgeTo.HasValue) {
				users = users.Where(u =>
					u.Person.Personal.DateOfBirth >= DateTime.Now.Date.AddYears(-model.AgeTo.Value - 1).AddDays(1));
			}

			if (model.OptInsSurveys.HasValue) {
				users = users.Where(u => (u.Person.Personal.Gender.HasValue
					&& u.OptIns.Surveys == model.OptInsSurveys.Value));
			}
			if (model.OptInsNewsletters.HasValue) {
				users = users.Where(u => (u.Person.Personal.Gender.HasValue
					&& u.OptIns.Newsletters == model.OptInsNewsletters.Value));
			}

			//
			// Input dates After and Before have the years replaced with fudgeYear.
			//
			// This code translates users DateOfBith into the fudgeYear to do comparisons.
			// Using 1980 as fudgeYear because it's a leap year - avoid out-of-range problems with 29th Feb.
			// When start-date is before end-date, then start and end bracket as normal.
			// When start-date is after end-date then we are looking for date wrapping 
			// from end of one year into start of next logical year.
			//
			// year time line is  ---S>>><<<E---   for normal start end
			// year time line is  <<<E------S>>>   which is a year end wrap situation
			//
			const int fudgeYear = 1980;
			if (model.BirthDateAfterMonthDayAlt.HasValue && model.BirthDateBeforeMonthDayAlt.HasValue) {
				var dateAfter = new DateTime(fudgeYear, model.BirthDateAfterMonthDayAlt.Value.Month, model.BirthDateAfterMonthDayAlt.Value.Day);
				var dateBefore = new DateTime(fudgeYear, model.BirthDateBeforeMonthDayAlt.Value.Month, model.BirthDateBeforeMonthDayAlt.Value.Day);

				if (dateAfter <= dateBefore) {
					users = users.Where(
						u => u.Person.Personal.DateOfBirth.HasValue
						&& (u.Person.Personal.DateOfBirth.Value.AddYears(fudgeYear - u.Person.Personal.DateOfBirth.Value.Year) >= dateAfter
							&& u.Person.Personal.DateOfBirth.Value.AddYears(fudgeYear - u.Person.Personal.DateOfBirth.Value.Year) <= dateBefore)
					);
				} else {
					users = users.Where(
						u => u.Person.Personal.DateOfBirth.HasValue
						&& (u.Person.Personal.DateOfBirth.Value.AddYears(fudgeYear - u.Person.Personal.DateOfBirth.Value.Year) >= dateAfter
							|| u.Person.Personal.DateOfBirth.Value.AddYears(fudgeYear - u.Person.Personal.DateOfBirth.Value.Year) <= dateBefore)
					);
				}
			} else if (model.BirthDateAfterMonthDayAlt.HasValue && !model.BirthDateBeforeMonthDayAlt.HasValue) {
				var dateAfter = new DateTime(fudgeYear, model.BirthDateAfterMonthDayAlt.Value.Month, model.BirthDateAfterMonthDayAlt.Value.Day);
				users = users.Where(
					u => u.Person.Personal.DateOfBirth.HasValue
						&& (u.Person.Personal.DateOfBirth.Value.AddYears(fudgeYear - u.Person.Personal.DateOfBirth.Value.Year) == dateAfter));
			} else if (!model.BirthDateAfterMonthDayAlt.HasValue && model.BirthDateBeforeMonthDayAlt.HasValue) {
				var dateBefore = new DateTime(fudgeYear, model.BirthDateBeforeMonthDayAlt.Value.Month, model.BirthDateBeforeMonthDayAlt.Value.Day);
				users = users.Where(
					u => u.Person.Personal.DateOfBirth.HasValue
						&& (u.Person.Personal.DateOfBirth.Value.AddYears(fudgeYear - u.Person.Personal.DateOfBirth.Value.Year) == dateBefore));
			}

			return users;
		}


		public IQueryable<Membership> ApplyMemberFilter(IQueryable<Membership> users, UserSurveyStatusEnum status, Survey survey, Channel channel)
		{
			Contract.Requires(users != null, "users");
			Contract.Requires(survey != null, "survey");
			Contract.Requires(channel != null, "channel");

			// ReSharper disable PossibleUnintendedReferenceComparison
			switch (status) {
				case UserSurveyStatusEnum.All:
					return users;

				case UserSurveyStatusEnum.Invited:
					return users.Where(
						u => u.RespondentStates
							.Where(rs => rs.Survey == survey)
							.Any(rs => rs.Invited));

				case UserSurveyStatusEnum.Completed:
					return users.Where(
						u => u.RespondentStates
							.Where(rs => rs.Survey == survey)
							.Any(rs => rs.Completed));

				case UserSurveyStatusEnum.Not_Invited:
					var invitedUsers =
						from c in ChannelRepository
						from u in c.Memberships
						from sr in u.RespondentStates
						where c == survey.Channel
						where sr.Invited && sr.Survey == survey && u.Role == MembershipRoleEnum.Respondent
						select u;
					var notInvitedUsers = users.Where(u => !invitedUsers.Contains(u));
					return notInvitedUsers;

				case UserSurveyStatusEnum.Uncompleted_Invitees:
					return users.Where(
						u => u.RespondentStates
							.Where(rs => rs.Survey == survey)
							.Any(rs => !rs.Completed && rs.Invited));
			}
			// ReSharper restore PossibleUnintendedReferenceComparison
			return users;
		}


		private readonly static List<string> CommonHeadersForExport = new List<string>() {
			Resources.Members.Id, Resources.Members.Enabled, Resources.Members.Username, Resources.Members.Password, Resources.Members.Email,
			Resources.Members.GivenName, Resources.Members.FamilyName, Resources.Members.Phone, Resources.Members.Mobile, Resources.Members.Street,
			Resources.Members.Suburb, Resources.Members.Postcode, Resources.Members.Gender, Resources.Members.Age, Resources.Members.DateOfBirth,
			Resources.Members.BirthMonth, Resources.Members.BirthDayOfMonth, Resources.Members.OptInsSurveys, Resources.Members.OptInsNewsletters,
			Resources.Members.Notes, Resources.Members.DateLastModified
		};

		private readonly static List<string> CsvRowItems = new List<string>() {
			"{Id}", "\"{Enabled}\"", "\"{Username}\"", "\"{Password}\"", "\"{Email}\"", "\"{GivenName}\"", "\"{FamilyName}\"", "\"{Phone}\"", "\"{Mobile}\"",
			"\"{Street}\"", "\"{Suburb}\"", "\"{Postcode}\"", "\"{Gender}\"", "{Age}", "\"{DateOfBirth}\"", "\"{BirthMonth}\"", "\"{BirthDayOfMonth}\"",
			"\"{OptInSurveys}\"", "\"{OptInNewsletters}\"", "\"{Notes}\"", "\"{DateModified}\""
		};

		private readonly static string CsvRowTemplate = String.Join(",", CsvRowItems);


		// ReSharper disable InconsistentNaming
		private readonly IMembershipRepository MembershipRepository;
		private readonly IChannelRepository ChannelRepository;
		private readonly ISurveyRepository SurveyRepository;
		private readonly IQuestionChoiceRepository QuestionChoiceRepository;
		// ReSharper restore InconsistentNaming


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

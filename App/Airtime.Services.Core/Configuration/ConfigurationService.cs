﻿using System;
using System.Configuration;

using Airtime.Services.Configuration;

using Xtra.Common;
using Xtra.Common.Web;


namespace Airtime.Services.Core.Configuration
{

	public class ConfigurationService : IConfigurationService
	{

		//TODO: investigate using UriTemplate instead of the custom UrlBuilder class

		public string DataDirectory { get { return AppDomain.CurrentDomain.GetData("DataDirectory").ToString(); } }

		public int MaximumEmailBytes { get { return ConfigurationManager.AppSettings["MaximumEmailBytes"].ConvertTo<int>(); } }
		public int MaximumSongListSize { get { return ConfigurationManager.AppSettings["MaximumSongListSize"].ConvertTo<int>(); } }
		public TimeSpan SessionTimeoutTimeSpan { get { return _sessionTimeoutMinutes; } }
		public TimeSpan AuthTicketTimeoutTimeSpan { get { return _authTicketTimeoutMinutes; } }

		public string HostCode { get { return ConfigurationManager.AppSettings["HostCode"]; } }
		public string XtraRedirectUrlHeader { get { return ConfigurationManager.AppSettings["XtraRedirectUrlHeader"]; } }
		public string AirtimeSupportEmail { get { return ConfigurationManager.AppSettings["Airtime.Support.Email"]; } }


		#region Android URLs
		public string AppThemeDirPath { get { return ConfigurationManager.AppSettings["Android.AppThemeDir.Path"]; } }
		public string AppThemeZipPath { get { return ConfigurationManager.AppSettings["Android.AppThemeZip.Path"]; } }
		#endregion


		#region Airtime2 URLs
		public string Airtime2Url { get { return _airtime2Url; } }
		public string Airtime2RecoverPasswordUrl { get { return _airtime2RecoverPasswordUrl; } }
		public string ChannelThemePath { get { return ConfigurationManager.AppSettings["Airtime2.ChannelTheme.Path"]; } }

		public string BuildAirtime2Url(string path = "", string query = "")
		{
			return _buildAirtime2Url(path, query);
		}
		#endregion


		#region Airtime1 URLs
		public string Airtime1Url { get { return _airtime1Url; } }
		public string Airtime1EditDetailsUrl { get { return _airtime1EditDetailsUrl; } }
		public string Airtime1SurveyPreviewUrl { get { return _airtime1SurveyPreviewUrl; } }
		public string Airtime1EmailPreviewUrl { get { return _airtime1EmailPreviewUrl; } }
		public string Airtime1TokenLoginUrl { get { return _airtime1TokenLoginUrl; } }
		public string Airtime1DefaultThemeUrl { get { return _airtime1DefaultThemeUrl; } }
		public string Airtime1ChannelThemeUrl { get { return _airtime1ChannelThemeUrl; } }
		public string Airtime1DefaultThemePath { get { return _airtime1DefaultThemePath; } }
		public string Airtime1ChannelThemePath { get { return _airtime1ChannelThemePath; } }

		public string BuildAirtime1Url(string path = "", string query = "")
		{
			return _buildAirtime1Url(path, query);
		}
		#endregion


		#region One-time initialization code for config properties
		private static TimeSpan _sessionTimeoutMinutes { get { return TimeSpan.Parse(ConfigurationManager.AppSettings["SessionTimeoutTimeSpan"]); } }
		private static TimeSpan _authTicketTimeoutMinutes { get { return TimeSpan.Parse(ConfigurationManager.AppSettings["AuthTicketTimeoutTimeSpan"]); } }

		private static string _airtime1EditDetailsPath { get { return ConfigurationManager.AppSettings["Airtime1.EditDetails.Url.Path"]; } }
		private static string _airtime1EditDetailsQuery { get { return ConfigurationManager.AppSettings["Airtime1.EditDetails.Url.Query"]; } }
		private static string _airtime1SurveyPreviewPath { get { return ConfigurationManager.AppSettings["Airtime1.SurveyPreview.Url.Path"]; } }
		private static string _airtime1SurveyPreviewQuery { get { return ConfigurationManager.AppSettings["Airtime1.SurveyPreview.Url.Query"]; } }
		private static string _airtime1EmailPreviewPath { get { return ConfigurationManager.AppSettings["Airtime1.EmailPreview.Url.Path"]; } }
		private static string _airtime1EmailPreviewQuery { get { return ConfigurationManager.AppSettings["Airtime1.EmailPreview.Url.Query"]; } }
		private static string _airtime1TokenLoginPath { get { return ConfigurationManager.AppSettings["Airtime1.TokenLogin.Url.Path"]; } }
		private static string _airtime1TokenLoginQuery { get { return ConfigurationManager.AppSettings["Airtime1.TokenLogin.Url.Query"]; } }
		private static string _airtime1DefaultThemePath { get { return ConfigurationManager.AppSettings["Airtime1.DefaultTheme.Url.Path"]; } }
		private static string _airtime1ChannelThemePath { get { return ConfigurationManager.AppSettings["Airtime1.ChannelTheme.Url.Path"]; } }

		private static readonly string _airtime1Url = _buildAirtime1Url();
		private static readonly string _airtime1EditDetailsUrl = _buildAirtime1Url(_airtime1EditDetailsPath, _airtime1EditDetailsQuery);
		private static readonly string _airtime1SurveyPreviewUrl = _buildAirtime1Url(_airtime1SurveyPreviewPath, _airtime1SurveyPreviewQuery);
		private static readonly string _airtime1EmailPreviewUrl = _buildAirtime1Url(_airtime1EmailPreviewPath, _airtime1EmailPreviewQuery);
		private static readonly string _airtime1TokenLoginUrl = _buildAirtime1Url(_airtime1TokenLoginPath, _airtime1TokenLoginQuery);
		private static readonly string _airtime1DefaultThemeUrl = _buildAirtime1Url(_airtime1DefaultThemePath);
		private static readonly string _airtime1ChannelThemeUrl = _buildAirtime1Url(_airtime1ChannelThemePath);


		private static string _airtime2RecoverPasswordPath { get { return ConfigurationManager.AppSettings["Airtime2.RecoverPassword.Url.Path"]; } }
		private static string _airtime2RecoverPasswordQuery { get { return ConfigurationManager.AppSettings["Airtime2.RecoverPassword.Url.Query"]; } }

		private static readonly string _airtime2Url = _buildAirtime2Url();
		private static readonly string _airtime2RecoverPasswordUrl = _buildAirtime2Url(_airtime2RecoverPasswordPath, _airtime2RecoverPasswordQuery);


		private static string _buildAirtime1Url(string path = "", string query = "")
		{
			string scheme = ConfigurationManager.AppSettings["Airtime1.Url.Scheme"];
			string host = ConfigurationManager.AppSettings["Airtime1.Url.Host"];
			int port = ConfigurationManager.AppSettings["Airtime1.Url.Port"].ConvertToOrDefault(-1);
			var builder = new UrlBuilder(scheme, host, port, path, extra: "", query: query);
			return builder.ToString("p");
		}


		private static string _buildAirtime2Url(string path = "", string query = "")
		{
			string scheme = ConfigurationManager.AppSettings["Airtime2.Url.Scheme"];
			string host = ConfigurationManager.AppSettings["Airtime2.Url.Host"];
			int port = ConfigurationManager.AppSettings["Airtime2.Url.Port"].ConvertToOrDefault(-1);
			var builder = new UrlBuilder(scheme, host, port, path, extra: "", query: query);
			return builder.ToString("p");
		}
		#endregion

	}

}

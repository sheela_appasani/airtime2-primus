﻿using System;
using System.Collections.Generic;

using OfficeOpenXml;


namespace Airtime.Services.Core
{

	internal class ExcelWorksheetCursor
	{

		public ExcelWorksheetCursor(ExcelWorksheet worksheet, int row = 0, int col = 0)
		{
			_worksheet = worksheet;
			_row = row;
			_col = col;
		}


		public ExcelRange AddCells(IEnumerable<object> values, Action<ExcelRange> cellAction = null)
		{
			int firstCol = _col + 1;
			foreach (var value in values) {
				AddCell(value, cellAction);
			}
			return _worksheet.Cells[_row, firstCol, _row, _col];
		}


		public ExcelRange AddCell(object value = null, Action<ExcelRange> cellAction = null)
		{
			_col++;
			if (_row == 0) {
				_row = 1;
			}

			var cell = _worksheet.Cells[_row, _col];
			if (cellAction != null) {
				cellAction(cell);
			}

			cell.Value = value;
			return cell;
		}


		public ExcelRow NextRow()
		{
			_row++;
			_col = 0;
			return _worksheet.Row(_row);
		}


		private int _row;
		private int _col;
		private readonly ExcelWorksheet _worksheet;

	}

}

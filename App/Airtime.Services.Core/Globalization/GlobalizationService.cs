﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;

using Airtime.Data.Globalization;
using Airtime.Services.Globalization;


namespace Airtime.Services.Core.Globalization
{

	public class GlobalizationService : IGlobalizationService
	{

		public GlobalizationService(ILanguageRepository languageRepository)
		{
			LanguageRepository = languageRepository;
		}


		public Language DefaultLanguage
		{
			get { return LanguageRepository.FindByName("English"); }
		}


		public TimeZoneInfo DefaultTimeZone
		{
			get { return TimeZoneInfo.Local; }
		}


		/// <summary>
		/// Convert a .NET date formatting string to a JQuery date formatting string (or as close as possible)
		/// </summary>
		/// <param name="format"></param>
		/// <returns></returns>
		public string ConvertDateFormatToJquery(string format)
		{
			return format
				.Replace("dddd", "DD")
				.Replace("ddd", "D")
				.Replace("MMMM", "~_~_")
				.Replace("MMM", "~_")
				.Replace("MM", "mm")
				.Replace("M", "m")
				.Replace("~_", "M")
				.Replace("yyyy", "~_~_")
				.Replace("yy", "~_")
				.Replace("~_", "y");
		}


		/// <summary>
		/// Convert a .NET time formatting string to a JQuery time formatting string (or as close as possible)
		/// </summary>
		/// <param name="format"></param>
		/// <returns></returns>
		public string ConvertTimeFormatToJquery(string format)
		{
			return format
				.Replace("H", "h")
				.Replace("tt", "TT")
				.Replace("t", "tt");
		}


		public string GetJQueryDayMonthFormat()
		{
			return ConvertDateFormatToJquery("d MMM");
		}


		public string ConvertCultureForCkEditor(CultureInfo culture = null)
		{
			//Note: had to move the code for this method into the Airtime.Resources assembly even though I
			//think it's more suitable here, because it needs to also be used from some T4 templates and
			//pulling THIS assembly into those templates opens up a whole can or ugliness and worms.

			if (culture == null) {
				culture = CultureInfo.CurrentUICulture;
			}

			return Resources.ResourceCultures.ConvertCultureForCkEditor(culture);
		}


		/// <summary>
		/// Returns a dictionary of all the resource keys and values from the specified ResourceManager with
		/// the current thread's UI culture. Useful for (partially) enumerating resources.
		/// </summary>
		/// <param name="resourceManager"></param>
		/// <returns></returns>
		public Dictionary<string, string> GetResourceDictionary(ResourceManager resourceManager)
		{
			return resourceManager
				.GetResourceSet(CultureInfo.CurrentUICulture, true, true)
				.Cast<DictionaryEntry>()
				.ToDictionary(res => res.Key.ToString(), res => res.Value.ToString());
		}


		private readonly ILanguageRepository LanguageRepository;

	}

}

﻿using System;
using System.Diagnostics.Contracts;
using System.IO;

using Airtime.Services.Media;

using NAudio.Wave;


namespace Airtime.Services.Core.Media
{

	public class MediaService : IMediaService
	{

		public double GetMp3TotalSeconds(string filename)
		{
			using (var stream = new FileStream(filename, FileMode.Open, FileAccess.Read)) {
				return GetMp3TotalSeconds(stream);
			}
		}


		public double GetMp3TotalSeconds(Stream stream)
		{
			var startPosition = stream.Position;

			int ch;
			bool done = false;
			do {
				ch = stream.ReadByte();
				if (ch == 255) {
					ch = stream.ReadByte();
					if ((ch / 16) == 15) {
						done = true;
					}
				}
			} while (!done);

			int version = ((ch % 16) / 4) / 2;
			ch = stream.ReadByte() / 16;

			long kiloBitFileSize = ((8 * stream.Length) / 1000);

			if (stream.CanSeek) {
				stream.Position = startPosition;
			}

			return ((double)kiloBitFileSize / bitrateTable[version, ch]);

			//using (var reader = new Mp3FileReader(stream)) {
			//    return reader.TotalTime.TotalSeconds;
			//}
		}


		private static readonly int[,] bitrateTable = new int[,] {
			// MPEG 2 & 2.5 Layer III
			{0,  8, 16, 24, 32, 40, 48, 56, 64, 80, 96,112,128,144,160,0},
			// MPEG 1 Layer III
			{0, 32, 40, 48, 56, 64, 80, 96,112,128,160,192,224,256,320,0}
		};

	}

}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Hosting;

using Airtime.Data.Remote;

using HtmlAgilityPack;

using Airtime.Data.Client;
using Airtime.Services.Configuration;
using Airtime.Services.Messaging;

using NLog;

using Xtra.Common;
using Xtra.Common.Templating;


namespace Airtime.Services.Core.Messaging
{

	public class MessagingService : IMessagingService
	{

		public MessagingService(IConfigurationService configurationService)
		{
			ConfigurationService = configurationService;
		}


		public string ConvertToNewTemplateFormat(string templateText)
		{
			return templateText
				.Replace("{", "{{").Replace("}", "}}")
				.Replace("[[", "{").Replace("]]", "}");
		}


		public ITemplate GetEmailReferralTemplate(Channel channel)
		{
			var channelInternalName = channel != null ? channel.InternalName : "Airtime";

			string virtualChannelThemePath = ConfigurationService.ChannelThemePath.NamedFormat(new { internalName = channelInternalName });
			string localChannelThemePath = HostingEnvironment.MapPath(virtualChannelThemePath) ?? "";
			string imageUrlPath = Directory.Exists(localChannelThemePath)
				? ConfigurationService.Airtime1ChannelThemePath.NamedFormat(new { internalName = channelInternalName })
				: ConfigurationService.Airtime1DefaultThemePath;

			var templatePath = HostingEnvironment.MapPath(Path.Combine(virtualChannelThemePath, @"templates\email_refer.txt"));
			if (templatePath == null || !File.Exists(templatePath)) {
				//TODO: throw exception?
				return null;
			}

			//Read template file and convert its text to the new format
			var templateString = ConvertToNewTemplateFormat(File.ReadAllText(templatePath));

			var emailTemplate = new StringTemplate(templateString, new {
				CSS = new FileTemplate(Path.Combine(virtualChannelThemePath, "email.css")),
				AppRootPathFull = VirtualPathUtility.RemoveTrailingSlash(ConfigurationService.Airtime1Url),
				AppRootPath = "",
				ImageLocation = VirtualPathUtility.RemoveTrailingSlash(imageUrlPath),
			}, expression => "{" + expression + "}");

			return emailTemplate;
		}


		public ITemplate GetEmailInvitationTemplate(Channel channel)
		{
			var channelInternalName = channel != null ? channel.InternalName : "Airtime";

			string virtualChannelThemePath = ConfigurationService.ChannelThemePath.NamedFormat(new { internalName = channelInternalName });
			string localChannelThemePath = HostingEnvironment.MapPath(virtualChannelThemePath) ?? "";
			string imageUrlPath = Directory.Exists(localChannelThemePath)
				? ConfigurationService.Airtime1ChannelThemePath.NamedFormat(new { internalName = channelInternalName })
				: ConfigurationService.Airtime1DefaultThemePath;

			var loginButtonTemplate = new FileTemplate(HostingEnvironment.MapPath(Path.Combine(virtualChannelThemePath, @"templates\login_button.txt")), new {
				LoginButtonImage = ConfigurationService.BuildAirtime1Url(Path.Combine(imageUrlPath, "images/btn/start-now.jpg")),
				LoginButtonMessage = Resources.Emails.ClickHereToLogin
			}, expression => "[[" + expression + "]]");

			var emailTemplate = new FileTemplate(HostingEnvironment.MapPath(Path.Combine(virtualChannelThemePath, @"templates\email_invitation.txt")), new {
				CSS = new FileTemplate(Path.Combine(virtualChannelThemePath, "email.css")),
				AppRootPathFull = VirtualPathUtility.RemoveTrailingSlash(ConfigurationService.Airtime1Url),
				AppRootPath = "",
				ImageLocation = VirtualPathUtility.RemoveTrailingSlash(imageUrlPath),
				EmailBody = loginButtonTemplate
			}, expression => "[[" + expression + "]]");

			return emailTemplate;
		}


		public ITemplate GetEmailMarketingTemplate(Channel channel)
		{
			var channelInternalName = channel != null ? channel.InternalName : "Airtime";

			string virtualChannelThemePath = ConfigurationService.ChannelThemePath.NamedFormat(new { internalName = channelInternalName });
			string localChannelThemePath = HostingEnvironment.MapPath(virtualChannelThemePath) ?? "";
			string imageUrlPath = Directory.Exists(localChannelThemePath)
				? ConfigurationService.Airtime1ChannelThemePath.NamedFormat(new { internalName = channelInternalName })
				: ConfigurationService.Airtime1DefaultThemePath;

			var emailTemplate = new FileTemplate(HostingEnvironment.MapPath(Path.Combine(virtualChannelThemePath, @"templates\email_marketing.txt")), new {
				CSS = new FileTemplate(Path.Combine(virtualChannelThemePath, "email.css")),
				AppRootPathFull = VirtualPathUtility.RemoveTrailingSlash(ConfigurationService.Airtime1Url),
				AppRootPath = "",
				ImageLocation = VirtualPathUtility.RemoveTrailingSlash(imageUrlPath),
				EmailBody = ""
			}, expression => "[[" + expression + "]]");

			return emailTemplate;
		}


		public ITemplate GetEmailPasswordTemplate(Channel channel)
		{
			var channelInternalName = channel != null ? channel.InternalName : "Airtime";

			string virtualChannelThemePath = ConfigurationService.ChannelThemePath.NamedFormat(new { internalName = channelInternalName });
			string localChannelThemePath = HostingEnvironment.MapPath(virtualChannelThemePath) ?? "";
			string imageUrlPath = Directory.Exists(localChannelThemePath)
				? ConfigurationService.Airtime1ChannelThemePath.NamedFormat(new { internalName = channelInternalName })
				: ConfigurationService.Airtime1DefaultThemePath;

			var emailTemplate = new FileTemplate(HostingEnvironment.MapPath(Path.Combine(virtualChannelThemePath, @"templates\email_password.txt")), new {
				CSS = new FileTemplate(Path.Combine(virtualChannelThemePath, "email.css")),
				AppRootPathFull = VirtualPathUtility.RemoveTrailingSlash(ConfigurationService.Airtime1Url),
				AppRootPath = "",
				ImageLocation = VirtualPathUtility.RemoveTrailingSlash(imageUrlPath),
				EmailBody = ""
			}, expression => "{" + expression + "}");

			return emailTemplate;
		}


		public ITemplate GetEmailPasswordTemplate(App app, string apiVersion)
		{
			string virtualThemePath = ConfigurationService.AppThemeDirPath.NamedFormat(new { apiVersion, appCode = app.Code });

			var emailTemplate = new FileTemplate(HostingEnvironment.MapPath(Path.Combine(virtualThemePath, @"web\templates\email_password.txt")), new {
				CSS = new FileTemplate(Path.Combine(virtualThemePath, @"web\email.css")),
				AppRootPathFull = VirtualPathUtility.RemoveTrailingSlash(ConfigurationService.Airtime1Url),
				AppRootPath = "",
				EmailBody = ""
			}, expression => "{" + expression + "}");

			return emailTemplate;
		}


		public bool SendHtmlEmail(string fromAddress, string toAddress, string subject, string bodyHtml)
		{
			try {
				var message = new MailMessage(fromAddress, toAddress, subject, bodyHtml) {
					IsBodyHtml = true
				};

				var smtp = new SmtpClient();
				smtp.Send(message);

			} catch (Exception ex) {
				string errorMessage = ex.InnerException != null
					? String.Format("Email errored: {0} ({1} {2})", toAddress, ex.Message, ex.InnerException.Message)
					: String.Format("Email errored: {0} ({1})", toAddress, ex.Message);
				Log.Error(errorMessage, ex);
				return false;
			}

			return true;
		}


		public bool SendTextEmail(string fromAddress, string toAddress, string subject, string bodyText)
		{
			try {
				var message = new MailMessage(fromAddress, toAddress, subject, bodyText) {
					IsBodyHtml = false
				};

				var smtp = new SmtpClient();
				smtp.Send(message);

			} catch (Exception ex) {
				string errorMessage = ex.InnerException != null
					? String.Format("Email errored: {0} ({1} {2})", toAddress, ex.Message, ex.InnerException.Message)
					: String.Format("Email errored: {0} ({1})", toAddress, ex.Message);
				Log.Error(errorMessage, ex);
				return false;
			}

			return true;
		}


		public string SanitizeHtmlForEmail(string content)
		{
			var doc = new HtmlDocument {
				OptionOutputAsXml = false,
				OptionWriteEmptyNodes = true
			};

			doc.LoadHtml(content);
			if (doc.DocumentNode == null) {
				return String.Empty;
			}

			var nodes = doc.DocumentNode.Descendants();

			//Remove blacklisted tags or tags with blacklisted attributes
			nodes.Where(x => CheckBlacklist(x) || x.Attributes.Any(CheckBlacklist))
				.ToList()
				.ForEach(x => x.ParentNode.RemoveChild(x));

			//Sanitise/remove tags with greylisted attributes
			nodes.Where(x => x.Attributes.Any(CheckGreylist))
				.SelectMany(x => x.Attributes.Where(CheckGreylist))
				.ToList()
				.ForEach(x => {
					if (x.OwnerNode.HasChildNodes) {
						x.Remove();
					} else {
						x.OwnerNode.ParentNode.RemoveChild(x.OwnerNode);
					}
				});

			//Find any links that have a "TokenUrl" template tag in them and truncate everything in the URL that is LEFT of the tag
			//(due to one client repeatedly, over and over, screwing up the links and putting crap in there left of the tag)
			nodes.Where(x => x.Attributes.Any(CheckTokenUrls))
				.SelectMany(x => x.Attributes.Where(CheckTokenUrls))
				.ToList()
				.ForEach(x => {
					int pos = x.Value.IndexOf(TokenUrlTag, StringComparison.OrdinalIgnoreCase);
					if (pos > 0) {
						x.Value = x.Value.Substring(pos);
					}
				});

			return doc.DocumentNode.WriteTo().Trim();
		}


		private bool CheckBlacklist(HtmlNode node)
		{
			return
				TagBlacklist.Contains(node.Name)
				|| node.Id.Equals("skype_highlighting_settings", StringComparison.OrdinalIgnoreCase);
		}


		private bool CheckBlacklist(HtmlAttribute attr)
		{
			return
				attr.Name == "src" && (
					attr.Value.StartsWith("file:")
					|| attr.Value.StartsWith("javascript:")
					|| attr.Value.StartsWith("jscript:")
					|| attr.Value.StartsWith("vbscript:")
				);
		}


		private bool CheckGreylist(HtmlAttribute attr)
		{
			return
				attr.Name == "href" && (
					attr.Value.StartsWith("file:")
					|| attr.Value.StartsWith("javascript:")
					|| attr.Value.StartsWith("jscript:")
					|| attr.Value.StartsWith("vbscript:")
				);
		}


		private bool CheckTokenUrls(HtmlAttribute attr)
		{
			return (attr.Name == "src" || attr.Name == "href")
				&& attr.Value.Contains(TokenUrlTag, StringComparison.OrdinalIgnoreCase);
		}


		private const string TokenUrlTag = "[[TokenUrl]]";

		private readonly IConfigurationService ConfigurationService;

		private static readonly HashSet<string> TagBlacklist = new HashSet<string>() { "script", "object", "applet", "embed", "frame", "iframe", "frameset", "meta" };
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

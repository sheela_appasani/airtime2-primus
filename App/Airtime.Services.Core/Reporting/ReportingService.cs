﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Drawing;
using System.IO;
using System.Linq;

using Airtime.Data;
using Airtime.Data.Client;
using Airtime.Data.Media;
using Airtime.Data.Account;
using Airtime.Data.Messaging;
using Airtime.Data.Reporting;
using Airtime.Data.Surveying;
using Airtime.Data.Surveying.Answers;
using Airtime.Services.Reporting;

using NLog;

using OfficeOpenXml;
using OfficeOpenXml.Style;

using Xtra.Common;


namespace Airtime.Services.Core.Reporting
{

	public class ReportingService : IReportingService
	{

		public List<DateRange> DefaultBirthdayRanges { get { return _defaultBirthdayRanges; } }
		public List<DateRange> DefaultNewMemberRanges { get { return _defaultNewMemberRanges; } }
		public List<DateRange> DefaultParticipationRanges { get { return _defaultParticipationRanges; } }
		public Dictionary<RatingLevelEnum, Range<int>> DefaultRatingRanges { get { return _defaultRatingRanges; } }


		public ReportingService(IReportingQuery reportingQuery, ISurveyRepository surveyRepository, IReportChannelCacheRepository reportChannelCacheRepository, IChannelRepository channelRepository)
		{
			ReportingQuery = reportingQuery;
			SurveyRepository = surveyRepository;
			ReportChannelCacheRepository = reportChannelCacheRepository;
			ChannelRepository = channelRepository;
		}


		public void ExportLoyaltyAsExcel(Stream stream, List<Channel> channels, DateTime endFromDate, DateTime endToDate, int? minimumInvited, int? minimumCompleted, double? minimumLoyalty)
		{
			Contract.Requires(stream != null, "stream");
			Contract.Requires(channels != null, "channels");
			Contract.Requires(channels.Count > 0);

			var header = new[] {
				"Loyalty", "InvitedCount", "CompletedCount", "IosCompletedCount", "AndroidCompletedCount", "LastSurveyCompleted", "UserId", "Email",
				"GivenName", "FamilyName", "DateOfBirth", "Age", "Gender", "DaytimePhone", "Address", "Suburb", "City", "Postcode"
			};

			using (var excel = new ExcelPackage()) {
				foreach (var channel in channels) {
					var loyaltyData = ReportingQuery.GetLoyaltyReport(new List<Channel> { channel }, endFromDate, endToDate, minimumInvited ?? 0, minimumCompleted ?? 0, minimumLoyalty ?? 0);

					var worksheet = excel.Workbook.Worksheets.Add(channel.AdminName);
					worksheet.View.FreezePanes(2, 1);
					worksheet.PrinterSettings.RepeatRows = new ExcelAddress("1:1");

					var range = worksheet.Cells[1, 1, 1, header.Length];
					range.Style.Border.Top.Style = ExcelBorderStyle.Dotted;
					range.Style.Border.Right.Style = ExcelBorderStyle.Dotted;
					range.Style.Border.Bottom.Style = ExcelBorderStyle.Dotted;
					range.Style.Border.Left.Style = ExcelBorderStyle.Dotted;
					range.Style.Fill.PatternType = ExcelFillStyle.Solid;
					range.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#DFEFFC"));
					range.Style.Font.Color.SetColor(ColorTranslator.FromHtml("#2E6E9E"));
					range.Style.Font.Bold = true;

					var cursor = new ExcelWorksheetCursor(worksheet);
					cursor.AddCells(header, c => c.AutoFitColumns());

					foreach (var loyaltyDatum in loyaltyData) {
						cursor.NextRow();
						cursor.AddCell(loyaltyDatum.Loyalty);
						cursor.AddCell(loyaltyDatum.InvitedCount);
						cursor.AddCell(loyaltyDatum.CompletedCount);
						cursor.AddCell(loyaltyDatum.IosCompletedCount);
						cursor.AddCell(loyaltyDatum.AndroidCompletedCount);
						cursor.AddCell(loyaltyDatum.LastSurveyCompleted, c => c.Style.Numberformat.Format = "m/d/yy h:mm");
						cursor.AddCell(loyaltyDatum.UserId);
						cursor.AddCell(loyaltyDatum.Email);
						cursor.AddCell(loyaltyDatum.GivenName);
						cursor.AddCell(loyaltyDatum.FamilyName);
						cursor.AddCell(loyaltyDatum.DateOfBirth, c => c.Style.Numberformat.Format = "mm-dd-yy");
						cursor.AddCell(loyaltyDatum.Age);
						cursor.AddCell(loyaltyDatum.Gender);
						cursor.AddCell(loyaltyDatum.DaytimePhone, c => c.Style.Numberformat.Format = "@");
						cursor.AddCell(loyaltyDatum.Address);
						cursor.AddCell(loyaltyDatum.Suburb);
						cursor.AddCell(loyaltyDatum.City);
						cursor.AddCell(loyaltyDatum.Postcode, c => c.Style.Numberformat.Format = "@");
					}
				}

				excel.SaveAs(stream);
			}
		}


		public IList GetChannelSummaryStationMost(Channel channel)
		{
			return ReportingQuery.GetChannelSummaryStationMost(channel);
		}


		public IList GetChannelSummaryStationsCumed(Channel channel)
		{
			return ReportingQuery.GetChannelSummaryStationsCumed(channel);
		}


		public IList GetChannelSummaryTimeSpentListening(Channel channel)
		{
			return ReportingQuery.GetChannelSummaryTimeSpentListening(channel);
		}


		private List<AgeGenderTotal> GetChannelAgeGenderTotals(Channel channel, IList<DateRange> birthdateRanges)
		{
			return birthdateRanges.Select(x => GetChannelAgeGenderTotal(channel, x)).ToList();
		}


		public IList<SongReport> GetMusicSummary(IList<int> surveyIds, IList<int> choiceIds = null, PersonGenderEnum? gender = null, int? lowerAge = null, int? upperAge = null)
		{
			return ReportingQuery.GetMusicSummary(surveyIds, choiceIds, gender, lowerAge, upperAge);
		}


		public IList<Membership> GetMusicSummaryMembers(Song song, int lowerRating, int upperRating, IList<int> surveyIds, IList<int> choiceIds = null, PersonGenderEnum? gender = null, int? lowerAge = null, int? upperAge = null)
		{
			return ReportingQuery.GetMusicSummaryMembers(song, lowerRating, upperRating, surveyIds, choiceIds, gender, lowerAge, upperAge);
		}


		public IList<OpenEndedReport> GetReportOpenEnded(IList<int> surveyIds, IList<int> choiceIds = null, PersonGenderEnum? gender = null, int? lowerAge = null, int? upperAge = null)
		{
			return ReportingQuery.GetReportOpenEnded(surveyIds, choiceIds, gender, lowerAge, upperAge);
		}


		public IList<MultiChoiceReport> GetReportMultiChoice(IList<int> surveyIds, IList<int> choiceIds = null, PersonGenderEnum? gender = null, int? lowerAge = null, int? upperAge = null)
		{
			return ReportingQuery.GetReportMultiChoice(surveyIds, choiceIds, gender, lowerAge, upperAge);
		}


		public List<Survey> GetRecentSurveys(Channel channel, int num = 6)
		{
			Contract.Requires(channel != null);
			Contract.Requires(num > 0);

			return SurveyRepository
				.Where(s => s.Channel == channel && s.Schedule.StartDate < DateTime.UtcNow)
				.OrderByDescending(s => s.Schedule.StartDate)
				.Take(num)
				.ToList();
		}


		public ReportChannelCache GetReportChannelCache(Channel channel)
		{
			return ReportChannelCacheRepository
				.Where(s => s.Channel == channel)
				.OrderByDescending(s => s.CacheUtcDate)
				.FirstOrDefault();
		}


		public ReportChannelCache BuildReportChannelCache(Channel channel, IQueryable<Membership> membershipQuery, int totalMembers, TimeIt timeIt)
		{
			var rcCache = new ReportChannelCache {
				Channel = channel,
				CacheUtcDate = DateTime.UtcNow
			};

			timeIt.Start("AgeGender");
			GetChannelAgeGenderTotals(channel, DefaultBirthdayRanges, rcCache);
			rcCache.DemographicsTotal = totalMembers;
			timeIt.Stop();

			var statsEnumerable = new List<ReportChannelCacheStat>().AsEnumerable();

			timeIt.Start("NewMembers");
			var newMembers = DefaultNewMemberRanges
				.Select(dateRange =>
					new ReportChannelCacheStat {
						Label = dateRange.Label,
						Count = NewMemberSince(membershipQuery, dateRange),
						Total = totalMembers,
						Report = CacheReportEnum.NewMembers,
						ReportChannelCache = rcCache
					});
			statsEnumerable = statsEnumerable.Concat(newMembers);
			timeIt.Stop();

			timeIt.Start("NewMembersInTarget");
			var newMembersInTarget = DefaultNewMemberRanges
				.Select(dateRange =>
					new ReportChannelCacheStat {
						Label = dateRange.Label,
						Count = NewMemberSince(ApplyChannelTargetToMemberships(membershipQuery, channel), dateRange),
						Total = totalMembers,
						Report = CacheReportEnum.NewMembersInTarget,
						ReportChannelCache = rcCache
					});
			statsEnumerable = statsEnumerable.Concat(newMembersInTarget);
			timeIt.Stop();

			timeIt.Start("ParticipationLevel");
			var participationLevel = DefaultParticipationRanges
				.Select(dateRange =>
					new ReportChannelCacheStat {
						Label = dateRange.Label,
						Count = dateRange.From.HasValue // special case OptIn value.
									? ReportingQuery.GetParticipationRateQuery(channel, dateRange)
									: GetParticipation(membershipQuery),
						Total = totalMembers,
						Report = CacheReportEnum.Participation,
						ReportChannelCache = rcCache
					});
			statsEnumerable = statsEnumerable.Concat(participationLevel);
			timeIt.Stop();

			timeIt.Start("StationMost");
			var stationMost = DataRowToCacheStat(
				GetChannelSummaryStationMost(channel), CacheReportEnum.StationMost, rcCache);
			statsEnumerable = statsEnumerable.Concat(stationMost);
			timeIt.Stop();

			timeIt.Start("StationsListen");
			var stationsListen = DataRowToCacheStat(
				GetChannelSummaryStationsCumed(channel), CacheReportEnum.StationsListen, rcCache);
			statsEnumerable = statsEnumerable.Concat(stationsListen);
			timeIt.Stop();

			timeIt.Start("TimeSpentListening");
			var timeSpentListening = DataRowToCacheStat(
				GetChannelSummaryTimeSpentListening(channel), CacheReportEnum.TimeSpentListening, rcCache);
			statsEnumerable = statsEnumerable.Concat(timeSpentListening);
			timeIt.Stop();

			rcCache.Stats = statsEnumerable.ToList();
			return rcCache;
		}


		public IQueryable<Membership> ApplyChannelTargetToMemberships(IQueryable<Membership> membershipQuery, Channel channel)
		{
			Contract.Requires(membershipQuery != null, "membershipQuery");
			Contract.Requires(channel != null, "channel");

			if (channel.TargetGender.HasValue) {
				membershipQuery = membershipQuery.Where(u => u.Person.Personal.Gender.HasValue && channel.TargetGender.Value == u.Person.Personal.Gender.Value);
			}
			if (channel.TargetMinimumAge > 0) {
				var dobBefore = DateTime.UtcNow.AddYears(-channel.TargetMinimumAge);
				membershipQuery = membershipQuery.Where(u => u.Person.Personal.DateOfBirth.HasValue && u.Person.Personal.DateOfBirth.Value.Date <= dobBefore);
			}
			if (channel.TargetMaximumAge > 0) {
				var dobAfter = DateTime.UtcNow.AddYears(-(channel.TargetMaximumAge + 1)).AddDays(1);
				membershipQuery = membershipQuery.Where(u => u.Person.Personal.DateOfBirth.HasValue && u.Person.Personal.DateOfBirth.Value.Date >= dobAfter);
			}

			return membershipQuery;
		}


		public IList<EmailBlastOverview> GetEmailBlastOverviews()
		{
			//TODO: return view-model objects instead?
			return ReportingQuery.GetEmailBlastOverviews();
		}


		public IQueryable<Membership> GetChannelMembers(Channel channel)
		{
			Contract.Requires(channel != null, "channel");

			return ChannelRepository
				.Where(c => c == channel)
				.SelectMany(c => c.Memberships)
				.Where(u => u.Enabled && u.Role == MembershipRoleEnum.Respondent);
		}


		private AgeGenderTotal GetChannelAgeGenderTotal(Channel channel, DateRange dateRange)
		{
			Contract.Requires(channel != null, "channel");
			Contract.Requires(dateRange != null, "dateRange");

			var genderCounts = ReportingQuery.GetDemographicCountQuery(channel, dateRange);
			var ageGenderTotal = new AgeGenderTotal { AgeRange = dateRange.Label };
			foreach (var data in genderCounts) {
				switch (data.Item1) {
					case PersonGenderEnum.M:
						ageGenderTotal.SubTotalMale = data.Item2;
						break;
					case PersonGenderEnum.F:
						ageGenderTotal.SubTotalFemale = data.Item2;
						break;
					default:
						ageGenderTotal.SubTotalUnknown = data.Item2;
						break;
				}
			}
			return ageGenderTotal;
		}


		private IEnumerable<ReportChannelCacheStat> DataRowToCacheStat(
			IEnumerable rowEnumerable,
			CacheReportEnum report,
			ReportChannelCache rcCache)
		{
			return rowEnumerable.Cast<IList>().Select(
				rowData => new ReportChannelCacheStat {
					Label = (string)rowData[0],
					Count = (int)rowData[1],
					Total = (int)rowData[2],
					Report = report,
					ReportChannelCache = rcCache
				}).ToList();
		}


		private void GetChannelAgeGenderTotals(Channel channel, IList<DateRange> birthdateRanges, ReportChannelCache rcCache)
		{
			var ageGenderTotals = GetChannelAgeGenderTotals(channel, birthdateRanges);
			rcCache.DemographicsTotalMale = ageGenderTotals[0].SubTotalMale;
			rcCache.DemographicsTotalFemale = ageGenderTotals[0].SubTotalFemale;
			rcCache.DemographicsTotalUnknown = ageGenderTotals[0].SubTotalUnknown;
			rcCache.DemographicsUnder20Male = ageGenderTotals[1].SubTotalMale;
			rcCache.DemographicsUnder20Female = ageGenderTotals[1].SubTotalFemale;
			rcCache.DemographicsUnder20Unknown = ageGenderTotals[1].SubTotalUnknown;
			rcCache.Demographics20_24Male = ageGenderTotals[2].SubTotalMale;
			rcCache.Demographics20_24Female = ageGenderTotals[2].SubTotalFemale;
			rcCache.Demographics20_24Unknown = ageGenderTotals[2].SubTotalUnknown;
			rcCache.Demographics25_29Male = ageGenderTotals[3].SubTotalMale;
			rcCache.Demographics25_29Female = ageGenderTotals[3].SubTotalFemale;
			rcCache.Demographics25_29Unknown = ageGenderTotals[3].SubTotalUnknown;
			rcCache.Demographics30_34Male = ageGenderTotals[4].SubTotalMale;
			rcCache.Demographics30_34Female = ageGenderTotals[4].SubTotalFemale;
			rcCache.Demographics30_34Unknown = ageGenderTotals[4].SubTotalUnknown;
			rcCache.Demographics35_39Male = ageGenderTotals[5].SubTotalMale;
			rcCache.Demographics35_39Female = ageGenderTotals[5].SubTotalFemale;
			rcCache.Demographics35_39Unknown = ageGenderTotals[5].SubTotalUnknown;
			rcCache.Demographics40_44Male = ageGenderTotals[6].SubTotalMale;
			rcCache.Demographics40_44Female = ageGenderTotals[6].SubTotalFemale;
			rcCache.Demographics40_44Unknown = ageGenderTotals[6].SubTotalUnknown;
			rcCache.DemographicsOver45Male = ageGenderTotals[7].SubTotalMale;
			rcCache.DemographicsOver45Female = ageGenderTotals[7].SubTotalFemale;
			rcCache.DemographicsOver45Unknown = ageGenderTotals[7].SubTotalUnknown;
		}


		private int NewMemberSince(IQueryable<Membership> membershipQuery, DateRange dateRange)
		{
			return membershipQuery.Count(u => u.DateCreated > dateRange.From);
		}


		private int GetParticipation(IQueryable<Membership> membershipQuery)
		{
			return membershipQuery.Count(u => u.OptIns.Surveys);
		}


		private readonly List<DateRange> _defaultBirthdayRanges = new List<DateRange> {
			new DateRange(null, null, Resources.Reports.Age_Total),
			new DateRange(null, DateTime.Now.AddYears(-20), Resources.Reports.Age_Under_20),
			new DateRange(DateTime.Now.AddYears(-20), DateTime.Now.AddYears(-25), Resources.Reports.Age_20_24),
			new DateRange(DateTime.Now.AddYears(-25), DateTime.Now.AddYears(-30), Resources.Reports.Age_25_29),
			new DateRange(DateTime.Now.AddYears(-30), DateTime.Now.AddYears(-35), Resources.Reports.Age_30_34),
			new DateRange(DateTime.Now.AddYears(-35), DateTime.Now.AddYears(-40), Resources.Reports.Age_35_39),
			new DateRange(DateTime.Now.AddYears(-40), DateTime.Now.AddYears(-45), Resources.Reports.Age_40_44),
			new DateRange(DateTime.Now.AddYears(-45), null, Resources.Reports.Age_Over_45)
		};


		private readonly List<DateRange> _defaultParticipationRanges = new List<DateRange> {
			new DateRange(null, null, Resources.Reports.OptedIntoSurveys),
			new DateRange(DateTime.Now.AddMonths(-1), null, Resources.Reports.PastMonth),
			new DateRange(DateTime.Now.AddMonths(-3), null, Resources.Reports.Past3Months),
			new DateRange(DateTime.Now.AddMonths(-6), null, Resources.Reports.Past6Months),
			new DateRange(DateTime.Now.AddMonths(-12), null, Resources.Reports.PastYear)
		};


		private readonly Dictionary<RatingLevelEnum, Range<int>> _defaultRatingRanges = new Dictionary<RatingLevelEnum, Range<int>> {
			{ RatingLevelEnum.Unfamiliar, new Range<int>(0, 0, Resources.Reports.Rating_Unfamiliar) },
			{ RatingLevelEnum.Hate, new Range<int>(1, 20, Resources.Reports.Rating_Hate) },
			{ RatingLevelEnum.Dislike, new Range<int>(21, 40, Resources.Reports.Rating_Dislike) },
			{ RatingLevelEnum.Okay, new Range<int>(41, 60, Resources.Reports.Rating_Okay) },
			{ RatingLevelEnum.Like, new Range<int>(61, 80, Resources.Reports.Rating_Like) },
			{ RatingLevelEnum.Love, new Range<int>(81, 100, Resources.Reports.Rating_Love) }
		};


		private readonly List<DateRange> _defaultNewMemberRanges = new List<DateRange> {
			new DateRange(DateTime.Now.AddHours(-24), null, Resources.Reports.Past24Hours),
			new DateRange(DateTime.Now.AddDays(-7), null, Resources.Reports.Past7Days),
			new DateRange(DateTime.Now.AddMonths(-1), null, Resources.Reports.PastMonth),
			new DateRange(DateTime.Now.AddMonths(-3), null, Resources.Reports.Past3Months),
			new DateRange(DateTime.Now.AddMonths(-6), null, Resources.Reports.Past6Months),
			new DateRange(DateTime.Now.AddMonths(-12), null, Resources.Reports.PastYear)
		};


		// ReSharper disable InconsistentNaming
		private readonly IReportingQuery ReportingQuery;
		private readonly ISurveyRepository SurveyRepository;
		private readonly IReportChannelCacheRepository ReportChannelCacheRepository;
		private readonly IChannelRepository ChannelRepository;
		// ReSharper restore InconsistentNaming


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

﻿using System;
using System.Web;
using System.Web.Security;

using Airtime.Data.Account;
using Airtime.Data.Server;
using Airtime.Services.Configuration;
using Airtime.Services.Security;

using NLog;

using Membership = Airtime.Data.Account.Membership;


namespace Airtime.Services.Core.Security
{

	#region Services
	// The FormsAuthentication type is sealed and contains static members, so it is difficult to
	// unit test code that calls its members. The interface and helper class below demonstrate
	// how to create an abstract wrapper around such a type in order to make the AccountController
	// code unit testable.

	public class FormsAuthenticationService : IFormsAuthenticationService
	{

		public FormsAuthenticationService(IPersonRepository personRepository, IMembershipRepository membershipRepository, IConfigurationService configurationService)
		{
			PersonRepository = personRepository;
			MembershipRepository = membershipRepository;
			ConfigurationService = configurationService;
		}


		public string GenerateEncryptedTicket(Person person, Membership membership, TimeSpan validityPeriod)
		{
			return FormsAuthentication.Encrypt(GenerateTicket(person, membership, validityPeriod));
		}


		public void SignIn(Guid guid, bool createPersistentCookie)
		{
			if (guid == Guid.Empty) {
				throw new ArgumentException("Value cannot be empty.", "guid");
			}

			var membership = MembershipRepository.FindByGuid(guid);
			if (membership == null) {
				throw new ArgumentException(String.Format("No Membership with the given guid ({0}) exists.", guid), "guid");
			}

			SignIn(membership, createPersistentCookie);
		}


		public void SignIn(string username, bool createPersistentCookie)
		{
			if (String.IsNullOrEmpty(username)) {
				throw new ArgumentException("Value cannot be null or empty.", "username");
			}

			var membership = MembershipRepository.FindByUsername(username);
			if (membership == null) {
				throw new ArgumentException(String.Format("No Membership with the given username ({0}) can be found.", username), "username");
			}

			SignIn(membership, createPersistentCookie);
		}


		public void SignInByEmail(string email, bool createPersistentCookie)
		{
			if (String.IsNullOrEmpty(email)) {
				throw new ArgumentException("Value cannot be null or empty.", "email");
			}

			var person = PersonRepository.FindByEmail(email);
			if (person == null) {
				throw new ArgumentException(String.Format("No Person with the given email ({0}) can be found.", email), "email");
			}

			SignIn(person, createPersistentCookie);
		}


		public void SignOut()
		{
			FormsAuthentication.SignOut();
		}


		private void SignIn(Membership membership, bool createPersistentCookie)
		{
			var now = DateTime.Now;
			var validityPeriod = createPersistentCookie
				? now.AddYears(50).Subtract(now)
				: ConfigurationService.SessionTimeoutTimeSpan;

			var ticket = GenerateTicket(membership.Person, membership, validityPeriod, createPersistentCookie);
			var ticketEnc = FormsAuthentication.Encrypt(ticket);
			var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, ticketEnc) {
				Expires = ticket.Expiration
			};
			HttpContext.Current.Response.Cookies.Add(cookie);
		}


		private void SignIn(Person person, bool createPersistentCookie)
		{
			var now = DateTime.Now;
			var validityPeriod = createPersistentCookie
				? now.AddYears(50).Subtract(now)
				: ConfigurationService.SessionTimeoutTimeSpan;

			var ticket = GenerateTicket(person, validityPeriod, createPersistentCookie);
			var ticketEnc = FormsAuthentication.Encrypt(ticket);
			var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, ticketEnc) {
				Expires = ticket.Expiration
			};
			HttpContext.Current.Response.Cookies.Add(cookie);
		}


		private FormsAuthenticationTicket GenerateTicket(Person person, TimeSpan validityPeriod, bool createPersistentCookie = true)
		{
			return GenerateTicket(person, null, validityPeriod, createPersistentCookie);
		}


		private FormsAuthenticationTicket GenerateTicket(Membership membership, TimeSpan validityPeriod, bool createPersistentCookie = true)
		{
			return GenerateTicket(null, membership, validityPeriod, createPersistentCookie);
		}


		private FormsAuthenticationTicket GenerateTicket(Person person, Membership membership, TimeSpan validityPeriod, bool createPersistentCookie = true)
		{
			var now = DateTime.Now;
			var expiryDate = now.Add(validityPeriod);

			string name = (person != null)
				? person.Email
				: (membership != null)
					? membership.Username
					: "";

			const int version = 2;

			var userData = SerializeAuthenticationData(version, new AuthenticationData {
				PersonId = person != null ? person.Id : (int?)null,
				MembershipId = membership != null ? membership.Id : (int?)null,
				Role = membership != null ? membership.Role.ToString() : null
			});

			Log.Trace(() => "Encrypting userData: " + userData);

			return new FormsAuthenticationTicket(version, name, now, expiryDate, createPersistentCookie, userData);
		}


		private static string SerializeAuthenticationData(int version, AuthenticationData data)
		{
			switch (version) {
				case 1:
					return data.MembershipId + "," + data.Role;
				case 2:
					return data.PersonId + "," + data.MembershipId + "," + data.Role;
			}
			throw new Exception("Unknown AuthenticationData version " + version);
		}


		private readonly IPersonRepository PersonRepository;
		private readonly IMembershipRepository MembershipRepository;
		private readonly IConfigurationService ConfigurationService;

		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}
	#endregion

}

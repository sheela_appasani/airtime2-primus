﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

using Airtime.Core.Framework.Security;
using Airtime.Data.Account;
using Airtime.Data.Client;
using Airtime.Data.Media;
using Airtime.Data.Messaging;
using Airtime.Data.Surveying;
using Airtime.Data.Surveying.Questions;
using Airtime.Services.Security;

using Membership = Airtime.Data.Account.Membership;


namespace Airtime.Services.Core.Security
{

	public class SecurityService : ISecurityService
	{

		public int MinPasswordLength { get; set; }


		public Person CurrentPerson
		{
			get {
				var principal = HttpContext.Current.User;
				var identity = (principal != null) ? principal.Identity as AirtimeIdentity : null;
				if (identity != null && identity.PersonId.HasValue) {
					return PersonRepository[identity.PersonId.Value];
				}
				return null;
			}
		}


		public Membership CurrentMembership
		{
			get {
				var principal = HttpContext.Current.User;
				var identity = (principal != null) ? principal.Identity as AirtimeIdentity : null;
				if (identity != null && identity.MembershipId.HasValue) {
					return MembershipRepository[identity.MembershipId.Value];
				}
				return null;
			}
		}


		public IList<Channel> AllowedChannels
		{
			get {
				if (CurrentPerson != null) {
					return CurrentPerson.HasAdminMembership()
						? ChannelRepository.ToList()
						: CurrentPerson.Memberships.SelectMany(x => x.Channels).Distinct().ToList();
				}
				if (CurrentMembership != null) {
					return CurrentMembership.IsAdmin()
						? ChannelRepository.ToList()
						: CurrentMembership.Channels;
				}
				throw new Exception("Person must be authenticated.");
			}
		}


		public IList<Channel> MembershipChannels
		{
			get {
				if (CurrentMembership != null) {
					return CurrentMembership.IsAdmin()
						? ChannelRepository.ToList()
						: CurrentMembership.Channels;
				}
				throw new Exception("Membership must be authenticated.");
			}
		}


		public SecurityService(IPersonRepository personRepository, IMembershipRepository membershipRepository, ISongRepository songRepository, IChannelRepository channelRepository, ISurveyRepository surveyRepository, IQuestionRepository questionRepository, IEmailBlastRepository emailBlastRepository)
		{
			PersonRepository = personRepository;
			MembershipRepository = membershipRepository;
			SongRepository = songRepository;
			ChannelRepository = channelRepository;
			SurveyRepository = surveyRepository;
			QuestionRepository = questionRepository;
			EmailBlastRepository = emailBlastRepository;

			MinPasswordLength = 6;
		}


		public IQueryable<Channel> QueryChannelsWithRole(params MembershipRoleEnum[] roles)
		{
			var membership = CurrentMembership;
			if (membership != null) {
				if (roles.Length == 0 || roles.Contains(membership.Role)) {
					return membership.IsAdmin()
						? ChannelRepository.AsQueryable()
						: membership.Channels.AsQueryable();
				} else {
					return Enumerable.Empty<Channel>().AsQueryable();
				}
			}

			var person = CurrentPerson;
			if (person != null) {
				//If CurrentPerson has an Admin/Developer membership, then return ALL channels
				var query = MembershipRepository.SetCacheable(
					MembershipRepository.Where(x => x.Enabled && x.Person == person && (x.Role == MembershipRoleEnum.Administrator || x.Role == MembershipRoleEnum.Developer))
				);
				if (query.Any()) {
					return ChannelRepository.AsQueryable();
				}

				//Otherwise return only channels that the CurrentPerson has memberships for
				return MembershipRepository
					.Where(x => x.Enabled && x.Person == person && (roles.Length == 0 || roles.Contains(x.Role)))
					.SelectMany(x => x.Channels);
			}

			throw new Exception("Person or Membership must be authenticated.");
		}


		public bool AuthenticatePerson(string email, string password)
		{
			if (String.IsNullOrEmpty(email)) throw new ArgumentException("Value cannot be null or empty.", "email");
			if (String.IsNullOrEmpty(password)) throw new ArgumentException("Value cannot be null or empty.", "password");

			//Note: do not query by email & password because we want Password to be case-sensitive and the collation type can't be enforced through NHibernate
			var person = PersonRepository.FindByEmail(email);
			if (person == null || person.Password == null || person.Password.Trim() != password.Trim()) {
				return false;
			}

			if (!person.Memberships.Any(x => x.Enabled)) {
				return false;
			}

			return true;
		}


		public bool AuthenticateMembership(string username, string password)
		{
			if (String.IsNullOrEmpty(username)) throw new ArgumentException("Value cannot be null or empty.", "username");
			if (String.IsNullOrEmpty(password)) throw new ArgumentException("Value cannot be null or empty.", "password");

			//Note: do not query by username & password because we want Password to be case-sensitive and the collation type can't be enforced through NHibernate
			var user = MembershipRepository.FindByUsername(username);
			if (user == null || !user.Enabled || user.Password == null || user.Password.Trim() != password.Trim()) {
				return false;
			}

			return true;
		}


		public bool AuthenticateMembership(Guid guid)
		{
			if (Guid.Empty == guid) {
				throw new ArgumentException("Value cannot be empty.", "guid");
			}

			var user = MembershipRepository.FindByGuid(guid);
			if (user == null || !user.Enabled) {
				return false;
			}

			return true;
		}


		public bool AuthenticateMembershipByEmail(string email, string password)
		{
			if (String.IsNullOrEmpty(email)) throw new ArgumentException("Value cannot be null or empty.", "email");
			if (String.IsNullOrEmpty(password)) throw new ArgumentException("Value cannot be null or empty.", "password");

			password = password.Trim();

			var memberships = MembershipRepository.Where(x => x.Enabled && x.Email == email && x.Password == password).ToList();
			if (memberships.Any(x => x.Password != null && x.Password.Trim() == password)) {
				return true;
			}

			return false;
		}


		/// <summary>
		/// Checks whether the Membership has all of the specified permissions.
		/// </summary>
		/// <param name="permissions"></param>
		/// <returns>True if the Membership has all of the specified permissions.</returns>
		public bool ValidatePermissions(params PermissionEnum[] permissions)
		{
			if (CurrentMembership != null) {
				if (permissions.Length == 0 || CurrentMembership.IsAdmin()) {
					return true;
				}
				return !permissions.Except(CurrentMembership.Permissions).Any();
			}

			if (CurrentPerson != null) {
				if (permissions.Length == 0 || CurrentPerson.HasAdminMembership()) {
					return true;
				}
				return !permissions.Except(CurrentPerson.Memberships.SelectMany(x => x.Permissions).Distinct()).Any();
			}

			return false;
		}


		/// <summary>
		/// Checks whether the Membership has any of the specified permissions.
		/// </summary>
		/// <param name="permissions"></param>
		/// <returns>True if the Membership has any of the specified permissions.</returns>
		public bool ValidateAnyPermissions(params PermissionEnum[] permissions)
		{
			if (CurrentMembership != null) {
				if (permissions.Length == 0 || CurrentMembership.IsAdmin()) {
					return true;
				}
				return permissions.Intersect(CurrentMembership.Permissions).Any();
			}

			if (CurrentPerson != null) {
				if (permissions.Length == 0 || CurrentPerson.HasAdminMembership()) {
					return true;
				}
				return permissions.Intersect(CurrentPerson.Memberships.SelectMany(x => x.Permissions).Distinct()).Any();
			}

			return false;
		}


		public bool ValidateChannelPermissions(Channel channel, params PermissionEnum[] permissions)
		{
			if (CurrentMembership != null) {
				if (permissions.Length == 0 || CurrentMembership.IsAdmin()) {
					return true;
				}
				return CurrentMembership.Channels.Contains(channel) && !permissions.Except(CurrentMembership.Permissions).Any();
			}

			if (CurrentPerson != null) {
				if (permissions.Length == 0 || CurrentPerson.HasAdminMembership()) {
					return true;
				}

				return CurrentPerson.Memberships
					.Where(x => x.Channels.Contains(channel))
					.Any(x => !permissions.Except(x.Permissions).Any());
			}

			return false;
		}


		/// <summary>
		/// Validates whether the current Membership is allowed access to the channel with the specified ID.
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns>Returns true if the current Membership is allowed access to the channel with the specified ID</returns>
		public bool ValidateChannel(int? channelId)
		{
			if (CurrentMembership.IsAdmin() || !channelId.HasValue || channelId == 0) {
				return true;
			}
			return MembershipChannels.Any(c => c.Id == channelId);
		}


		/// <summary>
		/// Validates whether the current Membership is allowed access to the specified channel.
		/// </summary>
		/// <param name="channel"></param>
		/// <returns>Returns true if the current Membership is allowed access to the specified channel</returns>
		public bool ValidateChannel(Channel channel)
		{
			if (CurrentMembership.IsAdmin() || channel == null) {
				return true;
			}
			return MembershipChannels.Contains(channel);
		}


		public bool ValidateChannelRole(int? channelId, params MembershipRoleEnum[] roles)
		{
			if (CurrentMembership.IsAdmin() || !channelId.HasValue || channelId == 0) {
				return true;
			}
			return QueryChannelsWithRole(roles).Where(c => c.Id == channelId).Any();
		}


		public bool ValidateChannelRole(Channel channel, params MembershipRoleEnum[] roles)
		{
			if (CurrentMembership.IsAdmin() || channel == null) {
				return true;
			}
			return QueryChannelsWithRole(roles).Where(c => c == channel).Any();
		}


		public bool ValidateOwnership(Channel channel, Membership membership)
		{
			return CurrentMembership.IsAdmin() || ValidateUserOwnership(channel, membership);
		}


		public bool ValidateOwnership(Channel channel, Song song)
		{
			return CurrentMembership.IsAdmin() || ValidateSongOwnership(channel, song);
		}


		public bool ValidateOwnership(Channel channel, Survey survey)
		{
			return CurrentMembership.IsAdmin() || ValidateSurveyOwnership(channel, survey);
		}


		public bool ValidateOwnership(Channel channel, Survey survey, QuestionBase question)
		{
			return CurrentMembership.IsAdmin() || ValidateQuestionOwnership(channel, survey, question);
		}


		public bool ValidateOwnership(Channel channel, Survey survey, EmailBlast blast)
		{
			return CurrentMembership.IsAdmin() || ValidateBlastOwnership(channel, survey, blast);
		}


		public bool ValidateOwnership(Channel channel, EmailBlast blast)
		{
			return CurrentMembership.IsAdmin() || ValidateBlastOwnership(channel, blast);
		}


		/// <summary>
		/// Validates whether the given Membership is a member of the given channel.
		/// </summary>
		/// <param name="channelId"></param>
		/// <param name="userId"></param>
		/// <returns>Returns true if the Membership is null or owned by the given channel.</returns>
		private bool ValidateUserOwnership(int channelId, int? userId)
		{
			var channel = ChannelRepository[channelId];
			var user = userId.HasValue ? MembershipRepository[userId.Value] : null;
			return ValidateUserOwnership(channel, user);
		}


		/// <summary>
		/// Validates whether the given Membership is a member of the given channel.
		/// </summary>
		/// <param name="channel"></param>
		/// <param name="membership"></param>
		/// <returns>Returns true if the Membership is null or a member of the given channel.</returns>
		private bool ValidateUserOwnership(Channel channel, Membership membership)
		{
			return membership == null || membership.Channels.Contains(channel);
		}


		/// <summary>
		/// Validates whether the given song is owned by the given channel's company.
		/// </summary>
		/// <param name="channelId"></param>
		/// <param name="songId"></param>
		/// <returns>Returns true if the song is null or owned by the given channel's company.</returns>
		private bool ValidateSongOwnership(int channelId, int? songId)
		{
			var channel = ChannelRepository[channelId];
			var song = songId.HasValue ? SongRepository[songId.Value] : null;
			return ValidateSongOwnership(channel, song);
		}


		/// <summary>
		/// Validates whether the given song is owned by the given channel's company.
		/// </summary>
		/// <param name="channel"></param>
		/// <param name="song"></param>
		/// <returns>Returns true if the song is null or owned by the given channel's company.</returns>
		private bool ValidateSongOwnership(Channel channel, Song song)
		{
			return song == null || song.Company == channel.Company;
		}


		/// <summary>
		/// Validates whether the given survey is owned by the given channel.
		/// </summary>
		/// <param name="channelId"></param>
		/// <param name="surveyId"></param>
		/// <returns>Returns true if the survey is null or owned by the given channel.</returns>
		private bool ValidateSurveyOwnership(int channelId, int? surveyId)
		{
			var channel = ChannelRepository[channelId];
			var survey = surveyId.HasValue ? SurveyRepository[surveyId.Value] : null;
			return ValidateSurveyOwnership(channel, survey);
		}


		/// <summary>
		/// Validates whether the given survey is owned by the given channel.
		/// </summary>
		/// <param name="channel"></param>
		/// <param name="survey"></param>
		/// <returns>Returns true if the survey is null or owned by the given channel.</returns>
		private bool ValidateSurveyOwnership(Channel channel, Survey survey)
		{
			return survey == null || survey.Channel == channel;
		}


		/// <summary>
		/// Validates whether the given question is owned by the given survey and channel.
		/// </summary>
		/// <param name="channelId"></param>
		/// <param name="surveyId"></param>
		/// <param name="questionId"></param>
		/// <returns>Returns true if the question is null or owned by the given survey and channel.</returns>
		private bool ValidateQuestionOwnership(int channelId, int surveyId, int? questionId)
		{
			var channel = ChannelRepository[channelId];
			var survey = SurveyRepository[surveyId];
			var question = questionId.HasValue ? QuestionRepository[questionId.Value] : null;
			return ValidateQuestionOwnership(channel, survey, question);
		}


		/// <summary>
		/// Validates whether the given question is owned by the given survey and channel.
		/// </summary>
		/// <param name="channel"></param>
		/// <param name="survey"></param>
		/// <param name="question"></param>
		/// <returns>Returns true if the question is null or owned by the given survey and channel.</returns>
		private bool ValidateQuestionOwnership(Channel channel, Survey survey, QuestionBase question)
		{
			return (question == null || question.Survey == survey) && survey.Channel == channel;
		}


		/// <summary>
		/// Validates whether the given survey-invitation blast is owned by the given survey and channel.
		/// </summary>
		/// <param name="channelId"></param>
		/// <param name="surveyId"></param>
		/// <param name="questionId"></param>
		/// <returns>Returns true if the survey-invitation blast is null or owned by the given survey and channel.</returns>
		private bool ValidateBlastOwnership(int channelId, int surveyId, int? blastId)
		{
			var channel = ChannelRepository[channelId];
			var survey = SurveyRepository[surveyId];
			var blast = blastId.HasValue ? EmailBlastRepository[blastId.Value] : null;
			return ValidateBlastOwnership(channel, survey, blast);
		}


		/// <summary>
		/// Validates whether the given survey-invitation blast is owned by the given survey and channel.
		/// </summary>
		/// <param name="channel"></param>
		/// <param name="survey"></param>
		/// <param name="question"></param>
		/// <returns>Returns true if the survey-invitation blast is null or owned by the given survey and channel.</returns>
		private bool ValidateBlastOwnership(Channel channel, Survey survey, EmailBlast blast)
		{
			//Check that the email blast is really a survey-invitation blast
			if (blast != null && blast.EmailBlastType != EmailBlastTypeEnum.SurveyEmail) {
				throw new ArgumentException("Blast should be a Survey Invitations Blast", "blast");
			}
			return (blast == null || blast.Survey == survey) && survey.Channel == channel;
		}


		/// <summary>
		/// Validates whether the given marketing-email blast is owned by the given channel.
		/// </summary>
		/// <param name="channelId"></param>
		/// <param name="surveyId"></param>
		/// <returns>Returns true if the marketing-email blast is null or owned by the given channel.</returns>
		private bool ValidateBlastOwnership(int channelId, int? blastId)
		{
			var channel = ChannelRepository[channelId];
			var blast = blastId.HasValue ? EmailBlastRepository[blastId.Value] : null;
			return ValidateBlastOwnership(channel, blast);
		}


		/// <summary>
		/// Validates whether the given marketing-email blast is owned by the given channel.
		/// </summary>
		/// <param name="channel"></param>
		/// <param name="survey"></param>
		/// <returns>Returns true if the marketing-email blast is null or owned by the given channel.</returns>
		private bool ValidateBlastOwnership(Channel channel, EmailBlast blast)
		{
			//Check that the email blast is really a marketing-email blast
			if (blast != null && blast.EmailBlastType != EmailBlastTypeEnum.ChannelEmail) {
				throw new ArgumentException("Blast should be a Marketing Emails Blast", "blast");
			}
			return blast == null || blast.Channel == channel;
		}


		public bool ChangePassword(string userName, string oldPassword, string newPassword)
		{
			if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
			if (String.IsNullOrEmpty(oldPassword)) throw new ArgumentException("Value cannot be null or empty.", "oldPassword");
			if (String.IsNullOrEmpty(newPassword)) throw new ArgumentException("Value cannot be null or empty.", "newPassword");

			var user = MembershipRepository.FindByUsername(userName);
			if (user == null || user.Password != oldPassword) {
				return false;
			}
	
			user.Password = newPassword;
			MembershipRepository.Save(user);

			return true;
		}


		public MembershipCreateStatus CreateUser(Membership newMembership)
		{
			if (newMembership == null) throw new ArgumentNullException("newMembership");
			if (newMembership.Id != 0) throw new ArgumentException("Value must be a new object instance.", "newMembership");
			if (String.IsNullOrEmpty(newMembership.Username)) throw new ArgumentException("Value cannot be null or empty.", "newMembership.Username");
			if (String.IsNullOrEmpty(newMembership.Password)) throw new ArgumentException("Value cannot be null or empty.", "newMembership.Password");

			var oldUser = MembershipRepository.FindByUsername(newMembership.Username);
			if (oldUser != null) {
				return MembershipCreateStatus.DuplicateUserName;
			}

			MembershipRepository.Save(newMembership);

			return MembershipCreateStatus.Success;
		}


		private readonly IPersonRepository PersonRepository;
		private readonly IMembershipRepository MembershipRepository;
		private readonly ISongRepository SongRepository;
		private readonly IChannelRepository ChannelRepository;
		private readonly ISurveyRepository SurveyRepository;
		private readonly IQuestionRepository QuestionRepository;
		private readonly IEmailBlastRepository EmailBlastRepository;

	}

}

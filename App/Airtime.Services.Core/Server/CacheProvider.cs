﻿using System;
using System.Diagnostics.Contracts;

using Airtime.Services.Server;


namespace Airtime.Services.Core.Server
{

	public class CacheProvider : ICacheProvider
	{

		public ICache InnerCache { get; private set; }


		public int Count { get { return InnerCache.Count; } }


		public CacheProvider(ICache cache)
		{
			InnerCache = cache;
		}


		public bool Contains(string cacheKey)
		{
			return InnerCache.Contains(cacheKey);
		}


		public T Get<T>(string cacheKey, DateTime absoluteExpiryDate, Func<T> getData) where T : class
		{
			return GetData(cacheKey, data => Add(cacheKey, absoluteExpiryDate, data), getData);
		}


		public T Get<T>(string cacheKey, TimeSpan slidingExpiryWindow, Func<T> getData) where T : class
		{
			return GetData(cacheKey, data => Add(cacheKey, slidingExpiryWindow, data), getData);
		}


		public T Get<T>(DateTime absoluteExpiryDate, Func<T> getData) where T : class
		{
			Contract.Requires(getData != null);
			return Get<T>(GetCacheKeyFromFuncDelegate(getData), absoluteExpiryDate, getData);
		}


		public T Get<T>(TimeSpan slidingExpiryWindow, Func<T> getData) where T : class
		{
			Contract.Requires(getData != null);
			return Get<T>(GetCacheKeyFromFuncDelegate(getData), slidingExpiryWindow, getData);
		}


		public void Evict(string cacheKey)
		{
			InnerCache.Evict(cacheKey);
		}


		public void Add(string cacheKey, object dataToAdd)
		{
			InnerCache.Add(cacheKey, dataToAdd);
		}


		public void Add(string cacheKey, DateTime absoluteExpiryDate, object dataToAdd)
		{
			InnerCache.Add(cacheKey, absoluteExpiryDate, dataToAdd);
		}


		public void Add(string cacheKey, TimeSpan slidingExpiryWindow, object dataToAdd)
		{
			InnerCache.Add(cacheKey, slidingExpiryWindow, dataToAdd);
		}


		public void Clear()
		{
			InnerCache.Clear();
		}


		private T GetData<T>(string cacheKey, Action<T> onCacheMiss, Func<T> getData) where T : class
		{
			var data = InnerCache.Get<T>(cacheKey);
			if (data == null) {
				//CacheKey is missing from the cache
				if (getData != null) {
					//Can get data externally, execute callback to retrieve it
					data = getData();
				}
				if (onCacheMiss != null) {
					//Execute callback to handle the cache-miss, pass the data too if we now have it
					onCacheMiss(data);
				}
			}
			return data;
		}


		private string GetCacheKeyFromFuncDelegate<T>(Func<T> getData) where T : class
		{
			return getData.Method.DeclaringType.FullName + "-" + getData.Method.Name;
		}

	}

}

﻿using System;
using System.Runtime.Caching;

using Airtime.Services.Server;


namespace Airtime.Services.Core.Server
{

	public class MemoryCacheAdapter : ICache
	{

		public int Count { get; private set; }


		public bool Contains(string cacheKey)
		{
			return _cache.Contains(cacheKey);
		}


		public T Get<T>(string cacheKey) where T : class
		{
			return _cache.Get(cacheKey) as T;
		}


		public void Add(string cacheKey, object dataToAdd)
		{
			if (dataToAdd != null) {
				_cache.Set(cacheKey, dataToAdd, null);
			}
		}


		public void Add(string cacheKey, DateTime absoluteExpiry, object dataToAdd)
		{
			if (dataToAdd != null) {
				_cache.Set(cacheKey, dataToAdd, new CacheItemPolicy { AbsoluteExpiration = new DateTimeOffset(absoluteExpiry) });
			}
		}


		public void Add(string cacheKey, TimeSpan slidingExpiryWindow, object dataToAdd)
		{
			if (dataToAdd != null) {
				_cache.Set(cacheKey, dataToAdd, new CacheItemPolicy { SlidingExpiration = slidingExpiryWindow });
			}
		}


		public void Evict(string cacheKey)
		{
			_cache.Remove(cacheKey);
		}


		public void Clear()
		{
			var oldCache = _cache;
			_cache = CreateMemoryCache();
			oldCache.Dispose();
		}


		private static MemoryCache CreateMemoryCache()
		{
			return new MemoryCache(typeof(MemoryCacheAdapter).FullName);
		}


		private MemoryCache _cache = CreateMemoryCache();

	}

}

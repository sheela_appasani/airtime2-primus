﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;

using Airtime.Services.Server;

using Microsoft.VisualBasic.Devices;


namespace Airtime.Services.Core.Server
{

	public class ServerService : IServerService
	{

		public ServerService(HttpContextBase httpContext)
		{
			HttpContext = httpContext;
		}


		public string GetMachineName()
		{
			return Environment.MachineName;
		}



		public ulong GetTotalMemory()
		{
			return new ComputerInfo().TotalPhysicalMemory;
		}


		public ulong GetAvailableMemory()
		{
			return new ComputerInfo().AvailablePhysicalMemory;
		}


		public ulong GetProcessMemory()
		{
			return (ulong)Process.GetCurrentProcess().PrivateMemorySize64;
		}


		public bool IsHostedParentUrlOf(string parentUrl, string childUrl)
		{
			string parentPath = HostingEnvironment.MapPath(parentUrl);
			string childPath = HostingEnvironment.MapPath(childUrl);

			string parentLocalPath = new Uri(parentPath).LocalPath;
			string childLocalPath = new Uri(childPath).LocalPath;

			return childLocalPath.StartsWith(parentLocalPath);
		}


		public string SanitizeFilename(string filename)
		{
			return InvalidFilenameRegex.Replace(filename, "");
		}


		private static readonly Regex InvalidFilenameRegex = new Regex("[" + Regex.Escape(new String(Path.GetInvalidFileNameChars())) + "]", RegexOptions.Compiled);

		private HttpContextBase HttpContext { get; set; }

	}

}

﻿using System;
using System.Collections;
using System.Web;
using System.Web.Caching;

using Airtime.Services.Server;


namespace Airtime.Services.Core.Server
{

	public class WebCacheAdapter : ICache
	{

		public int Count { get; private set; }


		public bool Contains(string cacheKey)
		{
			return _cache.Get(cacheKey) != null;
		}


		public void Add(string cacheKey, object dataToAdd)
		{
			_cache.Insert(cacheKey, dataToAdd);
		}


		public void Add(string cacheKey, DateTime expiry, object dataToAdd)
		{
			if (dataToAdd != null) {
				_cache.Insert(cacheKey, dataToAdd, null, expiry, Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
			}
		}


		public void Add(string cacheKey, TimeSpan slidingExpiryWindow, object dataToAdd)
		{
			if (dataToAdd != null) {
				_cache.Insert(cacheKey, dataToAdd, null, Cache.NoAbsoluteExpiration, slidingExpiryWindow, CacheItemPriority.BelowNormal, null);
			}
		}


		public T Get<T>(string cacheKey) where T : class
		{
			T data = _cache.Get(cacheKey) as T;
			return data;
		}


		public void Evict(string cacheKey)
		{
			if (_cache.Get(cacheKey) != null) {
				_cache.Remove(cacheKey);
			}
		}


		public void Clear()
		{
			int i = 0;
			var keys = new string[_cache.Count];
			foreach (DictionaryEntry item in _cache) {
				keys[i] = (string)item.Key;
				i++;
			}
			foreach (var key in keys) {
				_cache.Remove(key);
			}
		}


		private readonly Cache _cache = HttpRuntime.Cache;

	}

}

using System;

using Airtime.Services.Core.Server;
using Airtime.Services.Server;

using Autofac;


namespace Airtime.Services
{

	public class ServicesModule : Module
	{

		protected override void Load(ContainerBuilder builder)
		{
			if (builder == null) {
				throw new ArgumentNullException("builder");
			}

			//Register services
			builder.RegisterAssemblyTypes(Airtime.Services.Core.AssemblyHook.Assembly)
				.Where(t => !t.IsAbstract)
				.AsImplementedInterfaces()
				.InstancePerLifetimeScope();

			builder.RegisterType<MemoryCacheAdapter>()
				.As<ICache>()
				.SingleInstance();
		}

	}

}

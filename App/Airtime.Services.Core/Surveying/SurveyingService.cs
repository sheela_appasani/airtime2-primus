﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Web;
using System.Web.Security;
using Airtime.Data.Client;
using Airtime.Data.Surveying;
using Airtime.Data.Surveying.Answers;
using Airtime.Data.Surveying.Questions;
using Airtime.Services.Surveying;


namespace Airtime.Services.Core.Surveying
{

	public class SurveyingService : ISurveyingService
	{

		public SurveyingService(IChannelRepository channelRepository, ISurveyRepository surveyRepository, IQuestionRepository questionRepository)
		{
			ChannelRepository = channelRepository;
			SurveyRepository = surveyRepository;
			QuestionRepository = questionRepository;
		}


		public void SetupProfileQuestions(Survey survey)
		{
			Contract.Requires(survey != null, "survey");
			Contract.Requires(survey.Channel != null, "survey.Channel");

			survey.ProfileQuestions.Clear();
			foreach (var sourceQuestion in survey.Channel.ProfileQuestions) {
				var newQuestion = CopyQuestion(sourceQuestion, false);
				newQuestion.Survey = survey;
				survey.ProfileQuestions.Add(newQuestion);
			}
		}


		public void SetupQuestions(Survey survey)
		{
			Contract.Requires(survey != null, "survey");

			survey.Questions.Clear();
			survey.Questions.Add(new BreakQuestion { Survey = survey, Position = 0, Category = QuestionCategoryEnum.Normal_RegistrationIntroduction, Name = Resources.Surveys.RegistrationIntroductionPage });
			survey.Questions.Add(new BreakQuestion { Survey = survey, Position = 1, Category = QuestionCategoryEnum.Normal_RegistrationCompleted, Name = Resources.Surveys.RegistrationCompletedPage });
			survey.Questions.Add(new BreakQuestion { Survey = survey, Position = 2, Category = QuestionCategoryEnum.Normal_SurveyIntroduction, Name = Resources.Surveys.SurveyStartPage });
			survey.Questions.Add(new BreakQuestion { Survey = survey, Position = 3, Category = QuestionCategoryEnum.Normal_SurveyCompleted, Name = Resources.Surveys.SurveyCompletedPage });
		}


		public QuestionBase CopyQuestion(QuestionBase sourceQuestion, bool copyBranches)
		{
			Contract.Requires(sourceQuestion != null, "sourceQuestion");

			QuestionBase newQuestion;
			switch (sourceQuestion.QuestionType) {
				case QuestionTypeEnum.AUDIO:
				case QuestionTypeEnum.RADIO:
				case QuestionTypeEnum.CHECKBOX:
				case QuestionTypeEnum.CUSTOMRADIO:
				case QuestionTypeEnum.CUSTOMCHECKBOX:
					newQuestion = new MultiChoiceQuestion((MultiChoiceQuestion)sourceQuestion.Unproxied, copyBranches);
					break;
				case QuestionTypeEnum.VIDEO:
					newQuestion = new VideoQuestion((VideoQuestion)sourceQuestion.Unproxied, copyBranches);
					break;
				case QuestionTypeEnum.AUDIOPOD:
					newQuestion = new AudioPodQuestion((AudioPodQuestion)sourceQuestion.Unproxied);
					break;
				case QuestionTypeEnum.CUSTOMTEXT:
				case QuestionTypeEnum.TEXT:
					newQuestion = new TextQuestion((TextQuestion)sourceQuestion.Unproxied);
					break;
				case QuestionTypeEnum.BREAK:
					newQuestion = new BreakQuestion((BreakQuestion)sourceQuestion.Unproxied);
					break;
				case QuestionTypeEnum.SCALE:
					newQuestion = new ScaleQuestion((ScaleQuestion)sourceQuestion.Unproxied);
					break;
				default:
					throw new ArgumentException("Not a supported question type", "sourceQuestion");
			}
			return newQuestion;
		}


		public List<QuestionBase> CopyQuestions(IList<QuestionBase> sourceQuestions, bool copyBranches)
		{
			Contract.Requires(sourceQuestions != null, "sourceQuestions");

			if (!copyBranches) {
				return sourceQuestions.Select(sourceQuestion => CopyQuestion(sourceQuestion, copyBranches)).ToList();
			}

			var newQuestions = new List<QuestionBase>();
			var copiedQuestions = new Dictionary<QuestionBase, QuestionBase>();

			//Make a copy of each question and track which question it was copied from
			foreach (var sourceQuestion in sourceQuestions) {
				var newQuestion = CopyQuestion(sourceQuestion, copyBranches);
				copiedQuestions.Add(sourceQuestion, newQuestion);
				newQuestions.Add(newQuestion);
			}

			//Update any branches to point to the new copies whenever possible
			foreach (var question in newQuestions.OfType<MultiChoiceQuestion>()) {
				foreach (var choice in question.Choices) {
					if (choice.BranchQuestion != null && copiedQuestions.ContainsKey(choice.BranchQuestion)) {
						choice.BranchQuestion = copiedQuestions[choice.BranchQuestion];
					}
				}
			}

			return newQuestions;
		}


		public Survey CopySurvey(Survey sourceSurvey)
		{
			Contract.Requires(sourceSurvey != null, "sourceSurvey");

			var newSurvey = new Survey(sourceSurvey);
			SetupProfileQuestions(newSurvey);
			newSurvey.Questions = CopyQuestions(sourceSurvey.Questions, true);
			foreach (var question in newSurvey.Questions) {
				question.Survey = newSurvey;
			}
			return newSurvey;
		}


		public AnswerBase CopyAnswerForTemplate(AnswerBase sourceAnswer)
		{
			Contract.Requires(sourceAnswer != null, "sourceAnswer");

			var templateQuestion = sourceAnswer.Question.TemplateQuestion;
			if (templateQuestion == null) {
				return null;
			}

			AnswerBase newAnswer;

			if (sourceAnswer.AnswerType == AnswerTypeEnum.AudioPod) {
				var fromAnswer = (PlaylistAnswer)sourceAnswer;
				var playlistAnswer = new PlaylistAnswer();
				foreach (var fromScore in fromAnswer.SongScores) {
					var newScore = new SongScore(fromScore) { PlaylistAnswer = playlistAnswer };
					playlistAnswer.SongScores.Add(newScore);
				}
				newAnswer = playlistAnswer;

			} else {
				var fromAnswer = (SingleAnswer)sourceAnswer;
				var singleAnswer = new SingleAnswer {
					Choice = (fromAnswer.Choice != null) ? fromAnswer.Choice.TemplateChoice : null,
					Text = fromAnswer.Text
				};
				newAnswer = singleAnswer;
			}

			newAnswer.Membership = sourceAnswer.Membership;
			newAnswer.RespondentState = null;
			newAnswer.Question = templateQuestion;
			newAnswer.IsProfileAnswer = true;

			return newAnswer;
		}


		private readonly IChannelRepository ChannelRepository;
		private readonly ISurveyRepository SurveyRepository;
		private readonly IQuestionRepository QuestionRepository;

	}

}
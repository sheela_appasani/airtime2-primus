using System.IO;
using System.Linq;
using Airtime.Data.Client;
using Airtime.Data.Account;
using Airtime.Data.Surveying;
using Airtime.Models.Members;


namespace Airtime.Services.Account
{

	public interface IMembershipService
	{
		void ExportAsCsv(Stream stream, Channel channel, MemberFilterModel memberFilterModel);
		void ExportAsExcel(Stream stream, Channel channel, MemberFilterModel memberFilterModel);
		IQueryable<Membership> ApplyMemberFilter(IQueryable<Membership> users, MemberFilterModel model, Channel channel);
		IQueryable<Membership> ApplyMemberFilter(IQueryable<Membership> users, UserSurveyStatusEnum status, Survey survey, Channel channel);
	}

}

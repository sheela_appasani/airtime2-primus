using System.Reflection;


namespace Airtime.Services
{
	public abstract class AssemblyHook
	{
		public static Assembly Assembly { get { return typeof(AssemblyHook).Assembly; } }
	}
}
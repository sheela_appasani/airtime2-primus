using System;


namespace Airtime.Services.Configuration
{

	public interface IConfigurationService
	{
		string DataDirectory { get; }

		int MaximumEmailBytes { get; }
		int MaximumSongListSize { get; }
		TimeSpan SessionTimeoutTimeSpan { get; }
		TimeSpan AuthTicketTimeoutTimeSpan { get; }

		string HostCode { get; }
		string XtraRedirectUrlHeader { get; }
		string AirtimeSupportEmail { get; }

		string AppThemeDirPath { get; }
		string AppThemeZipPath { get; }

		string ChannelThemePath { get; }

		string Airtime2Url { get; }
		string Airtime2RecoverPasswordUrl { get; }

		string Airtime1Url { get; }
		string Airtime1EditDetailsUrl { get; }
		string Airtime1SurveyPreviewUrl { get; }
		string Airtime1EmailPreviewUrl { get; }
		string Airtime1TokenLoginUrl { get; }
		string Airtime1DefaultThemeUrl { get; }
		string Airtime1ChannelThemeUrl { get; }

		string Airtime1DefaultThemePath { get; }
		string Airtime1ChannelThemePath { get; }

		string BuildAirtime1Url(string path = "", string query = "");
		string BuildAirtime2Url(string path = "", string query = "");
	}

}

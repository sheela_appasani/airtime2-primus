using System;
using System.Collections.Generic;
using System.Globalization;
using System.Resources;

using Airtime.Data.Globalization;


namespace Airtime.Services.Globalization
{

	public interface IGlobalizationService
	{
		Language DefaultLanguage { get; }
		TimeZoneInfo DefaultTimeZone { get; }

		string ConvertDateFormatToJquery(string format);
		string ConvertTimeFormatToJquery(string format);
		string GetJQueryDayMonthFormat();

		string ConvertCultureForCkEditor(CultureInfo culture = null);
		Dictionary<string, string> GetResourceDictionary(ResourceManager resourceManager);
	}

}
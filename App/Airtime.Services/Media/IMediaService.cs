using System;
using System.IO;


namespace Airtime.Services.Media
{

	public interface IMediaService
	{
		double GetMp3TotalSeconds(string filename);
		double GetMp3TotalSeconds(Stream stream);
	}

}
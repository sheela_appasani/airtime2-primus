using Airtime.Data;
using Airtime.Data.Client;
using Airtime.Data.Remote;

using Xtra.Common.Templating;


namespace Airtime.Services.Messaging
{

	public interface IMessagingService
	{
		string SanitizeHtmlForEmail(string content);
		string ConvertToNewTemplateFormat(string templateText);

		ITemplate GetEmailReferralTemplate(Channel channel);
		ITemplate GetEmailInvitationTemplate(Channel channel);
		ITemplate GetEmailMarketingTemplate(Channel channel);
		ITemplate GetEmailPasswordTemplate(Channel channel);
		ITemplate GetEmailPasswordTemplate(App app, string apiVersion);

		bool SendHtmlEmail(string fromAddress, string toAddress, string subject, string bodyHtml);
		bool SendTextEmail(string fromAddress, string toAddress, string subject, string bodyHtml);
	}

}

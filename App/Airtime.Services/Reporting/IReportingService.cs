﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Airtime.Data.Media;
using Airtime.Data.Account;
using Airtime.Data.Messaging;
using Airtime.Data.Reporting;
using Airtime.Data.Surveying;
using Airtime.Data.Client;
using Airtime.Data.Surveying.Answers;

using Xtra.Common;


namespace Airtime.Services.Reporting
{

	public interface IReportingService
	{

		List<DateRange> DefaultBirthdayRanges { get; }
		List<DateRange> DefaultNewMemberRanges { get; }
		List<DateRange> DefaultParticipationRanges { get; }
		Dictionary<RatingLevelEnum, Range<int>> DefaultRatingRanges { get; }


		void ExportLoyaltyAsExcel(Stream stream, List<Channel> channels, DateTime endFromDate, DateTime endToDate, int? minimumInvited, int? minimumCompleted, double? minimumLoyalty);

		IList GetChannelSummaryStationMost(Channel channel);
		IList GetChannelSummaryStationsCumed(Channel channel);
		IList GetChannelSummaryTimeSpentListening(Channel channel);

		IList<SongReport> GetMusicSummary(IList<int> surveyIds, IList<int> choiceIds = null, PersonGenderEnum? gender = null, int? lowerAge = null, int? upperAge = null);
		IList<Membership> GetMusicSummaryMembers(Song song, int lowerRating, int upperRating, IList<int> surveyIds, IList<int> choiceIds = null, PersonGenderEnum? gender = null, int? lowerAge = null, int? upperAge = null);

		IList<OpenEndedReport> GetReportOpenEnded(IList<int> surveyIds, IList<int> choiceIds = null, PersonGenderEnum? gender = null, int? lowerAge = null, int? upperAge = null);
		IList<MultiChoiceReport> GetReportMultiChoice(IList<int> surveyIds, IList<int> choiceIds = null, PersonGenderEnum? gender = null, int? lowerAge = null, int? upperAge = null);

		List<Survey> GetRecentSurveys(Channel channel, int num = 6);

		ReportChannelCache BuildReportChannelCache(Channel channel, IQueryable<Membership> membershipQuery, int totalMembers, TimeIt timeIt);
		ReportChannelCache GetReportChannelCache(Channel channel);

		IQueryable<Membership> GetChannelMembers(Channel channel);
		IQueryable<Membership> ApplyChannelTargetToMemberships(IQueryable<Membership> membershipQuery, Channel channel);

		IList<EmailBlastOverview> GetEmailBlastOverviews();

	}

}

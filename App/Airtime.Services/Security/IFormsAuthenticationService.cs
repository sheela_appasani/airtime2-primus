﻿using System;

using Airtime.Data.Account;


namespace Airtime.Services.Security
{

	public interface IFormsAuthenticationService
	{
		string GenerateEncryptedTicket(Person person, Membership membership, TimeSpan validityPeriod);
		void SignInByEmail(string username, bool createPersistentCookie);
		void SignIn(string username, bool createPersistentCookie);
		void SignIn(Guid guid, bool createPersistentCookie);
		void SignOut();
	}

}

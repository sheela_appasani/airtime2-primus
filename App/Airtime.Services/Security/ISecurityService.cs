using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;

using Airtime.Data.Client;
using Airtime.Data.Media;
using Airtime.Data.Account;
using Airtime.Data.Messaging;
using Airtime.Data.Surveying;
using Airtime.Data.Surveying.Questions;

using Membership = Airtime.Data.Account.Membership;


namespace Airtime.Services.Security
{

	public interface ISecurityService
	{
		int MinPasswordLength { get; }
		Person CurrentPerson { get; }
		Membership CurrentMembership { get; }
		IList<Channel> AllowedChannels { get; }
		IList<Channel> MembershipChannels { get; }

		IQueryable<Channel> QueryChannelsWithRole(params MembershipRoleEnum[] roles);

		bool ValidatePermissions(params PermissionEnum[] permissions);
		bool ValidateAnyPermissions(params PermissionEnum[] permissions);

		bool ValidateChannelPermissions(Channel channel, params PermissionEnum[] permissions);

		bool ValidateChannel(int? channelId);
		bool ValidateChannel(Channel channel);

		bool ValidateChannelRole(int? channelId, params MembershipRoleEnum[] roles);
		bool ValidateChannelRole(Channel channel, params MembershipRoleEnum[] roles);

		bool ValidateOwnership(Channel channel, Membership membership);
		bool ValidateOwnership(Channel channel, Song song);
		bool ValidateOwnership(Channel channel, Survey survey);
		bool ValidateOwnership(Channel channel, Survey survey, QuestionBase question);
		bool ValidateOwnership(Channel channel, EmailBlast blast);
		bool ValidateOwnership(Channel channel, Survey survey, EmailBlast blast);

		bool AuthenticatePerson(string email, string password);
		bool AuthenticateMembership(string username, string password);
		bool AuthenticateMembership(Guid guid);
		bool AuthenticateMembershipByEmail(string email, string password);

		bool ChangePassword(string username, string oldPassword, string newPassword);
		MembershipCreateStatus CreateUser(Membership newMembership);
	}

}

﻿using System;


namespace Airtime.Services.Server
{

	public interface ICache
	{
		int Count { get; }
		bool Contains(string cacheKey);
		T Get<T>(string cacheKey) where T : class;
		void Add(string cacheKey, object dataToAdd);
		void Add(string cacheKey, DateTime absoluteExpiry, object dataToAdd);
		void Add(string cacheKey, TimeSpan slidingExpiryWindow, object dataToAdd);
		void Evict(string cacheKey);
		void Clear();
	}

}

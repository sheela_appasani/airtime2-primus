﻿using System;


namespace Airtime.Services.Server
{

	public interface ICacheProvider
	{
		int Count { get; }

		bool Contains(string cacheKey);

		T Get<T>(string cacheKey, DateTime absoluteExpiryDate, Func<T> getData) where T : class;
		T Get<T>(string cacheKey, TimeSpan slidingExpiryWindow, Func<T> getData) where T : class;
		T Get<T>(DateTime absoluteExpiryDate, Func<T> getData) where T : class;
		T Get<T>(TimeSpan slidingExpiryWindow, Func<T> getData) where T : class;

		void Add(string cacheKey, DateTime absoluteExpiryDate, object dataToAdd);
		void Add(string cacheKey, TimeSpan slidingExpiryWindow, object dataToAdd);

		void Evict(string cacheKey);
		void Clear();

		ICache InnerCache { get; }
	}

}

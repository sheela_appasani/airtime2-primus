using System;


namespace Airtime.Services.Server
{

	public interface IServerService
	{
		string GetMachineName();

		ulong GetTotalMemory();
		ulong GetAvailableMemory();
		ulong GetProcessMemory();

		bool IsHostedParentUrlOf(string parentUrl, string childUrl);
		string SanitizeFilename(string filename);
	}

}
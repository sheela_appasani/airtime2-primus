using System;

using Airtime.Data.Surveying;
using Airtime.Data.Surveying.Answers;
using Airtime.Data.Surveying.Questions;


namespace Airtime.Services.Surveying
{

	public interface ISurveyingService
	{
		void SetupProfileQuestions(Survey survey);
		void SetupQuestions(Survey survey);

		QuestionBase CopyQuestion(QuestionBase sourceQuestion, bool copyBranches);
		Survey CopySurvey(Survey sourceSurvey);

		AnswerBase CopyAnswerForTemplate(AnswerBase sourceAnswer);
	}

}
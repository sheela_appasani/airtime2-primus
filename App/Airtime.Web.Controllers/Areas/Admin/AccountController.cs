﻿using System;
using System.Web.Mvc;

using Airtime.Core.Framework.Mvc;

using NLog;


namespace Airtime.Web.Controllers.Areas.Admin
{

	public class AccountController : Controller
	{

		[HttpHeader("REQUIRES_AUTH", "1")]
		public ActionResult LogOn(Guid? guid = null, string returnUrl = null)
		{
			if (!String.IsNullOrEmpty(returnUrl)) {
				RouteData.Values["returnUrl"] = returnUrl;
			}
			if (guid.HasValue) {
				RouteData.Values["guid"] = guid;
			}

			Log.Trace("Permanent Redirect: {0} -> {1}", Request.Url.PathAndQuery, Url.Action("LogOn", "Home", RouteData.Values));
			return RedirectToActionPermanent("LogOn", "Home", RouteData.Values); ;
		}


		public ActionResult LogOff()
		{
			Log.Trace("Permanent Redirect: {0} -> {1}", Request.Url.PathAndQuery, Url.Action("LogOff", "Home", RouteData.Values));
			return RedirectToActionPermanent("LogOff", "Home", RouteData.Values);
		}


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

using Airtime.Core;
using Airtime.Core.Framework.Mvc;
using Airtime.Data.Client;
using Airtime.Data.Messaging;
using Airtime.Data.Surveying;
using Airtime.Data.Surveying.Questions;
using Airtime.Models;
using Airtime.Services.Configuration;
using Airtime.Services.Messaging;
using Airtime.Services.Reporting;

using NLog;

using Xtra.Common;


namespace Airtime.Web.Controllers.Areas.Admin
{

	[RequireLogin(Roles = "Developer,Administrator")]
	public class AdminToolsController : Controller
	{

		public AdminToolsController(IConfigurationService configurationService, IChannelRepository channelRepository, ISurveyRepository surveyRepository, IQuestionRepository questionRepository, IMessagingService messagingService, IReportingService reportingService, IEmailBlastRepository emailBlastRepository)
		{
			ConfigurationService = configurationService;
			MessagingService = messagingService;
			ReportingService = reportingService;
			ChannelRepository = channelRepository;
			SurveyRepository = surveyRepository;
			QuestionRepository = questionRepository;
			EmailBlastRepository = emailBlastRepository;
		}


		[HttpGet]
		public ActionResult Index(string type)
		{
			return View();
		}


		public ActionResult SurveyLinks(SubmitActionEnum? submitAction)
		{
			if (submitAction.HasValue && submitAction.Value == SubmitActionEnum.Back) {
				return RedirectToAction("Index");
			}

			var channelNames = new[] { "2Day Blind", "2MMM Blind", "3Fox Blind", "3MMM Blind", "B105 Blind", "4MMM Blind", "SAFM Blind", "5MMM Blind", "92.9 Blind", "MIXP Blind" };

			var channels = ChannelRepository.Where(c => channelNames.Contains(c.AdminName));
			channels.Apply();

			var surveys = SurveyRepository
				.Where(s => !s.Archived)
				.Where(s => s.Name.Contains(" Blind Round "))
				.Where(s => channels.Contains(s.Channel))
				.Where(s => s.Schedule.StartDate >= DateTime.UtcNow)
				.ToDictionary(s => s.Channel.AdminName, s => s);

			ViewBag.Surveys = surveys;

			return View();
		}


		[HttpGet]
		public ActionResult LoyaltyReports()
		{
			var channels = ChannelRepository.OrderBy(c => c.AdminName);
			ViewBag.Channels = channels;

			return View();
		}


		[HttpPost]
		public ActionResult LoyaltyReports(SubmitActionEnum? submitAction, ICollection<int> channelIds, DateTime fromDate, DateTime toDate, int? minimumInvited, int? minimumCompleted, double? minimumLoyalty)
		{
			if (submitAction.HasValue && submitAction.Value == SubmitActionEnum.Back) {
				return RedirectToAction("Index");
			}

			try {
				var channels = ChannelRepository.Where(x => channelIds.Contains(x.Id))
					.OrderBy(x => x.AdminName)
					.ToList();

				toDate = toDate.AddDays(1);

				return new DelegatingResult(context => {
					var response = context.HttpContext.Response;
					response.BufferOutput = false;
					ReportingService.ExportLoyaltyAsExcel(response.OutputStream, channels, fromDate.ToUniversalTime(), toDate.ToUniversalTime(), minimumInvited, minimumCompleted, minimumLoyalty);
				}) {
					ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
					FileDownloadName = String.Format("Loyalty Report {0}.xlsx", DateTime.Now.ToString("yyyy-MM-dd"))
				};

			} catch (Exception ex) {
				ModelState.AddModelError(String.Empty, ex.Message);
			}

			return LoyaltyReports();
		}


		public ActionResult EmailOverview(SubmitActionEnum? submitAction)
		{
			if (submitAction.HasValue && submitAction.Value == SubmitActionEnum.Back) {
				return RedirectToAction("Index");
			}

			return View(ReportingService.GetEmailBlastOverviews());
		}


		private IConfigurationService ConfigurationService { get; set; }
		private IMessagingService MessagingService { get; set; }
		private IReportingService ReportingService { get; set; }
		private IChannelRepository ChannelRepository { get; set; }
		private ISurveyRepository SurveyRepository { get; set; }
		private IQuestionRepository QuestionRepository { get; set; }
		private IEmailBlastRepository EmailBlastRepository { get; set; }


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

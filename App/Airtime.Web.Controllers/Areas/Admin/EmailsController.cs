﻿using System;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Collections.Generic;

using Airtime.Core;
using Airtime.Core.Framework.Mvc;
using Airtime.Core.Framework.Security;
using Airtime.Data.Account;
using Airtime.Data.Messaging;
using Airtime.Data.Client;
using Airtime.Models;
using Airtime.Models.Emails;
using Airtime.Services.Configuration;
using Airtime.Services.Messaging;
using Airtime.Services.Security;

using NLog;

using Xtra.Common;


namespace Airtime.Web.Controllers.Areas.Admin
{

	[RequireLogin(Roles = "Developer,Administrator,Client_Turnkey")]
	public class EmailsController : Controller
	{

		public EmailsController(IMapper<EmailBlast> mapper, ISecurityService securityService, IMessagingService messagingService, IConfigurationService configurationService, IEmailBlastRepository emailBlastRepository, IChannelRepository channelRepository)
		{
			Mapper = mapper;
			SecurityService = securityService;
			MessagingService = messagingService;
			ConfigurationService = configurationService;
			EmailBlastRepository = emailBlastRepository;
			ChannelRepository = channelRepository;
		}


		[HttpGet]
		[RequirePermission(PermissionEnum.ViewEmails)]
		public ActionResult Index()
		{
			return View();
		}


		[HttpPost]
		public ActionResult Index(int channelId, SubmitActionEnum submitAction, ICollection<int> idSelection)
		{
			var blastId = (idSelection != null && idSelection.Any()) ? idSelection.First() : (int?)null;
			switch (submitAction) {
				case SubmitActionEnum.Copy:
					return RedirectToAction("Edit", new { channelId, blastId, copy = true });
				case SubmitActionEnum.Add:
					return RedirectToAction("Edit", new { channelId });
				case SubmitActionEnum.Edit:
					return RedirectToAction("Edit", new { channelId, blastId });
				case SubmitActionEnum.Delete:
					return Delete(channelId, idSelection);
			}
			return RedirectToAction("Index", new { channelId });
		}


		public JsonResult List(int channelId, DataTableModel dataTable)
		{
			//Get marketing emails (channel emails)
			var blasts = EmailBlastRepository.Items
				.Where(blast => blast.EmailBlastType == EmailBlastTypeEnum.ChannelEmail);

			//Channel filter
			if (channelId > 0) {
				blasts = SecurityService.ValidateChannel(channelId)
					? blasts.Where(blast => blast.Channel.Id == channelId)
					: blasts.Where(blast => false);
			} else {
				blasts = blasts.Where(blast => SecurityService.MembershipChannels.Contains(blast.Channel));
			}

			//Search filter
			if (!string.IsNullOrWhiteSpace(dataTable.sSearch)) {
				blasts = blasts.Where(blast => blast.Subject.Contains(dataTable.sSearch) || blast.Status == dataTable.sSearch.ToEnumSafe<EmailBlastStatusEnum>());
			}

			//Column sorting
			for (int i = 0; i < dataTable.iSortCols.Count; i++) {
				blasts = (dataTable.sSortDirs[i] == DataTableSortDirection.Descending)
					? blasts.OrderBy(dataTable.iSortCols[i] + " desc")
					: blasts.OrderBy(dataTable.iSortCols[i]);
			}
			blasts = blasts.OrderByDescending(blast => blast.Id);

			var pagedBlasts = blasts.ToPagedList(dataTable.iDisplayStart, dataTable.iDisplayLength)
				.Select(blast => Mapper.Map<EmailBlastModel>(blast));

			var blastTable = new {
				currentPage = 0,
				totalRecords = blasts.Count(),
				list = pagedBlasts
			};

			return Json(blastTable, JsonRequestBehavior.AllowGet);
		}


		[HttpGet]
		public ActionResult Edit(int channelId, int? blastId, bool? copy)
		{
			var channel = ChannelRepository[channelId];

			var blast = blastId.HasValue
				? EmailBlastRepository.FindById(blastId.Value)
				: new EmailBlast {
					RecipientSource = RecipientSourceEnum.All_Participants,
					EmailBlastType = EmailBlastTypeEnum.ChannelEmail,
					Status = EmailBlastStatusEnum.Pending,
					Channel = channel,
					Criteria = new EmailBlastCriteria {
						Target = EmailBlastTargetEnum.All_Members
					},
					TargetPercentage = 100
				};

			if (!SecurityService.ValidateOwnership(channel, blast)) {
				return new HttpUnauthorizedResult(String.Format("Email blast {0} must be in the selected membership authorized channel ({1})", blastId, channelId));
			}

			var bodyTemplate = MessagingService.GetEmailMarketingTemplate(channel);
			if (blast.IsTransient() && bodyTemplate.Exists()) {
				blast.Body = bodyTemplate.ToString();
				blast.NoServerSideTemplate = true;
			}

			var model = Mapper.Map<EmailBlastEditModel>(blast);

			if (bodyTemplate.Exists()) {
				model.GotTemplate = true;
			}

			if (!blastId.HasValue) {
				//If this is a new email blast, force the Membership to manually select a start date
				model.ScheduleStartDate = "";
			}

			if (copy.HasValue && copy.Value) {
				model.MakeCopy = true;
				model.Status = EmailBlastStatusEnum.Pending;
				model.ScheduleStartDate = "";
			}

			model.Disabled = !model.MakeCopy && model.Status != EmailBlastStatusEnum.Pending;

			return View(model);
		}


		[HttpPost]
		[ValidateInput(false)]
		public ActionResult Edit(int channelId, int? blastId, bool? copy, SubmitActionEnum submitAction, EmailBlastEditModel model)
		{
			if (submitAction == SubmitActionEnum.Cancel) {
				return RedirectToAction("Index", new { channelId });
			}

			if (submitAction == SubmitActionEnum.Save) {
				var channel = ChannelRepository[channelId];

				bool makeCopy = copy.HasValue && copy.Value;
				var blast = (blastId.HasValue && !makeCopy)
					? EmailBlastRepository[blastId.Value]
					: new EmailBlast {
						RecipientSource = RecipientSourceEnum.All_Participants,
						EmailBlastType = EmailBlastTypeEnum.ChannelEmail,
						Status = EmailBlastStatusEnum.Pending,
						Channel = channel,
						Criteria = new EmailBlastCriteria {
							Target = EmailBlastTargetEnum.All_Members
						},
						TargetPercentage = 100
					};

				if (!SecurityService.ValidateOwnership(channel, blast)) {
					return new HttpUnauthorizedResult(String.Format("Email blast {0} must be in the selected membership authorized channel ({1})", blastId, channelId));
				}

				if (blast.Status != EmailBlastStatusEnum.Pending) {
					ModelState.AddModelError("", String.Format(Resources.Emails.StatusNotPendingSoCannotSave, blast.Status.ToResourceString()));
				}

				if (!String.IsNullOrEmpty(model.Body)) {
					model.Body = MessagingService.SanitizeHtmlForEmail(model.Body);
					if (model.Body.Length > ConfigurationService.MaximumEmailBytes) {
						ModelState.AddModelError("Body", Resources.Emails.MaximumEmailSizeExceeded);
					}
				}

				if (ModelState.IsValid) {
					blast.Schedule.StartDate = String.IsNullOrEmpty(model.ScheduleStartDate)
 						? (DateTime?)null
						: TimeZoneInfo.ConvertTimeToUtc(model.ScheduleStartDate.ConvertTo<DateTime>(), blast.Channel.TimeZoneInfo);
					blast.Schedule.EndDate = blast.Schedule.StartDate.HasValue
						? blast.Schedule.StartDate.Value.AddDays(2)
						: (DateTime?)null;

					blast.Subject = model.Subject.Trim();
					blast.Body = model.Body.Trim();
					blast.NoServerSideTemplate = model.NoServerSideTemplate;

					switch (model.CriteriaTarget) {
						case EmailBlastTargetEnum.Random_Members:
						case EmailBlastTargetEnum.Random_Members_Who_Completed:
						case EmailBlastTargetEnum.First_Members_To_Complete:
							blast.RecipientSource = RecipientSourceEnum.Specified_Addresses;
							break;
						default:
							blast.RecipientSource = RecipientSourceEnum.All_Participants;
							break;
					}

					if (blast.Criteria == null) {
						blast.Criteria = new EmailBlastCriteria();
					}
					blast.Criteria.EmailBlast = blast;
					blast.Criteria.Target = model.CriteriaTarget;
					blast.Criteria.Gender = model.CriteriaGender;
					blast.Criteria.LowerAge = model.CriteriaLowerAge;
					blast.Criteria.UpperAge = model.CriteriaUpperAge;
					blast.Criteria.OptInSurveys = model.CriteriaOptInSurveys;
					blast.Criteria.OptInNewsletters = model.CriteriaOptInNewsletters;
					blast.Criteria.NumberOfMembers = model.CriteriaNumberOfMembers;

					if (!String.IsNullOrWhiteSpace(model.CriteriaLowerCompletedSurveyDate)) {
						blast.Criteria.LowerCompletedSurveyDate = TimeZoneInfo.ConvertTimeToUtc(model.CriteriaLowerCompletedSurveyDate.ConvertTo<DateTime>(), blast.Channel.TimeZoneInfo);
					}
					if (!String.IsNullOrWhiteSpace(model.CriteriaLowerCompletedSurveyDate)) {
						blast.Criteria.UpperCompletedSurveyDate = TimeZoneInfo.ConvertTimeToUtc(model.CriteriaUpperCompletedSurveyDate.ConvertTo<DateTime>(), blast.Channel.TimeZoneInfo);
					}

					EmailBlastRepository.Save(blast);

					return RedirectToAction("Index", new { channelId });
				}
			}

			// If we got this far, something failed, redisplay form
			return View(model);
		}


		[HttpPost]
		private ActionResult Delete(int channelId, ICollection<int> idSelection)
		{
			if (idSelection == null) {
				return RedirectToAction("Index", new { channelId });
			}

			var channel = ChannelRepository[channelId];

			var blastsToDelete = new List<EmailBlast>(idSelection.Count);
			foreach (int blastId in idSelection) {
				var blast = EmailBlastRepository[blastId];
				if (!SecurityService.ValidateOwnership(channel, blast)) {
					return new HttpUnauthorizedResult(String.Format("Email blast {0} must be in the selected membership authorized channel ({1})", blastId, channelId));
				}
				//Only survey invitation blasts with a current status of Pending can be deleted
				if (blast.EmailBlastType == EmailBlastTypeEnum.ChannelEmail && blast.Status != EmailBlastStatusEnum.Sent && blast.Status != EmailBlastStatusEnum.Cancelled) {
					blastsToDelete.Add(blast);
				}
			}

			//Delete or cancel all selected blasts
			foreach (var blast in blastsToDelete) {
				switch (blast.Status) {
					case EmailBlastStatusEnum.Pending:
						EmailBlastRepository.Delete(blast);
						break;
					case EmailBlastStatusEnum.Sending:
						EmailBlastRepository.Cancel(blast);
						break;
				}
			}

			return RedirectToAction("Index", new { channelId });
		}


		protected IMapper<EmailBlast> Mapper { get; set; }
		protected ISecurityService SecurityService { get; set; }
		protected IMessagingService MessagingService { get; set; }
		protected IConfigurationService ConfigurationService { get; set; }
		protected IEmailBlastRepository EmailBlastRepository { get; set; }
		protected IChannelRepository ChannelRepository { get; set; }


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

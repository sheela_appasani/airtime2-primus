using System;
using System.Net;
using System.Web.Mvc;

using Airtime.Core;
using Airtime.Models.Error;

using Elmah;

using NLog;


namespace Airtime.Web.Controllers.Areas.Admin
{

	[ElmahHandleError]
	public class ErrorController : Controller
	{

		public ViewResult Unknown()
		{
			Response.StatusCode = (int)HttpStatusCode.InternalServerError;
			Response.TrySkipIisCustomErrors = true;
			return View("Unknown");
		}


		public ViewResult NotFound(string aspxerrorpath)
		{
			Response.StatusCode = (int)HttpStatusCode.NotFound;
			Response.TrySkipIisCustomErrors = true;

			var model = new NotFoundModel();

			model.RequestedUrl = !String.IsNullOrEmpty(aspxerrorpath) && Request.Url != null && Request.Url.OriginalString.Contains(aspxerrorpath) && Request.Url.OriginalString != aspxerrorpath
				? Request.Url.OriginalString
				: aspxerrorpath;

			model.ReferrerUrl = Request.UrlReferrer != null && Request.UrlReferrer.OriginalString != model.RequestedUrl 
				? Request.UrlReferrer.OriginalString 
				: null;

			Log.Trace("HTTP Code 404 (Not Found):\n\tRequested: " + model.RequestedUrl + "\n\tReferrer: " + model.ReferrerUrl);
	
			return View("NotFound", model);
		}


		public void LogJavascriptError(string message)
		{
			ErrorSignal.FromCurrentContext().Raise(new JavascriptException(message));
		}


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

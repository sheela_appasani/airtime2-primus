﻿using System;

using System.Collections.Generic;

using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

using Airtime.Core;
using Airtime.Core.Framework.Mvc;
using Airtime.Data.Account;
using Airtime.Models.Account;
using Airtime.Services.Security;

using NLog;


namespace Airtime.Web.Controllers.Areas.Admin
{

	public class HomeController : Controller
	{

		public HomeController(IFormsAuthenticationService formsService, ISecurityService securityService)
		{
			FormsService = formsService;
			SecurityService = securityService;
		}


		[RequireLogin(Roles = "Developer,Administrator,Client_Turnkey")]
		public ActionResult Index()
		{
			if (Request.IsAuthenticated) {
				ViewBag.Channels = SecurityService.MembershipChannels.OrderBy(channel => channel.AdminName);
			}

			return View();
		}


		[RequireLogin(Roles = "Developer,Administrator,Client_Turnkey")]
		public ActionResult SelectChannel(int channelId)
		{
			if (SecurityService.ValidatePermissions(PermissionEnum.ViewSummaryReports)) {
				return RedirectToAction("Summary", "Reports", new { channelId });
			}

			if (SecurityService.ValidatePermissions(PermissionEnum.ViewSurveys)) {
				return RedirectToAction("Index", "Surveys", new { channelId });
			}

			if (SecurityService.ValidatePermissions(PermissionEnum.ViewMembers)) {
				return RedirectToAction("Index", "Members", new { channelId });
			}

			if (SecurityService.ValidatePermissions(PermissionEnum.ViewEmails)) {
				return RedirectToAction("Index", "Emails", new { channelId });
			}

			return RedirectToAction("Index", "Home", new { channelId });
		}


		// **************************************
		// URL: /Home/LogOn
		// **************************************

		[HttpHeader("REQUIRES_AUTH", "1")]
		public ActionResult LogOn(Guid? guid = null, string returnUrl = null)
		{
			//Explicitly set the culture cookie, only when the Membership logs in (because this is the only screen which has the Language selector) - don't want to change the Membership's default culture if they click a link that was provided by somebody else
			var cookie = Request.Cookies["culture"];
			if (cookie != null) {
				cookie.Value = Thread.CurrentThread.CurrentCulture.Name;
			} else {
				cookie = new HttpCookie("culture") {
					Value = Thread.CurrentThread.CurrentCulture.Name,
					Expires = DateTime.UtcNow.AddYears(1)
				};
			}
			Response.Cookies.Add(cookie);

			if (guid.HasValue) {
				return LogOn(new LogOnModel { Guid = guid }, returnUrl);
			}

			return View();
		}


		[HttpPost]
		[HttpHeader("REQUIRES_AUTH", "1")]
		public ActionResult LogOn(LogOnModel model, string returnUrl)
		{
			if (ModelState.IsValid) {
				//Check if a login-guid has been provided, if so try using that to login
				if (model.Guid.HasValue && SecurityService.AuthenticateMembership(model.Guid.Value)) {
					FormsService.SignIn(model.Guid.Value, model.RememberMe);
					if (Url.IsLocalUrl(returnUrl)) {
						return Redirect(returnUrl);
					}
					return RedirectToAction("Index", "Home", new { Area = "Admin" });
				}

				//Try logging in using a username and password
				if (SecurityService.AuthenticateMembership(model.Username, model.Password)) {
					FormsService.SignIn(model.Username, model.RememberMe);
					if (Url.IsLocalUrl(returnUrl)) {
						return Redirect(returnUrl);
					}
					return RedirectToAction("Index", "Home", new { Area = "Admin" });
				}

				//Login failed!! Credentials were incorrect or not present
				ModelState.AddModelError("", Resources.Account.UsernameOrPasswordIncorrect);
			}

			// If we got this far, something failed, redisplay form
			return View(model);
		}


		// **************************************
		// URL: /Home/LogOff
		// **************************************

		public ActionResult LogOff()
		{
			FormsService.SignOut();

			return RedirectToAction("LogOn", "Home");
		}


		internal IFormsAuthenticationService FormsService { get; set; }
		internal ISecurityService SecurityService { get; set; }

		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

using Airtime.Core;
using Airtime.Core.Framework.Mvc;
using Airtime.Data.Account;
using Airtime.Data.Client;
using Airtime.Data.Messaging;
using Airtime.Data.Surveying;
using Airtime.Models;
using Airtime.Models.Emails;
using Airtime.Models.Invitations;
using Airtime.Services.Configuration;
using Airtime.Services.Messaging;
using Airtime.Services.Security;
using Xtra.Common;

using NLog;


namespace Airtime.Web.Controllers.Areas.Admin
{

	[RequireLogin(Roles = "Developer,Administrator,Client_Turnkey")]
	public class InvitationsController : Controller
	{

		public InvitationsController(IMapper<EmailBlast, EmailBlastModel> mapper, ISecurityService securityService,
			IMessagingService messagingService, IConfigurationService configurationService,
			IEmailBlastRepository emailBlastRepository, IChannelRepository channelRepository,
			ISurveyRepository surveyRepository)
		{
			Mapper = mapper;
			SecurityService = securityService;
			MessagingService = messagingService;
			ConfigurationService = configurationService;
			EmailBlastRepository = emailBlastRepository;
			ChannelRepository = channelRepository;
			SurveyRepository = surveyRepository;
		}


		[HttpGet]
		public ActionResult Index(int surveyId)
		{
			var survey = SurveyRepository[surveyId];
			return View(new InvitiationIndexModel {
				SurveyName = survey.Name
			});
		}


		[HttpPost]
		public ActionResult Index(int channelId, int surveyId, string submitAction, ICollection<int> idSelection)
		{
			var blastId = (idSelection != null) ? idSelection.First() : (int?)null;

			switch (submitAction) {
				case "cancel":
					return RedirectToAction("Index", "Questions", new { channelId, surveyId });
				case "save":
					return RedirectToAction("Edit", "Surveys", new { channelId, surveyId });
				case "copy":
					return RedirectToAction("Edit", new { channelId, surveyId, blastId, copy = true });
				case "addInvitation":
					return RedirectToAction("Edit", new { channelId, surveyId, recipientSource = RecipientSourceEnum.All_Participants });
				case "addReminder":
					return RedirectToAction("Edit", new { channelId, surveyId, recipientSource = RecipientSourceEnum.All_Uncompleted_Invitees });
				case "edit":
					return RedirectToAction("Edit", new { channelId, surveyId, blastId });
				case "delete":
					return Delete(channelId, surveyId, idSelection);
			}

			return RedirectToAction("Index", new { channelId, surveyId });
		}


		public ActionResult List(int channelId, int surveyId, DataTableModel dataTable)
		{
			var channel = ChannelRepository[channelId];
			var survey = SurveyRepository[surveyId];

			//Current channel access has already been verified, now check that the survey belongs to the same channel
			if (!SecurityService.ValidateOwnership(channel, survey)) {
				return new HttpUnauthorizedResult(String.Format("Survey {0} must be in the selected membership authorized channel", survey.Id));
			}

			//Get invitation emails (survey emails)
			var blasts = EmailBlastRepository.Items
			   .Where(blast => blast.EmailBlastType == EmailBlastTypeEnum.SurveyEmail);

			//Channel filter
			if (channelId > 0) {
				blasts = SecurityService.ValidateChannel(channelId)
					? blasts.Where(blast => blast.Survey.Channel.Id == channelId)
					: blasts.Where(blast => false);
			} else {
				blasts = blasts.Where(blast => SecurityService.MembershipChannels.Contains(blast.Channel));
			}

			blasts = blasts.Where(blast => blast.Survey.Id == surveyId);

			//Search filter
			if (!string.IsNullOrWhiteSpace(dataTable.sSearch)) {
				blasts = blasts.Where(blast => blast.Subject.Contains(dataTable.sSearch) || blast.Status == dataTable.sSearch.ToEnumSafe<EmailBlastStatusEnum>());
			}

			//Column sorting
			for (int i = 0; i < dataTable.iSortCols.Count; i++) {
				blasts = (dataTable.sSortDirs[i] == DataTableSortDirection.Descending)
					? blasts.OrderBy(dataTable.iSortCols[i] + " desc")
					: blasts.OrderBy(dataTable.iSortCols[i]);
			}
			blasts = blasts.OrderByDescending(blast => blast.Id);


			var pagedBlasts = blasts.ToPagedList(dataTable.iDisplayStart, dataTable.iDisplayLength)
				.Select(blast => Mapper.Map(blast));


			var blastTable = new {
				currentPage = 0,
				totalRecords = blasts.Count(),
				list = pagedBlasts
			};

			return Json(blastTable, JsonRequestBehavior.AllowGet);
		}


		[HttpGet]
		public ActionResult Edit(int channelId, int surveyId, int? blastId, bool? copy, RecipientSourceEnum? recipientSource)
		{
			var channel = ChannelRepository[channelId];
			var survey = SurveyRepository[surveyId];

			var blast = blastId.HasValue
				? EmailBlastRepository[blastId.Value]
				: new EmailBlast {
					EmailBlastType = EmailBlastTypeEnum.SurveyEmail,
					Status = EmailBlastStatusEnum.Pending,
					Channel = channel,
					Survey = survey,
					RecipientSource = recipientSource.HasValue ? recipientSource.Value : RecipientSourceEnum.All_Participants,
					TargetPercentage = 100,
					Schedule = {
						StartDate = null // date set / handled client side
					}
				};

			//Current channel access has already been verified, now check that the survey belongs to the same channel and the blast belongs to that survey
			if (!SecurityService.ValidateOwnership(channel, survey, blast)) {
				return new HttpUnauthorizedResult(String.Format("Blast {0} must be in the selected survey ({1}) and membership authorized channel ({2})", blastId, surveyId, channelId));
			}

			var bodyTemplate = MessagingService.GetEmailInvitationTemplate(channel);
			if (blast.IsTransient() && bodyTemplate.Exists()) {
				blast.Body = bodyTemplate.ToString();
				blast.NoServerSideTemplate = true;
			}

			var model = Mapper.Map(blast);

			if (bodyTemplate.Exists()) {
				model.GotTemplate = true;
			}

			model.AvailableRecipientSources = new List<RecipientSourceEnum> {
				RecipientSourceEnum.All_Participants,
				RecipientSourceEnum.All_Uncompleted_Invitees
			};

			var surveyHasStarted = survey.Schedule.StartDate <= DateTime.UtcNow;
			if (!blastId.HasValue && surveyHasStarted) {
				//If this is a new email blast and its survey has already started, force the Membership to manually select a start date
				model.ScheduleStartDate = "";
			}

			if (copy.HasValue && copy.Value) {
				model.MakeCopy = true;
				model.Status = EmailBlastStatusEnum.Pending;
				model.ScheduleStartDate = "";
			}

			model.Disabled = !model.MakeCopy && model.Status != EmailBlastStatusEnum.Pending;

			SetInvitationViewModelFields(model, survey);

			return View(model);
		}


		private void SetInvitationViewModelFields(EmailBlastModel model, Survey survey)
		{
			model.SurveyName = survey.Name;
			model.PageCreateTicks = DateTime.UtcNow.UnixTicks();
			model.SurveyScheduleStartTicks = survey.Schedule.StartDate.UnixTicks();
			model.SurveyScheduleEndTicks = survey.Schedule.EndDate.UnixTicks();
		}


		[HttpPost]
		[ValidateInput(false)]
		public ActionResult Edit(string submitAction, int channelId, int surveyId, int? blastId, bool? copy, EmailBlastModel model)
		{
			if (submitAction == "cancel") {
				return RedirectToAction("Index", new { channelId, surveyId });
			}

			if (submitAction == "save") {
				var channel = ChannelRepository[channelId];
				var survey = SurveyRepository[surveyId];
				bool makeCopy = copy.HasValue && copy.Value;

				var emailBlast = (blastId.HasValue && !makeCopy)
					? EmailBlastRepository[blastId.Value]
					: new EmailBlast {
						EmailBlastType = EmailBlastTypeEnum.SurveyEmail,
						Status = EmailBlastStatusEnum.Pending,
						Channel = channel,
						Survey = survey,
					};

				if (!SecurityService.ValidateOwnership(channel, survey, emailBlast)) {
					return new HttpUnauthorizedResult(String.Format("Email blast {0} must be in the selected membership authorized channel ({1}) and survey ({2})", blastId, channelId, surveyId));
				}

				if (emailBlast.Status != EmailBlastStatusEnum.Pending) {
					ModelState.AddModelError(String.Empty, String.Format(Resources.Emails.StatusNotPendingSoCannotSave, emailBlast.Status.ToResourceString()));
				}

				//Do not save changes if survey is over.
				if (!SecurityService.CurrentMembership.IsAdmin() && !makeCopy && survey.Schedule.EndDate.HasValue && survey.Schedule.EndDate.Value <= DateTime.UtcNow) {
					// this modifies ModelState IsValid to false.
					ModelState.AddModelError(String.Empty, Resources.Emails.SurveyHasFinishedInvitationsCannotBeSent);
				}

				if (!String.IsNullOrEmpty(model.Body)) {
					model.Body = MessagingService.SanitizeHtmlForEmail(model.Body);
					if (model.Body.Length > ConfigurationService.MaximumEmailBytes) {
						ModelState.AddModelError("Body", Resources.Emails.MaximumEmailSizeExceeded);
					}
				}

				if (ModelState.IsValid) {
					emailBlast.Subject = model.Subject.Trim();
					emailBlast.Body = model.Body;
					emailBlast.RecipientSource = model.RecipientSource;
					emailBlast.NoServerSideTemplate = model.NoServerSideTemplate;

					emailBlast.Schedule.StartDate = String.IsNullOrEmpty(model.ScheduleStartDate)
						? (DateTime?)null
						: TimeZoneInfo.ConvertTimeToUtc(model.ScheduleStartDate.ConvertTo<DateTime>(), channel.TimeZoneInfo);

					//Default to sending for 2 days, if that extends beyond the end of the survey though, set it to end at the same time as the survey instead
					emailBlast.Schedule.EndDate = emailBlast.Schedule.StartDate.HasValue
						? emailBlast.Schedule.StartDate.Value.AddDays(2)
						: (DateTime?)null;
					if (emailBlast.Schedule.EndDate > survey.Schedule.EndDate) {
						emailBlast.Schedule.EndDate = survey.Schedule.EndDate;
					}

					emailBlast.TargetPercentage = model.TargetPercentage;

					EmailBlastRepository.Save(emailBlast);
					return RedirectToAction("Index", new { channelId, surveyId });
				}

				SetInvitationViewModelFields(model, survey);
			}

			// If we got this far, something failed, redisplay form
			return View(model);
		}


		[HttpPost] // got a A potentially dangerous Request.Form value was detected from the client  despite body field having [AllowHtml] on it already
		public ActionResult Delete(int channelId, int surveyId, ICollection<int> idSelection)
		{
			if (idSelection == null) {
				return RedirectToAction("Index", new { channelId });
			}

			var channel = ChannelRepository[channelId];
			var survey = SurveyRepository[surveyId];

			var blastsToDelete = new List<EmailBlast>(idSelection.Count);
			foreach (int blastId in idSelection) {
				var blast = EmailBlastRepository[blastId];
				if (!SecurityService.ValidateOwnership(channel, survey, blast)) {
					return new HttpUnauthorizedResult(String.Format("Email blast {0} must be in the selected membership authorized channel ({1}) and survey ({2})", blastId, channelId, surveyId));
				}
				//Only survey invitation blasts with a current status of Pending can be deleted
				if (blast.EmailBlastType == EmailBlastTypeEnum.SurveyEmail && blast.Status != EmailBlastStatusEnum.Sent && blast.Status != EmailBlastStatusEnum.Cancelled) {
					blastsToDelete.Add(blast);
				}
			}

			//Delete or cancel all selected blasts
			foreach (var blast in blastsToDelete) {
				switch (blast.Status) {
					case EmailBlastStatusEnum.Pending:
						EmailBlastRepository.Delete(blast);
						break;
					case EmailBlastStatusEnum.Sending:
						EmailBlastRepository.Cancel(blast);
						break;
				}
			}

			return RedirectToAction("Index", new { channelId, surveyId });
		}


		protected IMapper<EmailBlast, EmailBlastModel> Mapper { get; set; }
		protected ISecurityService SecurityService { get; set; }
		protected IMessagingService MessagingService { get; set; }
		protected IConfigurationService ConfigurationService { get; set; }
		protected IEmailBlastRepository EmailBlastRepository { get; set; }
		protected IChannelRepository ChannelRepository { get; set; }
		protected ISurveyRepository SurveyRepository { get; set; }

		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;

using Airtime.Data;
using Airtime.Data.Account;
using Airtime.Data.Client;
using Airtime.Models.Reports;
using Airtime.Services.Reporting;
using Airtime.Services.Security;

using Xtra.Common;

using NLog;


namespace Airtime.Web.Controllers.Areas.Admin
{

	public class JobsController : Controller
	{

		public JobsController(ISecurityService securityService, IReportingService reportingService, IChannelRepository channelRepository, IReportChannelCacheRepository reportChannelCacheRepository)
		{
			SecurityService = securityService;
			ReportingService = reportingService;

			ChannelRepository = channelRepository;
			ReportChannelCacheRepository = reportChannelCacheRepository;
		}


		[HttpGet]
		public ActionResult UpdateReportCache(int? channelId)
		{
			string msg;
			if (Request.UserHostAddress == null) {
				msg = "UpdateReportCache Request.UserHostAddress is null.";
				Log.Error(msg);
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest, msg);
			}

			if (Request.IsLocal
				|| Request.UserHostAddress.StartsWith("122.201.101.")
				|| Request.UserHostAddress.StartsWith("122.201.69.")
				|| SecurityService.CurrentMembership.IsAdmin()) {

				var timeIt = new TimeIt();
				if (channelId.HasValue) {
					try {
						UpdateReportCache(channelId.Value, timeIt);
						return Content(timeIt.ToString());
					} catch (Exception e) {
						msg = e.Message;
						Log.Error(msg);
						return new HttpStatusCodeResult(HttpStatusCode.BadRequest, msg);
					}
				}
				msg = "UpdateReportCache channelId not provided nothing done.";
				Log.Error(msg);
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest, msg);
			}

			msg = string.Format("UpdateReportCache called from unauthorised address: {0}", Request.UserHostAddress);
			Log.Error(msg);
			return new HttpStatusCodeResult(HttpStatusCode.Forbidden, msg);
		}


		private void UpdateReportCache(int channelId, TimeIt timeIt)
		{
			var channel = ChannelRepository[channelId];
			if (channel != null) {
				var channelMembers = ReportingService.GetChannelMembers(channel);
				var model = new SummaryReportModel {
					TotalMembers = channelMembers.Count()
				};

				var rcCache = ReportingService.BuildReportChannelCache(channel, channelMembers, model.TotalMembers, timeIt);
				ReportChannelCacheRepository.Save(rcCache);
			} else {
				throw new Exception("UpdateReportCache Cannot find channelId " + channelId);
			}
		}


		protected ISecurityService SecurityService { get; set; }
		protected IReportingService ReportingService { get; set; }
		protected IChannelRepository ChannelRepository { get; set; }
		protected IReportChannelCacheRepository ReportChannelCacheRepository { get; set; }


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

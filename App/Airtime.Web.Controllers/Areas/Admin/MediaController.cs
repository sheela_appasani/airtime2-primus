using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web;
using System.Web.Mvc;

using Airtime.Core;
using Airtime.Core.Framework.Mvc;
using Airtime.Data.Account;
using Airtime.Data.Client;
using Airtime.Data.Content.Fields;
using Airtime.Data.Media;
using Airtime.Models;
using Airtime.Models.Media;
using Airtime.Services.Media;
using Airtime.Services.Security;

using NLog;

using Xtra.Common;


namespace Airtime.Web.Controllers.Areas.Admin
{

	[RequireLogin(Roles = "Developer,Administrator,Client_Turnkey")]
	public class MediaController : Controller
	{

		public MediaController(IMapper<Song, SongModel> songMapper, ISecurityService securityService, IMediaService mediaService, IChannelRepository channelRepository, ISongRepository songRepository, ISongFieldRepository songFieldRepository)
		{
			SongMapper = songMapper;
			SecurityService = securityService;
			MediaService = mediaService;
			ChannelRepository = channelRepository;
			SongRepository = songRepository;
			SongFieldRepository = songFieldRepository;
		}


		public ActionResult Index(int channelId)
		{
			return View();
		}


		[HttpPost]
		public ActionResult Index(int channelId, SubmitActionEnum submitAction, ICollection<int> idSelection)
		{
			var songId = (idSelection != null) ? idSelection.First() : (int?)null;
			switch (submitAction) {
				case SubmitActionEnum.Add:
					return RedirectToAction("Edit", new { channelId });
				case SubmitActionEnum.Edit:
					return RedirectToAction("Edit", new { channelId, songId });
				case SubmitActionEnum.Delete:
					return Delete(channelId, idSelection);
			}

			return RedirectToAction("Index", new { channelId });
		}


		public JsonResult List(int channelId, DataTableModel dataTable)
		{
			var songs = SongRepository.Items
				.Where(s => s.Confirmed && s.Matched);

			//Channel filter
			if (channelId > 0) {
				var channel = ChannelRepository[channelId];
				if (!SecurityService.CurrentMembership.IsAdmin()) {
					songs = SecurityService.ValidateChannel(channelId)
						? songs.Where(s => s.Company == null || s.Company == channel.Company)
						: songs.Where(s => false);
				}
			}

			//Search filter
			if (!string.IsNullOrWhiteSpace(dataTable.sSearch)) {
				songs = songs.Where(s => s.Artist.Contains(dataTable.sSearch) || s.Title.Contains(dataTable.sSearch) || s.Library.Contains(dataTable.sSearch) || s.Company.Name.Contains(dataTable.sSearch));
			}

			//Column sorting
			for (int i = 0; i < dataTable.iSortCols.Count; i++) {
				songs = (dataTable.sSortDirs[i] == DataTableSortDirection.Descending)
					? songs.OrderBy(dataTable.iSortCols[i] + " desc")
					: songs.OrderBy(dataTable.iSortCols[i]);
			}
			songs = songs.OrderByDescending(s => s.Id);


			var pagedSongs = songs.ToPagedList(dataTable.iDisplayStart, dataTable.iDisplayLength)
				.Select(song => SongMapper.Map(song));

			var songsTable = new {
				currentPage = 0,
				totalRecords = songs.Count(),
				list = pagedSongs
			};

			return Json(songsTable, JsonRequestBehavior.AllowGet);
		}


		public ActionResult Edit(int channelId, int? songId)
		{
			var channel = ChannelRepository[channelId];

			var song = songId.HasValue
				? SongRepository[songId.Value]
				: new Song {
					Company = channel.Company,
					Library = channel.Company.Name
				};

			//Current channel access has already been verified, now check that the song belongs to the same channel's company
			if (!SecurityService.ValidateOwnership(channel, song)) {
				return new HttpUnauthorizedResult(String.Format("Song {0} must be owned by the selected membership authorized channel's company", song.Id));
			}

			var model = SongMapper.Map(song);

			ViewBag.CustomAttributes = SongFieldRepository
				.Where(x => x.Scope == channel.Company)
				.ToList()
				.Select(x => new KeyValuePair<string,string>(x.Name, song.Attributes.TryGetOrDefault(x)))
				.ToList();

			return View(model);
		}


		[HttpPost]
		public ActionResult Edit(int channelId, int? songId, SubmitActionEnum submitAction, SongModel model, string cart)
		{
			if (submitAction == SubmitActionEnum.Cancel) {
				return RedirectToAction("Index", new { channelId });
			}

			if (submitAction == SubmitActionEnum.Download) {
				if (songId.HasValue) {
					var channel = ChannelRepository[channelId];
					var song = SongRepository[songId.Value];
					if (song != null && !String.IsNullOrEmpty(song.Filename)) {
						//Current channel access has already been verified, now check that the song belongs to the same channel's company
						if (!SecurityService.ValidateOwnership(channel, song)) {
							return new HttpUnauthorizedResult(String.Format("Song {0} must be owned by the selected membership authorized channel's company", song.Id));
						}
						return File(Server.MapPath(song.Filename), "application/octet-stream", Path.GetFileName(song.Filename));
					}
				}
			}

			if (submitAction == SubmitActionEnum.Save) {
				var channel = ChannelRepository[channelId];

				var song = songId.HasValue
					? SongRepository[songId.Value]
					: new Song();

				//Current channel access has already been verified, now check that the song belongs to the same channel's company
				if (!SecurityService.ValidateOwnership(channel, song)) {
					return new HttpUnauthorizedResult(String.Format("Song {0} must be owned by the selected membership authorized channel's company", song.Id));
				}

				if (ModelState.IsValid) {
					song.Artist = model.Artist;
					song.Title = model.Title;
					song.Year = model.Year;
					song.Genre = model.Genre;
					song.Tempo = model.Tempo;

					var field = SongFieldRepository.FindByNameAndCompany("CART", channel.Company);
					if (field != null) {
						if (String.IsNullOrWhiteSpace(cart)) {
							song.Attributes.Remove(field);
						} else {
							song.Attributes[field] = cart.Trim();
						}
					}

					SongRepository.Save(song);

					return RedirectToAction("Index", new { channelId });
				}
			}

			// If we got this far, something failed, redisplay form
			return View(model);
		}


		[HttpPost]
		public ActionResult Delete(int channelId, ICollection<int> idSelection)
		{
			if (idSelection == null) {
				return RedirectToAction("Index", new { channelId });
			}

			var channel = ChannelRepository[channelId];

			foreach (int songId in idSelection) {
				var song = SongRepository[songId];

				if (!song.InUse) {
					if (SecurityService.CurrentMembership.IsAdmin()) {
						//Administrators and Developers can delete anything
						SongRepository.Delete(song);

					} else {
						//Other users can only delete songs owned by the company for their current channel
						if (song.Company != null) {
							if (!SecurityService.ValidateOwnership(channel, song)) {
								return new HttpUnauthorizedResult(String.Format("Song {0} must be owned by the selected membership authorized channel's company", song.Id));
							}
							SongRepository.Delete(song);
						}
					}
				}
			}

			return RedirectToAction("Index", new { channelId });
		}


		[HttpPost]
		public ActionResult Upload(int channelId, string artist, string title, string cart, HttpPostedFileBase file)
		{
			try {
				var channel = ChannelRepository[channelId];

				double totalSeconds = MediaService.GetMp3TotalSeconds(file.InputStream);
				if (totalSeconds < 1) {
					throw new Exception("Audio file must be 1 second or longer.");
				}

				var song = new Song {
					Artist = artist.Trim(),
					Title = title.Trim(),
					Length = (int)Math.Floor(totalSeconds),
					Filename = Path.GetFileName(file.FileName),
					Company = channel.Company,
					Library = channel.Company.Name
				};

				if (!String.IsNullOrWhiteSpace(cart)) {
					var field = SongFieldRepository.FindByNameAndCompany("CART", channel.Company)
								?? SongFieldRepository.CreateAndSaveField("CART", channel.Company);
					song.Attributes[field] = cart.Trim();
				}

				//Save new song db record and get new ID
				SongRepository.Save(song);

				//Save MP3 file into the ~/song_store/{ID % 100}/{ID}.mp3
				var virtPath = String.Format("~/song_store/{0:D2}/{1}.mp3", song.Id % 100, song.Id);
				var filePath = Server.MapPath(virtPath);
				file.SaveAs(filePath);

				//Update song db record with new filename and set matched & confirmed to true
				song.Matched = true;
				song.Confirmed = true;
				song.Filename = virtPath;
				SongRepository.Save(song);

				var songModel = SongMapper.Map(song);

				//Return using the "text/plain" content type because of a Firefox problem where it pops up a Save dialog box when a file is uploaded through an iframe (i.e. when using the JQuery Forms plugin)
				return Json(songModel, "text/plain", JsonRequestBehavior.AllowGet);

			} catch (Exception ex) {
				Log.Error(ex.Message, ex);
				return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
			}
		}


		private IMapper<Song, SongModel> SongMapper { get; set; }
		private ISecurityService SecurityService { get; set; }
		private IMediaService MediaService { get; set; }
		private IChannelRepository ChannelRepository { get; set; }
		private ISongRepository SongRepository { get; set; }
		private ISongFieldRepository SongFieldRepository { get; set; }


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

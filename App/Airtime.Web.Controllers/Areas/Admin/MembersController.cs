using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

using Airtime.Core;
using Airtime.Core.Framework.Mvc;
using Airtime.Core.Framework.Security;
using Airtime.Data.Client;
using Airtime.Data.Globalization;
using Airtime.Data.Account;
using Airtime.Data.Surveying;
using Airtime.Data.Surveying.Questions;
using Airtime.Models;
using Airtime.Models.Members;
using Airtime.Services.Account;
using Airtime.Services.Globalization;
using Airtime.Services.Security;

using NLog;

using Xtra.Common;


namespace Airtime.Web.Controllers.Areas.Admin
{

	[RequireLogin(Roles = "Developer,Administrator,Client_Turnkey")]
	public class MembersController : Controller
	{

		public MembersController(IMapper<Membership, UserModel> userMapper, ISecurityService securityService, IMembershipService membershipService, IChannelRepository channelRepository, IPersonRepository personRepository, IMembershipRepository membershipRepository, IQuestionChoiceRepository questionChoiceRepository, ISurveyRepository surveyRepository, ICountryRepository countryRepository, IGlobalizationService globalizationService)
		{
			UserMapper = userMapper;
			SecurityService = securityService;
			MembershipService = membershipService;
			ChannelRepository = channelRepository;
			PersonRepository = personRepository;
			MembershipRepository = membershipRepository;
			QuestionChoiceRepository = questionChoiceRepository;
			SurveyRepository = surveyRepository;
			CountryRepository = countryRepository;
			GlobalizationService = globalizationService;
		}


		[RequirePermission(PermissionEnum.ViewMembers)]
		public ActionResult Index(int channelId)
		{
			ViewBag.Channel = ChannelRepository[channelId];

			var surveyDropDownList = SurveyRepository
				.Where(s => s.Channel.Id == channelId)
				.OrderByDescending(s => s.Schedule.StartDate)
				.ThenBy(s => s.Name)
				.Select(s => new SelectListItem {
				        Value = s.Id.ToString(CultureInfo.InvariantCulture),
				        Text = s.Name
				})
				.ToList();
			surveyDropDownList.Insert(0, new SelectListItem { Value = String.Empty, Text = Resources.Shared.Any });

			var countryDropDownList = CountryRepository
				.OrderBy(c => c.Name)
				.Select(s => new SelectListItem {
					Value = s.Name,
					Text = s.Name
				})
				.ToList();
			countryDropDownList.Insert(0, new SelectListItem { Value = String.Empty, Text = Resources.Shared.Any });

			var model = new MemberFilterModel
			{
				SurveyDropDownList = surveyDropDownList,
				CountryDropDownList = countryDropDownList,
				BirthMonthDropDownList = RangeDropDownList(Enumerable.Range(1, 12)),
				BirthDayOfMonthDropDownList = RangeDropDownList(Enumerable.Range(1, 31))
			};

			ViewBag.DateFormat = GlobalizationService.ConvertDateFormatToJquery(CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern);
			ViewBag.DayMonthFormat = GlobalizationService.GetJQueryDayMonthFormat();
			return View(model);
		}
		

		private static IEnumerable<SelectListItem> RangeDropDownList(IEnumerable<int> enumerableRange)
		{
			var listSelectListItem = enumerableRange
				.Select(x => new SelectListItem {
				    Value = x.ToString(CultureInfo.InvariantCulture),
					// ReSharper disable SpecifyACultureInStringConversionExplicitly
					Text = x.ToString()
					// ReSharper restore SpecifyACultureInStringConversionExplicitly
				})
				.ToList();
			listSelectListItem.Insert(0, new SelectListItem { Value = String.Empty, Text = String.Empty });
			return listSelectListItem;
		}


		[HttpPost]
		public ActionResult Index(int channelId, SubmitActionEnum submitAction, ICollection<int> idSelection)
		{
			var userId = (idSelection != null) ? idSelection.First() : (int?)null;
			switch (submitAction) {
				case SubmitActionEnum.ExportAsCsv:
					return RedirectToAction("ExportAsCsv", new { channelId });
				case SubmitActionEnum.ExportAsExcel:
					return RedirectToAction("ExportAsExcel", new { channelId });
				case SubmitActionEnum.Add:
					return RedirectToAction("Edit", new { channelId });
				case SubmitActionEnum.Edit:
					return RedirectToAction("Edit", new { channelId, userId });
				case SubmitActionEnum.Delete:
					return Delete(channelId, idSelection);
			}

			return RedirectToAction("Index", new { channelId });
		}


		public JsonResult List(int channelId, DataTableModel dataTable, MemberFilterModel memberFilterModel)
		{
			var users = MembershipRepository
				.ApplyFetch(MembershipRepository, u => u.Person)
				.Where(u => u.Username != "purged_user")
				.Where(u => u.ExternalId == null || u.ExternalId.Length == 0)
				.Where(u => u.Role == MembershipRoleEnum.Respondent);

			//Channel filter
			var channel = ChannelRepository[channelId];
			if (channelId > 0)
			{
				users = SecurityService.ValidateChannel(channelId)
					? users.Where(u => u.Channels.Contains(channel))
					: users.Where(u => false);
			} else {
				users = users.Where(u => false);
			}

			users = MembershipService.ApplyMemberFilter(users, memberFilterModel, channel);

			//Search filter
			if (!string.IsNullOrWhiteSpace(dataTable.sSearch)) {
				if (dataTable.sSearch.IsNumeric()) {
					var numericValue = dataTable.sSearch.ConvertTo<long>();
					users = users.Where(u => u.Id == numericValue
						|| u.Person.Email.Contains(dataTable.sSearch)
						|| u.Person.GivenName.Contains(dataTable.sSearch)
						|| u.Person.FamilyName.Contains(dataTable.sSearch)
						|| u.Username.Contains(dataTable.sSearch));
				} else {
					users = users.Where(u => u.Person.Email.Contains(dataTable.sSearch)
						|| u.Person.GivenName.Contains(dataTable.sSearch)
						|| u.Person.FamilyName.Contains(dataTable.sSearch)
						|| u.Username.Contains(dataTable.sSearch));
				}
			}

			//Column sorting
			for (int i = 0; i < dataTable.iSortCols.Count; i++) {
				users = (dataTable.sSortDirs[i] == DataTableSortDirection.Descending)
					? users.OrderBy(dataTable.iSortCols[i] + " desc")
					: users.OrderBy(dataTable.iSortCols[i]);
			}
			users = users.OrderByDescending(u => u.Id);

			var pagedUsers = users
				.ToPagedList(dataTable.iDisplayStart, dataTable.iDisplayLength)
				.Select(u => UserMapper.Map(u));

			var usersTable = new {
				currentPage = 0,
				totalRecords = users.Count(),
				list = pagedUsers
			};

			return Json(usersTable, JsonRequestBehavior.AllowGet);
		}


		public ActionResult ExportAsCsv(int channelId, DataTableModel dataTable, MemberFilterModel memberFilterModel)
		{
			var channel = ChannelRepository[channelId];

			return new DelegatingResult(context => {
				var response = context.HttpContext.Response;
				response.BufferOutput = false;
				MembershipService.ExportAsCsv(response.OutputStream, channel, memberFilterModel);
			}) {
				ContentType = "text/csv",
				FileDownloadName = String.Format("{0} {1} {2}.csv", channel.InternalName, Resources.Shared.Members, TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, channel.TimeZoneInfo).ToString("yyyy-MM-dd"))
			};
		}


		public ActionResult ExportAsExcel(int channelId, DataTableModel dataTable, MemberFilterModel memberFilterModel)
		{
			var channel = ChannelRepository[channelId];
			
			return new DelegatingResult(context => {
				var response = context.HttpContext.Response;
				response.BufferOutput = false;
				MembershipService.ExportAsExcel(response.OutputStream, channel, memberFilterModel);
			}) {
				ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
				FileDownloadName = String.Format("{0} {1} {2}.xlsx", channel.InternalName, Resources.Shared.Members, TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, channel.TimeZoneInfo).ToString("yyyy-MM-dd"))
			};
		}


		public ActionResult Edit(int channelId, int? userId)
		{
			var channel = ChannelRepository[channelId];

			var user = userId.HasValue
				? MembershipRepository[userId.Value]
				: new Membership {
					Role = MembershipRoleEnum.Respondent,
					Channels = new List<Channel> { channel },
					OptIns = new MembershipOptIns { Surveys = true, Newsletters = true },
					Enabled = true
				};

			//Current channel access has already been verified, now check that the Membership is a member of the same channel
			if (!SecurityService.ValidateOwnership(channel, user) || user.Role != MembershipRoleEnum.Respondent) {
				return new HttpUnauthorizedResult(String.Format("Membership {0} must be a respondent member of the selected membership authorized channel", user.Id));
			}

			var model = UserMapper.Map(user);
			return View(model);
		}


		[HttpPost]
		public ActionResult Edit(SubmitActionEnum submitAction, int channelId, int? userId, UserModel model)
		{
			if (submitAction == SubmitActionEnum.Cancel) {
				return RedirectToAction("Index", new { channelId });
			}

			if (submitAction == SubmitActionEnum.Delete) {
				return userId.HasValue
					? Delete(channelId, new[] { userId.Value })
					: RedirectToAction("Index", new { channelId });
			}

			if (submitAction == SubmitActionEnum.Save) {
				var channel = ChannelRepository[channelId];

				var user = userId.HasValue
					? MembershipRepository[userId.Value]
					: new Membership {
						Role = MembershipRoleEnum.Respondent,
						Channels = { channel },
						OptIns = new MembershipOptIns { Surveys = true, Newsletters = true },
						Guid = Guid.NewGuid()
					};

				//Current channel access has already been verified, now check that the song belongs to the same channel's company
				if (!SecurityService.ValidateOwnership(channel, user) || user.Role != MembershipRoleEnum.Respondent) {
					return new HttpUnauthorizedResult(String.Format("Membership {0} must be a member of the selected membership authorized channel", user.Id));
				}

				if (ModelState.IsValid) {
					user.Username = model.Username;
					user.Password = model.Password;
					user.Enabled = model.Enabled;
					user.Notes = model.Notes;

					var email = (model.PersonEmail ?? "").Trim();
					if (user.Person == null || !email.Equals(user.Person.Email, StringComparison.OrdinalIgnoreCase)) {
						//Email address has changed or the membership is not yet attached to any Person record
						user.Email = email;
						user.Person = PersonRepository.FindByEmail(email) ?? new Person { Email = email };
						user.Person.Memberships.Add(user);
					}

					var person = user.Person;
					person.GivenName = (model.PersonGivenName ?? "").Trim();
					person.FamilyName = (model.PersonFamilyName ?? "").Trim();
					person.Personal.Gender = model.PersonPersonalGender;
					person.Personal.DateOfBirth = model.PersonPersonalDateOfBirth.ConvertToOrDefault<DateTime?>();
					person.Contact.Address = (model.PersonContactAddress ?? "").Trim();
					person.Contact.Suburb = (model.PersonContactSuburb ?? "").Trim();
					person.Contact.City = (model.PersonContactCity ?? "").Trim();
					person.Contact.State = (model.PersonContactState ?? "").Trim();
					person.Contact.Postcode = (model.PersonContactPostcode ?? "").Trim();
					person.Contact.Country = (model.PersonContactCountry ?? "").Trim();
					person.Contact.DaytimePhone = (model.PersonContactDaytimePhone ?? "").Trim();
					person.Contact.MobilePhone = (model.PersonContactMobilePhone ?? "").Trim();

					PersonRepository.Save(person);

					return RedirectToAction("Index", new { channelId });
				}
			}

			// If we got this far, something failed, redisplay form
			return View(model);
		}


		public ActionResult Delete(int channelId, ICollection<int> idSelection)
		{
			if (idSelection == null) {
				return RedirectToAction("Index", new { channelId });
			}

			var channel = ChannelRepository[channelId];

			foreach (int userId in idSelection) {
				var user = MembershipRepository[userId];

				if (user.Role == MembershipRoleEnum.Respondent) {
					if (SecurityService.CurrentMembership.IsAdmin()) {
						//Administrators and Developers can delete any respondent
						MembershipRepository.Purge(user);

					} else {
						//Other users can only purge users who are members of their current channel
						if (!SecurityService.ValidateOwnership(channel, user)) {
							return new HttpUnauthorizedResult(String.Format("Membership {0} must be a member of the selected membership authorized channel", user.Id));
						}
						MembershipRepository.Purge(user);
					}
				}
			}

			return RedirectToAction("Index", new { channelId });
		}


		protected IMapper<Membership, UserModel> UserMapper { get; set; }
		protected ISecurityService SecurityService { get; set; }
		protected IMembershipService MembershipService { get; set; }
		protected IChannelRepository ChannelRepository { get; set; }
		protected IPersonRepository PersonRepository { get; set; }
		protected IMembershipRepository MembershipRepository { get; set; }
		protected IQuestionChoiceRepository QuestionChoiceRepository { get; set; }
		protected ISurveyRepository SurveyRepository { get; set; }
		protected ICountryRepository CountryRepository { get; set; }
		protected IGlobalizationService GlobalizationService { get; set; }

		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

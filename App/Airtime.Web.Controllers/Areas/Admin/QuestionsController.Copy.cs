using System;
using System.Collections.Generic;
using System.Web.Mvc;

using Airtime.Data.Surveying.Questions;


namespace Airtime.Web.Controllers.Areas.Admin
{

	public partial class QuestionsController : Controller
	{

		public ActionResult Copy(int channelId, int surveyId, ICollection<int> idSelection)
		{
			if (idSelection == null) {
				return RedirectToAction("Index", new { channelId, surveyId });
			}

			var channel = ChannelRepository[channelId];
			var survey = SurveyRepository[surveyId];

			//Current channel access has already been verified, now check that all selected questions belong to the same channel and survey and can be deleted
			var questionsToCopy = new List<QuestionBase>(idSelection.Count);
			foreach (int questionId in idSelection) {
				var question = QuestionRepository[questionId];

				//Current channel access has already been verified, now check that the survey belongs to the same channel and the question belongs to that survey
				if (!SecurityService.ValidateOwnership(channel, survey, question)) {
					return new HttpUnauthorizedResult(String.Format("Question {0} must be in the selected survey ({1}) and membership authorized channel ({2})", questionId, surveyId, channelId));
				}

				if (question.Category == QuestionCategoryEnum.Normal) {
					questionsToCopy.Add(question);
				}
			}

			//Copy and append all selected questions
			foreach (var question in questionsToCopy) {
				var newQuestion = SurveyingService.CopyQuestion(question, true);
				newQuestion.Position = survey.Questions.Count;
				survey.Questions.Add(newQuestion);
			}

			if (questionsToCopy.Count > 0) {
				SurveyRepository.Save(survey);
			}

			return RedirectToAction("Index", new { channelId, surveyId });
		}

	}

}

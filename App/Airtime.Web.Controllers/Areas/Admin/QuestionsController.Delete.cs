using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using Airtime.Data.Surveying.Questions;


namespace Airtime.Web.Controllers.Areas.Admin
{

	public partial class QuestionsController : Controller
	{

		[HttpPost]
		public ActionResult Delete(int channelId, int surveyId, ICollection<int> idSelection)
		{
			if (idSelection == null) {
				return RedirectToAction("Index", new { channelId, surveyId });
			}

			var channel = ChannelRepository[channelId];
			var survey = SurveyRepository[surveyId];

			//Current channel access has already been verified, now check that all selected questions belong to the same channel and survey and can be deleted
			var questionsToDelete = new List<QuestionBase>(idSelection.Count);
			foreach (int questionId in idSelection) {
				var question = QuestionRepository[questionId];

				//Current channel access has already been verified, now check that the survey belongs to the same channel and the question belongs to that survey
				if (!SecurityService.ValidateOwnership(channel, survey, question)) {
					return new HttpUnauthorizedResult(String.Format("Question {0} must be in the selected survey ({1}) and membership authorized channel ({2})", questionId, surveyId, channelId));
				}

				if (question.Category == QuestionCategoryEnum.Normal) {
					questionsToDelete.Add(question);
				}
			}

			//Delete all selected questions
			if (questionsToDelete.Count > 0) {
				foreach (var question in questionsToDelete) {
					var branchChoices = QuestionChoiceRepository.Where(c => c.BranchQuestion == question).ToList();
					foreach (var choice in branchChoices) {
						choice.BranchQuestion = null;
					}
					survey.Questions.Remove(question);
					QuestionRepository.Delete(question);
				}
				for (int i = 0; i < survey.Questions.Count; i++) {
					survey.Questions[i].Position = i;
				}
			}

			return RedirectToAction("Index", new { channelId, surveyId });
		}

	}

}

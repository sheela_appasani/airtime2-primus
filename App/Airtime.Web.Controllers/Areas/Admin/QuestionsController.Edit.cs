using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

using Airtime.Data.Surveying;
using Airtime.Data.Surveying.Questions;
using Airtime.Models.Media;
using Airtime.Models.Questions;


namespace Airtime.Web.Controllers.Areas.Admin
{

	public partial class QuestionsController : Controller
	{

		[HttpGet]
		public ActionResult Edit(int channelId, int surveyId, int? questionId, QuestionTypeEnum? questionType)
		{
			if (questionId.HasValue) {
				var question = QuestionRepository[questionId.Value];
				questionType = question.QuestionType;
			}

			switch (questionType) {
				case QuestionTypeEnum.BREAK:
					return EditBreak(channelId, surveyId, questionId);
				case QuestionTypeEnum.TEXT:
					return EditText(channelId, surveyId, questionId);
				case QuestionTypeEnum.RADIO:
				case QuestionTypeEnum.CHECKBOX:
				case QuestionTypeEnum.AUDIO:
					return EditMultiChoice(channelId, surveyId, questionId, questionType);
				case QuestionTypeEnum.AUDIOPOD:
					return EditSongs(channelId, surveyId, questionId);
			}

			//TODO: there's some kind of problem (probably an unknown question-type was specified or found) - display an error message
			return new HttpNotFoundResult();
		}


		[HttpGet]
		private ActionResult EditBreak(int channelId, int surveyId, int? questionId)
		{
			var channel = ChannelRepository[channelId];
			var survey = SurveyRepository[surveyId];

			var question = questionId.HasValue
				? (BreakQuestion)QuestionRepository[questionId.Value]
				: new BreakQuestion {
					Survey = survey,
					Category = QuestionCategoryEnum.Normal
				};

			//Current channel access has already been verified, now check that the survey belongs to the same channel and the question belongs to that survey
			if (!SecurityService.ValidateOwnership(channel, survey, question)) {
				return new HttpUnauthorizedResult(String.Format("Question {0} must be in the selected survey ({1}) and membership authorized channel ({2})", questionId, surveyId, channelId));
			}

			var model = new QuestionEditModel {
				Id = question.Id,
				Name = question.Name,
				Content = question.Text,
				QuestionType = question.QuestionType,
				Category = question.Category
			};

			return View("EditBreak", model);
		}


		[HttpGet]
		private ActionResult EditText(int channelId, int surveyId, int? questionId)
		{
			var channel = ChannelRepository[channelId];
			var survey = SurveyRepository[surveyId];

			var question = questionId.HasValue
				? (TextQuestion)QuestionRepository[questionId.Value]
				: new TextQuestion {
					Survey = survey,
					Category = QuestionCategoryEnum.Normal
				};

			//Current channel access has already been verified, now check that the survey belongs to the same channel and the question belongs to that survey
			if (!SecurityService.ValidateOwnership(channel, survey, question)) {
				return new HttpUnauthorizedResult(String.Format("Question {0} must be in the selected survey ({1}) and membership authorized channel ({2})", questionId, surveyId, channelId));
			}

			var model = new QuestionEditModel {
				Id = question.Id,
				Name = question.Name,
				Content = question.Text,
				QuestionType = question.QuestionType,
				Category = question.Category
			};

			return View("EditText", model);
		}


		[HttpGet]
		private ActionResult EditMultiChoice(int channelId, int surveyId, int? questionId, QuestionTypeEnum? questionType)
		{
			var channel = ChannelRepository[channelId];
			var survey = SurveyRepository[surveyId];

			var question = questionId.HasValue
				? (MultiChoiceQuestion)QuestionRepository[questionId.Value]
				: new MultiChoiceQuestion(questionType ?? QuestionTypeEnum.RADIO) {
					Survey = survey,
					Category = QuestionCategoryEnum.Normal
				};

			//Current channel access has already been verified, now check that the survey belongs to the same channel and the question belongs to that survey
			if (!SecurityService.ValidateOwnership(channel, survey, question)) {
				return new HttpUnauthorizedResult(String.Format("Question {0} must be in the selected survey ({1}) and membership authorized channel ({2})", questionId, surveyId, channelId));
			}


			var branchQuestions = survey.Questions
				.Where(q => q != question && (q.Category == QuestionCategoryEnum.Normal || q.Category == QuestionCategoryEnum.Normal_SurveyCompleted))
				.OrderBy(q => q.Position)
				.ToList();

			//Move the Survey Completed page to the end of the list
			int completedPosition = branchQuestions.FindIndex(q => q.Category == QuestionCategoryEnum.Normal_SurveyCompleted);
			if (completedPosition >= 0) {
				var completedQuestion = branchQuestions[completedPosition];
				branchQuestions.RemoveAt(completedPosition);
				branchQuestions.Add(completedQuestion);
			}

			var model = new QuestionEditModel {
				Id = question.Id,
				Name = question.Name,
				Content = question.Text,
				QuestionType = question.QuestionType,
				Category = question.Category,
				BranchToQuestions = branchQuestions.Select(m => new QuestionListModel {
					Id = m.Id,
					Name = m.Name.Trim(),
					Position = m.Position
				}).ToList(),
				Choices = question.Choices.Select(c => new QuestionChoiceModel {
					Id = c.Id,
					Name = c.Name.Trim(),
					Position = c.Position,
					BranchQuestionId = c.BranchQuestion != null ? c.BranchQuestion.Id : 0,
					BranchQuestionName = c.BranchQuestion != null ? c.BranchQuestion.Name.Trim() : null
				}).ToList()
			};

			if (question.Song != null) {
				model.Songs.Add(new SongModel {
					Id = question.Song.Id,
					Artist = question.Song.Artist,
					Title = question.Song.Title,
					Length = question.Song.Length,
					Library = question.Song.Library,
					Filename = question.Song.Filename,
					Tempo = question.Song.Tempo,
					Genre = question.Song.Genre,
					Year = question.Song.Year
				});
			}

			ViewBag.CustomSongFields = SongFieldRepository.Where(x => x.Scope == channel.Company).ToList();

			var stationMostQuestion = channel.ProfileQuestions
				.FirstOrDefault(q => q.Category == QuestionCategoryEnum.Registration_StationMost) as MultiChoiceQuestion;

			var stationNames = stationMostQuestion != null
				? stationMostQuestion.Choices.Select(c => c.Name.Trim())
				: new string[0];

			var jsSerializer = new JavaScriptSerializer();
			ViewBag.Branches = jsSerializer.Serialize(model.BranchToQuestions);
			ViewBag.Choices = jsSerializer.Serialize(model.Choices);
			ViewBag.StationNames = jsSerializer.Serialize(stationNames);

			return View("EditMultiChoice", model);
		}


		[HttpGet]
		private ActionResult EditSongs(int channelId, int surveyId, int? questionId)
		{
			var channel = ChannelRepository[channelId];
			var survey = SurveyRepository[surveyId];

			var question = questionId.HasValue
				? (AudioPodQuestion)QuestionRepository[questionId.Value]
				: new AudioPodQuestion {
					Survey = survey,
					Category = QuestionCategoryEnum.Normal
				};

			//Current channel access has already been verified, now check that the survey belongs to the same channel and the question belongs to that survey
			if (!SecurityService.ValidateOwnership(channel, survey, question)) {
				return new HttpUnauthorizedResult(String.Format("Question {0} must be in the selected survey ({1}) and membership authorized channel ({2})", questionId, surveyId, channelId));
			}

			var model = new QuestionEditModel {
				Id = question.Id,
				Name = question.Name,
				Content = question.Text,
				QuestionType = question.QuestionType,
				Category = question.Category,
				Songs = new List<SongModel>(question.Songs.Select(
					s => new SongModel {
						Id = s.Id,
						Artist = s.Artist,
						Title = s.Title,
						Length = s.Length,
						Library = s.Library,
						Filename = s.Filename,
						Tempo = s.Tempo,
						Genre = s.Genre,
						Year = s.Year
					}
				))
			};

			ViewBag.CustomSongFields = SongFieldRepository.Where(x => x.Scope == channel.Company).ToList();

			return View("EditSongs", model);
		}


		[HttpPost]
		public ActionResult Edit(string submitAction, int channelId, int surveyId, int? questionId, QuestionEditModel model)
		{
			if (submitAction == "cancel") {
				return RedirectToAction("Index", new { channelId, surveyId });
			}

			if (submitAction == "save") {
				var questionType = model.QuestionType;
				switch (questionType) {
					case QuestionTypeEnum.BREAK:
						return EditBreak(channelId, surveyId, questionId, model);
					case QuestionTypeEnum.TEXT:
						return EditText(channelId, surveyId, questionId, model);
					case QuestionTypeEnum.RADIO:
					case QuestionTypeEnum.CHECKBOX:
					case QuestionTypeEnum.AUDIO:
						return EditMultiChoice(channelId, surveyId, questionId, model);
					case QuestionTypeEnum.AUDIOPOD:
						return EditSongs(channelId, surveyId, questionId, model);
				}
			}

			//Something has gone wrong - the postback specified an unknown submitAction or question type
			return new HttpNotFoundResult();
		}


		[HttpPost]
		private ActionResult EditBreak(int channelId, int surveyId, int? questionId, QuestionEditModel model)
		{
			var channel = ChannelRepository[channelId];
			var survey = SurveyRepository[surveyId];

			var question = questionId.HasValue
				? (BreakQuestion)QuestionRepository[questionId.Value]
				: new BreakQuestion {
					Survey = survey,
					Category = QuestionCategoryEnum.Normal
				};

			//Current channel access has already been verified, now check that the survey belongs to the same channel and the question belongs to that survey
			if (!SecurityService.ValidateOwnership(channel, survey, question)) {
				return new HttpUnauthorizedResult(String.Format("Question {0} must be in the selected survey ({1}) and membership authorized channel ({2})", questionId, surveyId, channelId));
			}

			if (ModelState.IsValid) {
				question.Name = model.Name.Trim();
				question.Text = model.Content.Trim();

				if (!survey.Questions.Contains(question)) {
					//TODO: is there a better way to do this?
					var position = survey.Questions.Count;
					survey.Questions.Add(question);
					question.Position = position;
				}

				QuestionRepository.Save(question);

				return RedirectToAction("Index", new { channelId, surveyId });
			}

			return View("EditBreak", model);
		}


		[HttpPost]
		private ActionResult EditText(int channelId, int surveyId, int? questionId, QuestionEditModel model)
		{
			var channel = ChannelRepository[channelId];
			var survey = SurveyRepository[surveyId];

			var question = questionId.HasValue
				? (TextQuestion)QuestionRepository[questionId.Value]
				: new TextQuestion {
					Survey = survey,
					Category = QuestionCategoryEnum.Normal
				};

			//Current channel access has already been verified, now check that the survey belongs to the same channel and the question belongs to that survey
			if (!SecurityService.ValidateOwnership(channel, survey, question)) {
				return new HttpUnauthorizedResult(String.Format("Question {0} must be in the selected survey ({1}) and membership authorized channel ({2})", questionId, surveyId, channelId));
			}

			if (ModelState.IsValid) {
				question.Name = model.Name.Trim();
				question.Text = model.Content.Trim();

				if (!survey.Questions.Contains(question)) {
					//TODO: is there a better way to do this?
					var position = survey.Questions.Count;
					survey.Questions.Add(question);
					question.Position = position;
				}

				QuestionRepository.Save(question);

				return RedirectToAction("Index", new { channelId, surveyId });
			}

			return View("EditText", model);
		}


		[HttpPost]
		private ActionResult EditMultiChoice(int channelId, int surveyId, int? questionId, QuestionEditModel model)
		{
			var channel = ChannelRepository[channelId];
			var survey = SurveyRepository[surveyId];

			var question = questionId.HasValue
				? (MultiChoiceQuestion)QuestionRepository[questionId.Value]
				: new MultiChoiceQuestion(model.QuestionType) {
					Survey = survey,
					Category = QuestionCategoryEnum.Normal
				};

			//Current channel access has already been verified, now check that the survey belongs to the same channel and the question belongs to that survey
			if (!SecurityService.ValidateOwnership(channel, survey, question)) {
				return new HttpUnauthorizedResult(String.Format("Question {0} must be in the selected survey ({1}) and membership authorized channel ({2})", questionId, surveyId, channelId));
			}

			if (ModelState.IsValid) {
				question.Name = model.Name.Trim();
				question.Text = model.Content.Trim();

				if (!survey.Questions.Contains(question)) {
					//TODO: is there a better way to do this?
					var position = survey.Questions.Count;
					survey.Questions.Add(question);
					question.Position = position;
				}

				question.Song = model.Songs.Take(1).Select(s => SongRepository[s.Id]).FirstOrDefault();

				question.Choices.Clear();
				foreach (var c in model.Choices) {
					var choice = c.Id > 0
						? QuestionChoiceRepository[c.Id]
						: new QuestionChoice();

					var branchQuestion = (c.BranchQuestionId.HasValue && c.BranchQuestionId.Value > 0)
						? QuestionRepository[c.BranchQuestionId.Value]
						: null;

					choice.Name = c.Name.Trim();
					choice.Code = c.Name.Trim();
					choice.Position = c.Position;
					choice.BranchQuestion = branchQuestion;
					choice.Question = question;

					question.Choices.Add(choice);
				}

				QuestionRepository.Save(question);

				return RedirectToAction("Index", new { channelId, surveyId });
			}

			var jsSerializer = new JavaScriptSerializer();
			ViewBag.Branches = jsSerializer.Serialize(model.BranchToQuestions);
			ViewBag.Choices = jsSerializer.Serialize(model.Choices);

			return View("EditMultiChoice", model);
		}


		[HttpPost]
		private ActionResult EditSongs(int channelId, int surveyId, int? questionId, QuestionEditModel model)
		{
			var channel = ChannelRepository[channelId];
			var survey = SurveyRepository[surveyId];

			var question = questionId.HasValue
				? (AudioPodQuestion)QuestionRepository[questionId.Value]
				: new AudioPodQuestion {
					Survey = survey,
					Category = QuestionCategoryEnum.Normal
				};

			//Current channel access has already been verified, now check that the survey belongs to the same channel and the question belongs to that survey
			if (!SecurityService.ValidateOwnership(channel, survey, question)) {
				return new HttpUnauthorizedResult(String.Format("Question {0} must be in the selected survey ({1}) and membership authorized channel ({2})", questionId, surveyId, channelId));
			}

			if (ModelState.IsValid) {
				question.Name = model.Name.Trim();
				question.Text = model.Content.Trim();

				if (!survey.Questions.Contains(question)) {
					//TODO: is there a better way to do this?
					var position = survey.Questions.Count;
					survey.Questions.Add(question);
					question.Position = position;
				}

				question.Songs.Clear();
				foreach (var s in model.Songs) {
					var song = SongRepository[s.Id];
					question.Songs.Add(song);

					//Copy song data into the model in case we have to re-display the EditSongs form again (the model currently only holds the song ID)
					s.Title = song.Title;
					s.Artist = song.Artist;
					s.Library = song.Library;
					s.Filename = song.Filename;
					s.Length = song.Length;
					s.Genre = song.Genre;
					s.Tempo = song.Tempo;
					s.Year = song.Year;
				}

				QuestionRepository.Save(question);

				return RedirectToAction("Index", new { channelId, surveyId });
			}

			return View("EditSongs", model);
		}


	}

}

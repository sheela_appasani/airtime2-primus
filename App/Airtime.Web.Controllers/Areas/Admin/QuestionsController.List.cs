using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using Airtime.Core;
using Airtime.Data.Surveying.Questions;
using Airtime.Models;
using Airtime.Models.Questions;

using Xtra.Common;


namespace Airtime.Web.Controllers.Areas.Admin
{

	public partial class QuestionsController : Controller
	{

		public ActionResult List(int channelId, int surveyId, DataTableModel dataTable)
		{
			var questions = QuestionRepository
				.Where(q => q.Survey.Id == surveyId)
				.Where(q => q.Survey.Channel.Id == channelId)
				.Where(q => q.Category == QuestionCategoryEnum.Normal || q.Category == QuestionCategoryEnum.Normal_SurveyCompleted)
				.OrderBy(q => q.Category == QuestionCategoryEnum.Normal ? 0 : 1)
				.ThenBy(q => q.Position)
				.Select(q => new QuestionListModel {
					Id = q.Id,
					Name = q.Name,
					Position = q.Position - 3,
					QuestionType = q.QuestionType.ToResourceString(),
					Category = q.Category.ToString().Trim()
				}).ToList();

			var questionsTable = new {
				currentPage = 0,
				totalRecords = questions.Count,
				list = questions
			};

			return Json(questionsTable, JsonRequestBehavior.AllowGet);
		}


		public ActionResult ListChoices(int channelId, int surveyId, int? questionId, DataTableModel dataTable)
		{
			var channel = ChannelRepository[channelId];
			var survey = SurveyRepository[surveyId];
			var question = questionId.HasValue ? QuestionRepository[questionId.Value] : null;
			if (!SecurityService.ValidateOwnership(channel, survey, question)) {
				return new HttpUnauthorizedResult();
			}

			var choices = !questionId.HasValue
				? new List<QuestionChoiceModel>()
				: ((MultiChoiceQuestion)QuestionRepository[questionId.Value]).Choices
					.Select(choice => new QuestionChoiceModel {
						Id = choice.Id,
						Name = choice.Name,
						Position = choice.Position + 1,
						BranchQuestionId = choice.BranchQuestion != null ? choice.BranchQuestion.Id : (int?)null,
						BranchQuestionName = choice.BranchQuestion != null ? choice.BranchQuestion.Name : null
					})
					.ToList();

			var choicesTable = new {
				currentPage = 0,
				totalRecords = choices.Count,
				list = choices
			};

			return Json(choicesTable, JsonRequestBehavior.AllowGet);
		}

	}

}

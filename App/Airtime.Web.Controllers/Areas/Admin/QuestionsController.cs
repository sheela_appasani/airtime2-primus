using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using Airtime.Core.Framework.Mvc;
using Airtime.Data.Client;
using Airtime.Data.Content.Fields;
using Airtime.Data.Media;
using Airtime.Data.Surveying;
using Airtime.Data.Surveying.Questions;
using Airtime.Models.Questions;
using Airtime.Services.Security;
using Airtime.Services.Surveying;

using NLog;


namespace Airtime.Web.Controllers.Areas.Admin
{

	[RequireLogin(Roles = "Developer,Administrator,Client_Turnkey")]
	public partial class QuestionsController : Controller
	{

		public QuestionsController(ISecurityService securityService, ISurveyingService surveyingService, IChannelRepository channelRepository, ISurveyRepository surveyRepository, IQuestionRepository questionRepository, IQuestionChoiceRepository questionChoiceRepository, ISongRepository songRepository, ISongFieldRepository songFieldRepository)
		{
			SecurityService = securityService;
			SurveyingService = surveyingService;
			ChannelRepository = channelRepository;
			SurveyRepository = surveyRepository;
			QuestionRepository = questionRepository;
			QuestionChoiceRepository = questionChoiceRepository;
			SongRepository = songRepository;
			SongFieldRepository = songFieldRepository;
		}


		public ActionResult Index(int channelId, int surveyId)
		{
			var model = new QuestionIndexModel {
				AvailableQuestionTypes = new List<QuestionTypeEnum> {
					QuestionTypeEnum.TEXT,
					QuestionTypeEnum.RADIO,
					QuestionTypeEnum.CHECKBOX,
					QuestionTypeEnum.AUDIO,
					QuestionTypeEnum.AUDIOPOD,
					QuestionTypeEnum.BREAK,
				},
				Survey = SurveyRepository[surveyId]
			};

			return View(model);
		}


		[HttpPost]
		public ActionResult Index(int channelId, int surveyId, string submitAction, ICollection<int> idSelection, QuestionTypeEnum? questionType)
		{
			var questionId = (idSelection != null) ? idSelection.First() : (int?)null;
			switch (submitAction) {
				case "cancel":
					return RedirectToAction("Index", "Surveys", new { channelId });
				case "save":
					return RedirectToAction("Index", "Invitations", new { channelId, surveyId });
				case "copy":
					return Copy(channelId, surveyId, idSelection);
				case "add":
					return RedirectToAction("Edit", new { channelId, surveyId, questionType });
				case "edit":
					return RedirectToAction("Edit", new { channelId, surveyId, questionId });
				case "delete":
					return Delete(channelId, surveyId, idSelection);
			}

			return RedirectToAction("Index", new { channelId, surveyId });
		}


		public ActionResult Reorder(int channelId, int surveyId, ICollection<int> orderedIds)
		{
			var channel = ChannelRepository[channelId];
			var survey = SurveyRepository[surveyId];

			//Current channel access has already been verified, now check that the survey belongs to the same channel
			if (!SecurityService.ValidateOwnership(channel, survey)) {
				return new HttpUnauthorizedResult(String.Format("Survey {0} must be in the selected membership authorized channel", surveyId));
			}

			//Sanity check - remove any null questions that are in the list because of corrupted data
			for (int i = survey.Questions.Count - 1; i >= 0; i--) {
				if (survey.Questions[i] == null) {
					survey.Questions.RemoveAt(i);
				}
			}

			//Check that each question exists in the survey before trying to change anything
			var questions = new List<QuestionBase>(orderedIds.Count);
			foreach (int id in orderedIds) {
				int questionId = id;
				var question = survey.Questions.FirstOrDefault(q => q.Id == questionId);
				if (question == null) {
					return new HttpNotFoundResult(String.Format("No question with ID {0} could be found in the selected survey {1}", questionId, surveyId));
				}
				questions.Add(question);
			}

			//Reorder questions based on their position of their IDs in the orderedIds parameter
			int newPosition = 4;
			foreach (var question in questions) {
				if (question.Category == QuestionCategoryEnum.Normal) {
					question.Position = newPosition;
					newPosition++;
				}
			}

			return new EmptyResult();
		}


		protected ISecurityService SecurityService { get; set; }
		protected ISurveyingService SurveyingService { get; set; }
		protected IChannelRepository ChannelRepository { get; set; }
		protected ISurveyRepository SurveyRepository { get; set; }
		protected IQuestionRepository QuestionRepository { get; set; }
		protected IQuestionChoiceRepository QuestionChoiceRepository { get; set; }
		protected ISongRepository SongRepository { get; set; }
		protected ISongFieldRepository SongFieldRepository { get; set; }


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

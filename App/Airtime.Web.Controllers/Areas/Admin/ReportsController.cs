using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using Airtime.Core;
using Airtime.Core.Framework.Mvc;
using Airtime.Core.Framework.Security;
using Airtime.Data;
using Airtime.Data.Client;
using Airtime.Data.Content.Fields;
using Airtime.Data.Media;
using Airtime.Data.Account;
using Airtime.Data.Reporting;
using Airtime.Data.Surveying;
using Airtime.Data.Surveying.Answers;
using Airtime.Data.Surveying.Questions;
using Airtime.Models;
using Airtime.Models.Members;
using Airtime.Models.Reports;
using Airtime.Models.Surveys;
using Airtime.Services.Reporting;
using Airtime.Services.Security;

using NLog;
using Xtra.Common;


namespace Airtime.Web.Controllers.Areas.Admin
{

	[RequireLogin(Roles = "Developer,Administrator,Client_Turnkey")]
	public class ReportsController : Controller
	{

		public ReportsController(ISecurityService securityService, IReportingService reportingService, IChannelRepository channelRepository, IMembershipRepository membershipRepository,
			ISurveyRepository surveyRepository, IQuestionRepository questionRepository, IQuestionChoiceRepository questionChoiceRepository, IAnswerRepository answerRepository,
			ISongAttributeRepository songAttributeRepository, ISongRepository songRepository, ISongFieldRepository songFieldRepository, IMapper<Survey, SurveyModel> surveyMapper,
			IMapper<Membership, UserModel> userMapper, IReportChannelCacheRepository reportChannelCacheRepository, IMapper<DateTime, string> dateMapper)
		{
			SecurityService = securityService;
			ReportingService = reportingService;

			MembershipRepository = membershipRepository;
			ChannelRepository = channelRepository;
			SurveyRepository = surveyRepository;
			QuestionRepository = questionRepository;
			QuestionChoiceRepository = questionChoiceRepository;
			AnswerRepository = answerRepository;
			SongRepository = songRepository;
			ReportChannelCacheRepository = reportChannelCacheRepository;
			SongFieldRepository = songFieldRepository;
			SongAttributeRepository = songAttributeRepository;

			SurveyMapper = surveyMapper;
			UserMapper = userMapper;
			DateMapper = dateMapper;
		}


		[HttpGet]
		[RequirePermission(PermissionEnum.ViewMusicReports)]
		public ActionResult Music(int channelId, int? songId, MusicReportModel model, ReportFilterModel filter, SubmitActionEnum? submitAction)
		{
			var channel = ChannelRepository[channelId];

			if (submitAction == SubmitActionEnum.Back) {
				return RedirectToAction("Music", "Reports", new { channelId });
			}

			ViewBag.Channel = channel;

			//TODO: this should NOT be a hardcoded check - it needs to be made a configurable channel option
			if (channel.Company.Name == "TRN New Zealand") {
				model.ReportType = ReportTypeEnum.TRN;
			} else {
				model.ReportType = ReportTypeEnum.XtraResearch;
			}

			model.Filter = filter;

			//If no surveys have been selected, then select the latest one
			if (model.Filter.SelectedSurveyIds.Count == 0) {
				var latestSurvey = ReportingService.GetRecentSurveys(channel, 1).FirstOrDefault();
				if (latestSurvey != null) {
					model.Filter.SelectedSurveyIds.Add(latestSurvey.Id);
				}
			}


			//Repopulate the view model so that the filter-editor can display all required options

			//Retrieve list of recent surveys
			model.Filter.Surveys = ReportingService.GetRecentSurveys(channel)
				.Select(s => new ReportFilterSurveyModel() {
					Id = s.Id,
					Name = s.Name,
					ScheduleStartDate = s.Schedule.LocalStartDate.HasValue ? s.Schedule.LocalStartDate.Value.ToString("g") : Resources.Shared.NotAvailable,
					ScheduleEndDate = s.Schedule.LocalEndDate.HasValue ? s.Schedule.LocalEndDate.Value.ToString("g") : Resources.Shared.NotAvailable
				})
				.ToList();

			//Retrieve list of current channel screener/profile questions
			var profileQuestions = channel.ProfileQuestions
				.OfType<MultiChoiceQuestion>()
				.Where(q => q.Category != QuestionCategoryEnum.Registration_Age && q.Category != QuestionCategoryEnum.Registration_Gender)
				.ToList();

			model.Filter.Questions = new List<ReportFilterQuestionModel>();
			foreach (var question in profileQuestions) {
				var questionModel = new ReportFilterQuestionModel { Name = question.Name, Choices = question.Choices.ToDictionary(c => c, c => model.Filter.SelectedQuestionChoiceIds.Contains(c.Id)) };
				model.Filter.Questions.Add(questionModel);
			}


			if (songId.HasValue) {
				//Display the detail view for one selected song
				return View("MusicDetail", model);

			} else {
				//Display the view for all filtered songs in selected surveys
				model.SongReports = ReportingService
					.GetMusicSummary(model.Filter.SelectedSurveyIds, choiceIds: model.Filter.SelectedQuestionChoiceIds, gender: model.Filter.Gender, lowerAge: model.Filter.LowerAge, upperAge: model.Filter.UpperAge)
					.OrderByDescending(r => r.Score)
					.ToList();

				model.SampleSize = model.SongReports.Any() ? model.SongReports.Max(r => r.UserTotal) : 0;

				//TODO: this is a quick hack - needs to be redone to handle variable custom attributes (and also ANY number of songs; this implementation is limited by SQL Server's limit of 2100 parameters)
				var field = SongFieldRepository.FindByNameAndCompany("CART", channel.Company);
				if (field != null) {
					var allSongIds = model.SongReports.Select(x => x.SongId).ToList();
					model.SongAttributes = SongAttributeRepository
						.Where(x => x.Field == field)
						.Where(x => allSongIds.Contains(x.Song.Id))
						.ToDictionary(x => x.Song.Id, x => x.Value);
				}

				return View("Music", model);
			}
		}


		[HttpPost]
		[RequirePermission(PermissionEnum.ViewMusicReports)]
		public ActionResult Music(int channelId, MusicReportModel model, ReportFilterModel filter)
		{
			return RedirectToAction("Music",
				new {
					channelId,
					model.Filter.Gender,
					model.Filter.LowerAge,
					model.Filter.UpperAge,
					FilterSurveys = String.Join(",", model.Filter.SelectedSurveyIds),
					FilterQuestionChoices = String.Join(",", model.Filter.SelectedQuestionChoiceIds)
				});
		}


		[HttpGet]
		[RequirePermission(PermissionEnum.ViewMusicReports)]
		public ActionResult Members(int channelId, int songId, RatingLevelEnum ratingLevel, ReportFilterModel filter)
		{
			var channel = ChannelRepository[channelId];
			var song = SongRepository[songId];

			var model = new MusicReportModel { Filter = filter };

			var ratingRange = ReportingService.DefaultRatingRanges[ratingLevel];
			ViewBag.RatingLevel = ratingLevel;
			ViewBag.Song = song;
			ViewBag.Users = ReportingService.GetMusicSummaryMembers(song, lowerRating: ratingRange.From, upperRating: ratingRange.To,
					surveyIds: filter.SelectedSurveyIds, choiceIds: filter.SelectedQuestionChoiceIds,
					gender: filter.Gender, lowerAge: filter.LowerAge, upperAge: filter.UpperAge);

			return View(model);
		}


		[HttpPost]
		[RequirePermission(PermissionEnum.ViewMusicReports)]
		public ActionResult Members(int channelId, int songId, SubmitActionEnum submitAction, RatingLevelEnum ratingLevel, ReportFilterModel filter)
		{
			if (submitAction == SubmitActionEnum.Back) {
				return RedirectToAction("Music",
					new {
						channelId,
						filter.Gender,
						filter.LowerAge,
						filter.UpperAge,
						FilterSurveys = String.Join(",", filter.SelectedSurveyIds),
						FilterQuestionChoices = String.Join(",", filter.SelectedQuestionChoiceIds)
					});
			}

			return RedirectToAction("Members",
				new {
					channelId,
					songId,
					ratingLevel,
					filter.Gender,
					filter.LowerAge,
					filter.UpperAge,
					FilterSurveys = String.Join(",", filter.SelectedSurveyIds),
					FilterQuestionChoices = String.Join(",", filter.SelectedQuestionChoiceIds)
				});
		}


		[HttpGet]
		[RequirePermission(PermissionEnum.ViewPerceptualReports)]
		public ActionResult Perceptuals(int channelId, PerceptualReportModel model, ReportFilterModel filter, SubmitActionEnum? submitAction)
		{
			var channel = ChannelRepository[channelId];

			if (submitAction == SubmitActionEnum.Back) {
				return RedirectToAction("Perceptuals", "Reports", new { channelId });
			}

			ViewBag.Channel = channel;

			//TODO: this should NOT be a hardcoded check - it needs to be made a configurable channel option
			if (channel.Company.Name == "TRN New Zealand") {
				model.ReportType = ReportTypeEnum.TRN;
			} else {
				model.ReportType = ReportTypeEnum.XtraResearch;
			}

			model.Filter = filter;

			//If no surveys have been selected, then select the latest one
			if (model.Filter.SelectedSurveyIds.Count == 0) {
				var latestSurvey = ReportingService.GetRecentSurveys(channel, 1).FirstOrDefault();
				if (latestSurvey != null) {
					model.Filter.SelectedSurveyIds.Add(latestSurvey.Id);
				}
			}


			//Repopulate the view model so that the filter-editor can display all required options
			model.Filter.Surveys = ReportingService.GetRecentSurveys(channel)
				.Select(s => new ReportFilterSurveyModel() {
					Id = s.Id,
					Name = s.Name,
					ScheduleStartDate = s.Schedule.LocalStartDate.HasValue ? s.Schedule.LocalStartDate.Value.ToString("g") : Resources.Shared.NotAvailable,
					ScheduleEndDate = s.Schedule.LocalEndDate.HasValue ? s.Schedule.LocalEndDate.Value.ToString("g") : Resources.Shared.NotAvailable
				})
				.ToList();

			var profileQuestions = channel.ProfileQuestions
				.OfType<MultiChoiceQuestion>()
				.Where(q => q.Category != QuestionCategoryEnum.Registration_Age && q.Category != QuestionCategoryEnum.Registration_Gender)
				.ToList();

			model.Filter.Questions = new List<ReportFilterQuestionModel>();
			foreach (var question in profileQuestions) {
				var questionModel = new ReportFilterQuestionModel { Name = question.Name, Choices = question.Choices.ToDictionary(c => c, c => model.Filter.SelectedQuestionChoiceIds.Contains(c.Id)) };
				model.Filter.Questions.Add(questionModel);
			}



			var openEndedReports = ReportingService.GetReportOpenEnded(model.Filter.SelectedSurveyIds, choiceIds: model.Filter.SelectedQuestionChoiceIds, gender: model.Filter.Gender, lowerAge: model.Filter.LowerAge, upperAge: model.Filter.UpperAge);
			var multiChoiceReports = ReportingService.GetReportMultiChoice(model.Filter.SelectedSurveyIds, choiceIds: model.Filter.SelectedQuestionChoiceIds, gender: model.Filter.Gender, lowerAge: model.Filter.LowerAge, upperAge: model.Filter.UpperAge);

			var questions = QuestionRepository.Where(q => model.Filter.SelectedSurveyIds.Contains(q.Survey.Id))
				.Where(q => q.Category == QuestionCategoryEnum.Normal)
				.Where(
					q =>
					q.QuestionType == QuestionTypeEnum.AUDIO || q.QuestionType == QuestionTypeEnum.RADIO ||
					q.QuestionType == QuestionTypeEnum.CHECKBOX || q.QuestionType == QuestionTypeEnum.TEXT)
				.OrderBy(q => q.Survey.Schedule.StartDate)
				.ThenBy(q => q.Position)
				.ToList();

			model.Questions = questions;

			foreach (var question in questions) {
				model.OpenEndedReports[question] = new List<OpenEndedReport>();
				model.MultiChoiceReports[question] = new List<MultiChoiceReport>();
			}

			foreach (var reportItem in openEndedReports) {
				var question = QuestionRepository[reportItem.QuestionId];
				if (!model.OpenEndedReports.ContainsKey(question)) {
					model.OpenEndedReports[question] = new List<OpenEndedReport>();
				}
				model.OpenEndedReports[question].Add(reportItem);
			}

			foreach (var reportItem in multiChoiceReports) {
				var question = QuestionRepository[reportItem.QuestionId];
				if (!model.MultiChoiceReports.ContainsKey(question)) {
					model.MultiChoiceReports[question] = new List<MultiChoiceReport>();
				}
				model.MultiChoiceReports[question].Add(reportItem);
			}

			return View("Perceptuals", model);
		}


		[HttpPost]
		[RequirePermission(PermissionEnum.ViewPerceptualReports)]
		public ActionResult Perceptuals(int channelId, PerceptualReportModel model, ReportFilterModel filter)
		{
			return RedirectToAction("Perceptuals",
				new {
					channelId,
					model.Filter.Gender,
					model.Filter.LowerAge,
					model.Filter.UpperAge,
					FilterSurveys = String.Join(",", model.Filter.SelectedSurveyIds),
					FilterQuestionChoices = String.Join(",", model.Filter.SelectedQuestionChoiceIds)
				});
		}


		[HttpGet]
		[RequirePermission(PermissionEnum.ViewSummaryReports)]
		public ActionResult Summary(int channelId)
		{
			var timeIt = new TimeIt();
			var channel = ChannelRepository[channelId];

			var channelMembers = ReportingService.GetChannelMembers(channel);

			var model = new SummaryReportModel {
				TotalMembers = channelMembers.Count()
			};

			timeIt.Start("Get Stat Cache");
			var rcCache = ReportingService.GetReportChannelCache(channel);
			timeIt.Stop();
			model.CacheLocalDate = @Resources.Reports.NoPregeneratedStatisticsData;
			if (rcCache != null) {
				var localDate = TimeZoneInfo.ConvertTimeFromUtc(rcCache.CacheUtcDate, channel.TimeZoneInfo);
				model.CacheLocalDate = DateMapper.Map(localDate);
			}
			model.ApplyCacheValues(rcCache, ReportingService.DefaultBirthdayRanges);

			timeIt.Start("Latest Survey");
			GetLatestSurvey(channel, model);
			timeIt.Stop();

			if (SecurityService.CurrentMembership.IsDeveloper()) {
				model.TimeIt = timeIt;
			}

			model.ShowRefresh = SecurityService.CurrentMembership.IsAdmin();
			return View(model);
		}


		private void GetLatestSurvey(Channel channel, SummaryReportModel model)
		{
			var latestSurvey = SurveyRepository
				.Where(s => s.Schedule.EndDate.HasValue && s.Schedule.EndDate.Value.Year <= 9000)
				.Where(s => s.Channel.Id == channel.Id)
                .Where(s => !s.Archived)
                .OrderByDescending(s => s.Schedule.EndDate)
				.FirstOrDefault();

			if (latestSurvey != null) {
				model.LatestSurveyName = latestSurvey.Name;
				model.LatestSurveyLocalStartDate = latestSurvey.Schedule.LocalStartDate.HasValue ? latestSurvey.Schedule.LocalStartDate.Value.ToString("g") : Resources.Shared.NotAvailable;
				model.LatestSurveyLocalEndDate = latestSurvey.Schedule.LocalEndDate.HasValue ? latestSurvey.Schedule.LocalEndDate.Value.ToString("g") : Resources.Shared.NotAvailable;
				// ReSharper disable SpecifyACultureInStringConversionExplicitly
				model.LatestSurveyInvited = latestSurvey.Statistics.Invited.ToString();
				model.LatestSurveyCompleted = latestSurvey.Statistics.Completed.ToString();
				// ReSharper restore SpecifyACultureInStringConversionExplicitly
				model.LatestSurveyResponseRate = FormatString.GetPercentageString(latestSurvey.Statistics.Completed / (double)latestSurvey.Statistics.Invited);
				model.LatestSurveyStatus = latestSurvey.Status.ToResourceString();
				model.ChannelTargetMessage = GetCompletedSurveyTargetDemographicCount(channel, latestSurvey).ToString();

			} else {
				model.LatestSurveyName = Resources.Shared.NotAvailable;
				model.LatestSurveyLocalStartDate = Resources.Shared.NotAvailable;
				model.LatestSurveyLocalEndDate = Resources.Shared.NotAvailable;
				model.LatestSurveyInvited = Resources.Shared.NotAvailable;
				model.LatestSurveyCompleted = Resources.Shared.NotAvailable;
				model.LatestSurveyResponseRate = Resources.Shared.NotAvailable;
				model.LatestSurveyStatus = Resources.Shared.NotAvailable;
				model.ChannelTargetMessage = Resources.Shared.NotAvailable;
			}
		}


		private int GetCompletedSurveyTargetDemographicCount(Channel channel, Survey latestSurvey)
		{
			if (!channel.TargetGender.HasValue && channel.TargetMinimumAge <= 0 && channel.TargetMaximumAge <= 0) {
				return latestSurvey.Statistics.Completed;
			}

			var surveyCompletesInChannelTarget = MembershipRepository
				.Where(u => u.Channels.Contains(channel))
				.Where(u => u.Role == MembershipRoleEnum.Respondent)
				.Where(u => u.RespondentStates
				             .Where(rs => rs.Survey == latestSurvey)
				             .Any(rs => rs.Completed));

			surveyCompletesInChannelTarget = ReportingService.ApplyChannelTargetToMemberships(surveyCompletesInChannelTarget, channel);

			return surveyCompletesInChannelTarget.Count();
		}


		public List<StatRate> GetNewMemberRates(IQueryable<Membership> membershipQuery, IEnumerable<DateRange> dateRanges, int total, bool haveCache)
		{
			return dateRanges.Select(range => GetNewMember(membershipQuery, range, haveCache))
				.Select(stat => { stat.Total = total; return stat; }).ToList();
		}


		private StatRate GetNewMember(IQueryable<Membership> membershipQuery, DateRange range, bool haveCache)
		{
			return new StatRate {
				Label = range.Label,
				Count = haveCache ? 0 : membershipQuery.Count(u => u.DateCreated > range.From)
			};
		}


		private ISecurityService SecurityService { get; set; }
		private IReportingService ReportingService { get; set; }

		private IMembershipRepository MembershipRepository { get; set; }
		private IChannelRepository ChannelRepository { get; set; }
		private ISurveyRepository SurveyRepository { get; set; }
		private IQuestionRepository QuestionRepository { get; set; }
		private IQuestionChoiceRepository QuestionChoiceRepository { get; set; }
		private IAnswerRepository AnswerRepository { get; set; }
		private ISongRepository SongRepository { get; set; }
		private IReportChannelCacheRepository ReportChannelCacheRepository { get; set; }
		private ISongFieldRepository SongFieldRepository { get; set; }
		private ISongAttributeRepository SongAttributeRepository { get; set; }

		private IMapper<Survey, SurveyModel> SurveyMapper { get; set; }
		private IMapper<Membership, UserModel> UserMapper { get; set; }
		private IMapper<DateTime, string> DateMapper { get; set; }

		private static readonly Logger Log = LogManager.GetCurrentClassLogger();


	}

}

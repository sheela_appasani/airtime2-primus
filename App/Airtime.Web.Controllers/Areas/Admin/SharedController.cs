using System;
using System.Configuration;
using System.Globalization;
using System.Resources;
using System.Web;
using System.Web.Mvc;

using Airtime.Core;
using Airtime.Core.Framework.Mvc;
using Airtime.Data.Account;
using Airtime.Data.Client;
using Airtime.Data.Messaging;
using Airtime.Data.Surveying;

using Airtime.Services.Configuration;

using Airtime.Services.Globalization;
using Airtime.Services.Security;

using NLog;

using Xtra.Common;


namespace Airtime.Web.Controllers.Areas.Admin
{

	public class SharedController : Controller
	{

		public SharedController(IConfigurationService configurationService, IGlobalizationService globalizationService, ISecurityService securityService, IChannelRepository channelRepository, ISurveyRepository surveyRepository, IEmailBlastRepository emailBlastRepository)
		{
			ConfigurationService = configurationService;
			GlobalizationService = globalizationService;
			SecurityService = securityService;
			ChannelRepository = channelRepository;
			SurveyRepository = surveyRepository;
			EmailBlastRepository = emailBlastRepository;
		}


		public PartialViewResult DateTimePickerScript()
		{
			ViewBag.DateFormat = GlobalizationService.ConvertDateFormatToJquery(CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern);
			ViewBag.TimeFormat = GlobalizationService.ConvertTimeFormatToJquery(CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern);

			return PartialView("_DateTimePickerScript");
		}


		public PartialViewResult AudioPlayerScript()
		{
			return PartialView("_AudioPlayerScript");
		}


		public ContentResult GetActionUrl(string action, string controller, int? channelId, int? surveyId, int? questionId, int? blastId, int? userId, int? songId)
		{
			return Content(Url.Action(action, controller, new { channelId, surveyId, questionId, blastId, userId, songId }));
		}


		[RequireLogin]
		public ActionResult GetSurveyPreviewUrl(int channelId, int? surveyId, int? questionId)
		{
			var channel = ChannelRepository[channelId];

			if (surveyId.HasValue) {
				var survey = SurveyRepository[surveyId.Value];
				//Current channel access has already been verified, now check that the survey belongs to the same channel
				if (!SecurityService.ValidateOwnership(channel, survey)) {
					return new HttpUnauthorizedResult(String.Format("Survey {0} must be in the selected membership authorized channel", survey.Id));
				}
			}

			string surveyPreviewUrl = ConfigurationService.Airtime1SurveyPreviewUrl.NamedFormat(new {
				surveyId = surveyId ?? 0,
				questionId = questionId ?? 0,
				guid = SecurityService.CurrentMembership.Guid.ToString("N")
			});

			return Content(surveyPreviewUrl);
		}


		[RequireLogin]
		public ActionResult GetEmailPreviewUrl(int? blastId)
		{
			if (blastId.HasValue) {
				var blast = EmailBlastRepository[blastId.Value];
				if (blast == null) {
					return new HttpNotFoundResult();
				}

				//Verify that the email blast belongs to a channel that the member is allowed access to
				var channel = blast.Channel ?? (blast.Survey != null ? blast.Survey.Channel : null);
				if (channel != null && !SecurityService.ValidateChannel(channel)) {
					return new HttpUnauthorizedResult(String.Format("Email blast {0} must be in the selected membership authorized channel", blast.Id));
				}
			}

			string emailPreviewUrl = ConfigurationService.Airtime1EmailPreviewUrl.NamedFormat(new {
				blastId = blastId ?? 0,
				guid = SecurityService.CurrentMembership.Guid.ToString("N")
			});

			return Content(emailPreviewUrl);
		}


		public ContentResult GetTokenLoginUrl(Guid guid)
		{
			string tokenLoginUrl = ConfigurationService.Airtime1TokenLoginUrl.NamedFormat(new { guid = guid.ToString("N") });
			return Content(tokenLoginUrl);
		}


		public ContentResult GetChannelThemeUrl(int channelId)
		{
			var channel = ChannelRepository[channelId];
			string channelThemeUrl = HttpUtility.UrlDecode(ConfigurationService.Airtime1ChannelThemeUrl).NamedFormat(new { internalName = channel.InternalName });
			var uri = new Uri(channelThemeUrl);
			return Content(uri.AbsoluteUri + uri.Fragment);
		}


		public JsonResult GetResourcesJson(ResourceManager resourcesManager)
		{
			return Json(GlobalizationService.GetResourceDictionary(resourcesManager), JsonRequestBehavior.AllowGet);
		}


		protected IConfigurationService ConfigurationService { get; set; }
		protected IGlobalizationService GlobalizationService { get; set; }
		protected ISecurityService SecurityService { get; set; }
		protected IChannelRepository ChannelRepository { get; set; }
		protected ISurveyRepository SurveyRepository { get; set; }
		protected IEmailBlastRepository EmailBlastRepository { get; set; }


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

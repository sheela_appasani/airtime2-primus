using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web.Mvc;

using Airtime.Core;
using Airtime.Core.Framework.Mvc;
using Airtime.Core.Framework.Security;
using Airtime.Data.Client;
using Airtime.Data.Account;
using Airtime.Data.Messaging;
using Airtime.Data.Surveying;
using Airtime.Models;
using Airtime.Models.Surveys;
using Airtime.Services.Configuration;
using Airtime.Services.Globalization;
using Airtime.Services.Security;
using Airtime.Services.Surveying;

using AutoMapper;

using NLog;

using Xtra.Common;


namespace Airtime.Web.Controllers.Areas.Admin
{

	[RequireLogin(Roles = "Developer,Administrator,Client_Turnkey")]
	public class SurveysController : Controller
	{

		public SurveysController(IGlobalizationService globalizationService, ISecurityService securityService, ISurveyingService surveyingService, IChannelRepository channelRepository, ISurveyRepository surveyRepository, IConfigurationService configurationService)
		{
			GlobalizationService = globalizationService;
			SecurityService = securityService;
			SurveyingService = surveyingService;

			ChannelRepository = channelRepository;
			SurveyRepository = surveyRepository;

			ConfigurationService = configurationService;
		}


		[HttpGet]
		[RequirePermission(PermissionEnum.ViewSurveys)]
		public ActionResult Index(int channelId)
		{
			return View();
		}


		[HttpPost]
		public ActionResult Index(int channelId, SubmitActionEnum submitAction, ICollection<int> idSelection)
		{
			var surveyId = (idSelection != null) ? idSelection.First() : (int?)null;
			switch (submitAction) {
				case SubmitActionEnum.Copy:
					return RedirectToAction("Edit", new { channelId, surveyId, copy = true });
				case SubmitActionEnum.Add:
					return RedirectToAction("Edit", new { channelId });
				case SubmitActionEnum.Edit: // Do not think this is in use anymore, client manages it now.
					return RedirectToAction("Index", "Questions", new { channelId, surveyId });
				case SubmitActionEnum.Delete:
					return Delete(channelId, idSelection);
				case SubmitActionEnum.AddReminder:
					return RedirectToAction("Edit", "Invitations", new { channelId, surveyId, recipientSource = RecipientSourceEnum.All_Uncompleted_Invitees });
			}

			return RedirectToAction("Index", new { channelId });
		}


		public JsonResult List(int channelId, DataTableModel dataTable)
		{
			var surveys = SurveyRepository.Items;

			if (!SecurityService.CurrentMembership.IsAdmin()) {
				surveys = surveys.Where(s => !s.Archived);
			}

			//Channel filter
			if (channelId > 0) {
				surveys = SecurityService.ValidateChannel(channelId)
					? surveys.Where(s => s.Channel.Id == channelId)
					: surveys.Where(s => false);
			} else {
				surveys = surveys.Where(s => SecurityService.MembershipChannels.Contains(s.Channel));
			}

			//Search filter
			if (!string.IsNullOrWhiteSpace(dataTable.sSearch)) {
				surveys = surveys.Where(s => s.Name.Contains(dataTable.sSearch));
			}

			//Column sorting
			for (int i = 0; i < dataTable.iSortCols.Count; i++) {
				surveys = (dataTable.sSortDirs[i] == DataTableSortDirection.Descending)
					? surveys.OrderBy(dataTable.iSortCols[i] + " desc")
					: surveys.OrderBy(dataTable.iSortCols[i]);
			}
			surveys = surveys.OrderByDescending(s => s.Id);


			var pagedSurveys = surveys.ToPagedList(dataTable.iDisplayStart, dataTable.iDisplayLength)
				.Select(Mapper.Map<SurveyModel>);

			var surveysTable = new {
				currentPage = 0,
				totalRecords = surveys.Count(),
				list = pagedSurveys
			};

			return Json(surveysTable, JsonRequestBehavior.AllowGet);
		}


		[HttpGet]
		public ActionResult Edit(int channelId, int? surveyId)
		{
			//Get the default Language and TimeZone for the specified channel, otherwise fall back on the app's defaults
			var channel = ChannelRepository[channelId];

			//Load existing survey or else load a new one with defaults set
			if (!surveyId.HasValue) {
				// surveys created via javascript Post to Copy() - not having surveyId in Edit() is now an error.
				return new HttpUnauthorizedResult("SurveyController Edit does not support creating any longer.");
			}
			var survey = SurveyRepository[surveyId.Value];

			//Current channel access has already been verified, now check that the survey belongs to the same channel
			if (!SecurityService.ValidateOwnership(channel, survey)) {
				return new HttpUnauthorizedResult(String.Format("Survey {0} must be in the selected membership authorized channel ({1})", surveyId, channelId));
			}

			//Load survey data into the view model
			var model = Mapper.Map<SurveyModel>(survey);

			var surveyAlert = survey.Alerts.FirstOrDefault();
			if (surveyAlert != null) {
				model.AlertModel.Id = surveyAlert.Id;
				model.AlertModel.Gender = surveyAlert.Gender;
				model.AlertModel.LowerAge = surveyAlert.LowerAge;
				model.AlertModel.UpperAge = surveyAlert.UpperAge;
				model.AlertModel.SampleSize = surveyAlert.SampleSize;
			}

			model.Disabled = (SecurityService.CurrentMembership.Role != MembershipRoleEnum.Developer
				&& survey.Status != SurveyStatusEnum.Setup
				&& survey.Status != SurveyStatusEnum.Preview);

			return View(model);
		}


		private Survey NewDefaultSurvey(int channelId)
		{
			var channel = ChannelRepository[channelId];
			var company = (channel != null) ? channel.Company : SecurityService.CurrentMembership.Company;
			var channelLanguage = (channel != null) ? channel.Language ?? GlobalizationService.DefaultLanguage : GlobalizationService.DefaultLanguage;
			var channelTimeZone = (channel != null) ? channel.TimeZoneInfo ?? GlobalizationService.DefaultTimeZone : GlobalizationService.DefaultTimeZone;

			return new Survey {
				Channel = channel,
				Company = company,
				Language = channelLanguage,
				Schedule = {
					TimeZoneInfo = channelTimeZone
				},
				BurnButtons = {
					ShowNotTired = true,
					ShowLittleTired = true,
					ShowVeryTired = true,
					ShowUnfamiliar = true,
					NotTiredText = Resources.Surveys.NotTired,
					LittleTiredText = Resources.Surveys.LittleTired,
					VeryTiredText = Resources.Surveys.VeryTired,
					UnfamiliarText = Resources.Surveys.Unfamiliar,
				},
				Options = {
					EnableBurnButtons = true
				}
			};
		}


		/// <summary>
		/// Designed to be called via client javascript not browser, does not redirect a browser.
		/// Creates a new survey or copies an existing, ensuring no duplicate names.
		/// </summary>
		[HttpPost]
		public ActionResult Create(SubmitActionEnum submitAction, int channelId, string name, int? surveyId)
		{
			if (string.IsNullOrWhiteSpace(name)) {
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest, Resources.Surveys.SurveyRequiresNameField);
			}

			name = name.Trim();

			var channel = ChannelRepository[channelId];

			//Check whether any surveys with this name already exist
			if (SurveyRepository.Any(s => s.Channel == channel && s.Name == name)) {
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest, Resources.Surveys.SurveyDuplicateName);
			}

			Survey newSurvey;
			switch (submitAction) {
				case SubmitActionEnum.Copy:
					//Get source survey
					var sourceSurvey = surveyId.HasValue ? SurveyRepository[surveyId.Value] : null;
					if (sourceSurvey == null) {
						return new HttpStatusCodeResult(HttpStatusCode.BadRequest, String.Format(Resources.Surveys.SurveyCannotFindSourceSurveyF1, surveyId));
					}
					//Verify that Membership is allowed access to that source survey
					if (!SecurityService.ValidateOwnership(channel, sourceSurvey)) {
						return new HttpUnauthorizedResult(String.Format("Survey {0} must be in the selected membership authorized channel ({1})", surveyId, channelId));
					}
					//Make copy of source survey
					newSurvey = SurveyingService.CopySurvey(sourceSurvey);
					break;

				case SubmitActionEnum.Add:
					//Make new (mostly empty) survey
					newSurvey = NewDefaultSurvey(channelId);
					SurveyingService.SetupProfileQuestions(newSurvey);
					SurveyingService.SetupQuestions(newSurvey);
					break;

				default:
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest, Resources.Surveys.SurveyCopySubmitActionInvalid);
			}

			if (newSurvey != null) {
				newSurvey.Name = name;
				newSurvey.Schedule.StartDate = DateTime.MaxValue;
				newSurvey.Schedule.EndDate = DateTime.MaxValue;
				newSurvey.Schedule.ClientViewDate = DateTime.MaxValue;
				newSurvey.Schedule.RegistrationCutOffDate = DateTime.MaxValue;

				// Survey Alerts are not currently addressed, though they are copied above in case of copy.
				SurveyRepository.Save(newSurvey);

				// reference: http://stackoverflow.com/questions/199099/how-to-manage-a-redirect-request-after-a-jquery-ajax-call
				HttpContext.Response.AddHeader(ConfigurationService.XtraRedirectUrlHeader, Url.Action("Index", "Questions", new { channelId, surveyId = newSurvey.Id }));
			}

			return Json(true);
		}


		[HttpPost]
		public ActionResult Edit(string submitAction, int channelId, int? surveyId, SurveyModel model)
		{
			if (submitAction == "cancel") {
				return RedirectToAction("Index", "Invitations", new { channelId, surveyId });
			}

			if (submitAction == "save") {
				var channel = ChannelRepository[channelId];


				//Retrieve or create a survey object
				Survey survey;
				if (surveyId.HasValue) {
					//We're saving changes to an existing survey, so first retrieve the original object
					survey = SurveyRepository[surveyId.Value];

				} else {
					// surveys created via javascript Post to Copy() - not having surveyId in Edit() is now an error.
					return new HttpUnauthorizedResult("SurveyController Edit does not support creating any longer.");
				}


				//Current channel access has already been verified, now check that the survey belongs to the same channel
				if (!SecurityService.ValidateOwnership(channel, survey)) {
					return new HttpUnauthorizedResult(String.Format("Survey {0} must be in the selected membership authorized channel ({1})", surveyId, channelId));
				}


				if (SurveyRepository.Any(s => s.Channel == channel && s.Id != surveyId && s.Name == model.Name.Trim())) {
					ModelState.AddModelError(String.Empty, Resources.Surveys.SurveyDuplicateName);
				}


				if (ModelState.IsValid) {
					//Copy values into the entity from the model
					survey.Name = model.Name.Trim();

					survey.Options.ShowSongSummaryAtEnd = model.OptionsShowSongSummaryAtEnd;
					survey.Options.RandomiseSongLists = model.OptionsRandomiseSongLists;

					//Start and End dates in the model are in the survey's timezone, we need to convert them to UTC before storing in the database (the Local* fields will do that automatically)
					survey.Schedule.LocalStartDate = string.IsNullOrWhiteSpace(model.ScheduleStartDate)
						? (DateTime?)null
						: model.ScheduleStartDate.ConvertTo<DateTime>();
					survey.Schedule.LocalEndDate = string.IsNullOrWhiteSpace(model.ScheduleEndDate)
						? (DateTime?)null
						: model.ScheduleEndDate.ConvertTo<DateTime>();

					survey.BurnButtons.NotTiredText = model.BurnButtonsNotTiredText;
					survey.BurnButtons.LittleTiredText = model.BurnButtonsLittleTiredText;
					survey.BurnButtons.VeryTiredText = model.BurnButtonsVeryTiredText;
					survey.BurnButtons.UnfamiliarText = model.BurnButtonsUnfamiliarText;

					//Set other survey defaults (i.e. for fields that are hidden from the Membership but still need to be set)
					survey.Schedule.ClientViewDate = survey.Schedule.StartDate;
					survey.Schedule.RegistrationCutOffDate = survey.Schedule.EndDate;
					survey.BurnButtons.ShowNotTired = true;
					survey.BurnButtons.ShowLittleTired = true;
					survey.BurnButtons.ShowVeryTired = true;
					survey.BurnButtons.ShowUnfamiliar = true;

					//Survey alerts - currently we only support having zero or one, but the facility for multiple alerts is there for future development
					if (model.AlertModel.SampleSize.HasValue && model.AlertModel.SampleSize > 0) {
						//Want to add/edit the first alert
						var surveyAlert = survey.Alerts.FirstOrDefault();

						//If a previous alert already exists and has already been Sent
						if (surveyAlert != null && surveyAlert.Sent) {
							//Check if any of the alert's properties will be changing, if so then clear the Sent flag
							bool alertUpdated = (
								surveyAlert.Gender != model.AlertModel.Gender ||
								surveyAlert.LowerAge != model.AlertModel.LowerAge ||
								surveyAlert.UpperAge != model.AlertModel.UpperAge ||
								surveyAlert.SampleSize != model.AlertModel.SampleSize
							);
							if (alertUpdated) {
								surveyAlert.Sent = false;
							}
						}

						if (surveyAlert == null) {
							survey.Alerts.Add(surveyAlert = new SurveyAlert { Survey = survey });
						}

						//If a sent alert already exists and is being changed, then clear the Sent flag so the new one can be sent again
						surveyAlert.Gender = model.AlertModel.Gender;
						surveyAlert.LowerAge = model.AlertModel.LowerAge;
						surveyAlert.UpperAge = model.AlertModel.UpperAge;
						surveyAlert.SampleSize = model.AlertModel.SampleSize;

					} else {
						//Want to remove any alerts
						survey.Alerts.Clear();
					}

					//Save survey
					SurveyRepository.Save(survey);

					//Redirect to the Questions list page
					return RedirectToAction("Index", "Surveys", new { channelId, surveyId });
				}
			}

			// If we got this far, something failed, redisplay form
			return View(model);
		}


		[HttpPost]
		public ActionResult Delete(int channelId, ICollection<int> idSelection)
		{
			if (idSelection == null) {
				return RedirectToAction("Index", new { channelId });
			}

			var channel = ChannelRepository[channelId];

			//Check that all selected surveys belongs to the same channel and can be deleted
			var surveysToDelete = new List<Survey>(idSelection.Count);
			foreach (int surveyId in idSelection) {
				var survey = SurveyRepository[surveyId];
				if (!SecurityService.ValidateOwnership(channel, survey)) {
					return new HttpUnauthorizedResult(String.Format("Survey {0} must be in the selected membership authorized channel ({1})", surveyId, channelId));
				}
				surveysToDelete.Add(survey);
			}

			//Delete all selected surveys (don't actually delete - archive them instead)
			foreach (var survey in surveysToDelete) {
				if (survey.Archived) {
					SurveyRepository.Delete(survey);
				} else {
					SurveyRepository.Archive(survey);
				}
			}

			return RedirectToAction("Index", new { channelId });
		}


		protected IGlobalizationService GlobalizationService { get; set; }
		protected ISecurityService SecurityService { get; set; }
		protected ISurveyingService SurveyingService { get; set; }

		protected IChannelRepository ChannelRepository { get; set; }
		protected ISurveyRepository SurveyRepository { get; set; }
		protected IConfigurationService ConfigurationService { get; set; }

		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

﻿using System;
using System.Web.Mvc;

using Airtime.Core;
using Airtime.Core.Framework.Mvc;
using Airtime.Services.Server;

using NLog;

using Xtra.Common;


namespace Airtime.Web.Controllers.Areas.Dev
{

	[RequireLogin(Roles = "Developer")]
	public class CacheMgrController : Controller
	{

		public CacheMgrController(IServerService serverService, ICacheProvider cacheProvider)
		{
			ServerService = serverService;
			Cache = cacheProvider;
		}


		public ActionResult Index(string type)
		{
			return View();
		}


		public ActionResult Clear()
		{
			Cache.Clear();

			return RedirectToAction("Index");
		}


		public ActionResult Stats()
		{
			ViewBag.ServerName = ServerService.GetMachineName();
			ViewBag.AppPath = Request.ApplicationPath;

			ViewBag.TotalMemory = (FileSize)ServerService.GetTotalMemory();
			ViewBag.AvailableMemory = (FileSize)ServerService.GetAvailableMemory();
			ViewBag.ProcessMemory = (FileSize)ServerService.GetProcessMemory();

			ViewBag.CacheItems = Cache.Count;

			return PartialView("_CacheStatsPartial");
		}


		private IServerService ServerService { get; set; }
		private ICacheProvider Cache { get; set; }

		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

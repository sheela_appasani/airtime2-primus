﻿using System;
using System.Web;
using System.Web.Mvc;

using Airtime.Core.Framework.Mvc;

using Elmah;


namespace Airtime.Web.Controllers.Areas.Dev
{

	[RequireLogin(Roles = "Developer")]
	public class ElmahController : Controller
	{

		public ActionResult Index(string type)
		{
			return new ElmahResult(type);
		}

		public ActionResult Detail(string type)
		{
			return new ElmahResult(type);
		}

	}

	class ElmahResult : ActionResult
	{
		private string _resouceType;

		public ElmahResult(string resouceType)
		{
			_resouceType = resouceType;
		}

		public override void ExecuteResult(ControllerContext context)
		{
			var factory = new ErrorLogPageFactory();
			if (!string.IsNullOrEmpty(_resouceType)) {
				var pathInfo = "." + _resouceType;
				HttpContext.Current.RewritePath(_resouceType != "stylesheet"
						? HttpContext.Current.Request.Path.Replace(String.Format("/{0}", _resouceType), string.Empty)
						: HttpContext.Current.Request.Path,
					pathInfo, HttpContext.Current.Request.QueryString.ToString());
			}

			var handler = factory.GetHandler(HttpContext.Current, null, null, null);

			handler.ProcessRequest(HttpContext.Current);
		}
	}

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

using Airtime.Core;
using Airtime.Core.Framework.Mvc;
using Airtime.Data.Account;
using Airtime.Data.Client;
using Airtime.Data.Content.Fields;
using Airtime.Data.Content.Fields.Types;
using Airtime.Data.Media;
using Airtime.Data.Surveying;
using Airtime.Data.Surveying.Questions;
using Airtime.Models;
using Airtime.Services.Configuration;
using Airtime.Services.Messaging;
using Airtime.Services.Reporting;
using Airtime.Services.Security;

using NLog;

using Xtra.Common;


namespace Airtime.Web.Controllers.Areas.Dev
{

	[RequireLogin(Roles = "Developer")]
	public class HomeController : Controller
	{

		public HomeController(ISecurityService securityService, IConfigurationService configurationService, IChannelRepository channelRepository, ISurveyRepository surveyRepository, IQuestionRepository questionRepository, IMessagingService messagingService, IReportingService reportingService, IPersonRepository personRepository, IMembershipRepository membershipRepository, ISongRepository songRepository, ISongFieldRepository songFieldRepository, ICompanyRepository companyRepository)
		{
			SecurityService = securityService;
			ConfigurationService = configurationService;
			MessagingService = messagingService;
			ReportingService = reportingService;
			PersonRepository = personRepository;
			MembershipRepository = membershipRepository;
			ChannelRepository = channelRepository;
			SurveyRepository = surveyRepository;
			QuestionRepository = questionRepository;
			SongRepository = songRepository;
			SongFieldRepository = songFieldRepository;
			CompanyRepository = companyRepository;
		}


		[HttpGet]
		public ActionResult Index(string type)
		{
			ViewBag.ActiveTab = "Links";
			return View();
		}


		[HttpPost]
		[ValidateInput(false)]
		public ActionResult Index(SubmitActionEnum? submitAction, string htmlCode)
		{
			if (submitAction.HasValue && submitAction.Value == SubmitActionEnum.Clean) {
				ViewBag.ActiveTab = "SanitizerTesting";
				ViewBag.HtmlCodeClean = MessagingService.SanitizeHtmlForEmail(htmlCode);

			} else {
				ViewBag.ActiveTab = "Links";
			}

			return View();
		}


		public ActionResult Test(SubmitActionEnum? submitAction)
		{
			var survey = SurveyRepository.First(x => x.Name == "Action Test");

			foreach (var question in survey.Questions) {
				Log.Debug("{0}: {1} action/s", question.Name, question.PostActions.Count);
			}



			if (submitAction.HasValue && submitAction.Value == SubmitActionEnum.Back) {
				return RedirectToAction("Index");
			}

			return View();
		}


		public ActionResult ThrowException()
		{
			throw new Exception();
		}


		[HttpGet]
		public ActionResult ImportQuestions()
		{
			var surveyDropDownList = SurveyRepository
				.OrderByDescending(s => s.Schedule.StartDate)
				.ThenBy(s => s.Name)
				.Select(s => new SelectListItem {
					Value = s.Id.ToString(CultureInfo.InvariantCulture),
					Text = s.Name
				})
				.ToList();
			surveyDropDownList.Insert(0, new SelectListItem());

			ViewBag.SurveyDropDownList = surveyDropDownList;

			return View();
		}


		[HttpPost]
		public ActionResult ImportQuestions(SubmitActionEnum? submitAction, int? surveyId, HttpPostedFileBase file)
		{
			if (submitAction.HasValue && submitAction.Value == SubmitActionEnum.Back) {
				return RedirectToAction("Index");
			}

			Log.Debug(surveyId);
			if (surveyId.HasValue && file != null && file.ContentLength > 0) {
				var matcher = new Regex(@"^(?<text>.*?)\s*(?<choices>\[.+?])?\s*$", RegexOptions.Compiled);
				var filename = Path.GetFileName(file.FileName);

				var survey = SurveyRepository[surveyId.Value];
				var lastQuestionIndex = survey.Questions.Max(q => q.Position);
				var stationMostQuestion = survey.ProfileQuestions.FirstOrDefault(q => q.Category == QuestionCategoryEnum.Registration_StationMost) as MultiChoiceQuestion;
				var stationList = stationMostQuestion != null
					? stationMostQuestion.Choices.Select(c => c.Name).ToList()
					: new List<string>();

				int i = 0;
				using (var reader = new StreamReader(file.InputStream)) {
					while (!reader.EndOfStream) {
						string line = reader.ReadLine();
						if (String.IsNullOrEmpty(line)) {
							continue;
						}

						var match = matcher.Match(line);
						if (!match.Success) {
							continue;
						}

						i++;

						QuestionBase question;
						if (match.Groups["choices"].Success) {
							question = new MultiChoiceQuestion(QuestionTypeEnum.RADIO);
							var q = (MultiChoiceQuestion)question;

							var choiceNames = ParseChoices(match.Groups["choices"].ToString());

							for (int j = choiceNames.Count - 1; j >= 0; j--) {
								string choiceName = choiceNames[j];
								if (stationMostQuestion != null && choiceName.Equals("Station List", StringComparison.InvariantCultureIgnoreCase)) {
									choiceNames.RemoveAt(j);
									choiceNames.InsertRange(j, stationList);
								}
							}

							for (int j = 0; j < choiceNames.Count; j++) {
								string choiceName = choiceNames[j];

								q.Choices.Add(new QuestionChoice() {
									Question = q,
									Name = choiceName,
									Code = j.ToString(),
									Position = j
								});
							}
						} else {
							question = new TextQuestion();
						}
						question.Name = "Tracking Question " + i;
						question.Text = match.Groups["text"].ToString().Trim();
						question.Position = lastQuestionIndex + i;
						question.Survey = survey;
						survey.Questions.Add(question);
					}
				}
			}

			return ImportQuestions();
		}


		public ActionResult ResourceStrings()
		{
			var resourceSet = Resources.CKEditor.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
			var resources = resourceSet.Cast<DictionaryEntry>().ToDictionary(res => res.Key.ToString(), res => res.Value.ToString());
			ViewBag.Resources = resources;
			ViewBag.ResourcesJson = Newtonsoft.Json.JsonConvert.SerializeObject(resources);
			return View();
		}


		private List<String> ParseChoices(string input)
		{
			input = input.Substring(1, input.Length - 2)
				.Replace(" inc - ", "/", StringComparison.InvariantCultureIgnoreCase)
				.Replace(" inc ", "/", StringComparison.InvariantCultureIgnoreCase)
				.Replace("list stations", "Station List", StringComparison.InvariantCultureIgnoreCase);

			var choiceNames = input.Split(new[]{'/'}, StringSplitOptions.RemoveEmptyEntries);
			for (int i=0; i<choiceNames.Length; i++) {
				choiceNames[i] = choiceNames[i].Trim();
			}
	
			return choiceNames.ToList();
		}


		private ISecurityService SecurityService { get; set; }
		private IConfigurationService ConfigurationService { get; set; }
		private IMessagingService MessagingService { get; set; }
		private IReportingService ReportingService { get; set; }
		private IPersonRepository PersonRepository { get; set; }
		private IMembershipRepository MembershipRepository { get; set; }
		private IChannelRepository ChannelRepository { get; set; }
		private ISurveyRepository SurveyRepository { get; set; }
		private IQuestionRepository QuestionRepository { get; set; }
		private ISongRepository SongRepository { get; set; }
		private ISongFieldRepository SongFieldRepository { get; set; }
		private ICompanyRepository CompanyRepository { get; set; }


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

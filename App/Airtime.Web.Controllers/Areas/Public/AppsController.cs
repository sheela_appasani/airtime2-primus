﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Airtime.Data.Remote;
using Airtime.Services.Configuration;

using NLog;


namespace Airtime.Web.Controllers.Areas.Public
{

	public class AppsController : Controller
	{

		public AppsController(IConfigurationService configurationService, IAppRepository appRepository)
		{
			ConfigurationService = configurationService;
			AppRepository = appRepository;
		}


		public ActionResult Index(string appId)
		{
			var app = AppRepository.FindByCode(appId);
			if (app == null) {
				return new HttpNotFoundResult("App not found: " + appId);
			}

			if (String.IsNullOrEmpty(app.AppStoreUrl)) {
				return new HttpNotFoundResult("App is currently unavailable: " + appId);
			}

			return Redirect(app.AppStoreUrl);
		}


		private IConfigurationService ConfigurationService { get; set; }
		private IAppRepository AppRepository { get; set; }

		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

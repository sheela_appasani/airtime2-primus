﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

using Airtime.Core.Framework.Mvc;
using Airtime.Data.Account;
using Airtime.Data.Client;
using Airtime.Data.Remote;
using Airtime.Models.Account;
using Airtime.Services.Configuration;
using Airtime.Services.Messaging;
using Airtime.Services.Security;

using NLog;


namespace Airtime.Web.Controllers.Areas.Public
{

	public class HomeController : Controller
	{

		public HomeController(ISecurityService securityService, IFormsAuthenticationService formsService, IConfigurationService configurationService, IMessagingService messagingService, IAppRepository appRepository, IPersonRepository personRepository, IChannelRepository channelRepository, IMembershipRepository membershipRepository)
		{
			SecurityService = securityService;
			FormsService = formsService;
			ConfigurationService = configurationService;
			MessagingService = messagingService;
			AppRepository = appRepository;
			PersonRepository = personRepository;
			ChannelRepository = channelRepository;
			MembershipRepository = membershipRepository;
		}


		[HttpGet]
		public ActionResult RecoverPassword(int? channelId, string appId, string api)
		{
			if (!String.IsNullOrEmpty(api) && !ApiVersionRegex.IsMatch(api)) {
				throw new ArgumentException("Invalid API version format", "api");
			}

			return View();
		}


		[HttpPost]
		public ActionResult RecoverPassword(int? channelId, string appId, string api, RecoverPasswordModel model)
		{
			if (!String.IsNullOrEmpty(api) && !ApiVersionRegex.IsMatch(api)) {
				throw new ArgumentException("Invalid API version format", "api");
			}

			var person = PersonRepository.FindByEmail(model.Email);
			if (person == null) {
				ModelState.AddModelError(String.Empty, Resources.Account.EmailIncorrect);
			}

			if (ModelState.IsValid && person != null) {
				var channel = channelId.HasValue
					? ChannelRepository[channelId.Value]
					: null;

				var membership = MembershipRepository
					.Where(m => m.Enabled)
					.Where(m => m.Person == person)
					.Where(m => m.Role == MembershipRoleEnum.Respondent)
					.Where(m => channel == null || (channel != null && m.Channels.Contains(channel)))
					.OrderBy(m => m.Id)
					.FirstOrDefault();

				if (membership == null && person.HasAdminMembership()) {
					membership = MembershipRepository
						.Where(m => m.Enabled)
						.Where(m => m.Person == person)
						.Where(m => m.Role == MembershipRoleEnum.Administrator || m.Role == MembershipRoleEnum.Developer)
						.OrderBy(m => m.Id)
						.FirstOrDefault();
				}

				var app = AppRepository.FindByCode(appId);

				var emailPasswordTemplate = app != null
					? MessagingService.GetEmailPasswordTemplate(app, api)
					: MessagingService.GetEmailPasswordTemplate(channel);

				emailPasswordTemplate.AddValues(new {
					FirstName = person.GivenName,
					LastName = person.FamilyName,
					GivenName = person.GivenName,
					FamilyName = person.FamilyName,
					Email = person.Email,
					Password = person.Password,
					Id = person.Id,
					TokenGuid = membership != null ? membership.Guid.ToString("N") : ""
				});

				string fromAddress = channel != null
					? channel.EmailFrom ?? ConfigurationService.AirtimeSupportEmail
					: ConfigurationService.AirtimeSupportEmail;

				MessagingService.SendHtmlEmail(fromAddress, person.Email, Resources.Account.RecoverPassword, emailPasswordTemplate.ToString());

				return RedirectToAction("PasswordEmailed", new { appId, api, channelId });
			}

			return View(model);
		}


		public ActionResult PasswordEmailed(int? channelId, string appId, string api)
		{
			if (!String.IsNullOrEmpty(api) && !ApiVersionRegex.IsMatch(api)) {
				throw new ArgumentException("Invalid API version format", "api");
			}

			return View();
		}


		[RequireLogin]
		public ActionResult Index(int? channelId)
		{
			if (!channelId.HasValue) {
				var firstChannel = SecurityService.AllowedChannels.FirstOrDefault(c => c.EnableLandingPage);

				return firstChannel != null
					? RedirectToAction("Index", "Home", new { Area = "Public", channelId = firstChannel.Id })
					: RedirectToAction("LogOn", "Home", new { Area = "Public" });
			}

			var channel = ChannelRepository[channelId.Value];
			if (!channel.EnableLandingPage) {
				return new HttpUnauthorizedResult("Landing page is not enabled for this channel");
			}

			return View();
		}


		[HttpGet]
		public ActionResult LogOn(int? channelId, Guid? guid = null, string returnUrl = null)
		{
			if (guid.HasValue) {
				return LogOn(channelId, new LogOnModel { Guid = guid }, returnUrl);
			}

			return View();
		}


		[HttpPost]
		public ActionResult LogOn(int? channelId, LogOnModel model, string returnUrl)
		{
			if (ModelState.IsValid) {
				//Check if a login-guid has been provided, if so try using that to login
				if (model.Guid.HasValue && SecurityService.AuthenticateMembership(model.Guid.Value)) {
					FormsService.SignIn(model.Guid.Value, model.RememberMe);
					if (Url.IsLocalUrl(returnUrl)) {
						return Redirect(returnUrl);
					}
					return RedirectToAction("Index", "Home", new { Area = "Public", channelId });
				}

				//Try logging in using a username and password
				if (SecurityService.AuthenticatePerson(model.Email, model.Password)) {
					FormsService.SignInByEmail(model.Email, model.RememberMe);
					if (Url.IsLocalUrl(returnUrl)) {
						return Redirect(returnUrl);
					}
					return RedirectToAction("Index", "Home", new { Area = "Public", channelId });
				}

				//Login failed!! Credentials were incorrect or not present
				ModelState.AddModelError("", Resources.Account.EmailOrPasswordIncorrect);
			}

			// If we got this far, something failed, redisplay form
			return View(model);
		}


		public ActionResult LogOff(int? channelId)
		{
			FormsService.SignOut();

			return RedirectToAction("LogOn", "Home", new { Area = "Public", channelId });
		}


		private IFormsAuthenticationService FormsService { get; set; }
		private ISecurityService SecurityService { get; set; }
		private IConfigurationService ConfigurationService { get; set; }
		private IMessagingService MessagingService { get; set; }
		private IAppRepository AppRepository { get; set; }
		private IPersonRepository PersonRepository { get; set; }
		private IChannelRepository ChannelRepository { get; set; }
		private IMembershipRepository MembershipRepository { get; set; }

		private static readonly Regex ApiVersionRegex = new Regex(@"^(v\d+(\.\d+)*)?$", RegexOptions.IgnoreCase | RegexOptions.Compiled);

		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

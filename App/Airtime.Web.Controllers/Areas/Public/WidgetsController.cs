﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

using Airtime.Core.Framework.Mvc;
using Airtime.Core.Framework.Security;
using Airtime.Data.Account;
using Airtime.Data.Client;
using Airtime.Data.Content;
using Airtime.Data.Media;
using Airtime.Data.Surveying;
using Airtime.Data.Surveying.Answers;
using Airtime.Data.Surveying.Questions;
using Airtime.Models.Api.v1;
using Airtime.Services.Configuration;
using Airtime.Services.Messaging;
using Airtime.Services.Security;
using Airtime.Services.Server;
using Airtime.Services.Surveying;

using NLog;

using Xtra.Common;
using Xtra.Common.Templating;


namespace Airtime.Web.Controllers.Areas.Public
{

	[RequireLogin]
	public class WidgetsController : Controller
	{

		public WidgetsController(ISecurityService securityService, ISurveyingService surveyingService, IConfigurationService configurationService, IMessagingService messagingService, IChannelRepository channelRepository, ISurveyRepository surveyRepository, IQuestionRepository questionRepository, IMembershipRepository membershipRepository, IRespondentStateRepository respondentStateRepository, ISongScoreRepository songScoreRepository, ISongRepository songRepository, ICacheProvider cacheProvider)
		{
			SecurityService = securityService;
			SurveyingService = surveyingService;
			ConfigurationService = configurationService;
			MessagingService = messagingService;

			ChannelRepository = channelRepository;
			SurveyRepository = surveyRepository;
			QuestionRepository = questionRepository;
			MembershipRepository = membershipRepository;
			RespondentStateRepository = respondentStateRepository;
			SongScoreRepository = songScoreRepository;
			SongRepository = songRepository;

			Cache = cacheProvider;
		}


		[HttpPost]
		[ValidateInput(false)]
		[RequirePermission(PermissionEnum.EditLandingPages)]
		[RequireLogin]
		public ActionResult SaveWidget(int channelId, WidgetTypeEnum? widgetType, string heading, string content)
		{
			if (!widgetType.HasValue) {
				return new HttpNotFoundResult();
			}

			var channel = ChannelRepository[channelId];
			if (channel == null) {
				return new HttpNotFoundResult();
			}

			var widget = GetChannelWidget(channel, widgetType.Value);
			widget.Heading = heading;
			widget.Content = content;
			if (widget.IsTransient()) {
				channel.Widgets.Add(widget);
			}
			ChannelRepository.Save(channel);

			switch (widgetType) {
				case WidgetTypeEnum.ActiveSurveys:
					return ActiveSurveys(channelId);

				case WidgetTypeEnum.Custom:
					return Custom(channelId);

				case WidgetTypeEnum.ExternalSurveys:
					return ExternalSurveys(channelId);

				case WidgetTypeEnum.MessageTheBoss:
					return MessageTheBoss(channelId);

				case WidgetTypeEnum.News:
					return News(channelId);

				case WidgetTypeEnum.OthersTopSongs:
					return OthersTopSongs(channelId);

				case WidgetTypeEnum.ReferFriends:
					return ReferFriends(channelId);

				case WidgetTypeEnum.SocialMedia:
					return SocialMedia(channelId);

				case WidgetTypeEnum.SongsYouVotedOn:
					return SongsYouVotedOn(channelId);

				case WidgetTypeEnum.YourTopSongs:
					return YourTopSongs(channelId);
			}

			return new EmptyResult();
		}


		[HttpGet]
		public ActionResult SongsYouVotedOn(int channelId)
		{
			return PartialView("SongsYouVotedOn", GetChannelWidget(channelId, WidgetTypeEnum.SongsYouVotedOn));
		}


		[HttpPost]
		public JsonResult SongsYouVotedOn(int channelId, bool? _)
		{
			var channel = ChannelRepository[channelId];
			var person = SecurityService.CurrentPerson;

			var latestSurveyState = RespondentStateRepository
				.Where(x => x.Completed)
				.Where(x => x.Survey.Channel == channel)
				.Where(x => x.Membership.Person == person)
				.OrderByDescending(x => x.DateModified)
				.ThenBy(x => x.Membership.Id)
				.Take(1)
				.FirstOrDefault();

			if (latestSurveyState != null && IsActiveOrLatestSurvey(latestSurveyState.Survey)) {
				var songs = QuestionRepository
					.Where(x => x.Survey == latestSurveyState.Survey)
					.OfType<AudioPodQuestion>()
					.Apply()
					.SelectMany(x => x.Songs)
					.Distinct()
					.Select(x => new SongApiModel(x))
					.ToList();

				return Json(songs);
			}

			return Json(new SongApiModel[] { });
		}


		[HttpGet]
		public ActionResult YourTopSongs(int channelId)
		{
			return PartialView("YourTopSongs", GetChannelWidget(channelId, WidgetTypeEnum.YourTopSongs));
		}


		[HttpPost]
		public JsonResult YourTopSongs(int channelId, bool? _)
		{
			var channel = ChannelRepository[channelId];
			var person = SecurityService.CurrentPerson;

			var latestSurveyState = RespondentStateRepository
				.Where(x => x.Completed)
				.Where(x => x.Survey.Channel == channel)
				.Where(x => x.Membership.Person == person)
				.OrderByDescending(x => x.DateModified)
				.ThenBy(x => x.Membership.Id)
				.Take(1)
				.FirstOrDefault();

			if (latestSurveyState != null && IsActiveOrLatestSurvey(latestSurveyState.Survey)) {
				string cacheKey = "Widget.YourTopSongs." + latestSurveyState.Survey.Id + "." + latestSurveyState.Id;
				var songs = Cache.Get(cacheKey, new TimeSpan(0, 2, 0),
					() => SongScoreRepository
						.Where(x => x.PlaylistAnswer.AnswerType == AnswerTypeEnum.AudioPod)
						.Where(x => x.PlaylistAnswer.RespondentState == latestSurveyState)
						.OrderByDescending(x => x.FinalRating)
						.Take(5)
						.Select(x => new SongApiModel(x.Song))
						.ToList()
				);

				return Json(songs);
			}

			return Json(new SongApiModel[] { });
		}


		[HttpGet]
		public ActionResult OthersTopSongs(int channelId)
		{
			return PartialView("OthersTopSongs", GetChannelWidget(channelId, WidgetTypeEnum.OthersTopSongs));
		}


		[HttpPost]
		public JsonResult OthersTopSongs(int channelId, bool? _)
		{
			var channel = ChannelRepository[channelId];
			var person = SecurityService.CurrentPerson;

			var latestSurveyState = RespondentStateRepository
				.Where(x => x.Completed)
				.Where(x => x.Survey.Channel == channel)
				.Where(x => x.Membership.Person == person)
				.OrderByDescending(x => x.DateModified)
				.ThenBy(x => x.Membership.Id)
				.Take(1)
				.FirstOrDefault();

			if (latestSurveyState != null && IsActiveOrLatestSurvey(latestSurveyState.Survey)) {
				string cacheKey = "Widget.OthersTopSongs." + latestSurveyState.Survey.Id;
				var songs = Cache.Get(cacheKey, new TimeSpan(0, 2, 0),
					() => SongScoreRepository
						.Where(score => score.PlaylistAnswer.AnswerType == AnswerTypeEnum.AudioPod)
						.Where(score => score.PlaylistAnswer.Question.Survey == latestSurveyState.Survey)
						.GroupBy(score => score.Song.Id)
						.OrderByDescending(group => @group.Average(x => x.FinalRating))
						.Select(group => @group.Key)
						.Take(5)
						.Apply()
						.Select(id => new SongApiModel(SongRepository[id]))
						.ToList()
				);

				return Json(songs);
			}

			return Json(new SongApiModel[] { });
		}


		[HttpGet]
		public ActionResult MessageTheBoss(int channelId)
		{
			return PartialView("MessageTheBoss", GetChannelWidget(channelId, WidgetTypeEnum.MessageTheBoss));
		}


		[HttpPost]
		public ActionResult MessageTheBoss(int channelId, string message)
		{
			var channel = ChannelRepository[channelId];

			if (!channel.EnableLandingPage || String.IsNullOrEmpty(channel.EmailFeedbackTo)) {
				return new HttpNotFoundResult();
			}

			message = message.Trim();
			if (message.Length > 10 * 1024) {
				return new HttpStatusCodeResult(HttpStatusCode.RequestEntityTooLarge);
			}

			var person = SecurityService.CurrentPerson;

			string fromAddress = String.IsNullOrEmpty(channel.EmailFrom)
				? ConfigurationService.AirtimeSupportEmail
				: channel.EmailFrom;

			string body = @"Email: {Email}
Name: {GivenName} {FamilyName}
Address: {Contact.Address}, {Contact.Suburb}, {Contact.City}, {Contact.State} {Contact.Postcode}
Phone: {Contact.DaytimePhone}
Mobile: {Contact.MobilePhone}
Gender: {Personal.Gender}
Age: {Personal.Age}

{Message}";

			body = body.NamedFormat(person, s => "{" + s + "}");
			body = body.NamedFormat(new { Message = message }, s => "");

			var result = MessagingService.SendTextEmail(fromAddress, channel.EmailFeedbackTo, channel.AdminName + " Feedback", body);

			if (!result) {
				return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
			}

			return Json(true);
		}


		[HttpGet]
		public ActionResult ActiveSurveys(int channelId)
		{
			return PartialView("ActiveSurveys", GetChannelWidget(channelId, WidgetTypeEnum.ActiveSurveys));
		}


		[HttpPost]
		public JsonResult ActiveSurveys(int channelId, bool? _)
		{
			var utcNow = DateTime.UtcNow;

			var channel = ChannelRepository[channelId];
			var person = SecurityService.CurrentPerson;
			var membership = MembershipRepository.FindByPersonChannel(person, channel);

			var surveys = SurveyRepository
				.Where(x => !x.Archived)
				.Where(x => x.Schedule.StartDate <= utcNow)
				.Where(x => !x.Schedule.EndDate.HasValue || x.Schedule.EndDate.Value > utcNow);

			if (channel != null) {
				surveys = surveys.Where(s => s.Channel == channel);
			}

			if (person.HasAdminMembership()) {
				//For people with an admin account, show ALL active surveys which they've not completed
				surveys = surveys
					.Where(s => !s.RespondentStates.Any(rs => rs.Membership == membership && rs.Completed));
			} else {
				//For people without any admin accounts, show active surveys for which they've been granted access and have not completed
				surveys = surveys
					.Where(s => s.RespondentStates.Any(rs => rs.Membership == membership && !rs.Completed));
			}

			var surveyList = surveys
				.OrderBy(x => x.Channel.Name)
				.ThenBy(x => x.Name)
				.ThenBy(x => x.Id)
				.ToList();

			var surveysData = surveyList.Select(
				x => new SurveyWidgetModel {
					ChannelName = x.Channel.Name,
					EndDate = x.Schedule.EndDate,
					EndDateLong = x.Schedule.EndDate.HasValue ? x.Schedule.EndDate.Value.ToLongDateString() : null,
					LoginGuid = membership.Guid,
					LoginUrl = ConfigurationService.Airtime1TokenLoginUrl.NamedFormat(new {
						guid = membership.Guid
					})
				})
				.ToList();

			return Json(surveysData);
		}


		[HttpGet]
		public ActionResult ReferFriends(int channelId)
		{
			return PartialView("ReferFriends", GetChannelWidget(channelId, WidgetTypeEnum.ReferFriends));
		}


		[HttpPost]
		public ActionResult ReferFriends(int channelId, List<string> emails)
		{
			var channel = ChannelRepository[channelId];
			if (!channel.EnableLandingPage) {
				return new HttpNotFoundResult();
			}

			var referralSubject = channel.Contents.FirstOrDefault(x => x.ContentType == ContentTypeEnum.Email_ReferFriendSubject);
			var referralText = channel.Contents.FirstOrDefault(x => x.ContentType == ContentTypeEnum.Email_ReferFriend);
			if (referralSubject == null || referralText == null) {
				return new HttpNotFoundResult();
			}

			var person = SecurityService.CurrentPerson;
			string fromAddress = String.IsNullOrEmpty(channel.EmailFrom)
				? ConfigurationService.AirtimeSupportEmail
				: channel.EmailFrom;

			var innerTemplateString = MessagingService.ConvertToNewTemplateFormat(referralText.Text);
			var innerTemplate = new StringTemplate(innerTemplateString, new {
				GivenName = person.GivenName,
				FamilyName = person.FamilyName,
				FirstName = person.GivenName,
				LastName = person.FamilyName
			});

			var template = MessagingService.GetEmailReferralTemplate(channel).AddValues(new { EmailBody = innerTemplate });

			//Send emails and build a list of the ones which failed
			var failedEmails = emails.Where(email => !MessagingService.SendHtmlEmail(fromAddress, email, referralSubject.Text, template.ToString())).ToList();

			if (emails.Count == failedEmails.Count) {
				return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
			}

			return Json(true);
		}


		[HttpGet]
		public ActionResult News(int channelId)
		{
			return PartialView("News", GetChannelWidget(channelId, WidgetTypeEnum.News));
		}


		[HttpGet]
		public ActionResult SocialMedia(int channelId)
		{
			return PartialView("SocialMedia", GetChannelWidget(channelId, WidgetTypeEnum.SocialMedia));
		}


		[HttpGet]
		public ActionResult ExternalSurveys(int channelId)
		{
			return PartialView("ExternalSurveys", GetChannelWidget(channelId, WidgetTypeEnum.ExternalSurveys));
		}


		[HttpGet]
		public ActionResult Custom(int channelId)
		{
			return PartialView("Custom", GetChannelWidget(channelId, WidgetTypeEnum.Custom));
		}


		private Widget GetChannelWidget(int channelId, WidgetTypeEnum widgetType)
		{
			return GetChannelWidget(ChannelRepository[channelId], widgetType);
		}


		private Widget GetChannelWidget(Channel channel, WidgetTypeEnum widgetType)
		{
			Widget widget = null;
			if (channel != null) {
				widget = channel.Widgets.FirstOrDefault(x => x.WidgetType == widgetType);
			}

			return widget ?? new Widget { Channel = channel, WidgetType = widgetType, Heading = widgetType.ToString() };
		}


		private bool IsActiveOrLatestSurvey(Survey completedSurvey)
		{
			if (completedSurvey == null) {
				return false;
			}

			var channel = completedSurvey.Channel;
			switch (completedSurvey.Status) {
				case SurveyStatusEnum.Active:
					//Survey is active
					return true;

				case SurveyStatusEnum.Archived:
				case SurveyStatusEnum.Ended:
					//Survey is closed, check if it's the latest survey
					var utcNow = DateTime.UtcNow;
					return SurveyRepository
						.Where(s => !s.Archived)
						.Where(s => s.Schedule.StartDate <= utcNow)
						.Where(s => !s.Schedule.EndDate.HasValue || s.Schedule.EndDate.Value > utcNow)
						.Where(s => s.Channel == channel)
						.OrderBy(s => s.Schedule.StartDate)
						.ThenBy(s => s.Id)
						.Take(1)
						.Any(s => s == completedSurvey);
	
				default:
					//All other survey states - e.g. deleted, or not yet started
					return false;
			}
		}


		private ISecurityService SecurityService { get; set; }
		private ISurveyingService SurveyingService { get; set; }
		private IConfigurationService ConfigurationService { get; set; }
		private IMessagingService MessagingService { get; set; }

		private IChannelRepository ChannelRepository { get; set; }
		private ISurveyRepository SurveyRepository { get; set; }
		private IQuestionRepository QuestionRepository { get; set; }
		private IMembershipRepository MembershipRepository { get; set; }
		private IRespondentStateRepository RespondentStateRepository { get; set; }
		private ISongScoreRepository SongScoreRepository { get; set; }
		private ISongRepository SongRepository { get; set; }

		private ICacheProvider Cache { get; set; }


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}


	public class SurveyWidgetModel
	{
		public string ChannelName { get; set; }
		public DateTime? EndDate { get; set; }
		public string EndDateLong { get; set; }
		public Guid LoginGuid { get; set; }
		public string LoginUrl { get; set; }
	}

}

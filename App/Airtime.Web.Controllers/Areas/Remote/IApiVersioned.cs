﻿namespace Airtime.Web.Controllers.Areas.Remote
{

	internal interface IApiVersioned
	{
		string ApiVersion { get; }
	}

}

﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace Airtime.Web.Controllers.Areas.Remote.Server
{

	public class PingController : ApiController
	{

		public HttpResponseMessage Get()
		{
			return Request.CreateResponse(HttpStatusCode.OK);
		}

	}

}

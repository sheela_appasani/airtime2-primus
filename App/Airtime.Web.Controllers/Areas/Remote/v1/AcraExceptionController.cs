﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Xml.Serialization;

using Airtime.Core;
using Airtime.Core.Framework.Http;
using Airtime.Models.Api.v1;
using Airtime.Services.Configuration;
using Airtime.Services.Server;

using NLog;


namespace Airtime.Web.Controllers.Areas.Remote.v1
{

	public class AcraExceptionController : AirtimeApiController
	{

		public AcraExceptionController(IConfigurationService configurationService, IServerService serverService)
		{
			ConfigurationService = configurationService;
			ServerService = serverService;
		}


		public HttpResponseMessage Post(AcraExceptionApiModel reportData)
		{
			if (reportData == null) {
				throw new ApiResponseException(HttpStatusCode.BadRequest);
			}

			try {
				var filename = ServerService.SanitizeFilename(String.Format("{0:yyyyMMdd-HHmmss.fffff}#{1}.xml", DateTime.Now, reportData.REPORT_ID));

				string errorReportDir = Path.Combine(ConfigurationService.DataDirectory, "Errors", "ACRA");
				if (!Directory.Exists(errorReportDir)) {
					Directory.CreateDirectory(errorReportDir);
				}

				var serializer = new XmlSerializer(typeof(AcraExceptionApiModel));
				using (var writer = new StreamWriter(Path.Combine(errorReportDir, filename))) {
					serializer.Serialize(writer, reportData);
				}

			} catch (Exception ex) {
				Log.Error(ex.Message, ex);
				throw new ApiResponseException(ex);
			}

			return Request.CreateResponse(HttpStatusCode.Created, reportData.REPORT_ID);
		}


		private IConfigurationService ConfigurationService { get; set; }
		private IServerService ServerService { get; set; }


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

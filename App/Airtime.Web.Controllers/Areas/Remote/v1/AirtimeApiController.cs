﻿using System.Web.Http;


namespace Airtime.Web.Controllers.Areas.Remote.v1
{

	public abstract class AirtimeApiController : ApiController, IApiVersioned
	{
		public string ApiVersion { get { return "v1"; } }
	}

}

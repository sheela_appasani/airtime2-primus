﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;

using Airtime.Core.Framework.Http;
using Airtime.Data;
using Airtime.Data.Client;
using Airtime.Data.Account;
using Airtime.Data.Media;
using Airtime.Data.Remote;
using Airtime.Data.Surveying;
using Airtime.Data.Surveying.Answers;
using Airtime.Data.Surveying.Questions;
using Airtime.Models.Api.v1;
using Airtime.Services.Configuration;
using Airtime.Services.Security;
using Airtime.Services.Surveying;

using NLog;


namespace Airtime.Web.Controllers.Areas.Remote.v1
{

	[RequireLogin]
	public class AnswersController : AirtimeApiController
	{

		public AnswersController(
			ISecurityService securityService, IConfigurationService configurationService, ISurveyingService surveyingService,
			IAppRepository appRepository, IChannelRepository channelRepository, ISurveyRepository surveyRepository, IQuestionRepository questionRepository,
			IQuestionChoiceRepository questionChoiceRepository, IAnswerRepository answerRepository, IMembershipRepository membershipRepository,
			IRespondentStateRepository respondentStateRepository, ISongRepository songRepository, IUnitOfWork unitOfWork)
		{
			SecurityService = securityService;
			ConfigurationService = configurationService;
			SurveyingService = surveyingService;

			AppRepository = appRepository;
			ChannelRepository = channelRepository;
			SurveyRepository = surveyRepository;
			QuestionRepository = questionRepository;
			QuestionChoiceRepository = questionChoiceRepository;
			AnswerRepository = answerRepository;
			MembershipRepository = membershipRepository;
			RespondentStateRepository = respondentStateRepository;
			SongRepository = songRepository;

			UnitOfWork = unitOfWork;
		}


		public HttpResponseMessage Post(string appId, List<AnswerApiModel> answers)
		{
			if (appId == null) {
				throw new ApiResponseException(HttpStatusCode.BadRequest, new ArgumentNullException("appId"));
			}
			if (answers == null) {
				throw new ApiResponseException(HttpStatusCode.BadRequest, new ArgumentNullException("answers"));
			}

			var app = AppRepository.FindByCode(appId);
			if (app == null) {
				throw new ApiResponseException(HttpStatusCode.NotFound, "Unrecognized appId {0}", appId);
			}


			try {
				var person = SecurityService.CurrentPerson;
				var usedMemberships = new HashSet<Membership>();
				var newSurveyAnswers = new List<AnswerBase>();

				//Retrieve records for all Questions and Choices referenced by the given answers - main purpose of this is to allow them to be automatically cached here
				QuestionRepository.FindAllById(answers.Select(x => x.QuestionId).ToArray());
				QuestionChoiceRepository.FindAllById(answers.Where(x => x.ChoiceId.HasValue).Select(x => x.ChoiceId.Value).ToArray());

				//Validate and then setup new AnswerBase entities
				foreach (var answerModel in answers) {
					if (!IsValidAnswerModelGraph(app, answerModel)) {
						continue;
					}
					var newSurveyAnswer = CreateAnswerForSurveyQuestion(person, answerModel);
					newSurveyAnswers.Add(newSurveyAnswer);
					usedMemberships.Add(newSurveyAnswer.Membership);
				}

				//Filter and persist new AnswerBase entities for questions that have not previously been answered
				var addedAnswers = AddNewSurveyAnswers(usedMemberships, newSurveyAnswers);

				//For each survey answer that was successfully added and was for a cloned template profile question, save a corresponding answer against the Template question (replacing any pre-existing answer)
				var newChannelAnswers = addedAnswers.SelectMany(entry => entry.Value, (entry, answer) => SurveyingService.CopyAnswerForTemplate(answer))
					.Where(channelAnswer => channelAnswer != null)
					.ToList();
				QuestionRepository.FindAllById(newChannelAnswers.Select(x => x.Question.Id).ToArray());
				SetChannelAnswers(usedMemberships, newChannelAnswers);

				//Advance each Memberships' recorded survey position
				UpdateMembershipSurveyPositions(app, addedAnswers);

				return Request.CreateResponse(HttpStatusCode.Created);


			} catch (ApiResponseException) {
				throw;

			} catch (Exception ex) {
				Log.Error(ex.Message + "\n" + ex.StackTrace, ex);
				UnitOfWork.Rollback();
				throw new ApiResponseException(ex);
			}
		}


		private bool IsValidAnswerModelGraph(App app, AnswerApiModel answer)
		{
			//Retrieve and validate requested Question object
			var question = QuestionRepository[answer.QuestionId];
			if (question == null) {
				//The question does not exist!! Simply log it and silently skip
				Log.Warn("Failed to save answer because Question {0} does not exist", answer.QuestionId);
				return false;
			}


			//Retrieve and validate requested QuestionChoice object
			QuestionChoice choice = null;
			if (answer.ChoiceId.HasValue && answer.ChoiceId.Value > 0) {
				choice = QuestionChoiceRepository[answer.ChoiceId.Value];
				if (choice == null) {
					//The choice does not exist!! Simply log it and silently skip
					Log.Warn("Failed to save answer because QuestionChoice {0} does not exist", answer.ChoiceId);
					return false;
				}
			}


			//Validate relationship between Question and QuestionChoice objects - this one is a fatal error for the operation and should never happen unless there's data corruption somewhere!
			if (choice != null && !question.Equals(choice.Question)) {
				string errorMessage = String.Format("Mismatched data reference - QuestionChoice {0} does not belong to Question {1}", answer.ChoiceId, answer.QuestionId);
				Log.Error(errorMessage);
				throw new ApiResponseException(HttpStatusCode.BadRequest, errorMessage);
			}


			//Validate CurrentMembership is allowed access to the referenced Survey (if Membership is an Admin, then automatically create records granting them access)
			var survey = question.Survey;
			var channel = survey.Channel;

			if (!app.Channels.Contains(channel)) {
				Log.Warn("Failed to save answer because Question {0} cannot be accessed by app {1}", answer.QuestionId, app.Code);
				return false;
			}

			return true;
		}


		private AnswerBase CreateAnswerForSurveyQuestion(Person person, AnswerApiModel answerModel)
		{
			var question = QuestionRepository[answerModel.QuestionId];
			var survey = question.Survey;
			var channel = survey.Channel;

			var membership = GetFirstChannelMembership(person, channel);
			var respondentState = GetOrAddRespondentState(membership, survey);

			//Create new Answer object
			var newSurveyAnswer = (question.QuestionType == QuestionTypeEnum.AUDIOPOD)
				? (AnswerBase)new PlaylistAnswer {
					SongScores = ParseSongScoresString(answerModel.Text)
				}
				: (AnswerBase)new SingleAnswer {
					Choice = (answerModel.ChoiceId.HasValue && answerModel.ChoiceId.Value > 0)
						? QuestionChoiceRepository[answerModel.ChoiceId.Value]
						: null,
					Text = answerModel.Text
				};
			newSurveyAnswer.Membership = membership;
			newSurveyAnswer.RespondentState = respondentState;
			newSurveyAnswer.Question = question;
			newSurveyAnswer.IsProfileAnswer = question.IsProfileQuestion();

			//If we're creating a PlaylistAnswer, set each of its child SongScore objects to reference it
			var playlistAnswer = newSurveyAnswer as PlaylistAnswer;
			if (playlistAnswer != null) {
				foreach (var songScore in playlistAnswer.SongScores) {
					songScore.PlaylistAnswer = playlistAnswer;
				}
			}

			return newSurveyAnswer;
		}


		/// <summary>
		/// Add new answers to the membership's survey data, skipping new answers for any questions that were already previously answered
		/// </summary>
		/// <param name="usedMemberships"></param>
		/// <param name="newAnswers"></param>
		/// <returns>Dictionary listing all new answers which were actually added, keyed by RespondentState</returns>
		private Dictionary<RespondentState, List<AnswerBase>> AddNewSurveyAnswers(IEnumerable<Membership> usedMemberships, IEnumerable<AnswerBase> newAnswers)
		{
			//Filter out answers for questions that have already previously been answered
			var newlyAnsweredQuestions = newAnswers
				.Select(x => x.Question)
				.Distinct()
				.ToList();
			var priorAnsweredQuestions = AnswerRepository
				.Where(x => usedMemberships.ToList().Contains(x.Membership))
				.Where(x => newlyAnsweredQuestions.Contains(x.Question))
				.Select(x => x.Question)
				.Distinct()
				.ToList();
			var unsavedAnswers = newAnswers
				.Where(x => !priorAnsweredQuestions.Contains(x.Question))
				.ToList();

			//Save remaining new answers
			var surveyAnswers = new Dictionary<RespondentState, List<AnswerBase>>();
			foreach (var answer in unsavedAnswers) {
				var respondentState = answer.RespondentState;
				respondentState.SurveyAnswers.Add(answer);

				//Arrange newly added answers by RespondentState (survey) so that each RespondentState can have its survey position updated next
				if (!surveyAnswers.ContainsKey(respondentState)) {
					surveyAnswers[respondentState] = new List<AnswerBase> { answer };
				} else {
					surveyAnswers[respondentState].Add(answer);
				}
			}

			return surveyAnswers;
		}


		/// <summary>
		/// Set channel answers for the membership, replacing any answers for questions that were already previously answered
		/// </summary>
		/// <param name="usedMemberships"></param>
		/// <param name="newAnswers"></param>
		private void SetChannelAnswers(IEnumerable<Membership> usedMemberships, IEnumerable<AnswerBase> newAnswers)
		{
			var newlyAnsweredQuestions = newAnswers
				.Select(x => x.Question)
				.Distinct()
				.ToList();
			var priorAnswers = AnswerRepository
				.Where(x => usedMemberships.ToList().Contains(x.Membership))
				.Where(x => newlyAnsweredQuestions.Contains(x.Question))
				.Distinct()
				.ToList();
			foreach (var priorAnswer in priorAnswers) {
				AnswerRepository.Delete(priorAnswer);
			}
			foreach (var answer in newAnswers) {
				var membership = answer.Membership;
				membership.ProfileAnswers.Add(answer);
			}
		}


		/// <summary>
		/// Advance each Memberships' recorded survey position
		/// </summary>
		/// <param name="app"></param>
		/// <param name="addedAnswers"></param>
		private void UpdateMembershipSurveyPositions(App app, Dictionary<RespondentState, List<AnswerBase>> addedAnswers)
		{
			foreach (var entry in addedAnswers) {
				var respondentState = entry.Key;
				var newAnswers = entry.Value;

				respondentState.UsedPlatform = app.Platform;

				var finalQuestion = !respondentState.Completed
					? GetLastInteractiveQuestion(respondentState.Survey)
					: null;

				foreach (var answer in newAnswers) {
					if (answer.Question.Category == QuestionCategoryEnum.Normal_SurveyCompleted) {
						//Current answer is for the Survey Completed page, flag respondent as having completed
						respondentState.Completed = true;
					} else {
						//Get next question, based upon the newly added answer and the respondent's current survey position
						var nextQuestion = GetQuestionAfterAnswer(answer);
						respondentState.CurrentQuestion = nextQuestion;
					}
					if (!respondentState.Completed && finalQuestion != null && answer.Question == finalQuestion) {
						//Current answer is for the last interactive question, flag respondent as having completed
						respondentState.Completed = true;
					}
				}
			}
		}


		private Membership GetFirstChannelMembership(Person person, Channel channel)
		{
			var membership = MembershipRepository.FindByPersonChannel(person, channel);
			if (membership == null) {
				string msg = String.Format("Cannot find valid membership for Channel {0}", channel.Id);
				Log.Warn(msg);
				throw new ApiResponseException(msg);
			}
			return membership;
		}


		/// <summary>
		/// Find and return the membership's existing respondent-state record for this survey, otherwise create a new respondent-state record for this survey if the membership is an Admin
		/// </summary>
		/// <param name="membership"></param>
		/// <param name="survey"></param>
		/// <returns></returns>
		private RespondentState GetOrAddRespondentState(Membership membership, Survey survey)
		{
			var respondentState = RespondentStateRepository.FindByUserAndSurvey(membership, survey);
			if (respondentState == null) {
				//TODO: tune this to allow access to channel client accounts too
				if (membership.IsAdmin()) {
					respondentState = new RespondentState {
						Membership = membership,
						Survey = survey
					};
					survey.RespondentStates.Add(respondentState);
					membership.RespondentStates.Add(respondentState);

				} else {
					string msg = String.Format("Failed to save because Membership {0} has not been granted access to Survey {1}", membership.Id, survey.Id);
					Log.Warn(msg);
					throw new ApiResponseException(HttpStatusCode.Forbidden, msg);
				}
			}
			return respondentState;
		}


		private List<SongScore> ParseSongScoresString(string text)
		{
			var songScores = new List<SongScore>();

			var songScoreStrings = text.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
			foreach (var songScoreString in songScoreStrings) {
				try {
					var match = SongScoreFormatRegex.Match(songScoreString);
					if (!match.Success) {
						Log.Warn("Skipping invalid AudioPod data: " + songScoreString);
						continue;
					}

					int songId = Int32.Parse(match.Groups["songId"].Value);
					var songRatingsString = match.Groups["ratings"].Value;
					var songRatings = songRatingsString
						.Split(new[] { ',' })
						.Select(x => x.Length > 0 ? Int32.Parse(x) : (int?)null)
						.ToArray();

					// ReSharper disable PossibleInvalidOperationException
					int burnRating = songRatings.Last(rating => rating.HasValue).Value;
					// ReSharper restore PossibleInvalidOperationException
					int finalRating = burnRating > 100 ? burnRating % 100 : burnRating;
					var burnValue = burnRating > 0 ? burnRating - finalRating : -1;
					if (burnValue > (int)BurnEnum.VeryTired) {
						burnValue = (int)BurnEnum.VeryTired;
					}
					var burn = (BurnEnum)Enum.ToObject(typeof(BurnEnum), burnValue);

					var songScore = new SongScore {
						Song = SongRepository[songId],
						Ratings = songRatingsString,
						FinalRating = finalRating,
						Burn = burn
					};

					songScores.Add(songScore);

				} catch {
					Log.Warn("Skipping invalid AudioPod data: " + songScoreString);
				}
			}

			return songScores;
		}


		private QuestionBase GetQuestionAfterAnswer(AnswerBase answer)
		{
			var respondentState = answer.RespondentState;
			var survey = respondentState.Survey;
			var question = answer.Question;

			//If answer is a branching QuestionChoice then return the branch's target Question
			var singleAnswer = answer as SingleAnswer;
			if (singleAnswer != null && singleAnswer.Choice != null && question.QuestionType == QuestionTypeEnum.RADIO) {
				var branchToQuestion = singleAnswer.Choice.BranchQuestion;
				if (branchToQuestion != null) {
					return branchToQuestion;
				}
			}

			QuestionBase nextQuestion;
			if (question.IsProfileQuestion()) {
				//Get next profile question, or if the last profile question is already answered then get the first survey question
				//nextQuestion = survey.ProfileQuestions.Count > question.Position + 1
				//	? survey.ProfileQuestions[question.Position + 1]
				//	: (survey.Questions.FirstOrDefault(x => x.Category == QuestionCategoryEnum.Normal)
				//		?? survey.Questions.FirstOrDefault(x => x.Category == QuestionCategoryEnum.Normal_SurveyCompleted));
				return null;

			} else {
				//Get next survey question
				nextQuestion = survey.Questions.Count > question.Position + 1
					? survey.Questions[question.Position + 1]
					: GetSurveyCompletedQuestion(survey);
			}
			return nextQuestion;
		}


		private QuestionBase GetLastInteractiveQuestion(Survey survey)
		{
			return
				QuestionRepository.SetCacheable(
					QuestionRepository
						.Where(x => x.Survey == survey)
						.Where(x => x.QuestionType != QuestionTypeEnum.BREAK)
						.Where(x => x.Category == QuestionCategoryEnum.Normal)
						.OrderByDescending(x => x.Position)
				).FirstOrDefault();
		}


		private QuestionBase GetSurveyCompletedQuestion(Survey survey)
		{
			return
				QuestionRepository.SetCacheable(
					QuestionRepository
						.Where(x => x.Survey == survey)
						.Where(x => x.Category == QuestionCategoryEnum.Normal_SurveyCompleted)
						.OrderBy(x => x.Position)
				).FirstOrDefault();
		}


		protected ISecurityService SecurityService { get; set; }
		protected IConfigurationService ConfigurationService { get; set; }
		protected ISurveyingService SurveyingService { get; set; }

		protected IAppRepository AppRepository { get; set; }
		protected IChannelRepository ChannelRepository { get; set; }
		protected ISurveyRepository SurveyRepository { get; set; }
		protected IQuestionRepository QuestionRepository { get; set; }
		protected IQuestionChoiceRepository QuestionChoiceRepository { get; set; }
		protected IAnswerRepository AnswerRepository { get; set; }
		protected IMembershipRepository MembershipRepository { get; set; }
		protected IRespondentStateRepository RespondentStateRepository { get; set; }
		protected ISongRepository SongRepository { get; set; }

		protected IUnitOfWork UnitOfWork { get; set; }


		private static readonly Regex SongScoreFormatRegex = new Regex(@"^(?<songId>[0-9]+):(?<ratings>[0-9,]*)$", RegexOptions.Compiled | RegexOptions.CultureInvariant);

		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

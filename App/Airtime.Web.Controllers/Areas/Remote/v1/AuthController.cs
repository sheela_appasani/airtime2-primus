﻿using System;
using System.Linq;
using System.Net;

using Airtime.Core.Framework.Http;
using Airtime.Data.Account;
using Airtime.Data.Remote;
using Airtime.Models.Api.v1;
using Airtime.Services.Configuration;
using Airtime.Services.Security;

using NLog;


namespace Airtime.Web.Controllers.Areas.Remote.v1
{

	public class AuthController : AirtimeApiController
	{

		public AuthController(ISecurityService securityService, IFormsAuthenticationService formsAuthenticationService, IAppRepository appRepository, IConfigurationService configurationService, IPersonRepository personRepository)
		{
			SecurityService = securityService;
			FormsAuthenticationService = formsAuthenticationService;
			AppRepository = appRepository;
			ConfigurationService = configurationService;
			PersonRepository = personRepository;
		}


		public AuthApiModel Get(string appId, string email, string password, string notificationToken = null)
		{
			if (appId == null) {
				throw new ApiResponseException(HttpStatusCode.BadRequest, new ArgumentNullException("appId"));
			}
			if (email == null) {
				throw new ApiResponseException(HttpStatusCode.BadRequest, new ArgumentNullException("email"));
			}
			if (password == null) {
				throw new ApiResponseException(HttpStatusCode.BadRequest, new ArgumentNullException("password"));
			}

			var app = AppRepository.FindByCode(appId);
			if (app == null) {
				throw new ApiResponseException(HttpStatusCode.NotFound, "Unrecognized appId {0}", appId);
			}

			const string testSuffix = ".test";
			bool testMode = email.EndsWith(testSuffix);
			if (testMode) {
				email = email.Substring(0, email.Length - testSuffix.Length);
			}

			if (SecurityService.AuthenticatePerson(email, password) ||
				SecurityService.AuthenticateMembershipByEmail(email, password)) {

				var person = PersonRepository.FindByEmail(email);

				if (!String.IsNullOrEmpty(notificationToken)) {
					if (!person.DeviceRegistrations.Any(x => app == x.App && notificationToken.Equals(x.NotificationToken, StringComparison.Ordinal))) {
						var deviceRegistration = new DeviceRegistration() {
							App = app,
							Person = person,
							NotificationToken = notificationToken
						};
						person.DeviceRegistrations.Add(deviceRegistration);
					}
				}

				return new AuthApiModel {
					PersonId = person.Id,
					AuthToken = FormsAuthenticationService.GenerateEncryptedTicket(person, null, testMode ? new TimeSpan(0, 1, 0) : ConfigurationService.AuthTicketTimeoutTimeSpan)
				};
			}

			throw new ApiResponseException(HttpStatusCode.Unauthorized, "Incorrect email address or password");
		}


		protected ISecurityService SecurityService { get; set; }
		protected IFormsAuthenticationService FormsAuthenticationService { get; set; }
		protected IAppRepository AppRepository { get; set; }
		protected IConfigurationService ConfigurationService { get; set; }
		protected IPersonRepository PersonRepository { get; set; }


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Hosting;

using Airtime.Core;
using Airtime.Core.Framework.Http;
using Airtime.Data.Remote;
using Airtime.Services.Configuration;
using Airtime.Services.Security;

using Xtra.Common;

using Ionic.Zip;
using Ionic.Zlib;

using NLog;


namespace Airtime.Web.Controllers.Areas.Remote.v1
{

	public class BrandingController : AirtimeApiController
	{

		public BrandingController(ISecurityService securityService, IConfigurationService configurationService, IAppRepository appRepository)
		{
			SecurityService = securityService;
			ConfigurationService = configurationService;
			AppRepository = appRepository;
		}


		public HttpResponseMessage Get(string appId)
		{
			if (appId == null) {
				throw new ApiResponseException(HttpStatusCode.BadRequest, new ArgumentNullException("appId"));
			}

			var app = AppRepository.FindByCode(appId);
			if (app == null) {
				throw new ApiResponseException(HttpStatusCode.NotFound, "Unrecognized appId {0}", appId);
			}

			var appThemeDirVirtual = ConfigurationService.AppThemeDirPath.NamedFormat(new { apiVersion = ApiVersion, appCode = appId });
			var appThemeZipVirtual = ConfigurationService.AppThemeZipPath.NamedFormat(new { apiVersion = ApiVersion, appCode = appId });
			var appThemeDir = HostingEnvironment.MapPath(appThemeDirVirtual);
			var appThemeZip = HostingEnvironment.MapPath(appThemeZipVirtual);

			//Validate server config (for some reason 
			if (appThemeDir == null || appThemeZip == null) {
				var serverError = new NullReferenceException("appThemeDir and appThemeZip cannot be null");
				Log.Error(serverError.Message, serverError);
				throw new ApiResponseException(serverError);
			}

			//If theme zip file does not exist, but theme directory does, then create the theme zip file
			if (!File.Exists(appThemeZip) && Directory.Exists(appThemeDir)) {
				//TODO: IMPORTANT!!! THIS MUST BE DONE INSTEAD IN A THREAD-SYNCHRONIZED BACKGROUND JOB 
				try {
					using (var zip = new ZipFile()) {
						zip.CompressionLevel = CompressionLevel.BestCompression;
						zip.AddSelectedFiles(@"name = '*\drawable*\*.*'", appThemeDir, "", true);
						zip.Save(appThemeZip);
					}
					if (!ZipFile.IsZipFile(appThemeZip, true)) {
						throw new Exception("Generated theme zip file is invalid: " + appThemeZipVirtual);
					}
				} catch (Exception ex) {
					Log.Error(ex.Message, ex);
					throw new ApiResponseException(ex);
				}
			}

			//If theme zip file exists, return that
			if (File.Exists(appThemeZip)) {
				try {
					var streamContent = new StreamContent(new FileStream(appThemeZip, FileMode.Open, FileAccess.Read, FileShare.ReadWrite | FileShare.Delete));
					streamContent.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
					streamContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") {
						FileName = Path.GetFileName(appThemeZip)
					};

					var response = Request.CreateResponse(HttpStatusCode.OK);
					response.Content = streamContent;
					return response;

				} catch (Exception ex) {
					Log.Error(ex.Message, ex);
					throw new ApiResponseException(ex);
				}
			}

			//Theme zip file does not exist and theme directory does not exist, so return 404 NotFound error
			throw new ApiResponseException(HttpStatusCode.NotFound, "No themes were found at: " + appThemeDirVirtual);
		}


		protected ISecurityService SecurityService { get; set; }
		protected IConfigurationService ConfigurationService { get; set; }
		protected IAppRepository AppRepository { get; set; }


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

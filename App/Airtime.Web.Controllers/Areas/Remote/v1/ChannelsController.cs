﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

using Airtime.Core.Framework.Http;
using Airtime.Data.Client;
using Airtime.Data.Account;
using Airtime.Data.Remote;
using Airtime.Data.Surveying;
using Airtime.Models.Api.v1;
using Airtime.Services.Configuration;
using Airtime.Services.Security;

using NLog;

using Xtra.Common;


namespace Airtime.Web.Controllers.Areas.Remote.v1
{

	[RequireLogin]
	public class ChannelsController : AirtimeApiController
	{

		public ChannelsController(ISecurityService securityService, IConfigurationService configurationService, IAppRepository appRepository, IChannelRepository channelRepository, ISurveyRepository surveyRepository, IMembershipRepository membershipRepository)
		{
			SecurityService = securityService;
			ConfigurationService = configurationService;
			AppRepository = appRepository;
			ChannelRepository = channelRepository;
			SurveyRepository = surveyRepository;
			MembershipRepository = membershipRepository;
		}


		public List<ChannelApiModel> Get(string appId)
		{
			if (appId == null) {
				throw new ApiResponseException(HttpStatusCode.BadRequest, new ArgumentNullException("appId"));
			}

			var app = AppRepository.FindByCode(appId);
			if (app == null) {
				throw new ApiResponseException(HttpStatusCode.NotFound, "Unrecognized appId {0}", appId);
			}

			var person = SecurityService.CurrentPerson;
			bool personHasAdmin = MembershipRepository
				.Any(x => x.Enabled && x.Person == person && (x.Role == MembershipRoleEnum.Administrator || x.Role == MembershipRoleEnum.Developer));

			var channelsQuery = personHasAdmin
				? ChannelRepository.ApplyFetchMany(ChannelRepository.Items, x => x.Contents).Where(x => x.Apps.Contains(app))
				: ChannelRepository.ApplyFetchMany(ChannelRepository.Items, x => x.Contents)
					.Where(x => x.Memberships.Any(u => u.Enabled && u.Person == person))
					.Where(x => x.Apps.Contains(app));

			channelsQuery = channelsQuery
				.OrderBy(x => x.Name)
				.ThenBy(x => x.Id);

			var channels = channelsQuery
				.Select(x => new ChannelApiModel(x))
				.AsEnumerable()
				.Distinct(new DelegateComparer<ChannelApiModel>(
					equals: (x, y) => x.Id == y.Id,
					getHashCode: x => x.Id.GetHashCode()
				))
				.ToList();

			var templateValues = new {
				FirstName = person.GivenName,
				LastName = person.FamilyName,
				GivenName = person.GivenName,
				FamilyName = person.FamilyName,
				Email = person.Email
			};

			channels.ForEach(x => {
				x.Contents.ReferFriendsEmailBody = x.Contents.ReferFriendsEmailBody != null
					? x.Contents.ReferFriendsEmailBody
					.Replace("[[", "{").Replace("]]", "}")
					.NamedFormat(templateValues, expression => "{" + expression + "}")
					: null;
				x.Contents.ReferFriendsEmailSubject = x.Contents.ReferFriendsEmailSubject != null
					? x.Contents.ReferFriendsEmailSubject
					.Replace("[[", "{").Replace("]]", "}")
					.NamedFormat(templateValues, expression => "{" + expression + "}")
					: null;
			});

			return channels;
		}


		public ChannelApiModel Get(string appId, int id)
		{
			if (appId == null) {
				throw new ApiResponseException(HttpStatusCode.BadRequest, new ArgumentNullException("appId"));
			}

			var app = AppRepository.FindByCode(appId);
			if (app == null) {
				throw new ApiResponseException(HttpStatusCode.NotFound, "Unrecognized appId {0}", appId);
			}

			var person = SecurityService.CurrentPerson;

			var channel = ChannelRepository[id];

			if (!person.HasAdminMembership()) {
				if (channel == null) {
					//Non-admin accounts are not even informed if the channel does not exist - they're simply forbidden
					throw new ApiResponseException(HttpStatusCode.Forbidden);
				}
				var hasChannelAccess = SecurityService.AllowedChannels.Contains(channel) && channel.Apps.Contains(app);
				if (!hasChannelAccess) {
					throw new ApiResponseException(HttpStatusCode.Forbidden);
				}
			}

			if (channel == null) {
				throw new ApiResponseException(HttpStatusCode.NotFound);
			}

			return new ChannelApiModel(channel);
		}


		protected ISecurityService SecurityService { get; set; }
		protected IConfigurationService ConfigurationService { get; set; }
		protected IAppRepository AppRepository { get; set; }
		protected IChannelRepository ChannelRepository { get; set; }
		protected ISurveyRepository SurveyRepository { get; set; }
		protected IMembershipRepository MembershipRepository { get; set; }


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

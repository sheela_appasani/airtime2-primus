﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

using Airtime.Core.Framework.Http;
using Airtime.Data.Client;
using Airtime.Data.Account;
using Airtime.Data.Remote;
using Airtime.Models.Api.v1.AppResources;
using Airtime.Services.Configuration;
using Airtime.Services.Security;

using NLog;

using Xtra.Common;


namespace Airtime.Web.Controllers.Areas.Remote.v1
{

	public class ContentController : AirtimeApiController
	{

		public ContentController(ISecurityService securityService, IConfigurationService configurationService, IAppRepository appRepository, IChannelRepository channelRepository)
		{
			SecurityService = securityService;
			ConfigurationService = configurationService;
			AppRepository = appRepository;
			ChannelRepository = channelRepository;
		}


		public AppResourcesApiModel Get(string appId)
		{
			if (appId == null) {
				throw new ApiResponseException(HttpStatusCode.BadRequest, new ArgumentNullException("appId"));
			}
			
			var app = AppRepository.FindByCode(appId);
			if (app == null) {
				throw new ApiResponseException(HttpStatusCode.NotFound, "Unrecognized appId {0}", appId);
			}

			string recoverPasswordUrl = ConfigurationService.Airtime2RecoverPasswordUrl.NamedFormat(new { appCode = app.Code, apiVersion = ApiVersion });

			//TODO: pull these strings from resources (probably as ones persisted in the database)
			switch (app.Code) {
				case "au.com.xtraresearch.android.musicjury":
					return new AppMusicJuryResourcesApiModel(recoverPasswordUrl);

				case "au.com.xtraresearch.android.musicpanel":
					return new AppMusicPanelResourcesApiModel(recoverPasswordUrl);

				case "au.com.xtraresearch.android.pilkada":
					return new AppPilkadaResourcesApiModel(recoverPasswordUrl);

				case "au.com.xtraresearch.android.todayvip":
					return new AppTodayVipResourcesApiModel(recoverPasswordUrl);

				case "au.com.xtraresearch.android.genfmmusikterkini":
				case "au.com.xtraresearch.android.airtime":
				default:
					return new AppAirtimeResourcesApiModel(recoverPasswordUrl);
			}
		}


		protected ISecurityService SecurityService { get; set; }
		protected IConfigurationService ConfigurationService { get; set; }
		protected IAppRepository AppRepository { get; set; }
		protected IChannelRepository ChannelRepository { get; set; }


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

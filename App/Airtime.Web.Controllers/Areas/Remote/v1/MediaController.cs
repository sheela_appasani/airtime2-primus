﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

using Airtime.Core.Framework.Http;
using Airtime.Data.Media;
using Airtime.Models.Api.v1;
using Airtime.Services.Configuration;

using NLog;


namespace Airtime.Web.Controllers.Areas.Remote.v1
{

	public class MediaController : AirtimeApiController
	{

		public MediaController(IConfigurationService configurationService, ISongRepository songRepository)
		{
			ConfigurationService = configurationService;
			SongRepository = songRepository;
		}


		public SongApiModel Get(int id, bool? resolve = null)
		{
			var song = SongRepository[id];

			if (song == null) {
				throw new ApiResponseException(HttpStatusCode.NotFound, "Unrecognized media ID {0}", id);
			}

			var songModel = new SongApiModel(song);
			if (resolve.HasValue && resolve.Value) {
				songModel.Filename = ConfigurationService.BuildAirtime2Url(VirtualPathUtility.ToAbsolute(songModel.Filename));
			}
			return songModel;
		}


		protected IConfigurationService ConfigurationService { get; set; }
		protected ISongRepository SongRepository { get; set; }


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

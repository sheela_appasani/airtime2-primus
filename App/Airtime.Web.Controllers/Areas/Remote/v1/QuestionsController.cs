﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web.Http;

using Airtime.Core.Framework.Http;
using Airtime.Data.Client;
using Airtime.Data.Account;
using Airtime.Data.Remote;
using Airtime.Data.Surveying;
using Airtime.Data.Surveying.Answers;
using Airtime.Data.Surveying.Questions;
using Airtime.Models.Api.v1;
using Airtime.Services.Configuration;
using Airtime.Services.Security;

using NLog;

using Xtra.Common;


namespace Airtime.Web.Controllers.Areas.Remote.v1
{

	[RequireLogin]
	public class QuestionsController : AirtimeApiController
	{

		public QuestionsController(ISecurityService securityService, IConfigurationService configurationService, IAppRepository appRepository, IChannelRepository channelRepository, ISurveyRepository surveyRepository, IQuestionRepository questionRepository, IMembershipRepository membershipRepository, IAnswerRepository answerRepository)
		{
			SecurityService = securityService;
			ConfigurationService = configurationService;
			AppRepository = appRepository;
			ChannelRepository = channelRepository;
			SurveyRepository = surveyRepository;
			QuestionRepository = questionRepository;
			MembershipRepository = membershipRepository;
			AnswerRepository = answerRepository;
		}


		[ActionName("Get")]
		public List<QuestionApiModel> GetAll(string appId, int surveyId)
		{
			if (appId == null) {
				throw new ApiResponseException(HttpStatusCode.BadRequest, new ArgumentNullException("appId"));
			}

			var app = AppRepository.FindByCode(appId);
			if (app == null) {
				throw new ApiResponseException(HttpStatusCode.NotFound, "Unrecognized appId {0}", appId);
			}

			var person = SecurityService.CurrentPerson;

			var survey = SurveyRepository[surveyId];

			if (!person.HasAdminMembership()) {
				//Check if person has access to this survey
				var hasSurveyAccess = SurveyRepository
					.Where(s => SecurityService.AllowedChannels.Contains(s.Channel))
					.Where(s => s.RespondentStates.Any(rs => rs.Membership.Enabled && rs.Membership.Person == person))
					.Where(s => s.Channel.Apps.Contains(app))
					.Any(s => s.Id == surveyId);
				if (!hasSurveyAccess) {
					throw new ApiResponseException(HttpStatusCode.Forbidden);
				}

				//Normal members cannot access survey questions while the survey is inactive
				var utcNow = DateTime.UtcNow;
				if (survey.Schedule.StartDate > utcNow) {
					//Survey is temporarily unavailable to the user (for now)
					throw new ApiResponseException(HttpStatusCode.ServiceUnavailable);
				}
				if (survey.Schedule.EndDate.HasValue && survey.Schedule.EndDate.Value <= utcNow) {
					//Survey has ended and is no longer available to the user at all
					throw new ApiResponseException(HttpStatusCode.NotFound);
				}
			}

			var profileQuestionsQuery = survey.ProfileQuestions
				.Where(x => x.Category != QuestionCategoryEnum.Registration_Age)
				.Where(x => x.Category != QuestionCategoryEnum.Registration_Gender)
				.OrderBy(x => x.Position);

			var surveyQuestionsQuery = survey.Questions
				.Where(x => x.Category == QuestionCategoryEnum.Normal || x.Category == QuestionCategoryEnum.Normal_SurveyCompleted)
				.OrderBy(x => x.Category == QuestionCategoryEnum.Normal ? 1 : 2)
				.ThenBy(x => x.Position);

			var questions = survey.Channel.EnableRegistrationDetailsApp
				? profileQuestionsQuery
					.Concat(surveyQuestionsQuery)
					.ToList()
				: surveyQuestionsQuery
					.ToList();

			//Remove registration questions that have audio which the member has already answered in a previous survey
			var lazyMembership = new Lazy<Membership>(() => MembershipRepository.FindByPersonChannel(person, survey.Channel));
			for (int i = questions.Count - 1; i >= 0; i--) {
				var question = questions[i];
				if (question.Song != null && question.Category == QuestionCategoryEnum.Registration && question.TemplateQuestion != null && lazyMembership.Value != null) {
					if (HasAlreadyAnsweredQuestion(lazyMembership.Value, question.TemplateQuestion)) {
						questions.RemoveAt(i);
					}
				}
			}

			var questionModels = questions
				.Select((x, i) => new QuestionApiModel(x) { Position = i })
				.ToList();

			var templateValues = new {
				FirstName = person.GivenName,
				LastName = person.FamilyName,
				GivenName = person.GivenName,
				FamilyName = person.FamilyName,
				Email = person.Email
			};

			try {
				questionModels.ForEach(x => x.Text.Replace("[[", "{").Replace("]]", "}").NamedFormat(templateValues));
			} catch (Exception) {
			}

			return questionModels;
		}


		public QuestionApiModel Get(string appId, int id)
		{
			if (appId == null) {
				throw new ApiResponseException(HttpStatusCode.BadRequest, new ArgumentNullException("appId"));
			}

			var app = AppRepository.FindByCode(appId);
			if (app == null) {
				throw new ApiResponseException(HttpStatusCode.NotFound, "Unrecognized appId {0}", appId);
			}

			var person = SecurityService.CurrentPerson;

			var question = QuestionRepository[id];

			if (question == null) {
				throw new ApiResponseException(HttpStatusCode.NotFound);
			}

			//Check if person has access to this question's survey
			if (!person.HasAdminMembership()) {
				//Check if person has access to this survey
				var hasSurveyAccess = SurveyRepository
					.Where(s => SecurityService.AllowedChannels.Contains(s.Channel))
					.Where(s => s.RespondentStates.Any(rs => rs.Membership.Enabled && rs.Membership.Person == person))
					.Where(s => s.Channel.Apps.Contains(app))
					.Any(s => s == question.Survey);
				if (!hasSurveyAccess) {
					throw new ApiResponseException(HttpStatusCode.Forbidden);
				}

				//Normal members cannot access survey questions while the survey is inactive
				var utcNow = DateTime.UtcNow;
				if (question.Survey.Schedule.StartDate > utcNow) {
					//Survey is temporarily unavailable to the user (for now)
					throw new ApiResponseException(HttpStatusCode.ServiceUnavailable);
				}
				if (question.Survey.Schedule.EndDate.HasValue && question.Survey.Schedule.EndDate.Value <= utcNow) {
					//Survey has ended and is no longer available to the user at all
					throw new ApiResponseException(HttpStatusCode.NotFound);
				}
			}

			var questionModel = new QuestionApiModel(question);

			questionModel.Text.Replace("[[", "{").Replace("]]", "}").NamedFormat(new {
				FirstName = person.GivenName,
				LastName = person.FamilyName,
				GivenName = person.GivenName,
				FamilyName = person.FamilyName,
				Email = person.Email
			});

			return questionModel;
		}


		private bool HasAlreadyAnsweredQuestion(Membership membership, QuestionBase question)
		{
			return AnswerRepository.Any(qa => qa.Membership == membership && qa.Question == question);
		}


		private ISecurityService SecurityService { get; set; }
		private IConfigurationService ConfigurationService { get; set; }
		private IAppRepository AppRepository { get; set; }
		private IChannelRepository ChannelRepository { get; set; }
		private ISurveyRepository SurveyRepository { get; set; }
		private IQuestionRepository QuestionRepository { get; set; }
		private IMembershipRepository MembershipRepository { get; set; }
		private IAnswerRepository AnswerRepository { get; set; }


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

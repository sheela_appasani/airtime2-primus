﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

using Airtime.Core.Framework.Http;
using Airtime.Data.Client;
using Airtime.Data.Account;
using Airtime.Data.Remote;
using Airtime.Data.Surveying;
using Airtime.Data.Surveying.Questions;
using Airtime.Models.Api.v1;
using Airtime.Services.Configuration;
using Airtime.Services.Security;

using NLog;


namespace Airtime.Web.Controllers.Areas.Remote.v1
{

	[RequireLogin]
	public class SurveysController : AirtimeApiController
	{

		public SurveysController(ISecurityService securityService, IConfigurationService configurationService, IAppRepository appRepository, IChannelRepository channelRepository, ISurveyRepository surveyRepository)
		{
			SecurityService = securityService;
			ConfigurationService = configurationService;
			AppRepository = appRepository;
			ChannelRepository = channelRepository;
			SurveyRepository = surveyRepository;
		}


		public List<SurveyApiModel> Get(string appId)
		{
			if (appId == null) {
				throw new ApiResponseException(HttpStatusCode.BadRequest, new ArgumentNullException("appId"));
			}

			var app = AppRepository.FindByCode(appId);
			if (app == null) {
				throw new ApiResponseException(HttpStatusCode.NotFound, "Unrecognized appId {0}", appId);
			}

			var utcNow = DateTime.UtcNow;

			var surveys = SurveyRepository
				.Where(x => !x.Archived)
				.Where(x => x.Schedule.StartDate <= utcNow)
				.Where(x => !x.Schedule.EndDate.HasValue || x.Schedule.EndDate.Value > utcNow)
				.Where(x => x.Channel.Apps.Contains(app));

			var person = SecurityService.CurrentPerson;
			if (person.HasAdminMembership()) {
				//For people with an admin account, show ALL active surveys which they've not completed
				surveys = surveys
					.Where(s => !s.RespondentStates.Any(rs => rs.Membership.Person == person && rs.Completed));
			} else {
				//For people without any admin accounts, show active surveys for which they've been granted access and have not completed
				surveys = surveys
					.Where(s => s.RespondentStates.Any(rs => rs.Membership.Enabled && rs.Membership.Person == person && !rs.Completed));
			}

			//TODO: it's possible to return multiple active surveys for a single channel - is that okay?
			
			return surveys
				.OrderBy(x => x.Channel.Name)
				.ThenBy(x => x.Schedule.EndDate)
				.ThenBy(x => x.Id)
				.Select(x => new SurveyApiModel(x) {
					SurveyCompletedText = x.Questions
						.Where(q => q.Category == QuestionCategoryEnum.Normal_SurveyCompleted)
						.Select(q => q.Text)
						.SingleOrDefault()
				})
				.ToList();
		}


		public SurveyApiModel Get(string appId, int id)
		{
			if (appId == null) {
				throw new ApiResponseException(HttpStatusCode.BadRequest, new ArgumentNullException("appId"));
			}

			var app = AppRepository.FindByCode(appId);
			if (app == null) {
				throw new ApiResponseException(HttpStatusCode.NotFound, "Unrecognized appId {0}", appId);
			}

			var person = SecurityService.CurrentPerson;

			if (!person.HasAdminMembership()) {
				var hasSurveyAccess = SurveyRepository
					.Where(s => SecurityService.AllowedChannels.Contains(s.Channel))
					.Where(s => s.RespondentStates.Any(rs => rs.Membership.Enabled && rs.Membership.Person == person))
					.Where(s => s.Channel.Apps.Contains(app))
					.Any(s => s.Id == id);
				if (!hasSurveyAccess) {
					throw new ApiResponseException(HttpStatusCode.Forbidden);
				}
			}

			var survey = SurveyRepository[id];
			if (survey == null) {
				throw new ApiResponseException(HttpStatusCode.NotFound);
			}

			return new SurveyApiModel(survey);
		}


		protected ISecurityService SecurityService { get; set; }
		protected IConfigurationService ConfigurationService { get; set; }
		protected IAppRepository AppRepository { get; set; }
		protected IChannelRepository ChannelRepository { get; set; }
		protected ISurveyRepository SurveyRepository { get; set; }


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

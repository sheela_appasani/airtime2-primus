﻿using Airtime.Core;
using Airtime.Data.Hibernate;
using Airtime.Hubs;
using Airtime.Services;

using Autofac;
using Autofac.Integration.Mvc;

using AutofacContrib.CommonServiceLocator;

using Microsoft.Practices.ServiceLocation;


namespace Airtime.Web.App_Start
{

	public static class AutofacConfig
	{

		public static void Register(IContainer container)
		{
			var builder = new ContainerBuilder();
			builder.RegisterModule(new AutofacWebTypesModule());
			builder.RegisterModule(new AutoMapperModule());
			builder.RegisterModule(new HibernateModule(true));
			builder.RegisterModule(new ServicesModule());
			builder.RegisterModule(new HubsModule());
			builder.Update(container);

			//Set the Common Service Locator to use Autofac (highly discouraged - only use the CSL when there is simply no other option!)
			ServiceLocator.SetLocatorProvider(() => new AutofacServiceLocator(container));
		}

	}

}

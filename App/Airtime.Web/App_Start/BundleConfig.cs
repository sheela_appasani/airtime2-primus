﻿using System.Net;
using System.Web.Optimization;


namespace Airtime.Web.App_Start
{

	public static class BundleConfig
	{

		// For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
		public static void Register(BundleCollection bundles)
		{
			// When <compilation debug="true" />, MVC4 will render the full readable version. When set to <compilation debug="false" />, the minified version will be rendered automatically

			bundles.Add(new ScriptBundle("~/bundles/ie8").Include(
				"~/Scripts/IE8.js"));

			bundles.Add(new ScriptBundle("~/bundles/common").Include(
				"~/Scripts/json3.js",
				"~/Scripts/persist.js",
				"~/Scripts/jquery-{version}.js",
				"~/Scripts/jquery.validate.js",
				"~/Scripts/modernizr-*",
				"~/Scripts/airtime.errorhandler.js",
				"~/Scripts/jquery.airtime.tools.js"
			));

			bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
				"~/Scripts/jquery-ui-{version}.js",
				"~/Scripts/jquery-ui-timepicker-addon.js",
				"~/Scripts/jquery-ui-selectmenu.js"
			));

			bundles.Add(new ScriptBundle("~/bundles/audioplayer").Include(
				"~/Scripts/jquery.swfobject.js",
				"~/Scripts/airtime.audioplayer.js",
				"~/Scripts/airtime.audioplayer.interface.js"
			));

			bundles.Add(new ScriptBundle("~/bundles/ajaxforms").Include(
				"~/Scripts/jquery.form.js"
			));

			bundles.Add(new ScriptBundle("~/bundles/signalr").Include(
				"~/Scripts/jquery.signalR-{version}.js"
			));
			
			bundles.Add(new ScriptBundle("~/bundles/datatables").Include(
				"~/Scripts/jquery.linq.js",
				"~/Scripts/jquery.tmpl.js",
				"~/Scripts/jquery.tablednd.js",
				"~/Scripts/jquery.dataTables.js",
				"~/Scripts/jquery.dataTables.ColVis.js",
				"~/Scripts/jquery.dataTables.plugins.js"
			));

			bundles.Add(new ScriptBundle("~/bundles/tabletools").Include(
				"~/Scripts/ZeroClipboard.js",
				"~/Scripts/jquery.dataTables.TableTools.js"
			));

			bundles.Add(new ScriptBundle("~/bundles/ckeditor").Include(
				"~/Scripts/ckeditor/ckeditor.js",
				"~/Scripts/ckeditor/adapters/jquery.js"
			));

			bundles.Add(new ScriptBundle("~/bundles/highcharts").Include(
				"~/Scripts/highcharts/highcharts.js",
				"~/Scripts/highcharts/modules/exporting.js"
			));

			bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
				"~/Scripts/bootstrap.js"
			));

			bundles.Add(new ScriptBundle("~/bundles/angular").Include(
				"~/Scripts/angular.js"
			));

			//Do not allow this to be minified otherwise AngularJS dependency-injection won't work!
			bundles.Add(new Bundle("~/bundles/widgets").Include(
				"~/Scripts/Widgets/Controllers/ActiveSurveysController.js",
				"~/Scripts/Widgets/Controllers/CustomController.js",
				"~/Scripts/Widgets/Controllers/ExternalSurveysController.js",
				"~/Scripts/Widgets/Controllers/MessageTheBossController.js",
				"~/Scripts/Widgets/Controllers/NewsController.js",
				"~/Scripts/Widgets/Controllers/OthersTopSongsController.js",
				"~/Scripts/Widgets/Controllers/ReferFriendsController.js",
				"~/Scripts/Widgets/Controllers/SocialMediaController.js",
				"~/Scripts/Widgets/Controllers/SongsYouVotedOnController.js",
				"~/Scripts/Widgets/Controllers/YourTopSongsController.js"
			));

			//Do not allow this to be minified otherwise AngularJS dependency-injection won't work!
			bundles.Add(new Bundle("~/bundles/widgeteditor").Include(
				"~/Scripts/Widgets/WidgetEditor.js"
			));


			//--------------------------------------------------------------------------------


			bundles.Add(new StyleBundle("~/Content/ltr/css").Include(
				"~/Content/themes/airtime/ltr/jquery.ui.selectmenu.css",
				"~/Content/themes/airtime/ltr/jquery.ui.timepicker.css",
				"~/Content/themes/airtime/ltr/Site.css",
				"~/Content/themes/airtime/ltr/jquery.dataTables.css",
				"~/Content/themes/airtime/ltr/jquery.dataTables.ColVis.css",
				"~/Content/themes/airtime/ltr/jquery.dataTables.airtime.css"
			));

			bundles.Add(new StyleBundle("~/Content/rtl/css").Include(
				"~/Content/themes/airtime/rtl/jquery.ui.selectmenu.css",
				"~/Content/themes/airtime/rtl/jquery.ui.timepicker.css",
				"~/Content/themes/airtime/rtl/Site.css",
				"~/Content/themes/airtime/rtl/jquery.dataTables.css",
				"~/Content/themes/airtime/rtl/jquery.dataTables.ColVis.css",
				"~/Content/themes/airtime/rtl/jquery.dataTables.airtime.css"));

			bundles.Add(new StyleBundle("~/Content/ltr/css/public").Include(
				"~/Content/themes/airtime/ltr/Public.css"));

			bundles.Add(new StyleBundle("~/Content/rtl/css/public").Include(
				"~/Content/themes/airtime/rtl/Public.css"));

			bundles.Add(new StyleBundle("~/Content/bootstrap").Include(
				"~/Content/bootstrap.css",
				"~/Content/bootstrap-responsive.css"));

			bundles.Add(new StyleBundle("~/Content/themes/redmond/css").Include(
				"~/Content/themes/redmond/jquery-ui-{version}.custom.css"));
		}

	}

}

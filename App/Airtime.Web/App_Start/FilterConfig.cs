﻿using System.Web.Mvc;

using Airtime.Core;
using Airtime.Core.Framework.Mvc;

using Autofac;
using Autofac.Integration.Mvc;


namespace Airtime.Web.App_Start
{

	public static class FilterConfig
	{

		public static void Register(IContainer container)
		{
			var builder = new ContainerBuilder();
			builder.RegisterType<ExtensibleActionInvoker>().As<IActionInvoker>();
			builder.RegisterType<ElmahHandleErrorAttribute>().As<FilterAttribute>().SingleInstance();
			builder.RegisterType<ActionLogFilterAttribute>().As<IActionFilter>().InstancePerLifetimeScope();
			builder.RegisterFilterProvider();
			builder.Update(container);
		}

	}

}

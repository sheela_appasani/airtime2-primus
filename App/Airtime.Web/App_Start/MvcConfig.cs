﻿using System.Reflection;
using System.Web.Mvc;

using Autofac;
using Autofac.Integration.Mvc;


namespace Airtime.Web.App_Start
{

	public static class MvcConfig
	{

		public static void Register(IContainer container)
		{
			var builder = new ContainerBuilder();
			//Register model binders
			builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
			builder.RegisterModelBinderProvider();
			//Register MVC view controllers
			builder.RegisterControllers(Airtime.Web.Controllers.AssemblyHook.Assembly).InjectActionInvoker();
			builder.RegisterSource(new ViewRegistrationSource());
			builder.Update(container);

			//Set Autofac as the dependency resolver for MVC
			DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

			//This project doesn't need any view-engines other than Razor
			ViewEngines.Engines.Clear();
			ViewEngines.Engines.Add(new RazorViewEngine());
		}

	}

}

using DataAnnotationsExtensions.ClientValidation;

[assembly: WebActivator.PreApplicationStartMethod(typeof(Airtime.Web.App_Start.RegisterClientValidationExtensions), "Start")]

namespace Airtime.Web.App_Start
{

	public static class RegisterClientValidationExtensions
	{

		public static void Start()
		{
			DataAnnotationsModelValidatorProviderExtensions.RegisterValidationExtensions();
		}

	}

}

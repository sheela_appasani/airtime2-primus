﻿using System.Web.Mvc;
using System.Web.Routing;

using Airtime.Core.Globalization;


namespace Airtime.Web.App_Start
{

	public static class RouteConfig
	{

		public static void Register(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			//Automatically "inject" the culture parameter into the routes that are setup to use the MultiCultureMvcRouteHandler
			foreach (Route r in routes) {
				if (r.RouteHandler is MultiCultureMvcRouteHandler) {
					r.Url = "{culture}/" + r.Url;

					//Add default culture
					if (r.Defaults == null) {
						r.Defaults = new RouteValueDictionary();
					}
					r.Defaults.Add("culture", CultureSupport.DefaultCulture);

					//Add constraint for culture parameter
					if (r.Constraints == null) {
						r.Constraints = new RouteValueDictionary();
					}
					r.Constraints.Add("culture", new CultureRouteConstraint(CultureSupport.AllowedCultures));
				}
			}
		}

	}

}

﻿using System.Collections.Generic;
using System.Web.Routing;

using Autofac;
using Autofac.Integration.SignalR;

using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;


namespace Airtime.Web.App_Start
{

	public static class SignalRConfig
	{

		public static void Register(IContainer container)
		{
			GlobalHost.DependencyResolver = new AutofacDependencyResolver(container);
			
			//Add all Hub Pipeline Modules that have been registered with Autofac (see the HubsModule class)
			var pipelineModules = container.Resolve<IEnumerable<IHubPipelineModule>>();
			foreach (var pipelineModule in pipelineModules) {
				GlobalHost.HubPipeline.AddModule(pipelineModule);
			}

			RouteTable.Routes.MapHubs();		
		}

	}

}

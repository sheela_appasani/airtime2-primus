﻿using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Http.Tracing;

using Airtime.Core.Framework;
using Airtime.Core.Framework.Http;

using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;

using Newtonsoft.Json.Converters;


namespace Airtime.Web.App_Start
{

	public static class WebApiConfig
	{

		public static void Register(IContainer container, HttpConfiguration config)
		{
			var builder = new ContainerBuilder();
			builder.RegisterApiControllers(Airtime.Web.Controllers.AssemblyHook.Assembly).InjectActionInvoker();
			builder.RegisterWebApiFilterProvider(config);
			builder.Update(container);


			//Redirect Web API log traces to NLog
			config.Services.Replace(typeof(ITraceWriter), new NLogTraceWriter());

			//Set Autofac as the dependency resolver for Web API
			config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

			//Replace the DefaultHttpControllerSelector with a customised one that allows Web API controllers to work in controller Areas
			config.Services.Replace(typeof(IHttpControllerSelector), new AreaHttpControllerSelector(GlobalConfiguration.Configuration));

			//Configure JSON serializer (Newtonsoft JSON.Net) to serialize enums as strings by default
			config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter());

			config.Formatters.XmlFormatter.UseXmlSerializer = true;

			config.Filters.Add(new UnhandledExceptionFilter());


			config.EnableQuerySupport();
		}

	}

}

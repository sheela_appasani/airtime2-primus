﻿using System.Web.Mvc;

using Airtime.Core;
using Airtime.Core.Globalization;


namespace Airtime.Web.Areas.Admin
{

	public class AdminAreaRegistration : AreaRegistration
	{

		public override string AreaName { get { return "Admin"; } }


		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.Namespaces.Add("Airtime.Web.Controllers.Areas.Admin");


			context.MapRouteWithName(
				name: "Root",
				url: "",
				defaults: new { controller = "Home", action = "Index" }
			).RouteHandler = new MultiCultureMvcRouteHandler();

			context.MapRouteWithName(
				name: "Admin_Root",
				url: "Admin",
				defaults: new { controller = "Home", action = "Index" }
			).RouteHandler = new MultiCultureMvcRouteHandler();


			//Login
			context.MapRouteWithName(
				name: "Admin_LogOn",
				url: "Admin/Home/LogOn/{guid}",
				defaults: new { controller = "Home", action = "LogOn", guid = UrlParameter.Optional }
			).RouteHandler = new MultiCultureMvcRouteHandler();

			//Marketing Emails
			context.MapRouteWithName(
				name: "Admin_Emails",
				url: "Admin/Emails/{action}/{channelId}/{blastId}",
				defaults: new { controller = "Emails", action = "Index", channelId = "", blastId = UrlParameter.Optional },
				constraints: new { channelId = @"\d+", blastId = @"\d*" }
			).RouteHandler = new MultiCultureMvcRouteHandler();

			//Media
			context.MapRouteWithName(
				name: "Admin_Media",
				url: "Admin/Media/{action}/{channelId}/{songId}",
				defaults: new { controller = "Media", action = "Index", channelId = "", songId = UrlParameter.Optional },
				constraints: new { channelId = @"\d+", songId = @"\d*" }
			).RouteHandler = new MultiCultureMvcRouteHandler();

			//Members
			context.MapRouteWithName(
				name: "Admin_Members",
				url: "Admin/Members/{action}/{channelId}/{userId}",
				defaults: new { controller = "Members", action = "Index", channelId = "", userId = UrlParameter.Optional },
				constraints: new { channelId = @"\d+", userId = @"\d*" }
			).RouteHandler = new MultiCultureMvcRouteHandler();

			//Surveys
			context.MapRouteWithName(
				name: "Admin_Surveys",
				url: "Admin/Surveys/{action}/{channelId}/{surveyId}",
				defaults: new { controller = "Surveys", action = "Index", channelId = "", surveyId = UrlParameter.Optional },
				constraints: new { channelId = @"\d+", surveyId = @"\d*" }
			).RouteHandler = new MultiCultureMvcRouteHandler();

			//Questions
			context.MapRouteWithName(
				name: "Admin_Questions",
				url: "Admin/Questions/{action}/{channelId}/{surveyId}/{questionId}",
				defaults: new { controller = "Questions", action = "Index", channelId = "", surveyId = "", questionId = UrlParameter.Optional },
				constraints: new { channelId = @"\d+", surveyId = @"\d+", questionId = @"\d*" }
			).RouteHandler = new MultiCultureMvcRouteHandler();

			//Survey Invitation Emails
			context.MapRouteWithName(
				name: "Admin_Invitations",
				url: "Admin/Invitations/{action}/{channelId}/{surveyId}/{blastId}",
				defaults: new { controller = "Invitations", action = "Index", channelId = "", surveyId = "", blastId = UrlParameter.Optional },
				constraints: new { channelId = @"\d+", surveyId = @"\d+", blastId = @"\d*" }
			).RouteHandler = new MultiCultureMvcRouteHandler();

			//Music Reports
			context.MapRouteWithName(
				name: "Admin_ReportsMusic",
				url: "Admin/Reports/Music/{channelId}/{songId}",
				defaults: new { controller = "Reports", action = "Music", channelId = "", songId = UrlParameter.Optional },
				constraints: new { channelId = @"\d+", songId = @"\d*" }
			).RouteHandler = new MultiCultureMvcRouteHandler();

			//Members Reports
			context.MapRouteWithName(
				name: "Admin_ReportsMembers",
				url: "Admin/Reports/Members/{channelId}/{songId}",
				defaults: new { controller = "Reports", action = "Members", channelId = "", songId = "" },
				constraints: new { channelId = @"\d+", songId = @"\d+" }
			).RouteHandler = new MultiCultureMvcRouteHandler();

			context.MapRouteWithName(
				name: "Admin_ReportsListMembers",
				url: "Admin/Reports/ListMembers/{channelId}/{songId}",
				defaults: new { controller = "Reports", action = "ListMembers", channelId = "", songId = "" },
				constraints: new { channelId = @"\d+", songId = @"\d+" }
			).RouteHandler = new MultiCultureMvcRouteHandler();

			//Reports
			context.MapRouteWithName(
				name: "Admin_Reports",
				url: "Admin/Reports/{action}/{channelId}",
				defaults: new { controller = "Reports", action = "Summary", channelId = "" },
				constraints: new { channelId = @"\d+" }
			).RouteHandler = new MultiCultureMvcRouteHandler();

			context.MapRouteWithName(
				name: "Admin_Tools",
				url: "Admin/AdminTools/{action}",
				defaults: new { controller = "AdminTools", action = "Index" }
			).RouteHandler = new MultiCultureMvcRouteHandler();

			context.MapRouteWithName(
				name: "Admin_Shared",
				url: "Admin/Shared/{action}/{channelId}",
				defaults: new { controller = "Shared", channelId = UrlParameter.Optional },
				constraints: new { channelId = @"\d*" }
			).RouteHandler = new MultiCultureMvcRouteHandler();

			context.MapRouteWithName(
				name: "Admin_Jobs",
				url: "Admin/Jobs/{action}/{channelId}",
				defaults: new { controller = "Jobs", channelId = UrlParameter.Optional },
				constraints: new { channelId = @"\d*" }
			);


			context.MapRouteWithName(
				name: "Admin_Default",
				url: "Admin/{controller}/{action}/{channelId}",
				defaults: new { controller = "Home", action = "Index", channelId = UrlParameter.Optional }
			).RouteHandler = new MultiCultureMvcRouteHandler();


			context.MapRouteWithName(
				name: "Admin_NotFound",
				url: "Admin/{*url}",
				defaults: new { controller = "Error", action = "NotFound" }
			);

		}

	}

}

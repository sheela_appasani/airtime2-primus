﻿using System.Web.Mvc;

using Airtime.Core;
using Airtime.Core.Globalization;


namespace Airtime.Web.Areas.Dev
{

	public class DevAreaRegistration : AreaRegistration
	{

		public override string AreaName { get { return "Dev"; } }

		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.Namespaces.Add("Airtime.Web.Controllers.Areas.Dev");

			context.MapRouteWithName(
				name: "Elmah",
				url: "Dev/Errors/{type}",
				defaults: new { action = "Index", controller = "Elmah", type = UrlParameter.Optional }
			);

			context.MapRouteWithName(
				name: "Cache",
				url: "Dev/Cache",
				defaults: new { action = "Index", controller = "CacheMgr" }
			);

			context.MapRouteWithName(
				name: "Dev_Default",
				url: "Dev/{controller}/{action}/{id}",
				defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
			);
		}

	}

}

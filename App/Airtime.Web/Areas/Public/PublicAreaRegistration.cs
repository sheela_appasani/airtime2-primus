﻿using System.Web.Mvc;

using Airtime.Core;


namespace Airtime.Web.Areas.Public
{

	public class PublicAreaRegistration : AreaRegistration
	{

		public override string AreaName { get { return "Public"; } }


		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.Namespaces.Add("Airtime.Web.Controllers.Areas.Public");

			context.MapRouteWithName(
				name: "Public_Apps",
				url: "Public/Apps/{appId}",
				defaults: new { controller = "Apps", action = "Index" }
			);

			context.MapRouteWithName(
				name: "Public_LogOn",
				url: "Public/Home/LogOn/{channelId}/{guid}",
				defaults: new { controller = "Home", action = "LogOn", channelId = UrlParameter.Optional, guid = UrlParameter.Optional },
				constraints: new { channelId = @"\d*" }
			);

			context.MapRouteWithName(
				name: "Public_Home",
				url: "Public/Home/{action}/{channelId}",
				defaults: new { controller = "Home", action = "Index", channelId = UrlParameter.Optional },
				constraints: new { channelId = @"\d*" }
			);

			context.MapRouteWithName(
				name: "Public_Widgets",
				url: "Public/Widgets/{action}/{channelId}",
				defaults: new { controller = "Widgets", channelId = UrlParameter.Optional },
				constraints: new { channelId = @"\d*" }
			);

		}

	}

}

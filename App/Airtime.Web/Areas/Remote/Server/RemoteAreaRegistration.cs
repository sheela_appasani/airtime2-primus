﻿using System.Web.Http;
using System.Web.Mvc;

using Airtime.Core;

using NLog;


namespace Airtime.Web.Areas.Remote.Server
{

	public class RemoteAreaRegistration : AreaRegistration
	{

		public override string AreaName { get { return "Remote.Server"; } }


		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.Namespaces.Add("Airtime.Web.Controllers.Areas.Remote.Server");

			context.MapHttpRouteWithName(
				name: "DefaultRemoteServer",
				routeTemplate: "Remote/Server/{controller}/{id}",
				defaults: new { id = RouteParameter.Optional }
			);
		}


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

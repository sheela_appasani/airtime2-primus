﻿using System.Web.Http;
using System.Web.Mvc;

using Airtime.Core;

using NLog;


namespace Airtime.Web.Areas.Remote.v1
{

	public class RemoteAreaRegistration : AreaRegistration
	{

		public override string AreaName { get { return "Remote.v1"; } }


		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.Namespaces.Add("Airtime.Web.Controllers.Areas.Remote.v1");

			context.MapHttpRouteWithName(
				name: "DefaultRemoteV1",
				routeTemplate: "Remote/v1/{controller}/{id}",
				defaults: new { id = RouteParameter.Optional }
			);
		}


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

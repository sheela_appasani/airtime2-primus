using System;
using System.Web.Mvc;

using Airtime.Core;
using Airtime.Models;

using Autofac.Integration.Mvc;

using NLog;


namespace Airtime.Web.Binders
{

	[ModelBinderType(typeof(DataTableModel))]
	public class DataTableModelBinder : IModelBinder
	{

		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
		{
			//see http://datatables.net/usage/server-side

			var dataTable = new DataTableModel();

			dataTable.sEcho = bindingContext.ValueProvider.GetValue("sEcho").ConvertToOrDefault<string>("");
			dataTable.iDisplayStart = bindingContext.ValueProvider.GetValue("iDisplayStart").ConvertToOrDefault<int>(0);
			dataTable.iDisplayLength = bindingContext.ValueProvider.GetValue("iDisplayLength").ConvertToOrDefault<int>(25);
			dataTable.sSearch = bindingContext.ValueProvider.GetValue("sSearch").ConvertToOrDefault<string>(null);
			dataTable.bEscapeRegex = bindingContext.ValueProvider.GetValue("bEscapeRegex").ConvertToOrDefault<bool?>(null);

			dataTable.iColumns = bindingContext.ValueProvider.GetValue("iColumns").ConvertToOrDefault<int>(0);
			for (int i = 0; i < dataTable.iColumns; i++) {
				dataTable.bSortables.Add(bindingContext.ValueProvider.GetValue(string.Format("bSortable_{0}", i)).ConvertToOrDefault<bool>(false));
			}
			for (int i = 0; i < dataTable.iColumns; i++) {
				dataTable.bSearchables.Add(bindingContext.ValueProvider.GetValue(string.Format("bSearchable_{0}", i)).ConvertToOrDefault<bool>(false));
			}
			for (int i = 0; i < dataTable.iColumns; i++) {
				dataTable.sSearchs.Add(bindingContext.ValueProvider.GetValue(string.Format("sSearch_{0}", i)).ConvertToOrDefault<string>(string.Empty));
			}
			for (int i = 0; i < dataTable.iColumns; i++) {
				dataTable.bEscapeRegexs.Add(bindingContext.ValueProvider.GetValue(string.Format("bEscapeRegex_{0}", i)).ConvertToOrDefault<bool>(false));
			}

			dataTable.iSortingCols = bindingContext.ValueProvider.GetValue("iSortingCols").ConvertToOrDefault<int>(0);
			for (int i = 0; i < dataTable.iSortingCols; i++) {
				dataTable.iSortCols.Add(bindingContext.ValueProvider.GetValue(string.Format("iSortCol_{0}", i)).ConvertToOrDefault<string>(null));
			}
			for (int i = 0; i < dataTable.iSortingCols; i++) {
				string sSortDirString = bindingContext.ValueProvider.GetValue(string.Format("sSortDir_{0}", i)).ConvertToOrDefault<string>("asc");
				dataTable.sSortDirs.Add(sSortDirString == "asc"
					? DataTableSortDirection.Ascending
					: DataTableSortDirection.Descending);
			}

			return dataTable;
		}

		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}
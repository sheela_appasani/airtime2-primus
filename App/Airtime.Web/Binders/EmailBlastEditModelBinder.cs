using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using Airtime.Core;
using Airtime.Models.Emails;
using Airtime.Models.Members;

using Autofac.Integration.Mvc;

using Xtra.Common;


namespace Airtime.Web.Binders
{

	[ModelBinderType(typeof(EmailBlastEditModel))]
	public class EmailBlastEditModelBinder : DefaultModelBinder
	{

		public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
		{
			var blast = base.BindModel(controllerContext, bindingContext) as EmailBlastEditModel;

			var usersParam = bindingContext.ValueProvider.GetValue("UsersId");
			if (usersParam != null) {
				blast.Users = usersParam.AttemptedValue
					.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
					.Select(id => new UserModel { Id = id.ConvertTo<int>() })
					.ToList();
			}

			return blast;
		}

	}

}
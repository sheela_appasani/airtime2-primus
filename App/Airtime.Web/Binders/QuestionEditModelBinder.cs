using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using Airtime.Core;
using Airtime.Models.Media;
using Airtime.Models.Questions;

using Autofac.Integration.Mvc;

using Xtra.Common;


namespace Airtime.Web.Binders
{

	[ModelBinderType(typeof(QuestionEditModel))]
	public class QuestionEditModelBinder : DefaultModelBinder
	{

		public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
		{
			var question = base.BindModel(controllerContext, bindingContext) as QuestionEditModel;

			var songsParam = bindingContext.ValueProvider.GetValue("SongsId");
			if (songsParam != null) {
				question.Songs = songsParam.AttemptedValue
					.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
					.Select(id => new SongModel { Id = id.ConvertTo<int>() })
					.ToList();
			}

			return question;
		}

	}

}
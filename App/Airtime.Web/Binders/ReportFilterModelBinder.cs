using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using Airtime.Core;
using Airtime.Models.Reports;

using Autofac.Integration.Mvc;

using NLog;

using Xtra.Common;


namespace Airtime.Web.Binders
{

	[ModelBinderType(typeof(ReportFilterModel))]
	public class ReportFilterModelBinder : DefaultModelBinder
	{

		public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
		{
			var filter = (ReportFilterModel)base.BindModel(controllerContext, bindingContext);
			
			var surveysParam = bindingContext.ValueProvider.GetValue("FilterSurveys");
			if (surveysParam != null) {
				filter.SelectedSurveyIds = surveysParam.AttemptedValue
					.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
					.Select(id => id.ConvertTo<int>())
					.ToList();
			} else {
				filter.SelectedSurveyIds = new List<int>();
			}

			var choicesParam = bindingContext.ValueProvider.GetValue("FilterQuestionChoices");
			if (choicesParam != null) {
				filter.SelectedQuestionChoiceIds = choicesParam.AttemptedValue
					.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
					.Select(id => id.ConvertTo<int>())
					.ToList();
			} else {
				filter.SelectedQuestionChoiceIds = new List<int>();
			}

			return filter;
		}

		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}
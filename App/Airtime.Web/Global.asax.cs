﻿using System;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

using Airtime.Core;
using Airtime.Core.Framework.Security;
using Airtime.Data.Server;
using Airtime.Web.App_Start;

using Autofac;

using Elmah;

using NLog;



namespace Airtime.Web
{

	public class MvcApplication : HttpApplication
	{

		public static IContainer Container { get; private set; }


		protected void Application_Start()
		{
			Log.Info("Application starting");

			//Register areas and routes (the areas must be registered first!!!)
			AreaRegistration.RegisterAllAreas();
			RouteConfig.Register(RouteTable.Routes);

			//Create the Autofac IoC Container
			Container = new ContainerBuilder().Build();

			//Setup the application
			//Note: make sure that AutofacConfig is always setup before any of the others
			AutofacConfig.Register(Container);
			FilterConfig.Register(Container);
			MvcConfig.Register(Container);
			WebApiConfig.Register(Container, GlobalConfiguration.Configuration);
			SignalRConfig.Register(Container);
			BundleConfig.Register(BundleTable.Bundles);
			AuthConfig.Register();
		}


		protected void Application_AuthorizeRequest()
		{
			try {
				var context = HttpContext.Current;
				var authHeader = context.Request.Headers["X-Authorization"];
				var authCookie = context.Request.Cookies[FormsAuthentication.FormsCookieName];

				//Authorization by-HTTP-header takes precedence over by-cookie
				string encryptedTicket;
				if (!String.IsNullOrWhiteSpace(authHeader)) {
					encryptedTicket = authHeader.Trim();
				} else if (authCookie != null) {
					encryptedTicket = authCookie.Value;
				} else {
					encryptedTicket = null;
				}

				if (!String.IsNullOrEmpty(encryptedTicket)) {
					var authTicket = FormsAuthentication.Decrypt(encryptedTicket);
					if (authTicket != null && !authTicket.Expired) {
						var authData = DeserializeAuthenticationData(authTicket);
						var identity = new AirtimeIdentity(authTicket.Name, authData.PersonId, authData.MembershipId);
						var principal = new AirtimePrincipal(identity, new[] {authData.Role});
						Thread.CurrentPrincipal = principal;
						context.User = principal;
					}
				}
			} catch (Exception ex) {
				Log.Error(ex.Message + "\n" + ex.StackTrace, ex);
				ErrorLog.GetDefault(HttpContext.Current).Log(new Error(ex));
			}
		}


		private static AuthenticationData DeserializeAuthenticationData(FormsAuthenticationTicket ticket)
		{
			var splitData = ticket.UserData.Split(',');
			switch (ticket.Version) {
				case 1:
					return new AuthenticationData {
						MembershipId = Convert.ToInt32(splitData[0]),
						Role = splitData[1]
					};
				case 2:
					return new AuthenticationData {
						PersonId = splitData[0] == "" ? (int?)null : Convert.ToInt32(splitData[0]),
						MembershipId = splitData[1] == "" ? (int?)null : Convert.ToInt32(splitData[1]),
						Role = splitData[2]
					};
			}
			throw new Exception("Unknown authentication ticket version " + ticket.Version);
		}


		private static void AppDomainNotifierOnStop(object sender, AppDomainEventArgs e)
		{
			Log.Info("Application shutting down" + (e.StopImmediately ? " immediately" : ""));
		}


		protected readonly static AppDomainNotifier AppDomainNotifier = new AppDomainNotifier(AppDomainNotifierOnStop);

		protected readonly static Logger Log = LogManager.GetCurrentClassLogger();

	}

}

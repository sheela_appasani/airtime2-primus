﻿function MessageTheBossController($scope, $http) {
	var postUrl;

	$scope.isSending = false;
	$scope.alerts = [];
	$scope.emptyModel = { message: "" };

	$scope.init = function (url) {
		postUrl = url;
		$scope.reset();
	};

	$scope.reset = function () {
		$scope.model = angular.copy($scope.emptyModel);
	};

	$scope.send = function (model) {
		$scope.isSending = true;
		$http.post(postUrl, { message: model.message })
			.success(function() {
				$scope.reset();
				$scope.isSending = false;
				$scope.alerts.push({ type:"alert-success", msg: "Your message has been successfully sent!" });
			})
			.error(function() {
				$scope.isSending = false;
				$scope.alerts.push({ type:"alert-error", msg: "There was a problem sending your message, please try again later!" });
			});
	};
	
	$scope.closeAlert = function(index) {
		$scope.alerts.splice(index, 1);
	};

	$scope.isEmpty = function (model) {
		return !model.message.length;
	};
}

﻿function ReferFriendsController($scope, $http) {
	var postUrl;

	$scope.isSending = false;
	$scope.alerts = [];
	$scope.emptyModel = { emails: [{ value: "" }, { value: "" }, { value: "" }, { value: "" }, { value: "" }] };

	$scope.init = function (url) {
		postUrl = url;
		$scope.reset();
	};

	$scope.reset = function () {
		$scope.model = angular.copy($scope.emptyModel);
	};

	$scope.send = function (model) {
		$scope.isSending = true;
		var values = [];
		for (var i = 0; i < model.emails.length; i++) {
			var email = model.emails[i].value;
			if (email.length) {
				values.push(email);
			}
		}
		$http.post(postUrl, { emails: values })
			.success(function () {
				$scope.reset();
				$scope.isSending = false;
				$scope.alerts.push({ type: "alert-success", msg: "The emails have been successfully sent!" });
			})
			.error(function () {
				$scope.isSending = false;
				$scope.alerts.push({ type: "alert-error", msg: "There was a problem sending the emails, please try again later!" });
			});
	};

	$scope.closeAlert = function (index) {
		$scope.alerts.splice(index, 1);
	};

	$scope.isEmpty = function (model) {
		for (var i = 0; i < model.emails.length; i++) {
			if (model.emails[i].value.length) {
				return false;
			}
		}
		return true;
	};

}

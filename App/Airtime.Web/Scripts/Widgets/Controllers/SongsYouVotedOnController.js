﻿function SongsYouVotedOnController($scope, $http, $filter) {
	var postUrl;
	$scope.items = [];
	$scope.init = function (url) {
		postUrl = url;
		$scope.update();
	};
	$scope.update = function () {
		$http.post(postUrl).success(function (data, status, headers, config) {
			$scope.items = data;
			$scope.search();
		});
	};


	$scope.reverse = false;
	$scope.filteredItems = [];
	$scope.groupedItems = [];
	$scope.itemsPerPage = 10;
	$scope.pagedItems = [];
	$scope.currentPage = 0;

	var searchMatch = function (haystack, needle) {
		if (!needle) {
			return true;
		}
		return haystack.toString().toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};

	$scope.search = function () {
		$scope.filteredItems = $filter('filter')($scope.items, function (item) {
			for (var attr in item) {
				if (searchMatch(item[attr], $scope.query)) {
					return true;
				}
			}
			return false;
		});
		$scope.currentPage = 0;
		$scope.groupToPages();
	};

	$scope.groupToPages = function () {
		$scope.pagedItems = [];
		for (var i = 0; i < $scope.filteredItems.length; i++) {
			if (i % $scope.itemsPerPage === 0) {
				$scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [$scope.filteredItems[i]];
			} else {
				$scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
			}
		}
	};

	$scope.range = function (start, end) {
		var ret = [];
		if (!end) {
			end = start;
			start = 0;
		}
		for (var i = start; i < end; i++) {
			ret.push(i);
		}
		return ret;
	};

	$scope.prevPage = function () {
		if ($scope.currentPage > 0) {
			$scope.currentPage--;
		}
	};

	$scope.nextPage = function () {
		if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
		}
	};

	$scope.setPage = function () {
		$scope.currentPage = this.n;
	};
}

SongsYouVotedOnController.$inject = ['$scope', '$http', '$filter'];

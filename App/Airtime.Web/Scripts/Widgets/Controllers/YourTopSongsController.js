﻿function YourTopSongsController($scope, $http) {
	var postUrl;
	$scope.songs = [];
	$scope.init = function (url) {
		postUrl = url;
		$scope.update();
	};
	$scope.update = function () {
		$http.post(postUrl).success(function (data, status, headers, config) {
			$scope.songs = data;
		});
	};
}

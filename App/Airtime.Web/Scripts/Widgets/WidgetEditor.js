﻿function BindWidgetEditor(selector, saveUrl, resources) {
	var widgetDiv = $(selector);
	var widgetEditorHeading = $(".widgetEditorHeading", widgetDiv);
	var widgetEditorContent = $(".widgetEditorContent", widgetDiv);

	var editorDialog = $(".widgetEditor", widgetDiv).dialog({
		title: resources.EditWidget,
		appendTo: widgetDiv,
		autoOpen: false,
		modal: true,
		width: "800px",
		minWidth: 200,
		minHeight: 180,
		buttons: [
			{
				"data-action": "close",
				text: resources.Cancel,
				click: function () { $(this).dialog("close"); }
			},
			{
				"data-action": "ok",
				text: resources.Save,
				click: function () {
					$.ajax({
						type: "POST",
						url: saveUrl,
						traditional: true,
						data: { heading: widgetEditorHeading.val(), content: widgetEditorContent.val() },
						success: function() {
							window.location.href = window.location.href;
						}
					});
					$(this).dialog("close");
				}
			}
		]
	});

	widgetEditorContent.ckeditor({
		language: resources.Language,
		contentsLangDirection: resources.LanguageDirection,
		customConfig: "/Scripts/ckeditor-plugins/config2.js",
		height: "300px"
	});

	$(".widgetEditorButton", widgetDiv).click(function () {
		editorDialog.dialog("open");
	});
}

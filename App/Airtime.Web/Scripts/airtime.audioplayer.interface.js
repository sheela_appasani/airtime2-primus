﻿!function ($) {
	$(function () {
		var PLAY_ICON = "/Content/themes/airtime/images/icons/icon-play-button.gif";
		var STOP_ICON = "/Content/themes/airtime/images/icons/icon-stop-button.gif";

		$("#audioPlayerContainer").audioPlayer({
			OnPlayerLoaded: function () {
				var handler = this;

				$(document).on("click", ".playButton", function (event) {
					var songUrl = $(this).data("songUrl").replace("~/", "/");
					var soundInfo = handler.GetSoundInfo(songUrl);
					if (soundInfo && soundInfo.button == this) {
						//Stop playing selected song
						handler.Stop(songUrl);
						$(this).attr("src", PLAY_ICON);

					} else {
						//Stop any currently playing songs
						var stopped = handler.Stop();
						for (var url in stopped) {
							$(stopped[url].button).attr("src", PLAY_ICON);
						}
						//Play selected song
						handler.Play(songUrl, { url: songUrl, button: this });
						$(this).attr("src", STOP_ICON);
					}

					return false;
				});
			},

			OnSoundError: function (soundKey, event) {
				var song = this.GetSoundInfo(soundKey);
				if (song && song.button) {
					$(song.button).attr("src", PLAY_ICON);
				}
				setTimeout(function () {
					alert(event.text + ": Could not load \"" + soundKey + "\"");
				}, 0);
			},

			OnSoundComplete: function (soundKey, event) {
				var song = this.GetSoundInfo(soundKey);
				if (song && song.button) {
					$(song.button).attr("src", PLAY_ICON);
				}
			}
		});
	});
}(window.jQuery);

﻿//These have to be global (Window object) for some reason otherwise Flash is unable to call them
function OnPlayerLoaded(context) { return $.audioPlayer.flashEventRelay(arguments); }
function OnSoundLoad(context) { return $.audioPlayer.flashEventRelay(arguments); }
function OnSoundLoaded(context, event) { return $.audioPlayer.flashEventRelay(arguments); }
function OnSoundError(context, event) { return $.audioPlayer.flashEventRelay(arguments); }
function OnSoundProgress(context, event) { return $.audioPlayer.flashEventRelay(arguments); }
function OnSoundID3(context, event) { return $.audioPlayer.flashEventRelay(arguments); }
function OnSoundComplete(context, event) { return $.audioPlayer.flashEventRelay(arguments); }

(function ($) {

	$.audioPlayer = {
		players: {},
		flashEventRelay: function(args) {
			var args = Array.prototype.slice.call(args, 0);
			var ctx = args[0];
			args[0] = ctx.soundKey;
			var handler = $.audioPlayer.players[ctx.playerID];
			return (typeof handler !== "undefined")
				? (handler.events[$.audioPlayer.flashEventRelay.caller] || function() { }).apply(handler, args)
				: undefined;
		}
	};

	$.fn.audioPlayer = function (playerOptions) {
		return this.each(function () {

			var container = this;
			var playlist = {};
			var handler = {
				flash: null,
				options: playerOptions,
				events: {}
			};

			$(this).data("audioPlayer", handler);


			function Initialise() {
				$(container).flash({
					swf: "/Flash/AudioPlayer.swf",
					expressInstaller: "/Flash/expressInstall.swf",
					quality: "high",
					allowScriptAccess: "always",
					allowNetworking: "all",
					allowFullScreen: false,
					height: 1,
					width: 1,
					hasVersion: 9,
					flashvars: {
						OnPlayerLoaded: "OnPlayerLoaded",
						OnSoundLoad: "OnSoundLoad",
						OnSoundLoaded: "OnSoundLoaded",
						OnSoundError: "OnSoundError",
						OnSoundProgress: "OnSoundProgress",
						OnSoundID3: "OnSoundID3",
						OnSoundComplete: "OnSoundComplete"
					}
				});

				$.audioPlayer.players[container.firstChild.id] = handler;
			}


			handler.IsReady = function() {
				return handler.flash != null;
			};


			handler.Play = function(url, soundInfo) {
				if (!handler.IsReady()) {
					throw new Error("audioPlayer.Play failed: player is not ready");
				}
				handler.flash.startSound(url);
				playlist[url] = soundInfo || null;
			};


			handler.Stop = function(urls) {
				if (!handler.IsReady()) {
					throw new Error("audioPlayer.Stop failed: player is not ready");
				}

				var stopped = {};

				if (typeof urls === "undefined") {
					//Stop all currently playing songs
					for (var url in playlist) {
						stopped[url] = playlist[url];
						delete playlist[url];
						handler.flash.unloadSound(url);
					}

				} else if (urls.constructor === Array) {
					//Stop all listed songs
					for (var i = 0; i < urls.length; i++) {
						var url = urls[i];
						stopped[url] = playlist[url];
						delete playlist[url];
					}
					handler.flash.unloadSound(urls);

				} else if (urls.constructor === String) {
					//Stop the specified song
					stopped[urls] = playlist[urls];
					delete playlist[urls];
					handler.flash.unloadSound(urls);
				}

				return stopped;
			};


			handler.GetSoundInfo = function(url) {
				return (typeof url === "undefined") ? playlist : playlist[url];
			};


			handler.events[window.OnPlayerLoaded] = function () {
				handler.flash = container.firstChild;
				if (typeof handler.options.OnPlayerLoaded === "function") {
					handler.options.OnPlayerLoaded.apply(handler, arguments);
				}
			};


			handler.events[window.OnSoundLoad] = function(soundKey) {
				if (typeof handler.options.OnSoundLoad === "function") {
					handler.options.OnSoundLoad.apply(handler, arguments);
				}
			};


			handler.events[window.OnSoundLoaded] = function(soundKey, event) {
				if (typeof handler.options.OnSoundLoaded === "function") {
					handler.options.OnSoundLoaded.apply(handler, arguments);
				}
			};


			handler.events[window.OnSoundError] = function(soundKey, event) {
				//TODO: silently collect/log errors here, so they can be sent back to the server via a plugin class
				if (typeof handler.options.OnSoundError === "function") {
					handler.options.OnSoundError.apply(handler, arguments);
				}

				delete playlist[soundKey];

				//Unload sound object so it can be garbage-collected
				setTimeout(function() {
					handler.flash.unloadSound(soundKey);
				}, 0);
			};


			handler.events[window.OnSoundProgress] = function(soundKey, event) {
				if (typeof handler.options.OnSoundProgress === "function") {
					handler.options.OnSoundProgress.apply(handler, arguments);
				}
			};


			handler.events[window.OnSoundID3] = function(soundKey, event) {
				if (typeof handler.options.OnSoundID3 === "function") {
					handler.options.OnSoundID3.apply(handler, arguments);
				}
			};


			handler.events[window.OnSoundComplete] = function(soundKey, event) {
				if (typeof handler.options.OnSoundComplete === "function") {
					handler.options.OnSoundComplete.apply(handler, arguments);
				}

				delete playlist[soundKey];

				//Unload sound object so it can be garbage-collected
				setTimeout(function() {
					handler.flash.unloadSound(soundKey);
				}, 0);
			};


			Initialise();

		});
	};

})(jQuery);
﻿function logError(ex, stack) {
	if (typeof logErrorUrl == "undefined" || logErrorUrl == null) {
		return;
	}

	if (typeof ex == "undefined" || ex == null) {
		return;
	}

	var url = ex.fileName != null ? ex.fileName : document.location;
	if (stack == null && ex.stack != null) {
		stack = ex.stack;
	}

	var out = (ex.message != null ? ex.name + ": " + ex.message : ex) + "\n   in document path '" + url + "'";

	if (stack != null) {
		out += "\n   at " + stack.join("\n   at ");
	}

	var token = '';
	try {
		token = $('input[name=__RequestVerificationToken]').val();
	} catch (err) { }

	$.ajax({
		type: 'POST',
		url: logErrorUrl,
		data: {
			__RequestVerificationToken: token,
			message: out
		}
	});
}

Function.prototype.trace = function() {
	var trace = [];
	var current = this;
	while (current) {
		trace.push(current.signature());
		current = current.caller;
	}
	return trace;
};

Function.prototype.signature = function() {
	var signature = {
		name: this.getName(),
		params: [],
		toString: function() {
			var params = this.params.length > 0 ? "'" + this.params.join("', '") + "'" : "";
			return this.name + "(" + params + ")";
		}
	};
	if (this.arguments) {
		for (var x = 0; x < this.arguments.length; x++) {
			signature.params.push(this.arguments[x]);
		}
	}
	return signature;
};

Function.prototype.getName = function() {
	if (this.name) {
		return this.name;
	}
	var definition = this.toString().split("\n")[0];
	var exp = /^function ([^\s(]+).+/;
	if (exp.test(definition)) {
		return definition.split("\n")[0].replace(exp, "$1") || "anonymous";
	}
	return "anonymous";
};

window.onerror = function(msg, url, line) {
	if (msg != "Error loading script") {
		logError(msg, arguments.callee.trace());
	}
};

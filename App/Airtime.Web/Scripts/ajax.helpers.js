﻿function JsonDate(aspNetDate) {
	return new Date(parseInt(aspNetDate.substr(6), 10));
}


function JsonDateRenderer(obj) {
	return JsonDate(obj.aData[obj.iDataColumn]).format("dd/mm/yyyy HH:MM").toUpperCase();
	//return JsonDate(obj.aData[obj.iDataColumn]).format("ddmmmyyyy HH:MM").toUpperCase();
}


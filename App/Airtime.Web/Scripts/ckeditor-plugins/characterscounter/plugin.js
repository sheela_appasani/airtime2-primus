﻿(function () {
	CKEDITOR.plugins.charactersCounter = {
	};

	var plugin = CKEDITOR.plugins.charactersCounter;

	/**
    * Shows count in the DIV element created via setTimeout()
    * 
    * @param obj CKEditor Editor Object
    */
	function ShowCharactersCount(evt) {
		var editor = evt.editor;
		if ($('div#cke_characterscount_' + editor.name).length > 0) { // Check element exists
			// Because CKEditor uses Javascript to load parts of the editor, some of its elements are not immediately available in the DOM
			// Therefore, I use setTimeout.  There may be a better way of doing this.
			setTimeout(function () {
				try {
					$('div#cke_characterscount_' + editor.name).html(FormatString(editor.lang.charactersCounter.Length, editor.getData().length));
				} catch (e) {
				}
			}, 100);
		}
	}

	/**
    * Takes the given HTML data, replaces all its HTML tags with nothing, splits the result by spaces, 
    * and outputs the array length i.e. number of words.
    * 
    * @param string htmlData HTML Data
    * @return int Count
    */
	function GetCharactersCount(htmlData) {
		return htmlData.replace(/<(?:.|\s)*?>/g, '').length;
	}

	function GetHtmlCharactersCount(htmlData) {
		return htmlData.length;
	}

	function FormatString() {
		var args = Array.prototype.slice.call(arguments, 0);
		var str = args.shift();
		return str.replace(/\{\{|\}\}|\{(\d+)\}/g, function (m, n) {
			if (m === "{{") { return "{"; }
			if (m === "}}") { return "}"; }
			return args[n];
		});
	}

	/**
    * Adds the plugin to CKEditor
    */
	CKEDITOR.plugins.add('charactersCounter', {
		lang: ['en', 'ar', 'es', 'yo', 'zh-cn'],
		init: function (editor) {
			if (CKEDITOR.config.charactersCounter_autocount) {
				setTimeout(function () {
					$('td#cke_bottom_' + editor.name)
						.append('<div id="cke_characterscount_' + editor.name + '" style="display: inline-block; float: right; text-align: right; margin-top: 5px; white-space:nowrap; width:auto;">'
							+ FormatString(editor.lang.charactersCounter.Length, editor.getData().length)
							+ '</div>');
				}, 4000);

				editor.on('key', ShowCharactersCount);
			}

			editor.ui.addButton('CountSelection', {
				label: editor.lang.charactersCounter.CountSelection,
				command: 'countselection',
				icon: this.path + 'images/button.png'
			});
			editor.addCommand('countselection', {
				exec: function (editor) {
					var data = editor.getData();
					alert(FormatString(editor.lang.charactersCounter.TotalSizes, GetHtmlCharactersCount(data), GetCharactersCount(data), GetCharactersCount(editor.getSelection().getSelectedText())));
				},
				canUndo: false    // No support for undo/redo
			});
		}
	});

})();

// Plugin options
CKEDITOR.config.charactersCounter_autocount = true;
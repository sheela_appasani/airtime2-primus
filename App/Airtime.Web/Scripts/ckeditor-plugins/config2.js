﻿(function() {
	var basePath = CKEDITOR.basePath;
	basePath = basePath.substr(0, basePath.indexOf("ckeditor/"));

	CKEDITOR.plugins.addExternal("templateTags", basePath + "ckeditor-plugins/templateTags/", "plugin.js");
	CKEDITOR.plugins.addExternal("charactersCounter", basePath + "ckeditor-plugins/charactersCounter/", "plugin.js");

	CKEDITOR.editorConfig = function(config) {
		config.toolbar = 'AirtimeDefault';
		config.extraPlugins = 'templateTags,charactersCounter';
		config.toolbar_AirtimeDefault = [
			{ name: 'document', items: ['Source', '-', 'Preview', 'Print', '-', 'Templates'] },
			{ name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
			{ name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt', 'CountSelection'] },
			{ name: 'airtime', items: ['TemplateTags'] },
			'/',
			{ name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', '-', 'RemoveFormat'] },
			{ name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'] },
			{ name: 'links', items: ['Link', 'Unlink'] },
			{ name: 'insert', items: ['Image', 'Table', 'Smiley'] },
			{ name: 'styles', items: ['Format', 'Font', 'FontSize'] },
			{ name: 'colors', items: ['TextColor'] },
			{ name: 'tools', items: ['Maximize', '-', 'About'] }
		];
	};

})();

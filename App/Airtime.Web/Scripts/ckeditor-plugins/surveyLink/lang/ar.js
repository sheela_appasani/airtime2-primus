﻿CKEDITOR.plugins.setLang('surveyLink', 'ar', {
surveyLink: {
	InsertLoginButton: "إدراج زر تسجيل الدخول",
	TakeSurveyImageButton: "أخذ صورة زر المسح",
	Settings: "إعدادات",
	ThisDialogInsertsSurveyImageButton: "هذا إطار مربع حوار يتيح لك إدراج زر صورة التي يمكن انقر فوق أعضاء تؤخذ مباشرة في تلك الدراسة الاستقصائية.",
	ImageURL: "عنوان URL للصورة",
	ImageURLRequired: "عنوان URL للصورة للزر المطلوب.",
	ClickHereToLoginAndTakeTheSurvey: "انقر هنا لتسجيل الدخول، وتأخذ الدراسة",
}});

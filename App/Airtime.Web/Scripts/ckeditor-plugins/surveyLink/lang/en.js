﻿CKEDITOR.plugins.setLang('surveyLink', 'en', {
surveyLink: {
	InsertLoginButton: "Insert Login Button",
	TakeSurveyImageButton: "Take Survey Image-Button",
	Settings: "Settings",
	ThisDialogInsertsSurveyImageButton: "This dialog window lets you insert an image-button that members can click to be taken directly into their survey.",
	ImageURL: "Image URL",
	ImageURLRequired: "The URL of the image for the button is required.",
	ClickHereToLoginAndTakeTheSurvey: "Click here to login and take the survey"
}});

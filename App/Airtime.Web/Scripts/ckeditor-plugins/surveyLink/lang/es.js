﻿CKEDITOR.plugins.setLang('surveyLink', 'es', {
surveyLink: {
	InsertLoginButton: "Inserte el botón de inicio de sesión",
	TakeSurveyImageButton: "Tomar botón de encuesta de imagen",
	Settings: "Configuración",
	ThisDialogInsertsSurveyImageButton: "Esta ventana de diálogo le permite insertar un botón de imagen que los miembros pueden hacer clic para tomarse directamente en su encuesta.",
	ImageURL: "URL de la imagen",
	ImageURLRequired: "Se requiere la dirección URL de la imagen para el botón.",
	ClickHereToLoginAndTakeTheSurvey: "Haga clic aquí para iniciar sesión y realizar la encuesta"
}});

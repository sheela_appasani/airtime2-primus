﻿CKEDITOR.plugins.setLang('surveyLink', 'zh-cn', {
surveyLink: {
	InsertLoginButton: "插入按钮登录",
	TakeSurveyImageButton: "采取调查图像按钮",
	Settings: "设置",
	ThisDialogInsertsSurveyImageButton: "此对话框窗口允许您插入图像按钮，成员可以单击将采取直接进入他们的调查。",
	ImageURL: "图像 URL",
	ImageURLRequired: "需要为该按钮的图像的 URL。",
	ClickHereToLoginAndTakeTheSurvey: "单击此处登录并采取调查",
}});

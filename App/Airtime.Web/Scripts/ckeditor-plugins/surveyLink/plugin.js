﻿CKEDITOR.plugins.add('surveyLink', {
	lang: ['en', 'ar', 'es', 'yo', 'zh-cn'],
	init: function (editor) {
		var config = editor.config;
		var lang = editor.lang.format;

		editor.addCommand('surveyLinkDialog', new CKEDITOR.dialogCommand('surveyLinkDialog'));

		editor.ui.addButton('SurveyLink', {
			label: editor.lang.surveyLink.InsertLoginButton,
			command: 'surveyLinkDialog'
		});

		CKEDITOR.dialog.add('surveyLinkDialog', function(editor) {
			return {
				title: editor.lang.surveyLink.TakeSurveyImageButton,
				minWidth: 400,
				minHeight: 100,
				contents: [{
					id: 'general',
					label: editor.lang.surveyLink.Settings,
					elements: [
						{
							type: 'html',
							html: editor.lang.surveyLink.ThisDialogInsertsSurveyImageButton
						},
						{
							type: 'text',
							id: 'url',
							label: editor.lang.surveyLink.ImageURL,
							validate: CKEDITOR.dialog.validate.notEmpty(editor.lang.surveyLink.ImageURLRequired),
							required: true,
							commit: function(data) {
								data.url = this.getValue();
							},
							setup: function(element) {
								this.setValue(config.loginImageButtonUrl);
							}
						}
					]
				}],
				onOk: function() {
					var dialog = this;
					var data = {};
					this.commitContent(data);

					var link = editor.document.createElement('a');
					link.setAttribute('href', "[[TokenURL]]");
					link.setAttribute('target', '_blank');

					link.appendHtml("<img src='" + data.url + "' alt='" + editor.lang.surveyLink.ClickHereToLoginAndTakeTheSurvey + "' />");

					editor.insertElement(link);
				},
				onShow: function() {
					var elem = this.getParentEditor().getSelection().getSelectedElement();
					this.setupContent(elem);
				}
			};
		});
	}
});


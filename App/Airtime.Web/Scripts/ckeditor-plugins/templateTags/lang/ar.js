﻿CKEDITOR.plugins.setLang('templateTags', 'ar', {
templateTags: {
	GivenName: "الاسم المعطى",
	FamilyName: "اسم العائلة",
	Email: "البريد الإلكتروني",
	TokenLink: "ارتباط رمزي",
	TokenURL: "محدد موقع معلومات الرمز المميز",
	InsertTag: "إدراج العلامة",
	TemplateTags: "نموذج العلامات"
}});

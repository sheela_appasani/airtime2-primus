﻿CKEDITOR.plugins.setLang('templateTags', 'en', {
templateTags: {
	GivenName: "Given Name",
	FamilyName: "Family Name",
	Email: "Email",
	TokenLink: "Token Link",
	TokenURL: "Token URL",
	InsertTag: "Insert Tag",
	TemplateTags: "Template Tags"
}});

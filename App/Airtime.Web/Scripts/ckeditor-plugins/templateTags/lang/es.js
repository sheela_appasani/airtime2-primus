﻿CKEDITOR.plugins.setLang('templateTags', 'es', {
templateTags: {
	GivenName: "Nombre",
	FamilyName: "Nombre de la familia",
	Email: "Correo electrónico",
	TokenLink: "Enlace simbólico",
	TokenURL: "URL de token",
	InsertTag: "Insertar etiqueta",
	TemplateTags: "Etiquetas de plantilla"
}});

﻿CKEDITOR.plugins.setLang('templateTags', 'zh-cn', {
templateTags: {
	GivenName: "给定的名称",
	FamilyName: "系列名称",
	Email: "电子邮件",
	TokenLink: "标记的链接",
	TokenURL: "标记的 URL",
	InsertTag: "插入标记",
	TemplateTags: "模板标签"
}});

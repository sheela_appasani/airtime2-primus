﻿CKEDITOR.plugins.add('templateTags', {
	requires: ['richcombo'],
	lang: ['en', 'ar', 'es', 'yo', 'zh-cn'],
	init: function(editor) {
		var config = editor.config;
		var lang = editor.lang.format;

		var tags = [
			["[[GivenName]]", editor.lang.templateTags.GivenName, editor.lang.templateTags.GivenName],
			["[[FamilyName]]", editor.lang.templateTags.FamilyName, editor.lang.templateTags.FamilyName],
			["[[EmailAddress]]", editor.lang.templateTags.Email, editor.lang.templateTags.Email],
			["[[TokenLink]]", editor.lang.templateTags.TokenLink, editor.lang.templateTags.TokenLink],
			["[[TokenURL]]", editor.lang.templateTags.TokenURL, editor.lang.templateTags.TokenURL]
		];

		editor.ui.addRichCombo("TemplateTags", {
			label: editor.lang.templateTags.InsertTag,
			title: editor.lang.templateTags.InsertTag,
			voiceLabel: editor.lang.templateTags.InsertTag,
			className: 'cke_format',
			multiSelect: false,

			panel: {
				css: [config.contentsCss, CKEDITOR.getUrl(editor.skinPath + 'editor.css')],
				voiceLabel: lang.panelVoiceLabel
			},

			init: function() {
				this.startGroup(editor.lang.templateTags.TemplateTags);
				for (var i = 0; i < tags.length; i++) {
					this.add(tags[i][0], tags[i][1], tags[i][2]);
				}
			},

			onClick: function(value) {
				editor.focus();
				editor.fire('saveSnapshot');
				editor.insertHtml(value);
				editor.fire('saveSnapshot');
			}
		});
	}
});

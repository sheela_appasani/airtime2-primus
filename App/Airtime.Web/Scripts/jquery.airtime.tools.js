(function ($) {

	$.fn.reverse = function () {
		return this.pushStack(this.get().reverse(), arguments);
	};

	$.fn.sort = function () {
		return this.pushStack($.makeArray([].sort.apply(this, arguments)));
	};

	$.fn.replace = function () {
		var stack = [];
		return this.domManip(arguments, true, 1, function (a) {
			this.parentNode.replaceChild(a, this);
			stack.push(a);
		}).pushStack(stack);
	};

	$.fn.dump = function (groupTitle) {
		if ("console" in window && "log" in console) {
			if (groupTitle && typeof console.group == "function") console.group(groupTitle);
			for (var i = 0; i < this.length; i++) {
				console.log("(" + i + ") ", this[i]);
			}
			if (groupTitle && typeof console.groupEnd == "function") console.groupEnd();
		}
		return this;
	};

	$.fn.inArray = function (elem, array) {
		return array.indexOf(elem);
	};

	$.fn.appendHtml = function (content) {
		return this.append($("<div/>").text(content).html());
	};

	$.fn.loadAction = function (url, options) {
		var $this = this;
		if (typeof options.success == "function") {
			var successCallback = options.success;
			delete options.success;
		}
		if (typeof options.error == "function") {
			var errorCallback = options.error;
			delete options.error;
		}
		var settings = $.extend({}, {
			url: url,
			timeout: 750,
			cache: false,
			success: function (result) {
				$this.html(result);
				if (typeof successCallback != "undefined") {
					successCallback.apply(this, arguments);
				}
			},
			error: function () {
				if ($this.children(".ajax-error").length == 0) {
					$this.append("<div class='ajax-error'></div>");
				}
				if (typeof errorCallback != "undefined") {
					errorCallback.apply(this, arguments);
				}
			}
		}, options);

		$.ajax(settings);
		return $this;
	};

	$.extend({
		form: function (options) {
			if (!$.template["singleInputTemplate"]) $.template("singleInputTemplate", "<input type='hidden' name='${Key}' value='${Value}' />");
			if (!$.template["multiInputTemplate"]) $.template("multiInputTemplate", "{{each Value}}<input type='hidden' name='${Key}[${$index}]' value='${$value}' />{{/each}}");
			if (!$.template["formInput"]) $.template("formInput", "{{if $.isArray(Value)}}{{tmpl 'multiInputTemplate'}}{{else}}{{tmpl 'singleInputTemplate'}}{{/if}}");
			if (!$.template["formTemplate"]) $.template("formTemplate", "<form action='${action}' method='${method}' target='${target}' style='display:none'>{{tmpl($.Enumerable.From(data).ToArray()) 'formInput'}}</form>");

			var defaults = { method: "post", target: "_self", data: null };
			var settings = $.extend({}, defaults, options);
			return $.tmpl("formTemplate", settings);
		},
		openWindow: function (url, name, options) {
			var allSettings = $.extend({
				scrollbars: "yes",
				resizable: "yes",
				toolbar: "no",
				location: "no",
				status: "yes"
			}, options);
			var settingsArray = [];
			for (var key in allSettings) {
				settingsArray.push(key + "=" + allSettings[key]);
			}
			try {
				var win = window.open(url, name, settingsArray.join(","));
			} catch (e) { }
			if (typeof win == "undefined" || win == null) {
				return null;
			} else {
				try { win.focus(); } catch (e) { }
				return win;
			}
		}
	});

	String.prototype.format = function () {
		var args = arguments;
		return this.replace(/\{\{|\}\}|\{(\d+)\}/g, function (m, n) {
			if (m === "{{") { return "{"; }
			if (m === "}}") { return "}"; }
			return args[n];
		});
	};

})(jQuery);

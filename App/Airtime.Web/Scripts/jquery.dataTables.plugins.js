(function ($) {

	$.fn.airtimeTable = function (tableSettings) {
		var context = this;

		this.Settings = tableSettings;

		this.GetData = function () {
			return context.data("data");
		};

		this.GetRowData = function (tr) {
			var data = context.data("data");
			if (data) {
				return data[context.fnGetPosition(tr)];
			} else {
				return mapRowDataToJson(context.fnGetData(tr));
			}
		};

		this.AddData = function (dataObject) {
			var cols = context.fnSettings().aoColumns;
			var dataArray = [];
			for (var i = 0; i < cols.length; i++) {
				dataArray.push(dataObject[cols[i].sName.split("|")[0]]);
			}
			context.data("data").push(dataObject);
			context.fnAddData(dataArray);
		};

		function stringStartsWith(str, value) {
			return (str.substr(0, value.length) === value);
		}
		
		function mapRowDataToJson(rowData) {
			var cols = context.fnSettings().aoColumns;
			var json = {};
			for (var i = 0; i < cols.length && i < rowData.length; i++) {
				colName = cols[i].sName.split("|")[0] || cols[i].sTitle;
				if (colName) {
					json[colName] = rowData[i];
				}
			}
			return json;
		}

		var columns = [];
		var sorting = [];
		var hiddenColumns = [];
		var columnNames = {};
		for (var i = 0; i < tableSettings.Columns.length; i++) {
			var name = tableSettings.Columns[i].Property || tableSettings.Columns[i].Name || "";
			if (typeof tableSettings.Columns[i].QueryOn === "string") {
				name += "|" + tableSettings.Columns[i].QueryOn;
			}
			if (tableSettings.ServerSide && name.length == 0) {
				tableSettings.Columns[i].Sortable = false;
			}

			columnNames[name] = i;

			if (!!tableSettings.Columns[i].Internal) {
				tableSettings.Columns[i].Visible = false;
			}

			var column = {
				sName: name,
				sTitle: tableSettings.Columns[i].Title,
				sClass: tableSettings.Columns[i].CssClass,
				bSortable: tableSettings.Columns[i].Sortable,
				bSelectable: tableSettings.Columns[i].Selectable,
				bVisible: tableSettings.Columns[i].Visible,
				bInternal: tableSettings.Columns[i].Internal,
				iDataSort: tableSettings.Columns[i].DataSort,
				sType: tableSettings.Columns[i].Type
			};

			if (typeof tableSettings.Columns[i].Render != "undefined") {
				//The Render callback takes precedence over template rendering
				var renderer = tableSettings.Columns[i].Render;
				column.fnRender = function (obj) {
					return renderer(context.data("data")[obj.iDataRow], this);
				};

			} else if (typeof tableSettings.Columns[i].Template != "undefined") {
				//Render using a JQuery Template
				$.template("templateColumn" + i, "<template>" + tableSettings.Columns[i].Template + "</template>");
				column.fnRender = function (obj) {
					return $.tmpl("templateColumn" + obj.iDataColumn, context.data("data")[obj.iDataRow]).html();
				};
			}

			if (column.bSelectable || column.bInternal) {
				hiddenColumns.push(i);
			}
			columns.push(column);

			if (typeof tableSettings.Columns[i].DefaultSort === "string") {
				sorting.push([i, tableSettings.Columns[i].DefaultSort]);
			}
		}

		for (var i = 0; i < columns.length; i++) {
			var col = columns[i];
			if (typeof (col.iDataSort) == "string") {
				col.iDataSort = columnNames[col.iDataSort];
			}
		}

		if (typeof tableSettings.RowCallback != "undefined") {
			var rowCallback = function (row, data, displayIndex, displayIndexFull) {
				return tableSettings.RowCallback(context.data("data")[displayIndex], row);
			};
		}

		var settings = {
			oLanguage: {},
			sAjaxSource: tableSettings.AjaxSource,
			aoColumns: columns,
			aaSorting: sorting,
			bAutoWidth: false,
			bProcessing: typeof (tableSettings.Processing) != "undefined" ? tableSettings.Processing : true,
			bServerSide: typeof (tableSettings.ServerSide) != "undefined" ? tableSettings.ServerSide : true,
			bInfo: typeof (tableSettings.Info) != "undefined" ? tableSettings.Info : true,
			bFilter: typeof (tableSettings.Filter) != "undefined" ? tableSettings.Filter : true,
			bStateSave: typeof (tableSettings.StateSave) != "undefined" ? tableSettings.StateSave : true,
			bJQueryUI: typeof (tableSettings.JQueryUI) != "undefined" ? tableSettings.JQueryUI : true,
			bPaginate: typeof (tableSettings.Paginate) != "undefined" ? tableSettings.Paginate : true,
			sPaginationType: typeof (tableSettings.PaginationType) != "undefined" ? tableSettings.PaginationType : "full_numbers",
			iDisplayLength: typeof (tableSettings.DisplayLength) != "undefined" ? tableSettings.DisplayLength : 25,
			aLengthMenu: typeof (tableSettings.LengthMenu) != "undefined" ? tableSettings.LengthMenu : [5, 15, 25, 50, 100],
			sDom: typeof (tableSettings.Dom) != "undefined" ? tableSettings.Dom : 'T<"clear">r<"H"flCr>t<"F"ip>',
			sScrollX: typeof (tableSettings.ScrollX) != "undefined" ? tableSettings.ScrollX : "",
			sScrollY: typeof (tableSettings.ScrollY) != "undefined" ? tableSettings.ScrollY : "",
			oColVis: {
				aiExclude: hiddenColumns,
				bRestore: true,
				sAlign: "right",
				iOverlayFade: 200,
				buttonText: tableSettings.Language.oColVis.sButtonText,
				sRestore: tableSettings.Language.oColVis.sRestore,
				sShowAll: tableSettings.Language.oColVis.sShowAll
			},
			oTableTools: tableSettings.TableTools,
			fnDrawCallback: tableSettings.DrawCallback,
			fnRowCallback: rowCallback,
			fnStateSave: function (oSettings, oData) {
				if (airtimeStorage) {
					airtimeStorage.set("DataTables_" + window.location.pathname, JSON.stringify(oData));
				}
			},
			fnStateLoad: function (oSettings) {
				if (airtimeStorage) {
					var data = airtimeStorage.get('DataTables_' + window.location.pathname);
					return data == null ? null : JSON.parse(data);
				}
			},
			fnServerData: typeof (tableSettings.AjaxSource) == "undefined" ? undefined : function (sSource, aoData, fnCallback) {
				var cols = this.fnSettings().aoColumns;
				var simpleMap = {};
				for (var i = 0; i < aoData.length; i++) {
					var entry = aoData[i];
					if (stringStartsWith(entry.name, "iSortCol_")) {
						var column = cols[parseInt(entry.value, 10)];
						var columnParts = column.sName.split("|");
						simpleMap[entry.name] = columnParts[1] || columnParts[0];
					} else {
						simpleMap[entry.name] = entry.value;
					}
				}

				$.extend(simpleMap, tableSettings.AjaxData);
				if (tableSettings.MergeAjaxFieldsCallback) {
					$.extend(simpleMap, tableSettings.MergeAjaxFieldsCallback());
				}
				
				var sEcho = simpleMap["sEcho"];

				$.ajax({
					type: "POST",
					dataType: "json",
					url: sSource,
					data: simpleMap,
					success: function (data) {
						var json = { sEcho: sEcho, aaData: [] };
						json.iTotalRecords = data.totalRecords;
						json.iTotalDisplayRecords = data.totalRecords;
						context.data("data", data.list);
						$(data.list).each(function () {
							var rowData = [];
							for (var i = 0; i < cols.length; i++) {
								var colParts = cols[i].sName.split("|");
								rowData.push(this[colParts[0]]);
							}
							json.aaData.push(rowData);
						});
						fnCallback(json);
						
						if (tableSettings.PostAjaxCallback) {
							tableSettings.PostAjaxCallback();
						}
					}
				});
				
			}
		};

		$.extend(settings.oLanguage, tableSettings.Language);

		var result = context.selectableDataTable(settings).fnSetFilteringDelay(300);

		if (typeof tableSettings.DoubleClick === "function") {
			$(result).on("dblclick", "tbody tr", tableSettings.DoubleClick);
		}

		if (typeof tableSettings.FilterPlaceholder == "string") {
			$(".dataTables_filter input[type='text']", result.closest(".dataTables_wrapper")).attr("placeholder", tableSettings.FilterPlaceholder);
		}

		return result;
	};



	//------------------------------------------------------------------------------
	// Implement functionality for a selectable (checkboxes) column in a dataTable.
	//------------------------------------------------------------------------------
	$.fn.selectableDataTable = function (settings) {
		var dt = this;

		function dtSelectRow(rowElement, select) {
			var row = $(rowElement);
			if (typeof (select) == "undefined") {
				select = !row.hasClass("row_selected");
			}
			row[select ? "addClass" : "removeClass"]("row_selected");
			row.find('input[type="checkbox"]').attr('checked', select);
		}

		// We filter out input controls which are not checkboxes, so they can be used
		// in the datatable without causing row selection when clicked on.
		function dtOnRowClick(dataTable, event) {
			var target = event.target;
			if (typeof (target.type) == "undefined" || target.type == "checkbox") {
				dtSelectRow($(event.currentTarget).closest("tr,th"));
				if (dataTable.fnSelectionChangedCallback) {
					dataTable.fnSelectionChangedCallback(this);
				}
			}
		}

		function dtSelectAll(dataTable, event) {
			var target = event.target;
			if (typeof (target.type) == "undefined" || target.type == "checkbox") {
				var input = $(event.currentTarget).find('input[type="checkbox"]').get(0);
				if (input !== event.target) {
					input.checked = !input.checked;
				}
				$(input)[input.checked ? "addClass" : "removeClass"]("selected");
				$(dataTable.fnGetNodes()).each(function (i, node) {
					dtSelectRow(node, input.checked);
				});
				if (dataTable.fnSelectionChangedCallback) {
					dataTable.fnSelectionChangedCallback(this);
				}
			}
		}

		function dtGetSelected(dataTable) {
			// XXX I thought one of the following jQuery selection methods should work, but they don't:
			//   $('.row_selected', dataTable.fnGetNodes())
			//   $(dataTable.fnGetNodes()).find('.row_selected')
			var selected = [];
			var rows = dataTable.fnGetNodes();
			for (var i = 0; i < rows.length; ++i) {
				if ($(rows[i]).hasClass('row_selected')) {
					selected.push(rows[i]);
				}
			}
			return selected;
		}

		function dtGetSelectedIds(dataTable) {
			var selected = dtGetSelected(dataTable);
			var ids = [];
			for (var i = 0; i < selected.length; ++i) {
				ids[i] = selected[i].id;
			}
			return ids;
		}

		for (var i = 0; i < settings.aoColumns.length; i++) {
			var column = settings.aoColumns[i];
			if (column.bSelectable) {
				column.sTitle = '<input type="checkbox" title="' + (settings.aoColumns[i].sTitle || "Select All") + '" />';
				column.sClass = $.trim((settings.aoColumns[i].sClass || "") + " selectableColumn");
				column.fnRender = function (oObj) {
					return '<input type="checkbox" value="' + oObj.aData[oObj.iDataColumn] + '" name="' + (oObj.oSettings.aoColumns[oObj.iDataColumn].sName || "dataTables") + 'Selection" />';
				};
			}
		}

		var fnNewRowCallback = settings.fnRowCallback;
		settings.fnRowCallback = function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			$("td.selectableColumn", nRow).click(function (event) { dtOnRowClick(dt, event); });
			if (typeof fnNewRowCallback === "function") {
				return fnNewRowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull);
			}
			return nRow;
		};

		dt = this.dataTable(settings).addClass("selectableTable");

		this.fnSelectionChangedCallback = settings.fnSelectionChangedCallback;

		$("th.selectableColumn", this).click(function (event) { dtSelectAll(dt, event); });

		return dt;
	};



	jQuery.fn.dataTableExt.oApi.fnSetFilteringDelay = function (oSettings, iDelay) {
		/*
		* Type:        Plugin for DataTables (www.datatables.net) JQuery plugin.
		* Name:        dataTableExt.oApi.fnSetFilteringDelay
		* Version:     2.2.1
		* Description: Enables filtration delay for keeping the browser more
		*              responsive while searching for a longer keyword.
		* Inputs:      object:oSettings - dataTables settings object
		*              integer:iDelay - delay in miliseconds
		* Returns:     JQuery
		* Usage:       $('#example').dataTable().fnSetFilteringDelay(250);
		* Requires:	  DataTables 1.6.0+
		*
		* Author:      Zygimantas Berziunas (www.zygimantas.com) and Allan Jardine (v2)
		* Created:     7/3/2009
		* Language:    Javascript
		* License:     GPL v2 or BSD 3 point style
		* Contact:     zygimantas.berziunas /AT\ hotmail.com
		*/
		var 
		_that = this,
		iDelay = (typeof iDelay == 'undefined') ? 250 : iDelay;

		this.each(function (i) {
			$.fn.dataTableExt.iApiIndex = i;
			var 
			$this = this,
			oTimerId = null,
			sPreviousSearch = null,
			anControl = $('input', _that.fnSettings().aanFeatures.f);

			anControl.off('keyup').on('keyup', function () {
				if (sPreviousSearch === null || sPreviousSearch != anControl.val()) {
					window.clearTimeout(oTimerId);
					sPreviousSearch = anControl.val();
					oTimerId = window.setTimeout(function () {
						$.fn.dataTableExt.iApiIndex = i;
						_that.fnFilter(anControl.val());
					}, iDelay);
				}
			});

			return this;
		});
		return this;
	};

})(jQuery);

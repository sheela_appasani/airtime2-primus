﻿using System;
using System.Collections.Generic;


namespace Xtra.Common
{

	public class DelegateComparer<T> : IEqualityComparer<T>
	{

		public DelegateComparer(Func<T, T, bool> equals = null, Func<T, int> getHashCode = null)
		{
			_equals = equals;
			_getHashCode = getHashCode;
		}


		public bool Equals(T x, T y)
		{
			return _equals != null
				? _equals(x, y)
				: DefaultEquals(x, y);
		}


		public int GetHashCode(T obj)
		{
			return _getHashCode != null
				? _getHashCode(obj)
				: DefaultGetHashCode(obj);
		}


		private bool DefaultEquals(T x, T y)
		{
			if (x == null && y == null) {
				return true;
			}
			if (x == null || y == null) {
				return false;
			}
			return x.Equals(y);
		}


		private int DefaultGetHashCode(T obj)
		{
			return obj.GetHashCode();
		}


		private readonly Func<T, T, bool> _equals;
		private readonly Func<T, int> _getHashCode;

	}

}

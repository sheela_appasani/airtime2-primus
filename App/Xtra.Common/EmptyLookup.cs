﻿using System.Linq;


namespace Xtra.Common
{

	public static class EmptyLookup<TKey, TElement>
	{

		public static ILookup<TKey, TElement> Instance { get { return _instance; } }

		private static readonly ILookup<TKey, TElement> _instance = Enumerable.Empty<TElement>().ToLookup(x => default(TKey));

	}

}

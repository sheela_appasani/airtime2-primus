﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Xtra.Common
{

	public static class EnumHelper
	{

		public static List<T> GetValues<T>() where T : struct
		{
			return Enum.GetValues(typeof (T)).Cast<T>().ToList();
		}

	}

}
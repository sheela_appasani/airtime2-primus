﻿using System.Collections.Generic;


namespace Xtra.Common
{

	public static class ExtendDictionary
	{

		public static TValue TryGetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> self, TKey key, TValue defaultValue = default(TValue))
		{
			TValue value;
			return self.TryGetValue(key, out value) ? value : defaultValue;
		}

	}

}

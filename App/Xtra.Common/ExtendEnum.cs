using System;
using System.ComponentModel.DataAnnotations;


namespace Xtra.Common
{

	public static class ExtendEnum
	{

		public static T ToEnum<T>(this string value)
			where T : struct
		{
			return (T)Enum.Parse(typeof(T), value, true);
		}


		public static bool IsEnum<T>(this string value)
			where T : struct
		{
			return Enum.IsDefined(typeof(T), value);
		}


		public static T? ToEnumSafe<T>(this string value)
			where T : struct
		{
			T result;
			if (!Enum.TryParse(value, true, out result)) {
				return null;
			}
			return result;
		}


		public static string ToResourceString<T>(this T value) where T : struct
		{
			var fi = value.GetType().GetField(value.ToString());
			var attr = (DisplayAttribute)Attribute.GetCustomAttribute(fi, typeof(DisplayAttribute));
			return attr != null 
				? attr.GetName() 
				: value.ToString();
		}

		public static string ToResourceString<T>(this T? value) where T : struct
		{
			if (!value.HasValue) {
				return null;
			}

			var fi = value.Value.GetType().GetField(value.Value.ToString());
			var attr = (DisplayAttribute)Attribute.GetCustomAttribute(fi, typeof(DisplayAttribute));
			return attr != null
				? attr.GetName()
				: value.Value.ToString();
		}


	}

}
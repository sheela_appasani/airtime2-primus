﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Xtra.Common
{

	public static class ExtendEnumerable
	{

		public static IEnumerable<TSource> Apply<TSource>(this IEnumerable<TSource> self)
		{
			if (self == null) {
				throw new ArgumentNullException("self");
			}

			#pragma warning disable 219
			foreach (TSource t in self) {
			}
			#pragma warning restore 219

			return self;
		}


		public static IEnumerable<TSource> ForEach<TSource>(this IEnumerable<TSource> self, Action<TSource> action)
		{
			if (self == null) {
				throw new ArgumentNullException("self");
			}
			if (action == null) {
				throw new ArgumentNullException("action");
			}

			foreach (TSource t in self) {
				action(t);
				yield return t;
			}
		}


		public static IEnumerable<T> Distinct<T, TKey>(this IEnumerable<T> @this, Func<T, TKey> keySelector)
		{
			return @this.GroupBy(keySelector).Select(grps => grps).Select(e => e.First());
		}
		 
	}

}

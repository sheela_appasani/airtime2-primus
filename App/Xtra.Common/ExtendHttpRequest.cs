﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Web;


namespace Xtra.Common
{

	public static class ExtendHttpRequest
	{

		public static string RemoteHostAddress(this HttpRequestBase request)
		{
			//Check if the request visibly passed through any proxies
			string ipForwarded = request.Headers["X-Forwarded-For"];
			if (!String.IsNullOrEmpty(ipForwarded)) {
				IPAddress ipAddress = null;
				string[] ips = ipForwarded.Replace(" ", "").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
				//Find the first (inner-most) public IP address in the forwarding-list
				foreach (var ipString in ips) {
					if (IPAddress.TryParse(ipString, out ipAddress)) {
						if (ipAddress.AddressFamily == AddressFamily.InterNetwork) {
							if (ipAddress.IsIPv4Public()) {
								break;
							}
						} else if (ipAddress.AddressFamily == AddressFamily.InterNetworkV6) {
							if (!ipAddress.IsIPv6SiteLocal && !ipAddress.IsIPv6LinkLocal) {
								break;
							}
						}
					}
				}
				//Return the inner-most public IP address from the forwarding-list, or the outer-most IP if no public ones were found prior
				if (ipAddress != null) {
					return ipAddress.ToString();
				}
			}

			//An apparent direct request
			return request.UserHostAddress;
		}

	}

}

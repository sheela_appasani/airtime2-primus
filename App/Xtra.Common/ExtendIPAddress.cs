﻿using System;
using System.Net;
using System.Net.Sockets;


namespace Xtra.Common
{

	public static class ExtendIPAddress
	{

		public static uint ToInt(this IPAddress ipAddress)
		{
			if (ipAddress.AddressFamily != AddressFamily.InterNetwork) {
				throw new ArgumentException("IPAddress is not an IPv4 address", "ipAddress");
			}
			return BitConverter.ToUInt32(ipAddress.GetAddressBytes(), 0);
		}


		public static bool IsIPv4Public(this IPAddress ipAddress)
		{
			uint ipNumber = ipAddress.ToInt();
			return ((ipNumber & MaskA.Item1) != MaskA.Item2
				&& (ipNumber & MaskB.Item1) != MaskB.Item2
				&& (ipNumber & MaskC.Item1) != MaskC.Item2
				&& (ipNumber & MaskD.Item1) != MaskD.Item2
				&& (ipNumber & MaskE.Item1) != MaskE.Item2
			);
		}


		private static readonly Tuple<uint, uint> MaskA = new Tuple<uint, uint>(IPAddress.Parse("255.0.0.0").ToInt(), IPAddress.Parse("10.0.0.0").ToInt());
		private static readonly Tuple<uint, uint> MaskB = new Tuple<uint, uint>(IPAddress.Parse("255.240.0.0").ToInt(), IPAddress.Parse("172.16.0.0").ToInt());
		private static readonly Tuple<uint, uint> MaskC = new Tuple<uint, uint>(IPAddress.Parse("255.255.0.0").ToInt(), IPAddress.Parse("192.168.0.0").ToInt());
		private static readonly Tuple<uint, uint> MaskD = new Tuple<uint, uint>(IPAddress.Parse("255.0.0.0").ToInt(), IPAddress.Parse("127.0.0.0").ToInt());
		private static readonly Tuple<uint, uint> MaskE = new Tuple<uint, uint>(IPAddress.Parse("255.255.0.0").ToInt(), IPAddress.Parse("169.254.0.0").ToInt());

	} 

}

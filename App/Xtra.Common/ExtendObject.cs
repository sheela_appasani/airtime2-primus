using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;

using NLog;


namespace Xtra.Common
{

	public static class ExtendObject
	{

		public static bool IsDefault<T>(this T value) where T : struct
		{
			return value.Equals(default(T));
		}


		public static bool IsNull(this object value)
		{
			return value == null;
		}


		public static TResult IfNotNull<TSource, TResult>(this TSource source, Func<TSource, TResult> func)
		{
			if (func == null) {
				throw new ArgumentNullException("func");
			}

			return source == null ? default(TResult) : func(source);
		}


		public static T ConvertToOrDefault<T>(this object value)
		{
			return ConvertToOrDefault<T>(value, default(T), Thread.CurrentThread.CurrentCulture);
		}


		public static T ConvertToOrDefault<T>(this object value, T defaultValue)
		{
			return ConvertToOrDefault<T>(value, defaultValue, Thread.CurrentThread.CurrentCulture);
		}


		public static T ConvertToOrDefault<T>(this object value, IFormatProvider formatProvider)
		{
			return ConvertToOrDefault<T>(value, default(T), formatProvider);
		}


		public static T ConvertToOrDefault<T>(this object value, T defaultValue, IFormatProvider formatProvider)
		{
			try {
				return ConvertTo<T>(value, formatProvider);
			} catch {
				return defaultValue;
			}
		}


		public static T ConvertTo<T>(this object value)
		{
			return ConvertTo<T>(value, Thread.CurrentThread.CurrentCulture);
		}


		public static T ConvertTo<T>(this object value, IFormatProvider formatProvider)
		{
			if (value == null) {
				return (T)(object)null;
			}
			if (formatProvider == null) {
				throw new ArgumentNullException("formatProvider");
			}

			Type t = typeof(T);

			if (t.IsGenericType && (t.GetGenericTypeDefinition() == typeof(Nullable<>))) {
				//Convert to nullable types
				Type toUnderlyingType = Nullable.GetUnderlyingType(t);
				Type valueType = value.GetType();
				if (toUnderlyingType == valueType) {
					//Converting to same type - simply cast
					return (T)value;
				}
				if (toUnderlyingType.IsEnum) {
					//Converting to nullable Enum
					return (T)Enum.Parse(toUnderlyingType, value.ToString());
				}
				if (toUnderlyingType == typeof(Guid)) {
					//Converting to nullable Guid
					return (T)Convert.ChangeType(new Guid(value.ToString()), toUnderlyingType, formatProvider);
				}
				if (value is IConvertible) {
					//All other nullable conversions
					return (T)Convert.ChangeType(value, toUnderlyingType, formatProvider);
				}
				//Try casting as a last resort
				return (T)value;

			} else {
				//Convert other types
				Type valueType = value.GetType();
				if (t == valueType) {
					//Converting to same type - simply cast
					return (T)value;
				}
				if (t.IsEnum) {
					//Converting to Enum
					return (T)Enum.Parse(t, value.ToString());
				}
				if (t == typeof(Guid)) {
					//Converting to Guid
					return (T)(object)(new Guid(value.ToString()));
				}
				if (valueType == typeof(Guid) && t == typeof(String)) {
					//Converting from Guid
					return (T)(object)value.ToString();
				}
				if (value is IConvertible) {
					//All other conversions
					return (T)Convert.ChangeType(value, t, formatProvider);
				}
				//Try casting as a last resort
				return (T)value;
			}
		}


		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

	}

}

﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Xtra.Common
{

	public static class ExtendQueryable
	{

		public static List<T> ToPagedList<T>(this IQueryable<T> source, int skip, int take)
		{
			return source
				.Skip(skip)
				.Take(take)
				.ToList();
		}

		public static IQueryable<T> Paged<T>(this IQueryable<T> source, int skip, int take)
		{
			return source
				.Skip(skip)
				.Take(take);
		}

	}

}

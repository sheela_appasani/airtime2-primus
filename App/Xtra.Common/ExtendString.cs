﻿using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;

using HtmlAgilityPack;


namespace Xtra.Common
{

	public static class ExtendString
	{

		public static bool Contains(this string source, string value, StringComparison comparer)
		{
			return source.IndexOf(value, comparer) >= 0;
		}


		public static bool IsNumeric(this string value)
		{
			if (value == null) {
				throw new ArgumentNullException("value");
			}

			int len = value.Length;
			for (int i = 0; i < len; i++) {
				char c = value[i];
				if (c < '0' || c > '9') {
					return false;
				}
			}
			return true;
		}


		public static string StripHtml(this string value)
		{
			var doc = new HtmlDocument();
			doc.LoadHtml(value);
			return doc.DocumentNode.InnerText;
		}


		public static string Replace(this string value, string oldValue, string newValue, StringComparison comparison)
		{
			var sb = new StringBuilder();

			int previousIndex = 0;
			int index = value.IndexOf(oldValue, comparison);
			while (index != -1) {
				sb.Append(value.Substring(previousIndex, index - previousIndex));
				sb.Append(newValue);
				index += oldValue.Length;

				previousIndex = index;
				index = value.IndexOf(oldValue, index, comparison);
			}
			sb.Append(value.Substring(previousIndex));

			return sb.ToString();
		}


		public static string NamedFormat(this string format, object source, Func<string, string> missingItemFunc = null)
		{
			if (format == null) {
				throw new ArgumentNullException("format");
			}

			if (missingItemFunc == null) {
				missingItemFunc = expression => "{" + expression + "}";
			}

			var result = new StringBuilder(format.Length * 2);

			using (var reader = new StringReader(format)) {
				var expression = new StringBuilder();

				var state = State.OutsideExpression;
				do {
					int c;
					switch (state) {
						case State.OutsideExpression:
							c = reader.Read();
							switch (c) {
								case -1:
									state = State.End;
									break;
								case '{':
									state = State.OnOpenBracket;
									break;
								case '}':
									state = State.OnCloseBracket;
									break;
								default:
									result.Append((char)c);
									break;
							}
							break;
						case State.OnOpenBracket:
							c = reader.Read();
							switch (c) {
								case -1:
									throw new FormatException();
								case '{':
									result.Append('{');
									state = State.OutsideExpression;
									break;
								case '}':
									result.Append(OutExpression(source, "", missingItemFunc));
									expression.Length = 0;
									state = State.OutsideExpression;
									break;
								default:
									expression.Append((char)c);
									state = State.InsideExpression;
									break;
							}
							break;
						case State.InsideExpression:
							c = reader.Read();
							switch (c) {
								case -1:
									throw new FormatException();
								case '}':
									result.Append(OutExpression(source, expression.ToString(), missingItemFunc));
									expression.Length = 0;
									state = State.OutsideExpression;
									break;
								default:
									expression.Append((char)c);
									break;
							}
							break;
						case State.OnCloseBracket:
							c = reader.Read();
							switch (c) {
								case '}':
									result.Append('}');
									state = State.OutsideExpression;
									break;
								default:
									throw new FormatException();
							}
							break;
						default:
							throw new InvalidOperationException("Invalid state.");
					}
				} while (state != State.End);
			}

			return result.ToString();
		}


		private static string OutExpression(object source, string expression, Func<string, string> missingItemFunc)
		{
			if (String.IsNullOrEmpty(expression)) {
				return missingItemFunc(expression);
			}

			string format = "";
			int colonIndex = expression.IndexOf(':');
			if (colonIndex > 0) {
				format = expression.Substring(colonIndex + 1);
				expression = expression.Substring(0, colonIndex);
			}

			try {
				return String.IsNullOrEmpty(format)
					? (DataBinder.Eval(source, expression) ?? "").ToString()
					: DataBinder.Eval(source, expression, "{0:" + format + "}");

			} catch (HttpException) {
				return missingItemFunc(expression);
			}
		}


		private enum State
		{
			OutsideExpression,
			OnOpenBracket,
			InsideExpression,
			OnCloseBracket,
			End
		}

	}

}

﻿using System;
using System.Collections.Generic;
using System.Web;

using Xtra.Common.Web;


namespace Xtra.Common
{

	public static class ExtendUrlBuilder
	{

		public static void Navigate(this UrlBuilder self)
		{
			_Navigate(self, true);
		}


		public static void Navigate(this UrlBuilder self, bool endResponse)
		{
			_Navigate(self, endResponse);
		}


		private static void _Navigate(this UrlBuilder self, bool endResponse)
		{
			HttpContext.Current.Response.Redirect(self.ToString(), endResponse);
		}

	}

}

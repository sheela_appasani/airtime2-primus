﻿using System;


namespace Xtra.Common
{

	public struct FileSize : IComparable, IFormattable, IConvertible
	{

		static FileSize()
		{
		}


		public FileSize(ulong value)
		{
			_value = value;
		}


		override public string ToString()
		{
			return ToString(null, null);
		}


		public String ToString(IFormatProvider provider)
		{
			return ToString(null, provider);
		}


		public string ToString(string format)
		{
			return ToString(format, null);
		}


		public string ToString(string format, IFormatProvider formatProvider)
		{
			int precision;

			if (String.IsNullOrEmpty(format)) {
				return ToString(DEFAULT_PRECISION);
			}
			
			if (int.TryParse(format, out precision)) {
				return ToString(precision);
			}

			return _value.ToString(format, formatProvider);
		}


		/// <summary>
		/// Formats the FileSize using the given number of decimals.
		/// </summary>
		public string ToString(int precision)
		{
			short pow = (short)Math.Floor((_value > 0 ? Math.Log(_value) : 0) / Math.Log(1024));
			pow = Math.Min(pow, (short)(Units.Length - 1));
			double value = (double)_value / IntPower(1024, pow);
			return value.ToString(pow == 0 ? "F0" : "F" + precision.ToString()) + " " + Units[pow];
		}


		public int CompareTo(object obj)
		{
			return _value.CompareTo(obj);
		}


		public int CompareTo(UInt64 value)
		{
			return _value.CompareTo(value);
		}


		public int CompareTo(FileSize value)
		{
			return _value.CompareTo(value._value);
		}


		public override bool Equals(Object obj)
		{
			return _value.Equals(obj);
		}


		public bool Equals(UInt64 value)
		{
			return _value.Equals(value);
		}


		public bool Equals(FileSize value)
		{
			return _value.Equals(value._value);
		}


		public override int GetHashCode()
		{
			return _value.GetHashCode();
		} 


		public TypeCode GetTypeCode()
		{
			return TypeCode.UInt64;
		}


		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			return Convert.ToBoolean(_value);
		}


		char IConvertible.ToChar(IFormatProvider provider)
		{
			return Convert.ToChar(_value);
		}


		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			return Convert.ToSByte(_value);
		}


		byte IConvertible.ToByte(IFormatProvider provider)
		{
			return Convert.ToByte(_value);
		}


		short IConvertible.ToInt16(IFormatProvider provider)
		{
			return Convert.ToInt16(_value);
		}


		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			return Convert.ToUInt16(_value);
		}


		int IConvertible.ToInt32(IFormatProvider provider)
		{
			return Convert.ToInt32(_value);
		}


		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			return Convert.ToUInt32(_value);
		}


		long IConvertible.ToInt64(IFormatProvider provider)
		{
			return Convert.ToInt64(_value);
		}


		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			return _value;
		}


		float IConvertible.ToSingle(IFormatProvider provider)
		{
			return Convert.ToSingle(_value);
		}


		double IConvertible.ToDouble(IFormatProvider provider)
		{
			return Convert.ToDouble(_value);
		}


		Decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			return Convert.ToDecimal(_value);
		}


		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			return Convert.ToDateTime(_value);
		}


		Object IConvertible.ToType(Type type, IFormatProvider provider)
		{
			return Convert.ChangeType(this, type, provider);
		}


		public static explicit operator FileSize(ulong value)
		{
			return new FileSize(value);
		}


		public static implicit operator ulong(FileSize fileSize)
		{
			return fileSize._value;
		}


		public static explicit operator char(FileSize fileSize)
		{
			return (char)fileSize._value;
		}


		public static explicit operator sbyte(FileSize fileSize)
		{
			return (sbyte)fileSize._value;
		}


		public static explicit operator byte(FileSize fileSize)
		{
			return (byte)fileSize._value;
		}


		public static explicit operator short(FileSize fileSize)
		{
			return (short)fileSize._value;
		}

		
		public static explicit operator ushort(FileSize fileSize)
		{
			return (ushort)fileSize._value;
		}

		
		public static explicit operator int(FileSize fileSize)
		{
			return (int)fileSize._value;
		}


		public static explicit operator uint(FileSize fileSize)
		{
			return (uint)fileSize._value;
		}


		public static explicit operator long(FileSize fileSize)
		{
			return (long)fileSize._value;
		}


		public static explicit operator float(FileSize fileSize)
		{
			return (float)fileSize._value;
		}

		
		public static explicit operator double(FileSize fileSize)
		{
			return (double)fileSize._value;
		}


		public static explicit operator Decimal(FileSize fileSize)
		{
			return (Decimal)fileSize._value;
		}


		private static long IntPower(int x, short power)
		{
			if (power == 0) {
				return 1;
			}
			if (power == 1) {
				return x;
			}

			int n = 15;
			while ((power <<= 1) >= 0) {
				n--;
			}

			long tmp = x;
			while (--n > 0) {
				tmp = tmp * tmp * (((power <<= 1) < 0) ? x : 1);
			}
			return tmp;
		}


		private readonly ulong _value;

		private const int DEFAULT_PRECISION = 2;

		private static readonly string[] Units = new[] { "B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB" };

	}

}

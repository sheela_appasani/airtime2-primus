﻿using System;

namespace Xtra.Common
{
    public class FormatString
    {
        public static string GetPercentageString(double ratio)
        {
            return !Double.IsNaN(ratio)
                ? String.Format("{0:P0}", ratio)
                : "";
        }
    }
}

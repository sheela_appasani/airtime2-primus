﻿using System;
using System.IO;
using System.IO.Compression;


namespace Xtra.Common
{

	public static class GZipHelper
	{

		public static byte[] Compress(byte[] data)
		{
			using (var compressedData = new MemoryStream()) {
				using (var compressedStream = new DeflateStream(compressedData, CompressionMode.Compress, true)) {
					compressedStream.Write(data, 0, data.Length);
				}
				return compressedData.ToArray();
			}
		}


		public static byte[] Uncompress(byte[] data)
		{
			using (var compressedData = new MemoryStream(data))
			using (var compressedStream = new DeflateStream(compressedData, CompressionMode.Decompress, true))
			using (var uncompressedData = new MemoryStream()) {
				var buffer = new byte[1024];
				int bytesRead = compressedStream.Read(buffer, 0, buffer.Length);
				while (bytesRead > 0) {
					uncompressedData.Write(buffer, 0, bytesRead);
					bytesRead = compressedStream.Read(buffer, 0, buffer.Length);
				}
				return uncompressedData.ToArray();
			}
		}


		public static byte[] CompressString(string value)
		{
			return Compress(GetBytes(value));
		}


		public static string UncompressString(byte[] data)
		{
			return GetString(Uncompress(data));
		}


		private static byte[] GetBytes(string str)
		{
			var bytes = new byte[str.Length * sizeof(char)];
			Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
			return bytes;
		}


		private static string GetString(byte[] bytes)
		{
			var chars = new char[bytes.Length / sizeof(char)];
			Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
			return new String(chars);
		}

	}

}

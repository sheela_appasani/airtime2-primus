﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace Xtra.Common
{

	public class Range<T> : IComparable<T>, IComparable<Range<T>>
	{

		public string Label { get; set; }

		public T From {
			get { return _from; }
			set { _from = value; ResetLimits(); }
		}

		public T To {
			get { return _to; }
			set { _to = value; ResetLimits(); }
		}


		public Range()
			: this(default(T), default(T))
		{
		}


		public Range(T from, T to, string label = null)
		{
			From = from;
			To = to;
			Label = label;

			ResetLimits();
		}


		internal Range(T singleValue)
			: this(singleValue, singleValue)
		{
			_compareAsContainedValue = true;
		}


		public virtual bool Contains(T value)
		{
			return Equals(value);
		}


		public virtual int CompareTo(T other)
		{
			if (_lowerLimit == null && _upperLimit == null && other == null) {
				return 0;
			}

			if (other == null) {
				return 1;
			}

			if (_lowerLimit != null) {
				int result = DefaultComparer.Compare(_lowerLimit, other);
				if (result > 0) {
					return result;
				}
			}
			if (_upperLimit != null) {
				int result = DefaultComparer.Compare(_upperLimit, other);
				if (result < 0) {
					return result;
				}
			}
			return 0;
		}


		public virtual int CompareTo(Range<T> other)
		{
			if (other == null) {
				return 1;
			}

			if (other._compareAsContainedValue) {
				return CompareTo(other._lowerLimit);
			}

			int result = DefaultComparer.Compare(_lowerLimit, other._lowerLimit);
			if (result != 0) {
				return result;
			}

			result = DefaultComparer.Compare(_upperLimit, other._upperLimit);
			if (result != 0) {
				return result;
			}

			return 0;
		}


		public bool Equals(T other)
		{
			return CompareTo(other) == 0;
		}


		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj)) {
				return true;
			}

			if (obj is T) {
				return Equals((T)obj);
			}

			if (obj is Range<T>) {
				var compareTo = (Range<T>)obj;

				if (_compareAsContainedValue) {
					return Equals(compareTo._lowerLimit);
				}

				if (!_compareAsContainedValue) {
					return compareTo != null
						&& DefaultEqualityComparer.Equals(_lowerLimit, compareTo._lowerLimit)
						&& DefaultEqualityComparer.Equals(_upperLimit, compareTo._upperLimit);
				}
			}

			return false;
		}


		public override int GetHashCode()
		{
			int hashCode = this.GetType().GetHashCode();
			hashCode = (hashCode * HashMultiplier) ^ _lowerLimit.GetHashCode();
			hashCode = (hashCode * HashMultiplier) ^ _upperLimit.GetHashCode();
			return hashCode;
		}


		public override string ToString()
		{
			var str = String.Format("{0} - {1}", From, To);
			if (!String.IsNullOrEmpty(Label)) {
				str += " (" + Label + ")";
			}
			return str;
		}


		private void ResetLimits()
		{
			if (_to != null && _from != null && DefaultComparer.Compare(_from, _to) > 0) {
				//In this case the From parameter is actually lower than the To parameter, so swap them around internally
				_lowerLimit = _to;
				_upperLimit = _from;
			} else {
				_lowerLimit = _from;
				_upperLimit = _to;
			}
		}


		private T _from;
		private T _to;

		private T _lowerLimit;
		private T _upperLimit;

		private readonly bool _compareAsContainedValue = false;

		private const int HashMultiplier = 31;

		// ReSharper disable StaticFieldInGenericType
		private static readonly IEqualityComparer DefaultEqualityComparer = EqualityComparer<T>.Default;
		private static readonly IComparer DefaultComparer = Comparer<T>.Default;
		// ReSharper restore StaticFieldInGenericType

	}

}

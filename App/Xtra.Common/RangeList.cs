﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;


namespace Xtra.Common
{

	public class RangeList<T> : RangeListBase<T>
	{

		public Func<char, bool> IsWhiteSpace { get; set; }
		public Func<char, bool> IsValueCharacter { get; set; }
		public Func<char, bool> IsRangeDelimiter { get; set; }
		public Func<char, bool> IsBelowOperator { get; set; }
		public Func<char, bool> IsAboveOperator { get; set; }


		public RangeList() { }


		public RangeList(IEnumerable<Range<T>> collection) : base(collection) { }


		public RangeList(string inputString) : this(new StringReader(inputString)) { }


		public RangeList(TextReader reader)
		{
			Parse(reader);
		}


		public RangeList(TextReader reader, IFormatProvider formatProvider)
		{
			Parse(reader, formatProvider);
		}


		public void Parse(string inputString)
		{
			Parse(new StringReader(inputString), Thread.CurrentThread.CurrentCulture);
		}


		public void Parse(TextReader reader)
		{
			Parse(reader, Thread.CurrentThread.CurrentCulture);
		}


		public void Parse(TextReader reader, IFormatProvider formatProvider)
		{
			var parser = new RangeParser<T>(formatProvider);
			if (IsWhiteSpace != null) parser.IsWhiteSpace = IsWhiteSpace;
			if (IsValueCharacter != null) parser.IsValueCharacter = IsValueCharacter;
			if (IsRangeDelimiter != null) parser.IsRangeDelimiter = IsRangeDelimiter;
			if (IsBelowOperator != null) parser.IsBelowOperator = IsBelowOperator;
			if (IsAboveOperator != null) parser.IsAboveOperator = IsAboveOperator;
			var ranges = parser.Parse(reader);

			Clear();
			AddRange(ranges);
		}

	}

}

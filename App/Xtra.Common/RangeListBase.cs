﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Xtra.Common
{

	public abstract class RangeListBase<T> : List<Range<T>>
	{

		protected RangeListBase()
		{
		}


		protected RangeListBase(IEnumerable<Range<T>> collection):base(collection)
		{
		}


		public int BinarySearch(int index, int count, T item, IComparer<Range<T>> comparer)
		{
			return base.BinarySearch(index, count, new Range<T>(item), comparer);
		}


		public int BinarySearch(T item, IComparer<Range<T>> comparer)
		{
			return base.BinarySearch(new Range<T>(item), comparer);
		}


		public int BinarySearch(T item)
		{
			return base.BinarySearch(new Range<T>(item));
		}


		public bool Contains(T item)
		{
			if ((Object)item == null) {
				for (int i = 0; i < Count; i++) {
					if ((Object)this[i] == null) {
						return true;
					}
				}
				return false;
			}

			for (int i = 0; i < Count; i++) {
				if (this[i].Equals(item)) {
					return true;
				}
			}

			return false;
		}


		public int IndexOf(T item)
		{
			return IndexOf(item, 0, Count);
		}


		public int IndexOf(T item, int startIndex)
		{
			return IndexOf(item, startIndex, Count);
		}


		public int IndexOf(T item, int startIndex, int count)
		{
			int endIndex = startIndex + count;
			for (int i = startIndex; i < endIndex; i++) {
				if (this[i].Equals(item)) {
					return i;
				}
			}
			return -1;
		}


		public int LastIndexOf(T item)
		{
			return LastIndexOf(item, 0, Count);
		}


		public int LastIndexOf(T item, int index)
		{
			return LastIndexOf(item, index, Count);
		}


		public int LastIndexOf(T item, int startIndex, int count)
		{
			int endIndex = startIndex - count + 1;
			for (int i = startIndex; i >= endIndex; i--) {
				if (this[i].Equals(item)) {
					return i;
				}
			}
			return -1;
		}

	}

}

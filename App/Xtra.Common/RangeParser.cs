﻿using System;
using System.Collections.Generic;
using System.IO;

using Stateless;


namespace Xtra.Common
{

	internal class RangeParser<T>
	{

		internal Func<char, bool> IsWhiteSpace { get; set; }
		internal Func<char, bool> IsValueCharacter { get; set; }
		internal Func<char, bool> IsRangeDelimiter { get; set; }
		internal Func<char, bool> IsBelowOperator { get; set; }
		internal Func<char, bool> IsAboveOperator { get; set; }


		internal RangeParser(IFormatProvider formatProvider)
		{
			IsWhiteSpace = Char.IsWhiteSpace;
			IsValueCharacter = Char.IsDigit;
			IsRangeDelimiter = (c) => DefaultRangeDelimiters.Contains(c);
			IsBelowOperator = (c) => DefaultBelowOperators.Contains(c);
			IsAboveOperator = (c) => DefaultAboveOperators.Contains(c);

			_formatProvider = formatProvider;
		}


		internal List<Range<T>> Parse(TextReader reader)
		{
			_machine = new StateMachine<RangeParserState, RangeParserTrigger>(RangeParserState.START);
			ConfigureStateMachine(_machine);

			_results = new List<Range<T>>();
			while (!_machine.IsInState(RangeParserState.END)) {
				var data = GetData(reader);

				if (!data.HasValue) {
					_machine.Fire(RangeParserTrigger.NO_DATA);

				} else {
					_nextChar = data.Value;
					_machine.Fire(ExamineInputs(_nextChar));
				}
			}
			return _results;
		}


		private void ResetCurrentRange()
		{
			_currentRange = null;
			_currentOperator = null;
			_gotFrom = false;
			_gotTo = false;
		}


		private void PrepareCurrentRange()
		{
			if (_currentRange == null) {
				_currentRange = new Range<T>();
			}
		}


		private void AddCurrentRange()
		{
			if (_currentRange == null) {
				return;
			}

			if ((_currentOperator == RangeParserOperator.BELOW && _gotFrom && !_gotTo) || (_currentOperator == RangeParserOperator.ABOVE && !_gotFrom && _gotTo)) {
				//Single value with an operator, but the value is on the wrong side of the operator - swap the value's position around
				var temp = _currentRange.From;
				_currentRange.From = _currentRange.To;
				_currentRange.To = temp;

			} else if (!_currentOperator.HasValue) {
				//Single value without any operators - set the To value to be the same as the From value
				_currentRange.To = _currentRange.From;
			}

			_results.Add(_currentRange);
		}


		private void StoreAccumulatorAsFromValue()
		{
			if (_accumulator.Length <= 0) {
				return;
			}
			PrepareCurrentRange();
			_gotFrom = true;
			_currentRange.From = _accumulator.ConvertTo<T>(_formatProvider);
			_accumulator = "";
		}


		private void StoreAccumulatorAsToValue()
		{
			if (_accumulator.Length <= 0) {
				return;
			}
			PrepareCurrentRange();
			_gotTo = true;
			_currentRange.To = _accumulator.ConvertTo<T>(_formatProvider);
			_accumulator = "";
		}


		private void AppendToAccumulator()
		{
			_accumulator += _nextChar;
		}


		private void SetCurrentOperator(RangeParserOperator currentOperator)
		{
			_currentOperator = currentOperator;
		}


		private RangeParserTrigger ExamineInputs(char nextChar)
		{
			if (IsRangeDelimiter(nextChar)) {
				return RangeParserTrigger.DELIMITER;
			}
			if (IsBelowOperator(nextChar)) {
				return RangeParserTrigger.BELOW_OPERATOR;
			}
			if (IsAboveOperator(nextChar)) {
				return RangeParserTrigger.ABOVE_OPERATOR;
			}
			if (IsWhiteSpace(nextChar)) {
				return RangeParserTrigger.WHITESPACE;
			}
			if (IsValueCharacter(nextChar)) {
				return RangeParserTrigger.VALUE_CHAR;
			}
			return RangeParserTrigger.ERROR;
		}


		private static char? GetData(TextReader reader)
		{
			int i = reader.Read();
			if (i == -1) {
				return null;
			}
			return (char)i;
		}


		private void ConfigureStateMachine(StateMachine<RangeParserState, RangeParserTrigger> machine)
		{
			machine.Configure(RangeParserState.START)
				.Ignore(RangeParserTrigger.WHITESPACE)
				.Ignore(RangeParserTrigger.DELIMITER)
				.Permit(RangeParserTrigger.VALUE_CHAR, RangeParserState.GOT_FROM_CHAR)
				.Permit(RangeParserTrigger.BELOW_OPERATOR, RangeParserState.GOT_BELOW_OPERATOR)
				.Permit(RangeParserTrigger.NO_DATA, RangeParserState.END);


			machine.Configure(RangeParserState.GET_RANGE)
				.OnEntry(ResetCurrentRange)
				.OnExit(AddCurrentRange);


			/*
			 * FROM VALUE
			 */
			machine.Configure(RangeParserState.GET_FROM_VALUE).SubstateOf(RangeParserState.GET_RANGE)
				.OnExit(StoreAccumulatorAsFromValue);

			machine.Configure(RangeParserState.GOT_FROM_CHAR).SubstateOf(RangeParserState.GET_FROM_VALUE)
				.OnEntry(AppendToAccumulator)
				.PermitReentry(RangeParserTrigger.VALUE_CHAR)
				.Permit(RangeParserTrigger.WHITESPACE, RangeParserState.GOT_FROM_WHITESPACE)
				.Permit(RangeParserTrigger.DELIMITER, RangeParserState.GOT_DELIMITER)
				.Permit(RangeParserTrigger.BELOW_OPERATOR, RangeParserState.GOT_BELOW_OPERATOR)
				.Permit(RangeParserTrigger.ABOVE_OPERATOR, RangeParserState.GOT_ABOVE_OPERATOR)
				.Permit(RangeParserTrigger.NO_DATA, RangeParserState.END);

			machine.Configure(RangeParserState.GOT_FROM_WHITESPACE).SubstateOf(RangeParserState.GET_FROM_VALUE)
				.OnExit(StoreAccumulatorAsFromValue)
				.Ignore(RangeParserTrigger.WHITESPACE)
				.Permit(RangeParserTrigger.DELIMITER, RangeParserState.GOT_DELIMITER)
				.Permit(RangeParserTrigger.BELOW_OPERATOR, RangeParserState.GOT_BELOW_OPERATOR)
				.Permit(RangeParserTrigger.ABOVE_OPERATOR, RangeParserState.GOT_ABOVE_OPERATOR)
				.Permit(RangeParserTrigger.NO_DATA, RangeParserState.END);


			/*
			 * TO VALUE
			 */
			machine.Configure(RangeParserState.GET_TO_VALUE).SubstateOf(RangeParserState.GET_RANGE)
				.OnExit(StoreAccumulatorAsToValue);

			machine.Configure(RangeParserState.GOT_TO_CHAR).SubstateOf(RangeParserState.GET_TO_VALUE)
				.OnEntry(AppendToAccumulator)
				.PermitReentry(RangeParserTrigger.VALUE_CHAR)
				.Permit(RangeParserTrigger.WHITESPACE, RangeParserState.GOT_TO_WHITESPACE)
				.Permit(RangeParserTrigger.DELIMITER, RangeParserState.GOT_DELIMITER)
				.Permit(RangeParserTrigger.NO_DATA, RangeParserState.END);

			machine.Configure(RangeParserState.GOT_TO_WHITESPACE).SubstateOf(RangeParserState.GET_TO_VALUE)
				.OnExit(StoreAccumulatorAsToValue)
				.Ignore(RangeParserTrigger.WHITESPACE)
				.Permit(RangeParserTrigger.DELIMITER, RangeParserState.GOT_DELIMITER)
				.Permit(RangeParserTrigger.VALUE_CHAR, RangeParserState.GOT_FROM_CHAR)
				.Permit(RangeParserTrigger.NO_DATA, RangeParserState.END);


			/*
			 * OPERATORS
			 */
			machine.Configure(RangeParserState.GET_OPERATOR).SubstateOf(RangeParserState.GET_RANGE);

			machine.Configure(RangeParserState.GOT_BELOW_OPERATOR).SubstateOf(RangeParserState.GET_OPERATOR)
				.OnEntry(() => SetCurrentOperator(RangeParserOperator.BELOW))
				.Ignore(RangeParserTrigger.WHITESPACE)
				.Permit(RangeParserTrigger.VALUE_CHAR, RangeParserState.GOT_TO_CHAR)
				.Permit(RangeParserTrigger.DELIMITER, RangeParserState.GOT_DELIMITER)
				.Permit(RangeParserTrigger.NO_DATA, RangeParserState.END);

			machine.Configure(RangeParserState.GOT_ABOVE_OPERATOR).SubstateOf(RangeParserState.GET_OPERATOR)
				.OnEntry(() => SetCurrentOperator(RangeParserOperator.ABOVE))
				.Ignore(RangeParserTrigger.WHITESPACE)
				.PermitIf(RangeParserTrigger.VALUE_CHAR, RangeParserState.GOT_FROM_CHAR, () => !_gotFrom) //Only allow digits if the From value hasn't already been set
				.Permit(RangeParserTrigger.DELIMITER, RangeParserState.GOT_DELIMITER)
				.Permit(RangeParserTrigger.NO_DATA, RangeParserState.END);


			/*
			 * OTHER STATES
			 */
			machine.Configure(RangeParserState.GOT_DELIMITER)
				.Ignore(RangeParserTrigger.DELIMITER)
				.Ignore(RangeParserTrigger.WHITESPACE)
				.Permit(RangeParserTrigger.VALUE_CHAR, RangeParserState.GOT_FROM_CHAR)
				.Permit(RangeParserTrigger.BELOW_OPERATOR, RangeParserState.GOT_BELOW_OPERATOR)
				.Permit(RangeParserTrigger.ABOVE_OPERATOR, RangeParserState.GOT_ABOVE_OPERATOR)
				.Permit(RangeParserTrigger.NO_DATA, RangeParserState.END);

			machine.Configure(RangeParserState.END);
		}


		private List<Range<T>> _results;
		
		private char _nextChar;
		private string _accumulator = "";

		private bool _gotFrom;
		private bool _gotTo;
		private RangeParserOperator? _currentOperator;
		private Range<T> _currentRange;
		private IFormatProvider _formatProvider;

		private StateMachine<RangeParserState, RangeParserTrigger> _machine;

		private ICollection<char> DefaultRangeDelimiters = new[] { ',', ';', '\n', '\r' };
		private ICollection<char> DefaultBelowOperators = new[] { '-', '<' };
		private ICollection<char> DefaultAboveOperators = new[] { '+', '>' };

	}

}

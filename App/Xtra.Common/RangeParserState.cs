﻿namespace Xtra.Common
{

	//Enum values below are indented to match their hierarchy in the state-machine
	internal enum RangeParserState
	{
		START,
		GET_RANGE,
			GET_FROM_VALUE,
				GOT_FROM_CHAR,
				GOT_FROM_WHITESPACE,
			GET_TO_VALUE,
				GOT_TO_CHAR,
				GOT_TO_WHITESPACE,
			GET_OPERATOR,
				GOT_BELOW_OPERATOR,
				GOT_ABOVE_OPERATOR,
		GOT_DELIMITER,
		ERROR,
		END
	}

}

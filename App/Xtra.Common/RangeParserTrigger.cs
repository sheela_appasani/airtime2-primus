﻿namespace Xtra.Common
{

	internal enum RangeParserTrigger
	{
		WHITESPACE,
		VALUE_CHAR,
		DELIMITER,
		BELOW_OPERATOR,
		ABOVE_OPERATOR,
		NO_DATA,
		ERROR
	}

}

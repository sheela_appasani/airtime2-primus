﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace Xtra.Common
{

	public class SimpleKeyedCollection<TKey, T> : KeyedCollection<TKey, T>
	{

		public SimpleKeyedCollection(Func<T, TKey> getKeyForItem)
		{
			_getKeyForItem = getKeyForItem;
		}


		public SimpleKeyedCollection(IEqualityComparer<TKey> comparer, Func<T, TKey> getKeyForItem)
			: base(comparer)
		{
			_getKeyForItem = getKeyForItem;
		}


		public SimpleKeyedCollection(IEqualityComparer<TKey> comparer, int dictionaryCreationThreshold, Func<T, TKey> getKeyForItem)
			: base(comparer, dictionaryCreationThreshold)
		{
			_getKeyForItem = getKeyForItem;
		}


		protected override TKey GetKeyForItem(T item)
		{
			return _getKeyForItem(item);
		}


		private readonly Func<T, TKey> _getKeyForItem;

	}

}

﻿using System;
using System.IO;


namespace Xtra.Common.Templating
{

	public class FileTemplate : TextTemplate, ITemplate
	{

		public FileTemplate(string path, object templateValues = null, Func<string, string> missingItemFunc = null)
			: base(templateValues, missingItemFunc)
		{
			_localPath = path;
		}


		public override bool Exists()
		{
			return base.Exists() || File.Exists(_localPath);
		}


		protected override string ReadInput()
		{
			if (_localPath != null) {
				return File.ReadAllText(_localPath);
			}
			return base.ReadInput();
		}


		private readonly string _localPath;

	}

}

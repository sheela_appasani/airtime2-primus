﻿using System;


namespace Xtra.Common.Templating
{

	public interface ITemplate
	{
		ITemplate AddValues(object templateValues, Func<string, string> missingItemFunc = null);
		string ToString();
		bool Exists();
	}

}

﻿using System;


namespace Xtra.Common.Templating
{

	public class StringTemplate : TextTemplate, ITemplate
	{

		public StringTemplate(string templateString, object templateValues = null, Func<string, string> missingItemFunc = null)
			: base(templateValues, missingItemFunc)
		{
			_templateString = templateString;
		}


		public override bool Exists()
		{
			return base.Exists() || _templateString != null;
		}


		protected override string ReadInput()
		{
			if (_templateString != null) {
				return _templateString;
			}
			return base.ReadInput();
		}


		private readonly string _templateString;

	}

}

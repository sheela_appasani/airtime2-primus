﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace Xtra.Common.Templating
{

	public class TextTemplate : ITemplate
	{

		public TextTemplate(TextReader reader, object templateValues = null, Func<string, string> missingItemFunc = null)
			: this(templateValues, missingItemFunc)
		{
			_reader = reader;
		}


		protected TextTemplate(object templateValues = null, Func<string, string> missingItemFunc = null)
		{
			_values = new List<Tuple<object, Func<string, string>>>();
			if (templateValues != null) {
				_values.Add(new Tuple<object, Func<string, string>>(templateValues, missingItemFunc));
			}
		}


		public virtual bool Exists()
		{
			return _reader != null;
		}


		public ITemplate AddValues(object templateValues, Func<string, string> missingItemFunc = null)
		{
			_values.Add(new Tuple<object, Func<string, string>>(templateValues, missingItemFunc));
			return this;
		}


		public override string ToString()
		{
			if (!Exists()) {
				return "";
			}
			return _values.Aggregate(
				ReadInput(),
				(current, value) => current.NamedFormat(source: value.Item1, missingItemFunc: value.Item2)
			);
		}


		protected virtual string ReadInput()
		{
			return _reader.ReadToEnd();
		}


		private readonly TextReader _reader;
		private readonly List<Tuple<object, Func<string, string>>> _values;

	}

}

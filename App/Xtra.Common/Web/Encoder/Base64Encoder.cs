using System;
using System.IO;


namespace Xtra.Common.Web.Encoder
{

	public class Base64Encoder : IEncoder
	{

		public Stream Encode(Stream input)
		{
			Stream output;
			var stream = new MemoryStream();
			using (var reader = new BinaryReader(input))
			using (var writer = new StreamWriter(stream)) {
				int bytesRead;
				var block = new byte[BlockSize];
				do {
					bytesRead = reader.Read(block, 0, block.Length);
					writer.Write(Convert.ToBase64String(block, 0, bytesRead));
					writer.Flush();
				} while (bytesRead == block.Length);
				output = new MemoryStream(stream.GetBuffer(), 0, (int)stream.Length, writable: false);
			}
			return output;
		}


		public Stream Decode(Stream input)
		{
			Stream output;
			var stream = new MemoryStream();
			using (var reader = new StreamReader(input))
			using (var writer = new BinaryWriter(stream)) {
				writer.Write(Convert.FromBase64String(reader.ReadToEnd()));
				writer.Flush();
				output = new MemoryStream(stream.GetBuffer(), 0, (int)stream.Length, writable: false);
			}
			return output;
		}


		private const int BlockSize = 48;

	}

}

using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;


namespace Xtra.Common.Web.Encoder
{

	/// <summary>
	/// URL encodes invalid query parameter characters
	/// </summary>
	public class EncoderChain : IEncoderChain, IEncoder
	{

		public IEncoderChain Add(IEncoder encoder)
		{
			Contract.Assert(encoder != null);

			//Warning: there's nothing here to prevent indirect infinite loops from being formed (e.g. {chain1.Add(chain2); chain2.Add(chain1); chain1.Encode(val);})
			if (encoder != null && encoder != this) {
				_encoders.Add(encoder);
			}
			return this;
		}


		public string Encode(string input)
		{
			Stream output;
			var stream = new MemoryStream();
			using (var writer = new StreamWriter(stream)) {
				writer.Write(input);
				writer.Flush();
				output = new MemoryStream(stream.GetBuffer(), 0, (int)stream.Length, writable: false);
			}
			output = Encode(output);
			using (var reader = new StreamReader(output)) {
				return reader.ReadToEnd();
			}
		}


		public string Decode(string input)
		{
			Stream output;
			var stream = new MemoryStream();
			using (var writer = new StreamWriter(stream)) {
				writer.Write(input);
				writer.Flush();
				output = new MemoryStream(stream.GetBuffer(), 0, (int)stream.Length, writable: false);
			}
			output = Decode(output);
			using (var reader = new StreamReader(output)) {
				return reader.ReadToEnd();
			}
		}


		public Stream Encode(Stream input)
		{
			return _encoders.Aggregate(input, (current, encoder) => encoder.Encode(current));
		}


		public Stream Decode(Stream input)
		{
			return _encoders.Reverse().Aggregate(input, (current, encoder) => encoder.Decode(current));
		}


		private readonly ICollection<IEncoder> _encoders = new List<IEncoder>();

	}

}

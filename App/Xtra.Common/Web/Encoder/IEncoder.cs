using System.IO;


namespace Xtra.Common.Web.Encoder
{

	public interface IEncoder
	{

		Stream Encode(Stream input);
		Stream Decode(Stream input);

	}

}

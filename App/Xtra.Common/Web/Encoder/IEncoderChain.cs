namespace Xtra.Common.Web.Encoder
{

	public interface IEncoderChain : IEncoder
	{

		IEncoderChain Add(IEncoder encoder);
		string Encode(string input);
		string Decode(string input);

	}

}

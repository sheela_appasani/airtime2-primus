using System;
using System.IO;


namespace Xtra.Common.Web.Encoder
{

	public class StringEncoder : IEncoder
	{

		public StringEncoder(Func<string, string> encodingFunc = null, Func<string, string> decodingFunc = null)
		{
			_encodingFunc = encodingFunc ?? (val => val);
			_decodingFunc = decodingFunc ?? (val => val);
		}


		public Stream Encode(Stream input)
		{
			Stream output;
			var stream = new MemoryStream();
			using (var reader = new StreamReader(input))
			using (var writer = new StreamWriter(stream)) {
				writer.Write(_encodingFunc(reader.ReadToEnd()));
				writer.Flush();
				output = new MemoryStream(stream.GetBuffer(), 0, (int)stream.Length, writable: false);
			}
			return output;
		}


		public Stream Decode(Stream input)
		{
			Stream output;
			var stream = new MemoryStream();
			using (var reader = new StreamReader(input))
			using (var writer = new StreamWriter(stream)) {
				writer.Write(_decodingFunc(reader.ReadToEnd()));
				writer.Flush();
				output = new MemoryStream(stream.GetBuffer(), 0, (int)stream.Length, writable: false);
			}
			return output;
		}


		private readonly Func<string, string> _encodingFunc;
		private readonly Func<string, string> _decodingFunc;

	}

}

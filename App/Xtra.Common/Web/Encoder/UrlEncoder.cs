﻿using System;
using System.Web;


namespace Xtra.Common.Web.Encoder
{

	public class UrlEncoder : StringEncoder
	{

		public UrlEncoder()
			: base(HttpUtility.UrlEncode, HttpUtility.UrlDecode)
		{
		}

	}

}

using System;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

using Xtra.Common.Web.Encoder;


namespace Xtra.Common.Web
{

	public class QueryStringDictionary : NameValueCollection
	{

		public new string this[string name]
		{
			get { return base[name]; }
			set { base[name] = value; }
		}


		public new string this[int index]
		{
			get { return base[index]; }
		}


		public QueryStringDictionary()
		{
		}


		public QueryStringDictionary(string query, IEncoderChain valueEncoders = null, IEncoderChain paramEncoders = null, string paramPrefix = "")
		{
			_valueEncoders = valueEncoders;
			_paramEncoders = paramEncoders;
			_paramPrefix = paramPrefix;
			Initialise(query);
		}


		/// <summary>
		/// Determines if the QueryStringDictionary contains a specific key.
		/// </summary>
		/// <param name="key">The key to locate in the QueryStringDictionary.</param>
		/// <returns>True if the QueryStringDictionary contains an entry with the specified key; otherwise, false.</returns>
		public bool Contains(string key)
		{
			return !String.IsNullOrEmpty(base[key]);
		}


		private void Initialise(string query)
		{
			if (String.IsNullOrEmpty(query)) {
				return;
			}

			base.Clear();

			if (query[0] == '?') {
				query = query.Substring(1);
			}

			if (_valueEncoders != null) {
				query = _valueEncoders.Decode(query);
			}

			if (_paramEncoders != null && query.StartsWith(_paramPrefix)) {
				query = _paramEncoders.Decode(query.Substring(_paramPrefix.Length));
			}

			var pairs = query.Split(new[] { '&' }, StringSplitOptions.RemoveEmptyEntries);
			foreach (string s in pairs) {
				var pair = s.Split(new[] { '=' }, 2);
				base.Add(pair[0], (pair.Length > 1) ? pair[1] : String.Empty);
			}
		}


		public new QueryStringDictionary Clear()
		{
			base.Clear();
			return this;
		}


		public new QueryStringDictionary Remove(string name)
		{
			base.Remove(name);
			return this;
		}


		/// <summary>
		/// Adds a name value pair to the collection.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="value">The value associated to the name.</param>
		/// <param name="isUnique">True if the name is unique within the querystring. This allows us to override existing values.</param>
		/// <returns>The QueryStringDictionary object.</returns>
		public QueryStringDictionary Add(string name, string value, bool isUnique = false)
		{
			if (isUnique) {
				base[name] = value;
			} else {
				base.Add(name, value);
			}
			return this;
		}


		public new string ToString()
		{
			return _ToString("e");
		}


		public string ToString(string format)
		{
			return _ToString(format);
		}


		private string _ToString(string format)
		{
			if (!base.HasKeys()) {
				return String.Empty;
			}

			switch (format) {
				case "e":
					//Encrypted and then encoded
					if (_paramEncoders != null) {
						return _paramPrefix + _paramEncoders.Encode(ValuesToString());
					}
					return ValuesToString();

				case "p":
					//Encoded plaintext
					return ValuesToString();

				default:
					throw new FormatException();
			}
		}


		private string ValuesToString()
		{
			var builder = new StringBuilder();
			for (var i = 0; i < base.Keys.Count; i++) {
				string key = base.Keys[i];
				if (!String.IsNullOrEmpty(key)) {
					var values = base.GetValues(key);
					if (values != null) {
						foreach (var val in values) {
							if (builder.Length > 0) {
								builder.Append("&");
							}
							if (_valueEncoders != null) {
								builder.Append(_valueEncoders.Encode(key)).Append("=").Append(_valueEncoders.Encode(val));
							} else {
								builder.Append(key).Append("=").Append(val);
							}
						}
					}
				}
			}
			return builder.ToString();
		}


		private readonly IEncoderChain _valueEncoders;
		private readonly IEncoderChain _paramEncoders;
		private readonly string _paramPrefix;

	}

}

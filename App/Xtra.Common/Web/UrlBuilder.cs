using System;

using Xtra.Common.Web.Encoder;


namespace Xtra.Common.Web
{

	public class UrlBuilder : UriBuilder
	{

		public new QueryStringDictionary Query { get { return _queryString; } }


		/// <summary>
		/// Gets or sets the name of the page.
		/// </summary>
		public string PageName {
			get {
				string path = base.Path;
				return path.Substring(path.LastIndexOf("/", StringComparison.Ordinal) + 1);
			}
			set {
				string path = base.Path;
				path = path.Substring(0, path.LastIndexOf("/", StringComparison.Ordinal));
				base.Path = string.Concat(path, "/", value);
			}
		}


		public UrlBuilder()
		{
			_queryString = new QueryStringDictionary(base.Query, _valueEncoders, _paramEncoders, _paramPrefix);
		}


		public UrlBuilder(string uri, IEncoderChain valueEncoders = null, IEncoderChain paramEncoders = null, string paramPrefix = "")
			: base(uri)
		{
			_valueEncoders = valueEncoders;
			_paramEncoders = paramEncoders;
			_paramPrefix = paramPrefix;
			_queryString = new QueryStringDictionary(base.Query, _valueEncoders, _paramEncoders, _paramPrefix);
		}


		public UrlBuilder(Uri uri, IEncoderChain valueEncoders = null, IEncoderChain paramEncoders = null, string paramPrefix = "")
			: base(uri)
		{
			_valueEncoders = valueEncoders;
			_paramEncoders = paramEncoders;
			_paramPrefix = paramPrefix;
			_queryString = new QueryStringDictionary(base.Query, _valueEncoders, _paramEncoders, _paramPrefix);
		}


		public UrlBuilder(System.Web.UI.Page page, IEncoderChain valueEncoders = null, IEncoderChain paramEncoders = null, string paramPrefix = "")
			: base(page.Request.Url)
		{
			_valueEncoders = valueEncoders;
			_paramEncoders = paramEncoders;
			_paramPrefix = paramPrefix;
			_queryString = new QueryStringDictionary(base.Query, _valueEncoders, _paramEncoders, _paramPrefix);
		}


		public UrlBuilder(string scheme, string host, int port = -1, string path = "", string extra = "", string query = "",
			IEncoderChain valueEncoders = null, IEncoderChain paramEncoders = null, string paramPrefix = "")
			: base(scheme, host, port, path, extra)
		{
			base.Query = query;
			_valueEncoders = valueEncoders;
			_paramEncoders = paramEncoders;
			_paramPrefix = paramPrefix;
			_queryString = new QueryStringDictionary(base.Query, _valueEncoders, _paramEncoders, _paramPrefix);
		}


		/// <summary>
		/// Format options:
		///	"e" is the default and returns the string encrypted using the specified encoders, or the default encoders if none specified.
		///	"p" always returns the string as plaintext.
		/// </summary>
		/// <returns>A string representation of the Uri</returns>
		public new string ToString()
		{
			return _ToString("e");
		}


		public string ToString(string format)
		{
			return _ToString(format);
		}


		private string _ToString(string format)
		{
			base.Query = _queryString.ToString(format);
			return base.ToString();
		}


		private readonly QueryStringDictionary _queryString;
		private readonly IEncoderChain _valueEncoders;
		private readonly IEncoderChain _paramEncoders;
		private readonly string _paramPrefix = "";

	}

}

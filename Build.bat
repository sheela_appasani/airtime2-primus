@echo off
if "%1"=="" goto rebuildCurrent

msbuild Airtime.sln /t:Build /p:Configuration=%1 /m
goto end

:rebuildCurrent
msbuild Airtime.sln /t:Build /m
goto end

:end

@echo off
if "%1"=="" goto cleanAll

msbuild Airtime.sln /m /t:Clean /p:Configuration=%1
goto end

:cleanAll
msbuild Airtime.sln /m /t:Clean /p:Configuration=Debug
msbuild Airtime.sln /m /t:Clean /p:Configuration=Test
msbuild Airtime.sln /m /t:Clean /p:Configuration=Stage
msbuild Airtime.sln /m /t:Clean /p:Configuration=Release
msbuild Airtime.sln /m /t:Clean /p:Configuration=PCMT
goto end

:end

@echo off

if /i "%1"=="Test" goto publishTest
if /i "%1"=="Stage" goto publishStage
if /i "%1"=="Live" goto publishLive
if /i "%1"=="Release" goto publishLive
goto errorNeedConfig


:publishTest
msbuild /m /p:Configuration=Test /p:DeployOnBuild=True /p:PublishProfile="Test (Package)" Airtime.sln
goto end


:publishStage
msbuild /m /p:Configuration=Stage /p:DeployOnBuild=True /p:PublishProfile="Stage (Package)" Airtime.sln
goto end


:publishLive
msbuild /m /p:Configuration=Release /p:DeployOnBuild=True /p:PublishProfile="Live (Package)" Airtime.sln
goto end


:errorNeedConfig
echo.
echo You must specify a valid build config. Commands:
echo.
echo package test
echo package stage
echo package live
goto end


:end

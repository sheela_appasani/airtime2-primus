@echo off

if /i "%1"=="Test" goto publishTest
if /i "%1"=="Stage" goto publishStage
if /i "%1"=="Live" goto publishLive
if /i "%1"=="Release" goto publishLive
goto errorNeedConfig


:publishTest
msbuild /m /p:Configuration=Test /p:DeployOnBuild=True /p:DeployTarget=MsDeployPublish /p:CreatePackageOnPublish=True /p:MSDeployPublishMethod=RemoteAgent /p:MSDeployServiceUrl=http://test.airtimesurveys.com /p:DeployIisAppPath="com.airtimesurveys.test" /p:UserName=Administrator /p:Password=UXbQk5i4COzazg Airtime.sln
goto end


:publishStage
msbuild /m /p:Configuration=Stage /p:DeployOnBuild=True /p:DeployTarget=MsDeployPublish /p:CreatePackageOnPublish=True /p:MSDeployPublishMethod=RemoteAgent /p:MSDeployServiceUrl=http://stage2.airtimesurveys.com /p:DeployIisAppPath="com.airtimesurveys.stage2" /p:UserName=Administrator /p:Password=UXbQk5i4COzazg Airtime.sln
goto end


:publishLive
msbuild /m /p:Configuration=Release /p:DeployOnBuild=True /p:DeployTarget=MsDeployPublish /p:CreatePackageOnPublish=True /p:MSDeployPublishMethod=RemoteAgent /p:MSDeployServiceUrl=http://admin.airtimesurveys.com /p:DeployIisAppPath="com.airtimesurveys.admin" /p:UserName=Administrator /p:Password=UXbQk5i4COzazg Airtime.sln
goto end


:errorNeedConfig
echo.
echo You must specify a valid build config. Commands:
echo.
echo publish test
echo publish stage
echo publish live
goto end


:end

@echo off
if "%1"=="" goto rebuildCurrent

msbuild Airtime.sln /t:Rebuild /p:Configuration=%1 /m
goto end

:rebuildCurrent
msbuild Airtime.sln /t:Rebuild /m
goto end

:end

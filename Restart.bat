@echo off
rem Touches the Web.config file so that IIS will automatically restart the web-app.
pushd %~dp0\App\Airtime.Web\
type nul >>Web.config & copy /y /v /b Web.config +,, > nul
popd

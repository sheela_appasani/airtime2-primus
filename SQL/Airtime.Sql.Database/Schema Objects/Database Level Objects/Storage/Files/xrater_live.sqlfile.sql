﻿ALTER DATABASE [$(DatabaseName)]
    ADD FILE (NAME = [xrater_live], FILENAME = '$(DefaultDataPath)$(DatabaseName).mdf', FILEGROWTH = 1024 KB) TO FILEGROUP [PRIMARY];


﻿create function [dbo].[CalculateAge](@dob datetime, @later datetime)
returns int
as begin
	return DATEDIFF(yy, @dob, @later) - CASE WHEN @later >= DATEADD(yy,DATEDIFF(yy,@dob,@later), @dob) THEN 0 ELSE 1 END
end

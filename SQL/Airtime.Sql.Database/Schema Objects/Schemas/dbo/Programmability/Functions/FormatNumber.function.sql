﻿create function [dbo].[FormatNumber](@value bigint)
returns varchar(20)
as begin
	return REPLACE(CONVERT(varchar, CAST(@value as money), 1), '.00', '')
end

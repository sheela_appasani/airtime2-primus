﻿create function dbo.GuidString(@string char(32))
returns uniqueidentifier
as begin
	return CAST(
		SUBSTRING(@string, 1, 8) + '-' +
		SUBSTRING(@string, 9, 4) + '-' +
		SUBSTRING(@string, 13, 4) + '-' +
		SUBSTRING(@string, 17, 4) + '-' +
		SUBSTRING(@string, 21, 12)
	as uniqueidentifier)
end

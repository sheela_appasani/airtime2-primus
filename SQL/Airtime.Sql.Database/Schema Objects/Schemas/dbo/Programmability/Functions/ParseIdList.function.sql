﻿create function [dbo].ParseIdList(@idList varchar(MAX))
returns
	@ParsedList table (Id int)
as
begin
	declare @id varchar(10)
	declare @pos int

	set @idList = LTRIM(RTRIM(@idList))+ ','
	set @pos = CHARINDEX(',', @idList, 1)

	if (REPLACE(@idList, ',', '') <> '') begin
		while (@pos > 0) begin
			set @id = LTRIM(RTRIM(LEFT(@idList, @pos - 1)))

			if (@id <> '') begin
				insert into @ParsedList (Id) values (CAST(@id AS int))
			end
			set @idList = RIGHT(@idList, LEN(@idList) - @pos)
			set @pos = CHARINDEX(',', @idList, 1)
		end
	end

	return
end

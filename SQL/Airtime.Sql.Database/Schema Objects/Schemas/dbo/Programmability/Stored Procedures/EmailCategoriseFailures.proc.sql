﻿CREATE PROCEDURE [dbo].[EmailCategoriseFailures]
AS
begin
	set nocount on

	update  xr_email_recipient
	set     smtpfailreason='Cancel'
	where   smtpfailreason is null
	and     deliverystatus='Cancel'

	update  xr_email_recipient
	set     smtpfailreason='Quota'
	where   smtpfailreason is null
	and     deliverystatus='Fail'
	and     (
	        smtpstatusdescription like '%mailbox is full%'
	        or smtpstatusdescription like '%quota%'
	        or smtpstatusdescription like '%insufficient system storage%'
	)

	update  xr_email_recipient
	set     smtpfailreason='Spam'
	where   smtpfailreason is null
	and     deliverystatus='Fail'
	and     (
	        smtpstatusdescription like '%spam%'
	        or smtpstatusdescription like '%unsolicited%'
	        or smtpstatusdescription like '%content restrictions%'
	        or smtpstatusdescription like '%amount of html tags%'
	        or smtpstatusdescription like '%content rejected%'
	        or smtpstatusdescription like '%571 Message Refused%'
	        or smtpstatusdescription like '550 Blocked%'
	)

	update  xr_email_recipient
	set     smtpfailreason='Sender'
	where   smtpfailreason is null
	and     deliverystatus='Fail'
	and     (
	        smtpstatusdescription like '%requires PTR %'
	        or smtpstatusdescription like '%203.27.127%'
	)

	update  xr_email_recipient
	set     smtpfailreason='NoDomain'
	where   smtpfailreason is null
	and     deliverystatus='Fail'
	and     (
	        smtpstatusdescription='Recipient domain does not exist'
			or smtpstatusdescription like '%sorry, that domain isn''t in my list of allowed rcpthosts%'
	        or smtpstatusdescription like '%relaying denied%'
	)

	update  xr_email_recipient
	set     smtpfailreason='HardBounce'
	where   smtpfailreason is null
	and     deliverystatus='Fail'

end

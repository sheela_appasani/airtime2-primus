﻿CREATE PROCEDURE [dbo].[EmailStop]
      @emailid int
AS
begin
	update	xr_email_recipient
	set		deliverystatus='Cancel'
	where	deliverystatus='Pending'
	and		emailid = @emailid
end

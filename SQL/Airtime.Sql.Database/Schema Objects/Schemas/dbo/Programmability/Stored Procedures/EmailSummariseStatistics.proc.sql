﻿CREATE procedure [dbo].[EmailSummariseStatistics]
as
begin
	set nocount on

	set deadlock_priority low

	declare @latestDate datetime
	set @latestDate = DATEADD(dd,-60,GETDATE())

	while 1=1 begin
		begin try
			declare @emailId int

			select top 1 @emailId = e.id
			from xr_email e
			inner join xr_email_recipient er on er.emailId = e.id
			where
				--Summarise sent/cancelled emails, but if it has an end-date leave it alone for a period afterward
				(e.status = 'Sent' or e.status = 'Cancelled') 
				and (sendFinish is null or (sendFinish is not null and sendFinish < @latestDate))
				and e.summarised = 'N'
				and (
					(
						--Summarise old channel emails - but not yet ones without an end-date
						channelId > 0 and surveyId is null and sendFinish is not null
					) or (
						--Summarise old survey emails where the survey has ended or been deleted
						surveyId > 0 and surveyId in (
							select id from xr_survey where enddate < @latestDate or isDeleted > 0
						)
					)
				)
			group by e.id, e.sendAfter
			order by e.sendAfter, e.id

			if @@ROWCOUNT = 0 begin
				break
			end

			print 'Summarising email: ' + CAST(@emailId as varchar)

			begin transaction

			update xr_email set
				statsScheduled = tempEmailStats.Scheduled,
				statsDelivered = tempEmailStats.Delivered,
				statsFailed = tempEmailStats.Failed,
				statsCancelled = tempEmailStats.Cancelled,
				failedHardBounce = tempEmailStats.FailedHardBounce,
				failedNoDomain = tempEmailStats.FailedNoDomain,
				failedQuota = tempEmailStats.FailedQuota,
				failedSender = tempEmailStats.FailedSender,
				failedSpam = tempEmailStats.FailedSpam,
				summarised = 'Y'
			from xr_email e
			inner join (
				select
					emailId,
					count(*) as Scheduled,
					sum(case when er.deliveryStatus = 'Sent' then 1 else 0 end) as Delivered,
					sum(case when er.deliveryStatus = 'Fail' then 1 else 0 end) as Failed,
					sum(case when er.deliveryStatus = 'Cancel' then 1 else 0 end) as Cancelled,
					sum(case when er.smtpFailReason = 'HardBounce' then 1 else 0 end) as FailedHardBounce,
					sum(case when er.smtpFailReason = 'NoDomain' then 1 else 0 end) as FailedNoDomain,
					sum(case when er.smtpFailReason = 'Quota' then 1 else 0 end) as FailedQuota,
					sum(case when er.smtpFailReason = 'Sender' then 1 else 0 end) as FailedSender,
					sum(case when er.smtpFailReason = 'Spam' then 1 else 0 end) as FailedSpam
				from xr_email_recipient er
				where emailId = @emailId
				group by emailId
			) tempEmailStats on tempEmailStats.emailId = e.id

			delete from xr_email_recipient where emailId = @emailId

			commit transaction
		end try
		begin catch
			if @@TRANCOUNT > 0 begin
				rollback transaction
			end

			declare @errMsg nvarchar(4000)
			declare @errSeverity int
			select @errMsg = ERROR_MESSAGE(), @errSeverity = ERROR_SEVERITY()

			raiserror(@errMsg, @errSeverity, 1)

			return
		end catch
	end


	update xr_email set
		statsScheduled = tempEmailStats.Scheduled,
		statsDelivered = tempEmailStats.Delivered,
		statsFailed = tempEmailStats.Failed,
		statsCancelled = tempEmailStats.Cancelled
	from xr_email e
	inner join (
		select
			emailId,
			count(*) as Scheduled,
			sum(case when er.deliveryStatus = 'Sent' then 1 else 0 end) as Delivered,
			sum(case when er.deliveryStatus = 'Fail' then 1 else 0 end) as Failed,
			sum(case when er.deliveryStatus = 'Cancel' then 1 else 0 end) as Cancelled
		from xr_email_recipient er
		group by emailId
	) tempEmailStats on tempEmailStats.emailId = e.id
	where
		e.summarised = 'N'
		and (
			tempEmailStats.Scheduled > e.statsScheduled
			or tempEmailStats.Delivered > e.statsDelivered
			or tempEmailStats.Failed > e.statsFailed
			or tempEmailStats.Cancelled > e.statsCancelled
		)


	if exists (select top 1 1 from xr_survey where isDeleted > 0) begin
		update xr_question
		set songId = null
		where
			surveyId in (select id from xr_survey where isDeleted > 0)
			and songId is not null


		delete from xr_question_song
		where questionId in (
			select q.Id from xr_question q
			inner join xr_question_song qs on q.Id = qs.questionId
			where 
				q.surveyId in (select id from xr_survey where isDeleted > 0)
				and q.questionType = 'AUDIOPOD'
		)
	end
end

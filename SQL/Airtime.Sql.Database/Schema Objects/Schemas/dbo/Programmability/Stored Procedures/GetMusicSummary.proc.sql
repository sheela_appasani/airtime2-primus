﻿create procedure [dbo].[GetMusicSummary]
	@surveyIds varchar(MAX) = null,
	@choiceIds varchar(MAX) = null,
	@gender char(1) = null,
	@lowerAge int = null,
	@upperAge int = null
as
begin
	set nocount on

	declare @_surveyIds varchar(MAX) = @surveyIds
	declare @_choiceIds varchar(MAX) = @choiceIds
	declare @_gender char(1) = @gender
	declare @_lowerAge int = @lowerAge
	declare @_upperAge int = @upperAge

	declare @now datetime
	set @now = GETDATE()

	declare @surveys table (surveyId int)
	insert into @surveys (surveyId)
		select id from dbo.ParseIdList(@_surveyIds)

	declare @choices table (choiceId int, questionId int)
	insert into @choices (choiceId, questionId)
		select r.id, r.questionId
		from xr_response r
		inner join dbo.ParseIdList(@_choiceIds) choices on choices.Id = r.id
		order by r.questionId, r.id

	declare @numQuestions int
	select @numQuestions = count(distinct questionId) from @choices

	declare @ratings table (songId int, userId int, finalRating float, burn varchar(10))
	insert into @ratings (songId, userId, finalRating, burn)
		select
			qar.songId as songId, u.id as userId, CAST(qar.finalRating as float) as finalRating, qar.burn
		from xr_audiopod_answer_rates qar
		inner join xr_question_answer qa on qa.id = qar.answerId
		inner join xr_survey_respondent sr on sr.id = qa.surveyRespondentId and sr.completed = 'Y'
		inner join xr_user u on u.id = sr.userId
		inner join xr_person p on p.id = u.personId
		where 
			qa.answerType = 'AudioPod'
			and sr.surveyId in (select surveyId from @surveys)
			and (@_gender is null or (@_gender is not null and @_gender = p.gender))
			and (@_lowerAge is null or (@_lowerAge is not null and @_lowerAge <= dbo.CalculateAge(p.dateOfBirth, @now)))
			and (@_upperAge is null or (@_upperAge is not null and @_upperAge >= dbo.CalculateAge(p.dateOfBirth, @now)))
			and (@numQuestions = 0 or (@numQuestions <> 0 and u.id in (
				select userId
				from xr_question_answer qar
				where qar.responseId in (
					select choiceId from @choices
				)
				group by userId
				having count(distinct questionId) = @numQuestions
			)))


	select
		r.songId, s.artist, s.title, s.filename,
		round(avg(finalRating), 2) as [rawAverage],
		(select count(1) from @ratings where songId = r.songId and burn = 'Tired') as lowBurnTotal,
		(select count(1) from @ratings where songId = r.songId and burn = 'VeryTired') as highBurnTotal,
		(select count(1) from @ratings where songId = r.songId and finalRating between 81 and 100) as loveTotal,
		(select count(1) from @ratings where songId = r.songId and finalRating between 61 and 80) as likeTotal,
		(select count(1) from @ratings where songId = r.songId and finalRating between 41 and 60) as okayTotal,
		(select count(1) from @ratings where songId = r.songId and finalRating between 21 and 40) as dislikeTotal,
		(select count(1) from @ratings where songId = r.songId and finalRating between 1 and 20) as hateTotal,
		(select count(distinct userId) from @ratings where songId = r.songId) as userTotal
	from @ratings r
	inner join xr_song s on s.id = r.songId
	group by r.songId, s.artist, s.title, s.filename
	order by avg(r.finalRating) desc
end

﻿create procedure [dbo].[GetReportMultiChoice]
	@surveyIds varchar(MAX) = null,
	@choiceIds varchar(MAX) = null,
	@gender char(1) = null,
	@lowerAge int = null,
	@upperAge int = null
as
begin
	set nocount on

	declare @now datetime = GETDATE()

	declare @surveys table (surveyId int)
	insert into @surveys (surveyId)
		select id from dbo.ParseIdList(@surveyIds)

	declare @choices table (choiceId int, questionId int)
	insert into @choices (choiceId, questionId)
		select r.id, r.questionId
		from xr_response r
		inner join dbo.ParseIdList(@choiceIds) choices on choices.Id = r.id
		order by r.questionId, r.id

	declare @numQuestions int
	select @numQuestions = count(distinct questionId) from @choices

	declare @questionTotals table (questionId int, totalUsers int)
	insert into @questionTotals (questionId, totalUsers)
		select
			q.id as questionId,
			count(distinct p.id) as totalUsers
		from xr_question q
		inner join xr_response r on r.questionId = q.id
		inner join xr_survey s on s.id = q.surveyId
		left join xr_question_answer qa on qa.questionId = q.id and qa.responseId = r.id
		left join xr_user u on u.id = qa.userid
			and u.role = 'Respondent'
			and (@numQuestions = 0 or (@numQuestions <> 0 and u.id in (
				select userId
				from xr_question_answer qar
				where qar.responseId in (
					select choiceId from @choices
				)
				group by userId
				having count(distinct questionId) = @numQuestions
			)))
		left join xr_person p on p.id = u.personId
			and (@gender is null or (@gender is not null and @gender = p.gender))
			and (@lowerAge is null or (@lowerAge is not null and @lowerAge <= dbo.CalculateAge(p.dateOfBirth, @now)))
			and (@upperAge is null or (@upperAge is not null and @upperAge >= dbo.CalculateAge(p.dateOfBirth, @now)))
		left join xr_survey_respondent sr on sr.id = qa.surveyRespondentId
		where
			q.surveyId in (select surveyId from @surveys)
			and q.questionCategory = 'Normal'
			and q.questionType in ('RADIO', 'CHECKBOX', 'AUDIO')
			and (sr.completed is null or sr.completed = 'Y')
		group by q.id

	select
		results.questionId, results.responseId, results.responseName, sum(results.answered) as selectedTotal, qt.totalUsers as userTotal
	from (
		select
			s.startDate,
			q.id as questionId, q.ordering as questionPos, r.id as responseId, r.ordering as responsePos,
			r.name as responseName,
			CASE WHEN p.id is null THEN 0 ELSE 1 END as answered,
			u.id as userId
		from xr_question q
		inner join xr_response r on r.questionId = q.id
		inner join xr_survey s on s.id = q.surveyId
		left join xr_question_answer qa on qa.questionId = q.id and qa.responseId = r.id
		left join xr_user u on u.id = qa.userid
			and u.role = 'Respondent'
			and (@numQuestions = 0 or (@numQuestions <> 0 and u.id in (
				select userId
				from xr_question_answer qar
				where qar.responseId in (
					select choiceId from @choices
				)
				group by userId
				having count(distinct questionId) = @numQuestions
			)))
		left join xr_person p on p.id = u.personId
			and (@gender is null or (@gender is not null and @gender = p.gender))
			and (@lowerAge is null or (@lowerAge is not null and @lowerAge <= dbo.CalculateAge(p.dateOfBirth, @now)))
			and (@upperAge is null or (@upperAge is not null and @upperAge >= dbo.CalculateAge(p.dateOfBirth, @now)))
		left join xr_survey_respondent sr on sr.id = qa.surveyRespondentId
		where
			q.surveyId in (select surveyId from @surveys)
			and q.questionCategory = 'Normal'
			and q.questionType in ('RADIO', 'CHECKBOX', 'AUDIO')
			and (sr.completed is null or sr.completed = 'Y')
	) results
	left join @questionTotals qt on qt.questionId = results.questionId
	group by results.startDate, results.questionId, results.questionPos, results.responseId, results.responsePos, results.responseName, qt.totalUsers 
	order by startDate, questionPos, responsePos
end

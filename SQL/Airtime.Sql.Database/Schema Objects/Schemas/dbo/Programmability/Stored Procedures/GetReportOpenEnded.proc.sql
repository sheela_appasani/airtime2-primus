﻿create procedure [dbo].[GetReportOpenEnded]
	@surveyIds varchar(MAX) = null,
	@choiceIds varchar(MAX) = null,
	@gender char(1) = null,
	@lowerAge int = null,
	@upperAge int = null
as
begin
	set nocount on

	declare @now datetime
	set @now = GETDATE()

	declare @surveys table (surveyId int)
	insert into @surveys (surveyId)
		select id from dbo.ParseIdList(@surveyIds)

	declare @choices table (choiceId int, questionId int)
	insert into @choices (choiceId, questionId)
		select r.id, r.questionId
		from xr_response r
		inner join dbo.ParseIdList(@choiceIds) choices on choices.Id = r.id
		order by r.questionId, r.id

	declare @numQuestions int
	select @numQuestions = count(distinct questionId) from @choices

	select
		q.id as questionId,
		qa.answer as answer,
		u.id as userId, p.givenName, p.familyName, p.email, p.gender, dbo.CalculateAge(p.dateOfBirth, @now) as age
	from xr_question_answer qa
	inner join xr_survey_respondent sr on sr.id = qa.surveyRespondentId and sr.completed = 'Y'
	inner join xr_user u on u.id = qa.userid
	inner join xr_person p on p.id = u.personId
	inner join xr_question q on q.id = qa.questionId
	inner join xr_survey s on s.id = q.surveyId
	where
		q.surveyId in (select surveyId from @surveys)
		and qa.answerType = 'Common'
		and q.questionCategory = 'Normal'
		and q.questionType = 'TEXT'
		and (@gender is null or (@gender is not null and @gender = p.gender))
		and (@lowerAge is null or (@lowerAge is not null and @lowerAge <= dbo.CalculateAge(p.dateOfBirth, @now)))
		and (@upperAge is null or (@upperAge is not null and @upperAge >= dbo.CalculateAge(p.dateOfBirth, @now)))
		and (@numQuestions = 0 or (@numQuestions <> 0 and u.id in (
			select userId
			from xr_question_answer qar
			where qar.responseId in (
				select choiceId from @choices
			)
			group by userId
			having count(distinct questionId) = @numQuestions
		)))	
	order by s.startDate, q.ordering, qa.id
end

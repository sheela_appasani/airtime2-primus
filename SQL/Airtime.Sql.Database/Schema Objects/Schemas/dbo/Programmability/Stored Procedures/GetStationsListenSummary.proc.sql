﻿create procedure [dbo].[GetStationsListenSummary]
	@userId int = null,
	@channelId int = null
as
begin
	set nocount on

	declare @uId int = @userId
	declare @cId int = @channelId

	declare @stats table (
		Name nvarchar(255),
		Counts int
	)

	insert into @stats (Name, Counts)
		select
			Name, count(1) as Counts
		from (
			select
				r.Name, row_number() over (partition by qa.userId, qa.responseId order by qa.id desc) as RowNum
			from xr_question q
			inner join xr_response r on q.id = r.questionId
			inner join xr_question_answer qa on qa.responseId = r.Id
			inner join xr_user u on u.id = qa.userId
			where 
				q.channelId = @cId
				and u.isEnabled = 'Y'
				and u.role = 'Respondent'
				and q.questionCategory = 'Registration_StationsListen'
				and q.surveyId is null
		) as Answers
		where RowNum = 1
		group by Name

	declare @total int
	select @total = (
		select
			count(distinct u.id)
		from xr_question q
		inner join xr_response r on q.id = r.questionId
		inner join xr_question_answer qa on qa.responseId = r.Id
		inner join xr_user u on u.id = qa.userId
		where 
			(@cId is null or (@cId is not null and q.channelId = @cId))
			and (
				@uId is null or (@uId is not null and q.channelId in (
					select c.id
					from xr_channel c
					inner join xr_user_channel uc on uc.channelId = c.id
					where uc.userId = @uId
				))
			)
			and u.isEnabled = 'Y'
			and u.role = 'Respondent'
			and q.questionCategory = 'Registration_StationsListen'
			and q.surveyId is null
	)

	select *, @total as Total
	from @stats
	order by
		case when Name = 'Other' then 1 else 0 end,
		Counts desc, Name
end

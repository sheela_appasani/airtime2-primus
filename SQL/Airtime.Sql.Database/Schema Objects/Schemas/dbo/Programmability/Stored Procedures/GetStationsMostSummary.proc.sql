﻿create procedure [dbo].[GetStationsMostSummary]
	@userId int = null,
	@channelId int = null
as
begin
	set nocount on

	declare @uId int = @userId
	declare @cId int = @channelId

	declare @stats table (
		Name nvarchar(255),
		Counts int
	)

	insert into @stats (Name, Counts)
		select
			r.Name, count(1) as Counts
		from xr_question q
		inner join xr_response r on q.id = r.questionId
		inner join xr_question_answer qa on qa.responseId = r.Id
		inner join xr_user u on u.id = qa.userId
		where 
			(@cId is null or (@cId is not null and q.channelId = @cId))
			and (
				@uId is null or (@uId is not null and q.channelId in (
					select c.id
					from xr_channel c
					inner join xr_user_channel uc on uc.channelId = c.id
					where uc.userId = @uId
				))
			)
			and u.isEnabled = 'Y'
			and u.role = 'Respondent'
			and q.questionCategory = 'Registration_StationMost'
			and q.surveyId is null
		group by r.Name

	declare @total int
	select @total = SUM(Counts) from @stats

	select *, @total as Total
	from @stats
	order by
		case when Name = 'Other' then 1 else 0 end,
		Counts desc, Name
end

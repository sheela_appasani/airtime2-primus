﻿CREATE procedure [dbo].[PopulateSurveyRespondents]
as
begin
	set nocount on

	set deadlock_priority low

	while 1=1 begin
		begin try
			declare @surveyId int

			select top 1 @surveyId = s.id
			from xr_survey s
			where
				s.isPopulated = 'N'
			order by s.startDate, s.id

			if @@ROWCOUNT = 0 begin
				break
			end

			print 'Populating survey: ' + CAST(@surveyId as varchar)

			begin transaction

			insert into xr_survey_respondent (surveyId, userId, isEnabled)
				select @surveyId as surveyId, u.id as userId, 'Y' as isEnabled
				from xr_user u
				inner join xr_user_channel uc on uc.userId = u.id
				inner join xr_survey s on s.channelId = uc.channelId
				where u.isEnabled = 'Y'
					and u.role = 'Respondent' 
					and u.id not in (select userId from xr_survey_respondent where surveyId = @surveyId)
					and s.id = @surveyId

			update xr_survey set isPopulated = 'Y', dateModified = GETDATE() where id = @surveyId

			commit transaction
		end try
		begin catch
			if @@TRANCOUNT > 0 begin
				rollback transaction
			end

			declare @errMsg nvarchar(4000)
			declare @errSeverity int
			select @errMsg = ERROR_MESSAGE(), @errSeverity = ERROR_SEVERITY()

			raiserror(@errMsg, @errSeverity, 1)

			return
		end catch
	end

end

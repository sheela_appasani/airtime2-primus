﻿create procedure [dbo].[PurgeChannel]
	@channelId int
as
begin
	set deadlock_priority low
	set nocount on

	if (@channelId is null) begin
		print 'Parameter @channelId must be supplied'
		return
	end

	if not exists (select top 1 1 from xr_channel where id = @channelId) begin
		print 'Channel (' + CAST(@channelId as varchar(10)) + ') not found'
		return
	end

	declare @channelTitle nvarchar(255) = (select title from xr_channel where id = @channelId)

	begin try

		print 'Deleting channel: (' + CAST(@channelId as varchar) + ') ' + @channelTitle

		begin transaction

		delete from xr_app_channel where channelId = @channelId
		delete from xr_content where channelId = @channelId
		delete from xr_email_detail where id in (
			select criteriaId from xr_email where channelId = @channelId
		)
		delete from xr_email_recipient where emailId in (
			select id from xr_email where channelId = @channelId
		)
		delete from xr_email where channelId = @channelId

		delete from xr_user_channel where channelId = @channelId

		delete from xr_report_channel_cache_stat where reportChannelCacheId in (
			select id from xr_report_channel_cache where channelId = @channelId
		)

		delete from xr_report_channel_cache where channelId = @channelId

		while 1=1 begin
			declare @surveyName nvarchar(255)
			declare @surveyId int

			select top 1 @surveyId = id, @surveyName = name
			from xr_survey
			where channelId = @channelId
			order by id

			if @@ROWCOUNT = 0 begin
				break
			end

			print 'Deleting survey: (' + CAST(@surveyId as varchar) + ') ' + @surveyName


			delete from xr_content where surveyId = @surveyId

			delete from xr_email_recipient where id in (
				select id from xr_email where surveyId = @surveyId
			)

			delete from xr_email_detail where id in (
				select criteriaId from xr_email where surveyId = @surveyId
			)

			delete from xr_email where surveyId = @surveyId

			delete from xr_audiopod_rate where reportId in (
				select id from xr_report_summary where questionId in (
					select id from xr_question where surveyId = @surveyId
				)
			)

			delete from xr_audiopod_answer_rates where answerId in (
				select id from xr_question_answer where questionId in (
					select id from xr_question where surveyId = @surveyId
				)
			)

			delete from xr_question_answer_report where answerId in (
				select id from xr_question_answer where questionId in (
					select id from xr_question where surveyId = @surveyId
				)
			)

			delete from xr_report_summary where questionId in (
				select id from xr_question where surveyId = @surveyId
			)

			delete from xr_question_answer where questionId in (
				select id from xr_question where surveyId = @surveyId
			)

			delete from xr_group_response where groupId in (
				select id from xr_group where surveyId = @surveyId
			)

			delete from xr_group where surveyId = @surveyId

			delete from xr_question_song where questionId in (
				select id from xr_question where surveyId = @surveyId
			)

			delete from xr_response where questionId in (
				select id from xr_question where surveyId = @surveyId
			)

			delete from xr_question_respondent where questionId in (
				select id from xr_question where surveyId = @surveyId
			)

			delete from xr_sms_recipient where surveyRespondentId in (
				select id from xr_survey_respondent where surveyId = @surveyId
			)

			delete from xr_payment_detail where surveyRespondentId in (
				select id from xr_survey_respondent where surveyId = @surveyId
			)

			delete from xr_payment_field where surveyId = @surveyId

			delete from xr_survey_respondent where surveyId = @surveyId

			delete from xr_survey_recruiter where surveyId = @surveyId

			delete from xr_recruitment_company where recruitmentTestId = @surveyId

			delete from xr_recruit_respondent where recruitTestId = @surveyId

			delete from xr_recruit_answer where questionId in (
				select id from xr_question where surveyId = @surveyId
			)

			delete from xr_question_action where QuestionId in (
				select id from xr_question where surveyId = @surveyId
			)

			delete from xr_question_action where GoToQuestionId in (
				select id from xr_question where surveyId = @surveyId
			)

			delete from xr_question where surveyId = @surveyId

			delete from xr_referfriend where surveyId = @surveyId

			delete from xr_survey_alert where surveyId = @surveyId

			delete from xr_survey where id = @surveyId
		end

		delete from xr_channel where id = @channelId

		commit transaction

		print 'Completed'

	end try
	begin catch
		if @@TRANCOUNT > 0 begin
			rollback transaction
		end

		declare @errMsg nvarchar(4000)
		declare @errSeverity int
		select @errMsg = ERROR_MESSAGE(), @errSeverity = ERROR_SEVERITY()

		raiserror(@errMsg, @errSeverity, 1)

		return
	end catch
end

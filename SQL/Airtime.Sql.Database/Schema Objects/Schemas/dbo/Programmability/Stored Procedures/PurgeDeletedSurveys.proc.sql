﻿create procedure [dbo].[PurgeDeletedSurveys]
as
begin
	set deadlock_priority low
	set nocount on

	while 1=1 begin
		begin try
			declare @surveyId int
			declare @surveyName varchar(255)

			select top 1 @surveyId = id, @surveyName = name
			from xr_survey
			where isDeleted > 0
			order by id

			if @@ROWCOUNT = 0 begin
				break
			end

			print 'Purging survey: (' + CAST(@surveyId as varchar) + ') ' + @surveyName

			begin transaction

				delete from xr_content where surveyId = @surveyId

				delete from xr_email_recipient where id in (
					select id from xr_email where surveyId = @surveyId
				)

				delete from xr_email_detail where id in (
					select criteriaId from xr_email where surveyId = @surveyId
				)

				delete from xr_email where surveyId = @surveyId

				delete from xr_audiopod_rate where reportId in (
					select id from xr_report_summary where questionId in (
						select id from xr_question where surveyId = @surveyId
					)
				)

				delete from xr_audiopod_answer_rates where answerId in (
					select id from xr_question_answer where questionId in (
						select id from xr_question where surveyId = @surveyId
					)
				)

				delete from xr_question_answer_report where answerId in (
					select id from xr_question_answer where questionId in (
						select id from xr_question where surveyId = @surveyId
					)
				)

				delete from xr_report_summary where questionId in (
					select id from xr_question where surveyId = @surveyId
				)

				delete from xr_question_answer where questionId in (
					select id from xr_question where surveyId = @surveyId
				)

				delete from xr_group_response where groupId in (
					select id from xr_group where surveyId = @surveyId
				)

				delete from xr_group where surveyId = @surveyId

				delete from xr_question_song where questionId in (
					select id from xr_question where surveyId = @surveyId
				)

				delete from xr_response where questionId in (
					select id from xr_question where surveyId = @surveyId
				)

				delete from xr_question_respondent where questionId in (
					select id from xr_question where surveyId = @surveyId
				)

				delete from xr_sms_recipient where surveyRespondentId in (
					select id from xr_survey_respondent where surveyId = @surveyId
				)

				delete from xr_payment_detail where surveyRespondentId in (
					select id from xr_survey_respondent where surveyId = @surveyId
				)

				delete from xr_payment_field where surveyId = @surveyId

				delete from xr_survey_respondent where surveyId = @surveyId

				delete from xr_survey_recruiter where surveyId = @surveyId

				delete from xr_recruitment_company where recruitmentTestId = @surveyId

				delete from xr_recruit_respondent where recruitTestId = @surveyId

				delete from xr_recruit_answer where questionId in (
					select id from xr_question where surveyId = @surveyId
				)

				delete from xr_question_action where QuestionId in (
					select id from xr_question where surveyId = @surveyId
				)

				delete from xr_question_action where GoToQuestionId in (
					select id from xr_question where surveyId = @surveyId
				)

				delete from xr_question where surveyId = @surveyId

				delete from xr_referfriend where surveyId = @surveyId

				delete from xr_survey_alert where surveyId = @surveyId

				delete from xr_survey where id = @surveyId

			commit transaction
		end try
		begin catch
			if @@TRANCOUNT > 0 begin
				rollback transaction
			end

			declare @errMsg nvarchar(4000)
			declare @errSeverity int
			select @errMsg = ERROR_MESSAGE(), @errSeverity = ERROR_SEVERITY()

			raiserror(@errMsg, @errSeverity, 1)

			return
		end catch
	end
end

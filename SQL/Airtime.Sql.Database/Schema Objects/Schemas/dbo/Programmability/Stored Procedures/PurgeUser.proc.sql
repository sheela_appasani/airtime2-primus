﻿create procedure [dbo].[PurgeUser]
	@Id int = null,
	@Email varchar(255) = null
as begin
	if (@Id > 0) begin
		update xr_user set
			isEnabled = 'N',
			membershipStatus = 'Wiped',
			username = 'purged_user',
			password = '',
			firstName = '',
			lastName = '',
			email = 'purged@user.xtr',
			daytimePhone = null,
			mobilePhone = null,
			address = '',
			externalToken = null,
			dateModified = GETDATE()
		where id = @Id
	end
	if (@Email is not null) begin
		update xr_user set
			isEnabled = 'N',
			membershipStatus = 'Wiped',
			username = 'purged_user',
			password = '',
			firstName = '',
			lastName = '',
			email = 'purged@user.xtr',
			daytimePhone = null,
			mobilePhone = null,
			address = '',
			externalToken = null,
			dateModified = GETDATE()
		where email = @Email
	end
end

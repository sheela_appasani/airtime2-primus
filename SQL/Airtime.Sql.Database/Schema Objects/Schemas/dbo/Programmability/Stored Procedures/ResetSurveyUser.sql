﻿create procedure [dbo].[ResetSurveyUser]
	@userId int,
	@surveyId int
as
begin
	set nocount on

	declare @surveyRespondentId int = (select top 1 id from xr_survey_respondent where userId = @userId and surveyId = @surveyId)

	delete from xr_question_answer_report where answerId in (select id from xr_question_answer where surveyRespondentId = @surveyRespondentId)
	delete from xr_audiopod_answer_rates where answerId in (select id from xr_question_answer where surveyRespondentId = @surveyRespondentId)
	delete from xr_question_answer where surveyRespondentId = @surveyRespondentId
	update xr_survey_respondent set completed = 'N', currentQuestionId = null where id = @surveyRespondentId
end

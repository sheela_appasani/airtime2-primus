﻿CREATE procedure [dbo].[UpdateActiveUsers]
as begin
	set nocount on

	declare @now datetime = GETDATE()

	--disable users who are outside of their active dates
	update xr_user set
		isEnabled = 'N',
		membershipStatus = 'Expired'
	where
		isEnabled = 'Y' and (
			@now < activeFrom or DATEADD(d, 1, activeTo) <= @now
		) and membershipStatus in ('Active', 'Suspended')

	--enable users who are inside of their active dates
	update xr_user set
		isEnabled = 'Y',
		membershipStatus = 'Active'
	where
		isEnabled = 'N' and (
			(activeFrom <= @now and activeTo is null) or
			(activeFrom is null and @now < DATEADD(d, 1, activeTo)) or
			(activeFrom <= @now and @now < DATEADD(d, 1, activeTo))
		) and membershipStatus in ('Unconfirmed', 'Suspended', 'Expired')

	exec UpdateInvitedCounts
end

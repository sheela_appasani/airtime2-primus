﻿create procedure UpdateComplaints
as begin
	update xr_complaints_token set userid = u.id from xr_complaints_token c inner join xr_user u on u.token = c.token where c.userId is null
	update xr_complaints_token set userid = sr.userId from xr_complaints_token c inner join xr_survey_respondent sr on sr.token = c.token where c.userId is null
end

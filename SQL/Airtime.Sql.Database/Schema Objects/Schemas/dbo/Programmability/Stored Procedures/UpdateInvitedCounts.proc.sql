﻿create procedure [dbo].[UpdateInvitedCounts]
as begin
	set nocount on

	update xr_survey_respondent set
		invited = 'Y'
	from xr_survey_respondent sr
	inner join xr_email e on e.surveyId = sr.surveyId
	inner join xr_email_recipient er on er.emailId = e.id and er.userId = sr.userId
	where
		sr.invited = 'N'
		and e.status = 'Sending'
		and e.emailType = 'SurveyEmail'
		and e.summarised = 'N'

	update xr_mailinglist_user set 
		dateLastMailed = er.dateCreated
	from xr_mailinglist_user mlu
	inner join xr_email_recipient er on er.userId = mlu.userId
	inner join xr_email e on e.id = er.emailId
	where 
		mlu.mailinglistId = 1
		and e.status = 'Sending'
		and e.emailType = 'SurveyEmail'
		and e.summarised = 'N'
		and (er.dateCreated > mlu.dateLastMailed or mlu.dateLastMailed is null)

	update xr_survey
	set invited = stats.userCount
	from (
		select e.surveyId, COUNT(distinct er.userId) as userCount
		from xr_email e
		inner join xr_email_recipient er on er.emailid = e.id 
		inner join xr_survey s on s.id = e.surveyId
		where 
			e.surveyId > 0 
			and e.status <> 'Pending'
			and er.userid is not null 
			and s.isDeleted = 0 
			and s.startdate >= '2010-08-25 18:02:52.150' 
			and s.enddate > GETUTCDATE()
		group by e.surveyId
	) stats
	where
		stats.surveyId = xr_survey.id
		and stats.userCount > xr_survey.invited

	update xr_survey
	set
		completed = (select count(1) from xr_survey_respondent sr where sr.surveyId = s.id and sr.completed = 'Y'),
		iosCompleted = (select count(1) from xr_survey_respondent sr where sr.surveyId = s.id and sr.completed = 'Y' and sr.usedApp = 'iOS'),
		androidCompleted = (select count(1) from xr_survey_respondent sr where sr.surveyId = s.id and sr.completed = 'Y' and sr.usedApp = 'Android')
	from xr_survey s
	where
		s.isDeleted = 0 and s.isArchived = 'N' and
		(s.completed <> (select count(1) from xr_survey_respondent sr where sr.surveyId = s.id and sr.completed = 'Y'))

	update xr_survey
	set
		registered = (select count(1) from xr_survey_respondent sr where sr.surveyId = s.id and sr.registered = 'Y')
	from xr_survey s
	where 
		s.isDeleted = 0 and s.isArchived = 'N' and
		(s.registered <> (select count(1) from xr_survey_respondent sr where sr.surveyId = s.id and sr.registered = 'Y'))
end

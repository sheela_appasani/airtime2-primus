﻿create procedure UpdateRecipientsDateLastMailed
	@emailId int,
	@mailingListId int
as
begin
	set nocount on

	update xr_mailinglist_user set 
		dateLastMailed = er.dateCreated
	from xr_mailinglist_user mlu
	inner join xr_email_recipient er on er.userId = mlu.userId
	where 
		mlu.mailinglistId = @mailingListId
		and er.emailid = @emailId
		and (mlu.dateLastMailed is null or mlu.dateLastMailed < er.dateCreated)
end

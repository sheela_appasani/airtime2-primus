﻿ALTER TABLE [dbo].[xr_person]
	ADD CONSTRAINT [DF_xr_person_dateCreated]
	DEFAULT (GETDATE())
	FOR [dateCreated]

﻿ALTER TABLE [dbo].[xr_person]
	ADD CONSTRAINT [DF_xr_person_dateModified]
	DEFAULT (GETDATE())
	FOR [dateModified]

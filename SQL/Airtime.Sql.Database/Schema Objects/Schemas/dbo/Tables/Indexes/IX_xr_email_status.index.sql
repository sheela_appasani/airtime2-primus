﻿CREATE NONCLUSTERED INDEX [IX_xr_email_status]
    ON [dbo].[xr_email]([status] ASC, [emailType] ASC, [summarised] ASC)
    INCLUDE([id], [sendAfter], [surveyId], [channelId], [sendFinish]) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];

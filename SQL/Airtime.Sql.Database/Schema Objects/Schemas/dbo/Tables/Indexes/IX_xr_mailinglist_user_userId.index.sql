﻿CREATE NONCLUSTERED INDEX [IX_xr_mailinglist_user_userId]
    ON [dbo].[xr_mailinglist_user]([userId] ASC)
    INCLUDE([id], [mailinglistId], [dateCreated], [dateModified], [dateLastMailed]) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];


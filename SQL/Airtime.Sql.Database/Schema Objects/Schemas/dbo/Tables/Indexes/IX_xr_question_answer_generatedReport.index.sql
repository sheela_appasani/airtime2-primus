﻿CREATE NONCLUSTERED INDEX [IX_xr_question_answer_generatedReport]
    ON [dbo].[xr_question_answer]([generatedReport] ASC, [questionId] ASC, [id] ASC)
    INCLUDE([surveyRespondentId], [answertype], [dateCreated]) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];


﻿CREATE NONCLUSTERED INDEX [IX_xr_report_summary_agqr]
    ON [dbo].[xr_report_summary]([AnswerType] ASC, [groupId] ASC, [questionId] ASC, [responseId] ASC)
    INCLUDE([Id], [answer], [counter]) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];


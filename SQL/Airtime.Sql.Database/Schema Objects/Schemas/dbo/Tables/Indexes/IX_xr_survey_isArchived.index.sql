﻿CREATE NONCLUSTERED INDEX [IX_xr_survey_isArchived]
    ON [dbo].[xr_survey] ([isDeleted],[surveyType],[isArchived],[startdate],[enddate])
	
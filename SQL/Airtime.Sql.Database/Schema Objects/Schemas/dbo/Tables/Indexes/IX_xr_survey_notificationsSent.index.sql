﻿CREATE NONCLUSTERED INDEX [IX_xr_survey_notificationsSent]
    ON [dbo].[xr_survey] ([channelId],[isDeleted],[notificationsSent],[startdate],[enddate]) INCLUDE ([id])

﻿CREATE NONCLUSTERED INDEX [IX_xr_survey_respondent_completed]
    ON [dbo].[xr_survey_respondent] ([surveyId] asc, [completed] asc, [usedApp] asc, [dateModified] asc)
    INCLUDE ([id], [userId]) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];

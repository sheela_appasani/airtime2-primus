﻿CREATE NONCLUSTERED INDEX [IX_xr_survey_respondent_token]
    ON [dbo].[xr_survey_respondent]([isEnabled] ASC, [token] ASC)
    INCLUDE ([id], [surveyId], [userId]) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];


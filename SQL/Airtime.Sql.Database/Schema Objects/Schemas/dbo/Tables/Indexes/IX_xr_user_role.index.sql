﻿CREATE NONCLUSTERED INDEX [IX_xr_user_role]
    ON [dbo].[xr_user]([isEnabled] ASC, [role] ASC, [optInSurveys] ASC)
    INCLUDE([id], [dateOfBirth], [gender], [personId]) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];

﻿CREATE TABLE [dbo].[xr_app] (
    [id]           INT            IDENTITY (1, 1) NOT NULL,
    [name]         NVARCHAR (255) NOT NULL,
    [code]         VARCHAR (255)  NOT NULL,
    [platform]     VARCHAR (10)   NULL ,
    [appStoreUrl]  VARCHAR (1024) NULL,
	[pushMessage]  NVARCHAR (100) NULL,
    [dateCreated]  DATETIME       NOT NULL,
    [dateModified] DATETIME       NOT NULL, 
    [brandId] INT NULL, 
);


﻿CREATE TABLE [dbo].[xr_app_channel] (
    [id]           INT            IDENTITY (1, 1) NOT NULL,
    [appId]         INT NOT NULL,
    [channelId]         INT  NOT NULL,
    [dateCreated]  DATETIME       NOT NULL DEFAULT GETDATE(),
    [dateModified] DATETIME       NOT NULL DEFAULT GETDATE(), 
    CONSTRAINT [PK_xr_app_channel] PRIMARY KEY ([id]), 
);


GO

CREATE INDEX [IX_xr_app_channel_appId] ON [dbo].[xr_app_channel] ([appId]) INCLUDE ([channelId])

GO

CREATE INDEX [IX_xr_app_channel_channelId] ON [dbo].[xr_app_channel] ([channelId]) INCLUDE ([appId])

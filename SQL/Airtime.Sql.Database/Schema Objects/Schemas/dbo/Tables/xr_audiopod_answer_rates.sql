﻿CREATE TABLE [dbo].[xr_audiopod_answer_rates] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [songId]       INT           NOT NULL,
    [answerId]     INT           NOT NULL,
    [rates]        VARCHAR (MAX) NULL,
    [burn]         VARCHAR (10)  NOT NULL,
    [finalRating]  INT           NULL,
    [dateCreated]  DATETIME      NOT NULL,
    [dateModified] DATETIME      NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);


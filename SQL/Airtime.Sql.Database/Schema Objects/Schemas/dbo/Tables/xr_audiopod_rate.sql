﻿CREATE TABLE [dbo].[xr_audiopod_rate] (
    [Id]          INT IDENTITY (1, 1) NOT NULL,
    [counter]     INT NOT NULL,
    [total]       INT NOT NULL,
    [reportId]    INT NULL,
    [numOfSecond] INT NULL, 
    [dateCreated] DATETIME NOT NULL DEFAULT GETDATE(), 
    [dateModified] DATETIME NOT NULL DEFAULT GETDATE()
);


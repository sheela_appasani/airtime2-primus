﻿CREATE TABLE [dbo].[xr_brand]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(255) NOT NULL, 
    [DateCreated] DATETIME NOT NULL DEFAULT GETDATE(), 
    [DateModified] DATETIME NOT NULL DEFAULT GETDATE()
)

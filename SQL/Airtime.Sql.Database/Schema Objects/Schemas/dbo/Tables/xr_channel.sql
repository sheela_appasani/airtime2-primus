﻿CREATE TABLE [dbo].[xr_channel] (
    [id]                   INT            IDENTITY (1, 1) NOT NULL,
    [name]                 NVARCHAR (255) NOT NULL,
    [title]                NVARCHAR (255) NOT NULL,
    [internalName]         NVARCHAR (255) NOT NULL,
    [companyId]            INT            NOT NULL,
    [guid] UNIQUEIDENTIFIER NOT NULL DEFAULT NEWID(),
    [dateCreated]          DATETIME       NOT NULL,
    [dateModified]         DATETIME       NOT NULL,
    [externalUrl]          VARCHAR (1024) NULL,
    [emailFrom]            NVARCHAR (255) NULL,
    [contestRulesUrl]      VARCHAR (1024) NULL,
    [privacyPolicyUrl]     VARCHAR (1024) NULL,
    [contactDetailsUrl]    VARCHAR (1024) NULL,
    [emailNotificationsTo] NVARCHAR (255) NULL,
	[emailFeedbackTo]      NVARCHAR (255) NULL,
    [languageId]           INT            NULL,
    [useShortRegoForm]     CHAR (1)       NOT NULL,
    [logoUrl]              VARCHAR (1024) NULL,
    [timeZone]             VARCHAR (32)   NOT NULL,
    [appId]                INT            NULL,
    [fastTrackPods]        CHAR (1)       DEFAULT('N') NOT NULL, 
    [screenerId]           INT            NULL,
    [useRegistrationDetailsApp] CHAR NOT NULL DEFAULT ('Y'), 
    [useRegistrationDetailsWeb] CHAR NOT NULL DEFAULT ('Y'),
    [targetMinimumAge]     INT            NOT NULL DEFAULT(0),
    [targetMaximumAge]     INT            NOT NULL DEFAULT(0),
    [targetGender]         CHAR (1)       NULL, 
    [brandId] INT NULL, 
    [enableLandingPage] CHAR NOT NULL DEFAULT 'N' 
);


GO

CREATE UNIQUE INDEX [IX_xr_channel_guid] ON [dbo].[xr_channel] ([guid])

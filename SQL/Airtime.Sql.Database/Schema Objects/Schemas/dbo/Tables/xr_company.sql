﻿CREATE TABLE [dbo].[xr_company] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Company]      NVARCHAR (255) NOT NULL,
    [dateCreated]  DATETIME       NOT NULL,
    [dateModified] DATETIME       NOT NULL
);


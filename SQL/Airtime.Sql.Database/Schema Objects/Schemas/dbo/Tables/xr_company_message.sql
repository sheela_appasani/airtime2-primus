﻿CREATE TABLE [dbo].[xr_company_message] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [companyId]   INT           NOT NULL,
    [MessageCode] VARCHAR (16)  NULL,
    [message]     VARCHAR (MAX) NOT NULL
);


﻿CREATE TABLE [dbo].[xr_complaints] (
    [userId] INT              NOT NULL, 
    [number] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [PK_xr_complaints] PRIMARY KEY ([userId])
);


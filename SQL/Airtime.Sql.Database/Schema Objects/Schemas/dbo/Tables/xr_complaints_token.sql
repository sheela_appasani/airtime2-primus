﻿CREATE TABLE [dbo].[xr_complaints_token] (
    [token] UNIQUEIDENTIFIER NOT NULL,
    [userId] INT NULL, 
    [number] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [PK_xr_complaints_token] PRIMARY KEY ([token])
);


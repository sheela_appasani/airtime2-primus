﻿CREATE TABLE [dbo].[xr_content] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [companyId]    INT            NULL,
    [channelId]    INT            NULL,
    [surveyId]     INT            NULL,
    [contentType]  VARCHAR (50)   NOT NULL,
    [text]         NVARCHAR (MAX) NULL,
    [dateCreated]  DATETIME       NOT NULL,
    [dateModified] DATETIME       NOT NULL,
    [questionId]   INT            NULL
);


﻿CREATE TABLE [dbo].[xr_device] (
    [id]           INT            IDENTITY (1, 1) NOT NULL,
    [personId]     INT            NULL,
    [appId]        INT            NULL,
    [email]        NVARCHAR (255) NULL,
    [deviceToken]  VARCHAR(MAX)      NULL,
    [dateCreated]  DATETIME       NOT NULL,
    [dateModified] DATETIME       NOT NULL
);


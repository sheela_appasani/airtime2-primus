﻿CREATE TABLE [dbo].[xr_email_detail] (
    [Id]                     INT            IDENTITY (1, 1) NOT NULL,
    [ChannelEmailTarget]     VARCHAR (50)   NOT NULL,
    [TargetGenderMale]       CHAR (1)       NULL,
    [TargetGenderFemale]     CHAR (1)       NULL,
    [TargetStartAge]         INT            NULL,
    [TargetEndAge]           INT            NULL,
    [TargetPostcodes]        VARCHAR (2048) NULL,
    [TargetBirthdate]        SMALLDATETIME  NULL,
    [NumberOfWinners]        INT            NULL,
    [RandomWinnersOption]    VARCHAR (50)   NULL,
    [ReferredAFriend]        CHAR (1)       NULL,
    [CompletedStartDate]     DATETIME       NULL,
    [CompletedEndDate]       DATETIME       NULL,
    [DateCreated]            DATETIME       NOT NULL,
    [DateModified]           DATETIME       NOT NULL,
    [TargetBirthdateEnd]     DATETIME       NULL,
    [TargetOptInSurveys]     CHAR (1)       NULL,
    [TargetOptInNewsletters] CHAR (1)       NULL
);


﻿CREATE TABLE [dbo].[xr_email_recipient] (
    [id]                    INT              IDENTITY (1, 1) NOT NULL,
    [emailid]               INT              NULL,
    [address]               NVARCHAR (255)   NULL,
    [subject]               NVARCHAR (255)   NOT NULL,
    [isSent]                TINYINT          NOT NULL,
    [RecipientSource]       VARCHAR (50)     NOT NULL,
    [dateCreated]           DATETIME         NOT NULL,
    [dateModified]          DATETIME         NOT NULL,
    [body]                  NVARCHAR (MAX)   NOT NULL,
    [sender]                VARCHAR (255)    NOT NULL,
    [domain]                VARCHAR (255)    NOT NULL,
    [messageId]             UNIQUEIDENTIFIER NOT NULL,
    [smtpStatus]            INT              NOT NULL,
    [smtpStatusDescription] VARCHAR (2048)   NULL,
    [deliveryAttempts]      INT              NOT NULL,
    [deliveryStatus]        VARCHAR (10)     NOT NULL,
    [deliveryTime]          DATETIME         NULL,
    [smtpfailreason]        VARCHAR (64)     NULL,
    [shardid]               INT              DEFAULT ((1)) NOT NULL,
    [priority]              INT              NOT NULL,
    [name]                  NVARCHAR (255)   NULL,
    [mxipaddress]           VARCHAR (32)     NULL,
    [deliveryipaddress]     VARCHAR (32)     NULL,
    [userId]                INT              NULL,
    [bodyText]              NVARCHAR (MAX)   NULL,
    PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);

GO

CREATE NONCLUSTERED INDEX [IX_xr_email_recipient_emailId] ON [dbo].[xr_email_recipient]([emailid] ASC, [messageId] ASC) INCLUDE([address], [userId], [deliveryStatus]) ON [PRIMARY]

GO

CREATE NONCLUSTERED INDEX [IX_xr_email_recipient_deliveryStatus] ON [dbo].[xr_email_recipient]([deliveryStatus] ASC, [smtpfailreason] ASC) INCLUDE([emailId], [address], [id]) ON [PRIMARY]

GO

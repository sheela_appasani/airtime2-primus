﻿CREATE TABLE [dbo].[xr_field]
(
    CONSTRAINT [PK_xr_field] PRIMARY KEY ([Id]),
	[Id] INT NOT NULL IDENTITY,
    [DateCreated] DATETIME NOT NULL DEFAULT GETDATE(),
    [DateModified] DATETIME NOT NULL DEFAULT GETDATE(), 
    [FieldTypeId] INT NOT NULL, 
	[FieldTypeName] VARCHAR(50) NOT NULL,
    [Name] NVARCHAR(255) NOT NULL, 
    [ScopeId] INT NULL
)

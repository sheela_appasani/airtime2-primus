﻿CREATE TABLE [dbo].[xr_field_type]
(
    CONSTRAINT [PK_xr_field_type] PRIMARY KEY ([Id]), 
	[Id] INT NOT NULL IDENTITY,
    [DateCreated] DATETIME NOT NULL DEFAULT GETDATE(), 
    [DateModified] DATETIME NOT NULL DEFAULT GETDATE(), 
    [DataContext] VARCHAR(20) NOT NULL, 
    [DataScope] VARCHAR(20) NOT NULL, 
    [DataType] VARCHAR(10) NOT NULL,
	[Name] VARCHAR(255) NOT NULL
)
GO

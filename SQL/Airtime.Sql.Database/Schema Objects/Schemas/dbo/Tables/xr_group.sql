﻿CREATE TABLE [dbo].[xr_group] (
    [Id]       INT            IDENTITY (1, 1) NOT NULL,
    [Name]     NVARCHAR (255) NOT NULL,
    [surveyId] INT            NULL,
    [ordering] INT            NULL,
    [Color]    VARCHAR (255)  NULL,
    [Modified] CHAR (1)       NOT NULL,
    [Deleted]  CHAR (1)       NOT NULL,
	CONSTRAINT [PK_xr_group] PRIMARY KEY CLUSTERED ([Id])
);

GO;

CREATE INDEX [IX_xr_group_surveyId] ON [dbo].[xr_group] ([surveyId])

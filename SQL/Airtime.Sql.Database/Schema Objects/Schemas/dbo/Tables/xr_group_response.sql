﻿CREATE TABLE [dbo].[xr_group_response] (
	[Id] INT NOT NULL IDENTITY,
    [groupId]    INT NOT NULL,
    [responseId] INT NOT NULL, 
    [DateCreated] DATETIME NOT NULL DEFAULT GETDATE(), 
    [DateModified] DATETIME NOT NULL DEFAULT GETDATE(), 
    CONSTRAINT [PK_xr_group_response] PRIMARY KEY ([Id])
);

GO;

CREATE INDEX [IX_xr_group_response_groupId] ON [dbo].[xr_group_response] ([groupId], [responseId])

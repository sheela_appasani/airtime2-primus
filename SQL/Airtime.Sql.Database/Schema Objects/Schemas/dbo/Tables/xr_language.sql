﻿CREATE TABLE [dbo].[xr_language] (
    [id]           INT              IDENTITY (1, 1) NOT NULL,
    [languageName] NVARCHAR (50)    NOT NULL,
    [dateCreated]  DATETIME         NOT NULL,
    [dateModified] DATETIME         NOT NULL,
    [code]         UNIQUEIDENTIFIER NOT NULL
);


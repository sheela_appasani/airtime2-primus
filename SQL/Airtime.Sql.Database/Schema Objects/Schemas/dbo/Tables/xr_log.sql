﻿CREATE TABLE [dbo].[xr_log] (
    [id]                 INT            IDENTITY (1, 1) NOT NULL,
    [userId]             INT            NULL,
    [userName]           NVARCHAR (255) NULL,
    [sessionId]          CHAR (24)      NULL,
    [ipAddress]          VARCHAR (64)   NULL,
    [url]                VARCHAR (1024) NULL,
    [className]          VARCHAR (255)  NULL,
    [renderTime]         FLOAT          NULL,
    [dateCreated]        DATETIME       NOT NULL,
    [dateModified]       DATETIME       NOT NULL,
    [flashVersion]       VARCHAR (15)   NULL,
    [httpUserAgent]      VARCHAR (1024) NULL,
    [httpReferer]        VARCHAR (1024) NULL, 
    [hostCode]           VARCHAR(10)    NULL
);


﻿CREATE TABLE [dbo].[xr_mailinglist] (
    [id]              INT            IDENTITY (1, 1) NOT NULL,
    [name]            NVARCHAR (255) NOT NULL,
    [mailinglistType] VARCHAR (50)   NOT NULL,
    [dateCreated]     DATETIME       NOT NULL,
    [dateModified]    DATETIME       NOT NULL
);


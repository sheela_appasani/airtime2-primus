﻿CREATE TABLE [dbo].[xr_mailinglist_user] (
    [id]             INT      IDENTITY (1, 1) NOT NULL,
    [userId]         INT      NOT NULL,
    [mailinglistId]  INT      NULL,
    [dateCreated]    DATETIME NOT NULL,
    [dateModified]   DATETIME NOT NULL,
    [dateLastMailed] DATETIME NULL
);


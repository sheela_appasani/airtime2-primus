﻿CREATE TABLE [dbo].[xr_payment_field] (
    [id]               INT           IDENTITY (1, 1) NOT NULL,
    [surveyId]         INT           NULL,
    [ordering]         INT           NULL,
    [name]             VARCHAR (50)  NOT NULL,
    [description]      VARCHAR (255) NOT NULL,
    [paymentFieldType] VARCHAR (50)  NOT NULL,
    [isRequired]       CHAR (1)      NOT NULL,
    [dateCreated]      DATETIME      NOT NULL,
    [dateModified]     DATETIME      NOT NULL
);


﻿CREATE TABLE [dbo].[xr_person]
(
    [Id] INT IDENTITY(1, 1) NOT NULL PRIMARY KEY, 
    [email] NVARCHAR(255) NULL,
    [password] NVARCHAR (255) NULL,
    [givenName] NVARCHAR(255) NULL, 
    [familyName] NVARCHAR(255) NULL, 
    [daytimePhone] NVARCHAR(50) NULL, 
    [mobilePhone] NVARCHAR(50) NULL, 
    [address] NVARCHAR(100) NULL, 
    [suburb] NVARCHAR(50) NULL, 
    [state] NVARCHAR(50) NULL, 
    [city] NVARCHAR(50) NULL, 
    [postcode] NVARCHAR(10) NULL, 
    [country] NVARCHAR(50) NULL, 
    [dateOfBirth] DATE NULL, 
    [gender] CHAR NULL, 
    [dateCreated] DATETIME NOT NULL, 
    [dateModified] DATETIME NOT NULL
);

GO

﻿CREATE TABLE [dbo].[xr_question_action]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
	[DateCreated] DATETIME NOT NULL DEFAULT GETDATE(),
	[DateModified] DATETIME NOT NULL DEFAULT GETDATE(),
    [QuestionId] INT NOT NULL, 
	[Position] INT NOT NULL,
	[ActionType] VARCHAR(10) NOT NULL,
    [GoToQuestionId] INT NULL, 
    [RedirectUrl] NVARCHAR(1024) NULL
)

GO

CREATE INDEX [IX_xr_question_action_questionId] ON [dbo].[xr_question_action] ([QuestionId])

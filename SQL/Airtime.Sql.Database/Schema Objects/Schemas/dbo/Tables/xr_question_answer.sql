﻿CREATE TABLE [dbo].[xr_question_answer] (
    [id]                 INT            IDENTITY (1, 1) NOT NULL,
    [questionId]         INT            NULL,
    [surveyRespondentId] INT            NULL,
    [answer]             NVARCHAR (MAX) NULL,
    [answertype]         VARCHAR (10)   NULL,
    [generatedReport]    CHAR (1)       NULL,
    [responseId]         INT            NULL,
    [dateCreated]        DATETIME       NOT NULL,
    [dateModified]       DATETIME       NOT NULL,
    [userId]             INT            NULL,
    [isProfileAnswer]    CHAR (1)       NOT NULL DEFAULT ('N')
);


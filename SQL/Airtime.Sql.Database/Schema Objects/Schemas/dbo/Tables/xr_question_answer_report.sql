﻿CREATE TABLE [dbo].[xr_question_answer_report] (
    [answerId] INT NOT NULL,
    [reportId] INT NOT NULL, 
    [dateCreated] DATETIME NOT NULL DEFAULT GETDATE(), 
    [dateModified] DATETIME NOT NULL DEFAULT GETDATE()
);


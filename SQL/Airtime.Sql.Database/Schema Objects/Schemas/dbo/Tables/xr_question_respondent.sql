﻿CREATE TABLE [dbo].[xr_question_respondent] (
    [id]                 INT IDENTITY (1, 1) NOT NULL,
    [questionId]         INT NOT NULL,
    [surveyRespondentId] INT NOT NULL,
    [ordering]           INT NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);


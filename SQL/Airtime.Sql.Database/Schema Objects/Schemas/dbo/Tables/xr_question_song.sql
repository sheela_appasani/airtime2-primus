﻿CREATE TABLE [dbo].[xr_question_song] (
	[id] INT IDENTITY(1,1) NOT NULL,
    [questionId] INT NOT NULL,
    [ordering]   INT NOT NULL,
    [SongId]     INT NOT NULL, 
    [dateCreated] DATETIME NOT NULL DEFAULT GETDATE(), 
    [dateModified] DATETIME NOT NULL DEFAULT GETDATE(), 
    CONSTRAINT [PK_xr_question_song] PRIMARY KEY ([id])
);


﻿CREATE TABLE [dbo].[xr_recruit_answer] (
    [id]                  INT           IDENTITY (1, 1) NOT NULL,
    [datecreated]         DATETIME      NOT NULL,
    [datemodified]        DATETIME      NOT NULL,
    [questionId]          INT           NULL,
    [recruitRespondentId] INT           NULL,
    [answer]              VARCHAR (255) NOT NULL,
    [responseId]          INT           NULL,
    PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);


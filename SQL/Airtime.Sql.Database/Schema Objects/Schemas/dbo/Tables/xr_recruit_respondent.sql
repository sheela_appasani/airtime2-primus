﻿CREATE TABLE [dbo].[xr_recruit_respondent] (
    [id]               INT      NOT NULL,
    [isRecruitSuccess] CHAR (1) NULL,
    [recruitTestId]    INT      NOT NULL,
    [recruitCompanyId] INT      NOT NULL,
    [dateCreated] DATETIME NOT NULL DEFAULT GETDATE(), 
    [dateModified] DATETIME NOT NULL DEFAULT GETDATE(), 
    PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);


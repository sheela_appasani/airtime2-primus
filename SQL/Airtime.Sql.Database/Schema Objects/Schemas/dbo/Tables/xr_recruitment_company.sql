﻿CREATE TABLE [dbo].[xr_recruitment_company] (
	[id] INT IDENTITY (1, 1) NOT NULL,
    [companyId]         INT NOT NULL,
    [recruitmentTestId] INT NOT NULL, 
    [dateCreated] DATETIME NOT NULL DEFAULT GETDATE(), 
    [dateModified] DATETIME NOT NULL DEFAULT GETDATE(), 
    CONSTRAINT [PK_xr_recruitment_company] PRIMARY KEY ([id])
);


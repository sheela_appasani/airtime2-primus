﻿CREATE TABLE [dbo].[xr_referfriend] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [userId]       INT           NOT NULL,
    [surveyId]     INT           NOT NULL,
    [friendEmail]  VARCHAR (255) NOT NULL,
    [dateCreated]  DATETIME      NOT NULL,
    [dateModified] DATETIME      NOT NULL
);


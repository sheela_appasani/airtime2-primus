﻿CREATE TABLE [dbo].[xr_report_channel_cache_stat]
(
	[Id] INT IDENTITY (1, 1) NOT NULL,
    [dateCreated] DATETIME NOT NULL DEFAULT getdate(), 
    [dateModified] DATETIME NOT NULL DEFAULT getdate(),
    [report] NVARCHAR(30) NOT NULL, 
    [reportChannelCacheId] INT NOT NULL, 
    [count] INT NOT NULL, 
    [total] INT NOT NULL, 
    [label] NVARCHAR(255) NOT NULL, 
    CONSTRAINT [PK_xr_report_channel_cache_stat] PRIMARY KEY ([Id]) 
)

GO

CREATE INDEX [IX_xr_report_channel_cache_stat_reportChannelCacheId] ON [dbo].[xr_report_channel_cache_stat] ([reportChannelCacheId])

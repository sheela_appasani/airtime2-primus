﻿CREATE TABLE [dbo].[xr_report_summary] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [AnswerType] VARCHAR (255)  NOT NULL,
    [groupId]    INT            NULL,
    [questionId] INT            NULL,
    [responseId] INT            NULL,
    [answer]     NVARCHAR (255) NULL,
    [counter]    INT            NULL
);


﻿CREATE TABLE [dbo].[xr_response] (
    [Id]                 INT            IDENTITY (1, 1) NOT NULL,
    [Name]               NVARCHAR (255) NOT NULL,
    [Code]               NVARCHAR (255) NOT NULL,
    [questionId]         INT            NULL,
    [BranchQuestionId]   INT            NULL,
    [ordering]           INT            NULL,
    [dateCreated]        DATETIME       NOT NULL,
    [dateModified]       DATETIME       NOT NULL,
    [valueType]          VARCHAR (50)   NOT NULL,
    [isTerminate]        CHAR (1)       NULL,
    [templateResponseId] INT            NULL
);


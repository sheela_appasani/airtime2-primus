﻿CREATE TABLE [dbo].[xr_screener] (
    [Id]                   INT            IDENTITY (1, 1) NOT NULL,
    [IncludeFirstName]     CHAR (1)       NOT NULL,
    [IncludeLastName]      CHAR (1)       NOT NULL,
    [IncludeEmail]         CHAR (1)       NOT NULL,
    [IncludeDateOfBirth]   CHAR (1)       NOT NULL,
    [IncludeGender]        CHAR (1)       NOT NULL,
    [IncludeDaytimePhone]  CHAR (1)       NOT NULL,
    [IncludeMobilePhone]   CHAR (1)       NOT NULL,
    [IncludeStreetAddress] CHAR (1)       NOT NULL, 
    [IncludeSuburb]        CHAR (1)       NOT NULL, 
    [IncludeCity]          CHAR (1)       NOT NULL, 
    [IncludeState]         CHAR (1)       NOT NULL,
    [IncludePostalCode]    CHAR (1)       NOT NULL,
    [IncludeCountry]       CHAR (1)       NOT NULL, 
    [DateCreated]          DATETIME       NOT NULL DEFAULT GETDATE(),
    [DateModified]         DATETIME       NOT NULL DEFAULT GETDATE(), 
    CONSTRAINT [PK_xr_screener] PRIMARY KEY ([Id])
);


GO

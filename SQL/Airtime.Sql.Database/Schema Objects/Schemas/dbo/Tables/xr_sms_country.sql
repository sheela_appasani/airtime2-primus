﻿CREATE TABLE [dbo].[xr_sms_country] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [Country]      VARCHAR (255) NOT NULL,
    [Code]         VARCHAR (50)  NOT NULL,
    [NDD]          VARCHAR (5)   NULL,
    [dateCreated]  DATETIME      NOT NULL,
    [dateModified] DATETIME      NOT NULL
);


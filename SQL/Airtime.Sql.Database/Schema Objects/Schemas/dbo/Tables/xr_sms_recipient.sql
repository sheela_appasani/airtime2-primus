﻿CREATE TABLE [dbo].[xr_sms_recipient] (
    [Id]                 INT            IDENTITY (1, 1) NOT NULL,
    [surveyRespondentId] INT            NOT NULL,
    [phone]              VARCHAR (255)  NOT NULL,
    [isSent]             CHAR (1)       NOT NULL,
    [dateCreated]        DATETIME       NOT NULL,
    [dateModified]       DATETIME       NOT NULL,
    [message]            NVARCHAR (MAX) NOT NULL
);


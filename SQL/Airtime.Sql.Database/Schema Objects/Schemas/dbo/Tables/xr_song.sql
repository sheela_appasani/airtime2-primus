﻿CREATE TABLE [dbo].[xr_song] (
    [id]           INT            IDENTITY (1, 1) NOT NULL,
    [title]        NVARCHAR (255) NOT NULL,
    [artist]       NVARCHAR (255) NOT NULL,
    [year]         INT            NULL,
    [genre]        NVARCHAR (255) NULL,
    [tempo]        NVARCHAR (255) NULL,
    [filename]     NVARCHAR (255) NULL,
    [datecreated]  DATETIME       NOT NULL,
    [datemodified] DATETIME       NOT NULL,
    [second]       INT            NOT NULL,
    [confirmed]    CHAR (1)       NOT NULL,
    [matched]      CHAR (1)       NOT NULL,
    [used]         CHAR (1)       NOT NULL,
    [companyId]    INT            NULL,
    [client]       NVARCHAR (50)  NOT NULL
);


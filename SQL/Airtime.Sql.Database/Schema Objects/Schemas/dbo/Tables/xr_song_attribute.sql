﻿CREATE TABLE [dbo].[xr_song_attribute]
(
    CONSTRAINT [PK_xr_song_attribute] PRIMARY KEY ([Id]), 
	[Id] INT NOT NULL IDENTITY,
    [DateCreated] DATETIME NOT NULL DEFAULT GETDATE(), 
    [DateModified] DATETIME NOT NULL DEFAULT GETDATE(), 
    [SongId] INT NOT NULL, 
    [FieldId] INT NOT NULL, 
    [Value] NVARCHAR(255) NULL 
)

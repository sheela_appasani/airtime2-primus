﻿CREATE TABLE [dbo].[xr_survey_alert] (
    [id]                     INT              IDENTITY (1, 1) NOT NULL,
    [surveyId]               INT              NOT NULL,
    [sent]                   CHAR (1)         NOT NULL,
    [gender]                 CHAR (1)         NULL,
    [lowerAge]               INT              NULL,
    [upperAge]               INT              NULL,
    [sampleSize]             INT              NULL,
    [dateCreated]            DATETIME         NOT NULL,
    [dateModified]           DATETIME         NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);


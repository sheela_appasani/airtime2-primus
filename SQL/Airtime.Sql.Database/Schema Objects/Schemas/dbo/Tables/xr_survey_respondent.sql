﻿CREATE TABLE [dbo].[xr_survey_respondent] (
    [id]                     INT              IDENTITY (1, 1) NOT NULL,
    [surveyId]               INT              NOT NULL,
    [userId]                 INT              NOT NULL,
    [isEnabled]              CHAR (1)         NOT NULL,
    [completed]              CHAR (1)         NOT NULL,
    [registered]             CHAR (1)         NOT NULL,
    [invited]                CHAR (1)         NOT NULL,
    [token]                  UNIQUEIDENTIFIER NOT NULL,
    [currentQuestionId]      INT              NULL,
    [dateCreated]            DATETIME         NOT NULL,
    [dateModified]           DATETIME         NOT NULL,
    [paymentDetailConfirmed] CHAR (1)         NOT NULL,
    [usedApp]                VARCHAR(10)         NULL ,
    PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);


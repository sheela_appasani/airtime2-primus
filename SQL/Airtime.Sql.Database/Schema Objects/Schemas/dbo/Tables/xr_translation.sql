﻿CREATE TABLE [dbo].[xr_translation] (
    [id]           INT            IDENTITY (1, 1) NOT NULL,
    [languageId]   INT            NOT NULL,
    [container]    VARCHAR (255)  NULL,
    [control]      VARCHAR (255)  NOT NULL,
    [value]        NVARCHAR (MAX) NOT NULL,
    [dateCreated]  DATETIME       NOT NULL,
    [dateModified] DATETIME       NOT NULL
);


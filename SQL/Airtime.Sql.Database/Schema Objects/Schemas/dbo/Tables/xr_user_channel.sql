﻿CREATE TABLE [dbo].[xr_user_channel] (
    [id]           INT      IDENTITY (1, 1) NOT NULL,
    [userId]       INT      NOT NULL,
    [channelId]    INT      NOT NULL,
    [dateCreated]  DATETIME NOT NULL,
    [dateModified] DATETIME NOT NULL
);


﻿CREATE TABLE [dbo].[xr_user_permission] (
    [id]           INT          IDENTITY (1, 1) NOT NULL,
    [userId]       INT          NOT NULL,
    [permission]   VARCHAR(255) NOT NULL,
    [dateCreated]  DATETIME     NOT NULL,
    [dateModified] DATETIME     NOT NULL
);


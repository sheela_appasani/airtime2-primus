﻿CREATE TABLE [dbo].[xr_widget] (
    [Id]                   INT            IDENTITY (1, 1) NOT NULL,
    [ChannelId]                 INT NOT NULL,
	[WidgetType]         VARCHAR (20) NOT NULL,
    [Enabled]            CHAR            NOT NULL DEFAULT 'Y',
    [Heading]                NVARCHAR (255) NULL,
    [Content]         NTEXT NULL,
    [DateCreated]          DATETIME       NOT NULL DEFAULT GETDATE(),
    [DateModified]         DATETIME       NOT NULL DEFAULT GETDATE()
);


GO


CREATE INDEX [IX_xr_widget_channelId] ON [dbo].[xr_widget] ([ChannelId])

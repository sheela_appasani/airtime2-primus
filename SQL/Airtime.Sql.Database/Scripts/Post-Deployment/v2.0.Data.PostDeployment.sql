﻿-------------------------------------------------------------------------------
-- IOS APPS
-------------------------------------------------------------------------------

merge into xr_app a
using (values ('iOS', 'Airtime Surveys (iOS)', 'au.net.iapps.xtra', 'http://itunes.apple.com/au/app/airtime-surveys/id418821766')) S(platform, name, code, appStoreUrl) on a.code = S.code
when matched then update set a.platform = S.platform, a.name = S.Name, a.code = S.Code, a.appStoreUrl = S.appStoreUrl
when not matched then insert (platform, name, code, appStoreUrl) values (platform, name, code, appStoreUrl);

merge into xr_app a
using (values ('iOS', 'Rate The Hits (iOS)', 'au.net.iapps.ratethehits', 'http://itunes.apple.com/au/app/rate-the-hits/id450646279')) S(platform, name, code, appStoreUrl) on a.code = S.code
when matched then update set a.platform = S.platform, a.name = S.Name, a.code = S.Code, a.appStoreUrl = S.appStoreUrl
when not matched then insert (platform, name, code, appStoreUrl) values (platform, name, code, appStoreUrl);

merge into xr_app a
using (values ('iOS', 'Music Jury (iOS)', 'au.net.iapps.musicjury', 'http://itunes.apple.com/au/app/music-jury/id497888617')) S(platform, name, code, appStoreUrl) on a.code = S.code
when matched then update set a.platform = S.platform, a.name = S.Name, a.code = S.Code, a.appStoreUrl = S.appStoreUrl
when not matched then insert (platform, name, code, appStoreUrl) values (platform, name, code, appStoreUrl);

merge into xr_app a
using (values ('iOS', 'Music Panel (iOS)', 'au.net.iapps.musicpanel', 'http://itunes.apple.com/au/app/mix-music-panel/id497888235')) S(platform, name, code, appStoreUrl) on a.code = S.code
when matched then update set a.platform = S.platform, a.name = S.Name, a.code = S.Code, a.appStoreUrl = S.appStoreUrl
when not matched then insert (platform, name, code, appStoreUrl) values (platform, name, code, appStoreUrl);

merge into xr_app a
using (values ('iOS', 'Rate ZM Music (iOS)', 'au.com.xtraresearch.ratezmmusic', 'http://itunes.apple.com/nz/app/rate-zm-music/id524550246')) S(platform, name, code, appStoreUrl) on a.code = S.code
when matched then update set a.platform = S.platform, a.name = S.Name, a.code = S.Code, a.appStoreUrl = S.appStoreUrl
when not matched then insert (platform, name, code, appStoreUrl) values (platform, name, code, appStoreUrl);

merge into xr_app a
using (values ('iOS', 'MAX Music Team (iOS)', 'au.com.xtraresearch.max', 'http://itunes.apple.com/au/app/max-music-team/id541776913')) S(platform, name, code, appStoreUrl) on a.code = S.code
when matched then update set a.platform = S.platform, a.name = S.Name, a.code = S.Code, a.appStoreUrl = S.appStoreUrl
when not matched then insert (platform, name, code, appStoreUrl) values (platform, name, code, appStoreUrl);

merge into xr_app a
using (values ('iOS', '[V] Rater (iOS)', 'au.com.xtraresearch.vrater', 'http://itunes.apple.com/au/app/v-rater/id544613343')) S(platform, name, code, appStoreUrl) on a.code = S.code
when matched then update set a.platform = S.platform, a.name = S.Name, a.code = S.Code, a.appStoreUrl = S.appStoreUrl
when not matched then insert (platform, name, code, appStoreUrl) values (platform, name, code, appStoreUrl);

merge into xr_app a
using (values ('iOS', 'Pilkada Jak 101 (iOS)', 'au.com.xtraresearch.jakfm', null)) S(platform, name, code, appStoreUrl) on a.code = S.code
when matched then update set a.platform = S.platform, a.name = S.Name, a.code = S.Code, a.appStoreUrl = S.appStoreUrl
when not matched then insert (platform, name, code, appStoreUrl) values (platform, name, code, appStoreUrl);

merge into xr_app a
using (values ('iOS', 'Today VIP (iOS)', 'au.com.xtraresearch.vip', null)) S(platform, name, code, appStoreUrl) on a.code = S.code
when matched then update set a.platform = S.platform, a.name = S.Name, a.code = S.Code, a.appStoreUrl = S.appStoreUrl
when not matched then insert (platform, name, code, appStoreUrl) values (platform, name, code, appStoreUrl);
go


-------------------------------------------------------------------------------
-- ANDROID APPS
-------------------------------------------------------------------------------

merge into xr_app a
using (values ('Android', 'Airtime Surveys (Android)', 'au.com.xtraresearch.android.airtime', 'https://play.google.com/store/apps/details?id=au.com.xtraresearch.generic')) S(platform, name, code, appStoreUrl) on a.code = S.code
when matched then update set a.platform = S.platform, a.name = S.Name, a.code = S.Code, a.appStoreUrl = S.appStoreUrl
when not matched then insert (platform, name, code, appStoreUrl) values (platform, name, code, appStoreUrl);

merge into xr_app a
using (values ('Android', 'Music Jury (Android)', 'au.com.xtraresearch.android.musicjury', 'https://play.google.com/store/apps/details?id=au.com.xtraresearch.musicjury')) S(platform, name, code, appStoreUrl) on a.code = S.code
when matched then update set a.platform = S.platform, a.name = S.Name, a.code = S.Code, a.appStoreUrl = S.appStoreUrl
when not matched then insert (platform, name, code, appStoreUrl) values (platform, name, code, appStoreUrl);

merge into xr_app a
using (values ('Android', 'Music Panel (Android)', 'au.com.xtraresearch.android.musicpanel', 'https://play.google.com/store/apps/details?id=au.com.xtraresearch.musicpanel')) S(platform, name, code, appStoreUrl) on a.code = S.code
when matched then update set a.platform = S.platform, a.name = S.Name, a.code = S.Code, a.appStoreUrl = S.appStoreUrl
when not matched then insert (platform, name, code, appStoreUrl) values (platform, name, code, appStoreUrl);

merge into xr_app a
using (values ('Android', 'Today VIP (Android)', 'au.com.xtraresearch.android.todayvip', null)) S(platform, name, code, appStoreUrl) on a.code = S.code
when matched then update set a.platform = S.platform, a.name = S.Name, a.code = S.Code, a.appStoreUrl = S.appStoreUrl
when not matched then insert (platform, name, code, appStoreUrl) values (platform, name, code, appStoreUrl);

--merge into xr_app a
--using (values ('Android', 'Rate ZM Music (Android)', 'au.com.xtraresearch.android.ratezmmusic', null)) S(platform, name, code, appStoreUrl) on a.code = S.code
--when matched then update set a.platform = S.platform, a.name = S.Name, a.code = S.Code, a.appStoreUrl = S.appStoreUrl
--when not matched then insert (platform, name, code, appStoreUrl) values (platform, name, code, appStoreUrl);

--merge into xr_app a
--using (values ('Android', 'MAX Music Team (Android)', 'au.com.xtraresearch.android.max', null)) S(platform, name, code, appStoreUrl) on a.code = S.code
--when matched then update set a.platform = S.platform, a.name = S.Name, a.code = S.Code, a.appStoreUrl = S.appStoreUrl
--when not matched then insert (platform, name, code, appStoreUrl) values (platform, name, code, appStoreUrl);

--merge into xr_app a
--using (values ('Android', '[V] Rater (Android)', 'au.com.xtraresearch.android.vrater', null)) S(platform, name, code, appStoreUrl) on a.code = S.code
--when matched then update set a.platform = S.platform, a.name = S.Name, a.code = S.Code, a.appStoreUrl = S.appStoreUrl
--when not matched then insert (platform, name, code, appStoreUrl) values (platform, name, code, appStoreUrl);

merge into xr_app a
using (values ('Android', 'Pilkada Jak 101 (Android)', 'au.com.xtraresearch.android.pilkada', null)) S(platform, name, code, appStoreUrl) on a.code = S.code
when matched then update set a.platform = S.platform, a.name = S.Name, a.code = S.Code, a.appStoreUrl = S.appStoreUrl
when not matched then insert (platform, name, code, appStoreUrl) values (platform, name, code, appStoreUrl);

merge into xr_app a
using (values ('Android', 'Gen FM Musik Terkini (Android)', 'au.com.xtraresearch.android.genfmmusikterkini', null)) S(platform, name, code, appStoreUrl) on a.code = S.code
when matched then update set a.platform = S.platform, a.name = S.Name, a.code = S.Code, a.appStoreUrl = S.appStoreUrl
when not matched then insert (platform, name, code, appStoreUrl) values (platform, name, code, appStoreUrl);
go


-------------------------------------------------------------------------------
-- BRANDS
-------------------------------------------------------------------------------

if not exists (select top 1 1 from xr_brand where name = 'Airtime Surveys') insert into xr_brand (name) values ('Airtime Surveys')
if not exists (select top 1 1 from xr_brand where name = 'Rate The Hits') insert into xr_brand (name) values ('Rate The Hits')
if not exists (select top 1 1 from xr_brand where name = 'Music Jury') insert into xr_brand (name) values ('Music Jury')
if not exists (select top 1 1 from xr_brand where name = 'Music Panel') insert into xr_brand (name) values ('Music Panel')
if not exists (select top 1 1 from xr_brand where name = 'Rate ZM Music') insert into xr_brand (name) values ('Rate ZM Music')
if not exists (select top 1 1 from xr_brand where name = 'MAX Music Team') insert into xr_brand (name) values ('MAX Music Team')
if not exists (select top 1 1 from xr_brand where name = '[V] Rater') insert into xr_brand (name) values ('[V] Rater')
if not exists (select top 1 1 from xr_brand where name = 'Pilkada') insert into xr_brand (name) values ('Pilkada')
if not exists (select top 1 1 from xr_brand where name = 'Gen FM Musik Terkini') insert into xr_brand (name) values ('Gen FM Musik Terkini')
if not exists (select top 1 1 from xr_brand where name = 'Today VIP') insert into xr_brand (name) values ('Today VIP')
go

update xr_app
set brandId = b.id
from xr_app a
inner join xr_brand b on b.name = left(a.name, len(b.name))
where a.brandId is null and b.id is not null
go


-------------------------------------------------------------------------------

if not exists (select top 1 1 from xr_field_type where name = 'SongFieldType') insert into xr_field_type (DataContext, DataScope, DataType, Name) values ('Song', 'Company', 'String', 'SongFieldType')
go


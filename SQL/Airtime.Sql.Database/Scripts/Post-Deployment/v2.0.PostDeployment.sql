﻿-- =============================================
-- Script Template
-- =============================================

if exists (select top 1 1 from xr_question where questionType = 'TEXT' and questionCategory in ('Normal_RegistrationIntroduction', 'Normal_RegistrationCompleted', 'Normal_SurveyIntroduction', 'Normal_SurveyCompleted')) begin
	update xr_question 
	set questionType = 'BREAK' 
	where 
		questionCategory in ('Normal_RegistrationIntroduction', 'Normal_RegistrationCompleted', 'Normal_SurveyIntroduction', 'Normal_SurveyCompleted')
		and questionType = 'TEXT'
end
go

while exists (select top 1 1 from xr_audiopod_answer_rates where finalRating is null and rates is not null and rates <> '') begin
	update xr_audiopod_answer_rates set finalRating =
		CASE when CHARINDEX(',', REVERSE(rates)) = 0 then CAST(rates as int)
		else CAST(RIGHT(rates, CHARINDEX(',', REVERSE(rates)) - 1) as int) end
	from xr_audiopod_answer_rates
	where id in (
		select top 1000000 id from xr_audiopod_answer_rates where finalRating is null and rates is not null and rates <> ''
	)
end
go

if not exists (select top 1 1 from xr_user where role = 'Developer') begin
	update xr_user set role = 'Developer' where username in ('root', 'lethek')
end
go

if not exists (select top 1 1 from xr_survey where isPopulated = 'Y') begin
	update xr_survey set isPopulated = 'Y'
end
go

--Originally xr_email.id == xr_email_detail.id but they're now separate and the foreign key is referenced in xr_email.criteriaId; the SQL below is to help migrate to this new scheme
if not exists (select top 1 1 from xr_email where criteriaId is not null) begin
	if exists (select top 1 1 from xr_email_detail where id not in (select ISNULL(criteriaId, 0) from xr_email)) begin
		update xr_email
		set criteriaId = ed.id
		from xr_email e
		inner join xr_email_detail ed on e.id = ed.Id
		where
			e.criteriaId is null and ed.id not in (select ISNULL(criteriaId, 0) from xr_email)
	end
end
go

if not exists (select top 1 1 from xr_channel where internalName <> '') begin
	update xr_channel set internalName = name where internalName = ''
end
go

if exists (select top 1 1 from xr_content where contentType = 'Email_SurveyCompleted') begin
	update xr_content set contentType = 'Email_SurveyCompletedThanks' where contentType = 'Email_SurveyCompleted'
	update xr_content set contentType = 'Email_SurveyCompletedThanksSubject' where contentType = 'Email_SurveyCompletedSubject'
end
go

if not exists (select top 1 1 from xr_question_answer where isProfileAnswer = 'Y') begin
	update xr_question_answer
	set isProfileAnswer = 'Y'
	from xr_question_answer qa
	inner join xr_question q on q.id = qa.questionId
	where q.channelId > 0 and q.surveyId > 0 and qa.isProfileAnswer <> 'Y'
end
go

if exists (select top 1 1 from xr_user_permission where permission = 'ViewReports') begin
	insert into xr_user_permission (userId, permission) (select userId, 'ViewSummaryReports' from xr_user_permission where permission = 'ViewReports')
	insert into xr_user_permission (userId, permission) (select userId, 'ViewMusicReports' from xr_user_permission where permission = 'ViewReports')
	insert into xr_user_permission (userId, permission) (select userId, 'ViewPerceptualReports' from xr_user_permission where permission = 'ViewReports')
	delete from xr_user_permission where permission = 'ViewReports'
end
go

if exists (select top 1 1 from xr_user where membershipStatus is null) begin
	declare @now datetime = GETDATE()
	update xr_user set membershipStatus = 'Active' where membershipStatus is null and isEnabled = 'Y'
	update xr_user set membershipStatus = 'Wiped' where membershipStatus is null and isEnabled = 'N' and username = 'purged_user'
	update xr_user set membershipStatus = 'Expired' where membershipStatus is null and (activeTo is not null and activeTo < @now)
	update xr_user set membershipStatus = 'Unconfirmed' where membershipStatus is null and userStatus = 'Normal' and ABS(DATEDIFF(second, dateCreated, dateModified)) < 600
	update xr_user set membershipStatus = 'Suspended' where membershipStatus is null and userStatus = 'Rejected'
	update xr_user set membershipStatus = 'Suspended' where membershipStatus is null and isEnabled = 'N'
end
go

if not exists (select top 1 1 from xr_person) begin
	update xr_user set externalToken = null where externaltoken is not null and username not like 'ExtUserX%'

	insert into xr_person (email, password, givenName, familyName, daytimePhone, mobilePhone, address, suburb, state, city, postcode, country, dateOfBirth, gender)
		select
			x.email, x.password, x.givenName, x.familyName, x.daytimePhone, x.mobilePhone, x.address, x.suburb, x.state, x.city, x.postcode, x.country, x.dateOfBirth, x.gender
		from (
			select
				ROW_NUMBER() over (partition by u.email order by
					u.isEnabled desc,
					case
						when u.role = 'Developer' then 0
						when u.role = 'Administrator' then 1
						when u.role = 'Client_Turnkey' then 2
						when u.role = 'Client_Admin' then 3
						when u.role = 'Client_Manager' then 4
						when u.role = 'Client_Marketing' then 5
						when u.role = 'Client_Reports' then 6
						when u.role = 'Client' then 7
						when u.role = 'Moderator' then 8
						when u.role = 'Recruiter' then 9
						when u.role = 'Respondent' then 10
					end asc,
					u.dateModified desc,
					u.id asc
				) as RowNumber,
				u.id as userId,
				u.email,
				u.password,
				case when u.firstName = '' then null else u.firstName end as givenName,
				case when u.lastName = '' then null else u.lastName end  as familyName,
				case when u.daytimePhone = '' then null else u.daytimePhone end as daytimePhone,
				case when u.mobilePhone = '' then null else u.mobilePhone end as mobilePhone, 
				case when u.address = '' then null else u.address end as address,
				case when u.suburb = '' then null else u.suburb end as suburb,
				case when u.state = '' then null else u.state end as state,
				case when u.city = '' then null else u.city end as city,
				case when u.postcode = '' then null else u.postcode end as postcode,
				case when u.country = '' then null else u.country end as country,
				cast(u.dateOfBirth as date) as dateOfBirth,
				u.gender
			from xr_user u
			where 
				u.username <> 'purged_user'
				and u.userStatus = 'Normal'
				and u.externalToken is null
				and u.email is not null
				and u.personId is null
		) x
		where x.RowNumber = 1
		order by x.userId

	update xr_user
	set personId = p.id
	from xr_user u
	inner join xr_person p on p.email = u.email
	where u.email is not null and u.personId is null

	update xr_device
	set personId = p.id
	from xr_device d
	inner join xr_person p on p.email = d.email
	where d.email is not null and d.personId is null

	delete from xr_device where personId is null
end
go


if not exists (select top 1 1 from xr_app_channel) begin
	insert into xr_app_channel (appId, channelId) select appId, id as channelId from xr_channel where appId > 0 order by appId, id
end
go



while exists (select top 1 1 from xr_survey_respondent where usedApp in ('Y', 'N')) begin
	update xr_survey_respondent set usedApp = 'iOS' where id in (select top 500000 id from xr_survey_respondent where usedApp = 'Y')
	update xr_survey_respondent set usedApp = null where id in (select top 500000 id from xr_survey_respondent where usedApp = 'N')
end
go

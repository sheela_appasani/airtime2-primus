﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

using Airtime.Data.Client;
using Airtime.Data.Media;
using Airtime.Data.Account;
using Airtime.Data.Messaging;
using Airtime.Data.Surveying;
using Airtime.Data.Surveying.Questions;
using Airtime.Models.Account;
using Airtime.Services.Security;

using Airtime.Web.Controllers.Areas.Admin;

using NUnit.Framework;

using Membership = Airtime.Data.Account.Membership;


namespace Airtime.Tests.Controllers
{

	[TestFixture]
	public class HomeControllerTest
	{

		[Test]
		public void Index()
		{
			// Arrange
			HomeController controller = new HomeController(formsService: new MockFormsAuthenticationService(), securityService: new MockSecurityService());

			// Act
			ViewResult result = controller.Index() as ViewResult;
		}


		[Test]
		public void LogOff_LogsOutAndRedirects()
		{
			// Arrange
			var controller = GetHomeController();

			// Act
			var result = controller.LogOff();

			// Assert
			Assert.That(result, Is.InstanceOf(typeof(RedirectToRouteResult)));
			RedirectToRouteResult redirectResult = (RedirectToRouteResult)result;
			Assert.That(redirectResult.RouteValues["controller"], Is.EqualTo("Home"));
			Assert.That(redirectResult.RouteValues["action"], Is.EqualTo("Index"));
			// TODO: Mocking Framework
			Assert.IsTrue(((MockFormsAuthenticationService)controller.FormsService).SignOut_WasCalled);
		}


		[Test]
		public void LogOn_Get_ReturnsView()
		{
			// Arrange
			var controller = GetHomeController();

			// Act
			var result = controller.LogOn();

			// Assert
			Assert.That(result, Is.InstanceOf(typeof(ViewResult)));
		}


		[Test]
		public void LogOn_Post_ReturnsRedirectOnSuccess_WithoutReturnUrl()
		{
			// Arrange
			var controller = GetHomeController();
			var model = new LogOnModel() {
				Username = "someUser",
				Password = "goodPassword",
				RememberMe = false
			};

			// Act
			ActionResult result = controller.LogOn(model, null);

			// Assert
			Assert.That(result, Is.InstanceOf(typeof(RedirectToRouteResult)));
			RedirectToRouteResult redirectResult = (RedirectToRouteResult)result;
			Assert.That(redirectResult.RouteValues["controller"], Is.EqualTo("Home"));
			Assert.That(redirectResult.RouteValues["action"], Is.EqualTo("Index"));
			// TODO: Mocking Framework
			Assert.IsTrue(((MockFormsAuthenticationService)controller.FormsService).SignIn_WasCalled);
		}


		[Test]
		public void LogOn_Post_ReturnsRedirectOnSuccess_WithLocalReturnUrl()
		{
			// Arrange
			var controller = GetHomeController();
			var model = new LogOnModel() {
				Username = "someUser",
				Password = "goodPassword",
				RememberMe = false
			};

			// Act
			ActionResult result = controller.LogOn(model, "/someUrl");

			// Assert
			Assert.That(result, Is.InstanceOf(typeof(RedirectResult)));
			RedirectResult redirectResult = (RedirectResult)result;
			Assert.That(redirectResult.Url, Is.EqualTo("/someUrl"));
			// TODO: Mocking Framework
			Assert.IsTrue(((MockFormsAuthenticationService)controller.FormsService).SignIn_WasCalled);
		}


		[Test]
		public void LogOn_Post_ReturnsRedirectToHomeOnSuccess_WithExternalReturnUrl()
		{
			// Arrange
			var controller = GetHomeController();
			var model = new LogOnModel() {
				Username = "someUser",
				Password = "goodPassword",
				RememberMe = false
			};

			// Act
			ActionResult result = controller.LogOn(model, "http://malicious.example.net");

			// Assert
			Assert.That(result, Is.InstanceOf(typeof(RedirectToRouteResult)));
			RedirectToRouteResult redirectResult = (RedirectToRouteResult)result;
			Assert.That(redirectResult.RouteValues["controller"], Is.EqualTo("Home"));
			Assert.That(redirectResult.RouteValues["action"], Is.EqualTo("Index"));
			// TODO: Mocking Framework
			Assert.IsTrue(((MockFormsAuthenticationService)controller.FormsService).SignIn_WasCalled);
		}


		[Test]
		public void LogOn_Post_ReturnsViewIfModelStateIsInvalid()
		{
			// Arrange
			var controller = GetHomeController();
			var model = new LogOnModel() {
				Username = "someUser",
				Password = "goodPassword",
				RememberMe = false
			};
			controller.ModelState.AddModelError("", "Dummy error message.");

			// Act
			ActionResult result = controller.LogOn(model, null);

			// Assert
			Assert.That(result, Is.InstanceOf(typeof(ViewResult)));
			ViewResult viewResult = (ViewResult)result;
			Assert.That(viewResult.ViewData.Model, Is.EqualTo(model));
		}


		[Test]
		public void LogOn_Post_ReturnsViewIfValidateUserFails()
		{
			// Arrange
			var controller = GetHomeController();
			var model = new LogOnModel() {
				Username = "someUser",
				Password = "badPassword",
				RememberMe = false
			};

			// Act
			ActionResult result = controller.LogOn(model, null);

			// Assert
			Assert.That(result, Is.InstanceOf(typeof(ViewResult)));
			ViewResult viewResult = (ViewResult)result;
			Assert.That(viewResult.ViewData.Model, Is.EqualTo(model));
			Assert.That(controller.ModelState[""].Errors[0].ErrorMessage, Is.EqualTo("The Membership name or password provided is incorrect."));
		}


		private static HomeController GetHomeController()
		{
			var requestContext = new RequestContext(new MockHttpContext(), new RouteData());
			var controller = new HomeController(formsService: new MockFormsAuthenticationService(), securityService: new MockSecurityService()) {
				Url = new UrlHelper(requestContext),
			};
			controller.ControllerContext = new ControllerContext() {
				Controller = controller,
				RequestContext = requestContext
			};
			return controller;
		}


		private class MockFormsAuthenticationService : IFormsAuthenticationService
		{
			public bool SignIn_WasCalled;
			public bool SignOut_WasCalled;

			public string GenerateEncryptedTicket(Membership membership, TimeSpan validityPeriod)
			{
				return String.Empty;
			}


			public string GenerateEncryptedTicket(Person person, TimeSpan validityPeriod)
			{
				return String.Empty;
			}


			public string GenerateEncryptedTicket(Person person, Membership membership, TimeSpan validityPeriod)
			{
				return String.Empty;
			}


			public void SignInByEmail(string username, bool createPersistentCookie)
			{
			}


			public void SignIn(string username, bool createPersistentCookie)
			{
				// verify that the arguments are what we expected
				Assert.That(username, Is.EqualTo("someUser"));
				Assert.That(createPersistentCookie, Is.False);

				SignIn_WasCalled = true;
			}

			public void SignIn(Guid guid, bool createPersistentCookie)
			{
				Assert.That(guid, Is.Not.EqualTo(Guid.Empty));
				Assert.That(createPersistentCookie, Is.False);

				SignIn_WasCalled = true;
			}

			public void SignOut()
			{
				SignOut_WasCalled = true;
			}
		}


		private class MockSecurityService : ISecurityService
		{
			public int MinPasswordLength
			{
				get { return 10; }
			}

			public Person CurrentPerson
			{
				get { return new Person(); }
			}

			public Membership CurrentMembership
			{
				get { return new Membership(); }
			}

			public IList<Channel> AllowedChannels
			{
				get { return new List<Channel>(); }
			}

			public IList<Channel> MembershipChannels { get; private set; }


			public IQueryable<Channel> QueryChannelsWithRole(params MembershipRoleEnum[] roles)
			{
				throw new NotImplementedException();
			}


			public bool ValidatePermissions(params PermissionEnum[] permissions)
			{
				return true;
			}


			public bool ValidateAnyPermissions(params PermissionEnum[] permissions)
			{
				return true;
			}


			public bool ValidateChannelPermissions(int? channelId, params PermissionEnum[] permissions)
			{
				throw new NotImplementedException();
			}


			public bool ValidateChannelPermissions(Channel channel, params PermissionEnum[] permissions)
			{
				throw new NotImplementedException();
			}


			public bool ValidateChannel(int? channelId)
			{
				return true;
			}

			public bool ValidateChannel(Channel channel)
			{
				return true;
			}


			public bool ValidateChannelRole(int? channelId, params MembershipRoleEnum[] roles)
			{
				throw new NotImplementedException();
			}


			public bool ValidateChannelRole(Channel channel, params MembershipRoleEnum[] roles)
			{
				throw new NotImplementedException();
			}


			public bool ValidateOwnership(Channel channel, Membership membership)
			{
				return true;
			}

			public bool ValidateOwnership(Channel channel, Song song)
			{
				return true;
			}

			public bool ValidateOwnership(Channel channel, Survey survey)
			{
				return true;
			}

			public bool ValidateOwnership(Channel channel, Survey survey, QuestionBase question)
			{
				return true;
			}

			public bool ValidateOwnership(Channel channel, EmailBlast blast)
			{
				return true;
			}

			public bool ValidateOwnership(Channel channel, Survey survey, EmailBlast blast)
			{
				return true;
			}

			public bool AuthenticatePerson(string email, string password)
			{
				return true;
			}

			public bool AuthenticateMembership(Guid guid)
			{
				return guid != Guid.Empty;
			}


			public bool AuthenticateMembershipByEmail(string email, string password)
			{
				throw new NotImplementedException();
			}


			public bool AuthenticateMembership(string userName, string password)
			{
				return (userName == "someUser" && password == "goodPassword");
			}


			public bool ValidateMembershipByEmail(string email, string password)
			{
				return true;
			}


			public MembershipCreateStatus CreateUser(Membership newMembership)
			{
				if (newMembership.Username == "duplicateUser") {
					return MembershipCreateStatus.DuplicateUserName;
				}

				// verify that values are what we expected
				Assert.AreEqual("goodPassword", newMembership.Password);
				Assert.AreEqual("goodEmail", newMembership.Email);

				return MembershipCreateStatus.Success;
			}

			public bool ChangePassword(string userName, string oldPassword, string newPassword)
			{
				return (userName == "someUser" && oldPassword == "goodOldPassword" && newPassword == "goodNewPassword");
			}
		}


		private class MockHttpContext : HttpContextBase
		{
			private readonly IPrincipal _user = new GenericPrincipal(new GenericIdentity("someUser"), null /* roles */);
			private readonly HttpRequestBase _request = new MockHttpRequest();

			public override IPrincipal User
			{
				get
				{
					return _user;
				}
				set
				{
					base.User = value;
				}
			}

			public override HttpRequestBase Request
			{
				get
				{
					return _request;
				}
			}
		}
		

		private class MockHttpRequest : HttpRequestBase
		{
			private readonly Uri _url = new Uri("http://mysite.example.com/");

			public override Uri Url
			{
				get
				{
					return _url;
				}
			}
		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Airtime.Data.Globalization;
using Airtime.Services.Core.Globalization;
using NUnit.Framework;

namespace Airtime.Tests
{
	[TestFixture]
	class GlobalisationDayMonthTest
	{
		[Test]
		public void DumpCultures()
		{
			List<string> list = new List<string>();
			foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.AllCultures))
			{
				string specName = "(none)";
				try { 
					specName = CultureInfo.CreateSpecificCulture(ci.Name).Name; 
				} catch { }
				list.Add(String.Format("{0,-12}{1,-12}{2,-20},[{3}]", ci.Name, specName, ci.EnglishName, ci.DateTimeFormat.LongDatePattern));
			}

			list.Sort();  // sort by name

			Console.WriteLine("CULTURE   SPEC.CULTURE  ENGLISH NAME");
			Console.WriteLine("--------------------------------------------------------------");
			foreach (string str in list)
			Console.WriteLine(str);
		}

		[Test]
		public void DumpCulturesWithLongDateFormatAndHack()
		{
			foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.AllCultures))
			{
				try {
					Console.WriteLine("{0}", CultureInfo.CreateSpecificCulture(ci.Name).Name);
					var pattern = ci.DateTimeFormat.LongDatePattern;
					Console.WriteLine("{0}", pattern);
					Console.WriteLine("{0}", GlobalizationUtilsDemonstrateComplexity.RemoveDayNameAndYear(pattern));
				} catch { }
			}
		}

		public class GlobalizationUtilsDemonstrateComplexity
		{
			/// <summary>
			/// Remove day of week name, year and extra chars before or after the core month and day fields.
			/// </summary>
			public static string RemoveDayNameAndYear(string datePattern)
			{
				// longer patterns must be first.
				const string fieldPatternDelimter = "[ .,]*";
				var removeFields = new[] { "yyyyy", "dddd", "yyy", "ddd", "yy", "y" };
				return removeFields.Aggregate(datePattern,
					(current, field)
						=> Regex.Replace(current, fieldPatternDelimter + field + fieldPatternDelimter, ""));
			}
		}
	}
}

﻿using System;
using Airtime.Core;
using Airtime.Web.Controllers.Areas.Admin;
using NUnit.Framework;

namespace Airtime.Tests
{
	[TestFixture]
	class TestExtendDateTime
	{
		// ReSharper disable InconsistentNaming
		[Test]
		public void UnixTicks_of_Epoch_is_Zero()
		{
			var testDate = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
			var dateString = testDate.UnixTicks();
			Console.WriteLine(" trace [" + dateString + "]");
			Assert.That(dateString, Is.EqualTo("0"));
		}

		[Test]
		public void UnixTicks_of_Test1_is_Correct_Arbitrary_Test()
		{
			var testDate = new DateTime(1991, 11, 12, 3, 4, 3, 56, DateTimeKind.Utc);
			var dateString = testDate.UnixTicks();
			Console.WriteLine(" trace [" + dateString + "]");
			Assert.That(dateString, Is.EqualTo("689915043056"));
		}
		// ReSharper restore InconsistentNaming
	}
}
